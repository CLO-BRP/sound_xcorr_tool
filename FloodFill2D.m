function pixels = FloodFill2D(S, ix, iy, thresh, pixels)
% Recursive 2D flood-fill algorithm
%
% Inputs:
% S = S(ix,iy) = 2D surface
% (ix,iy) = node indices
% pixels = list of pixels in the current stack (initially set as pixels = [])
% thresh = threshold for detection
% Outputs:
% pixels = list of pixels on the same stack
%
% E-M Nosal 08/05/08, Honolulu
[nx,ny] = size(S);
if S(ix,iy) >= thresh
    pixels = [ pixels; [ix iy] ] ;
    % if the neighbor point isn't in pixels, and it isn't an edge-point, do flood-fill on it
    for ix2 = max(1,ix-1):min(nx,ix+1)
        for iy2 = max(1,iy-1):min(ny,iy+1)
            if sum([ix iy] == [ix2 iy2]) < 2 & isempty(intersect(find(pixels(:,1)==ix2),find(pixels(:,2)==iy2)))
                pixels = FloodFill2D(S, ix2, iy2, thresh, pixels);
            end
        end
    end
end