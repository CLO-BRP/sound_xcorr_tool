function sound_xcorr_tool_SCK(mode, varargin)

%%
%% 'sound_xcorr_tool'
%%
%% An interactive GUI interface to set up various correlation
%% processes on sound clips
%%
%% Kathryn A. Cortopassi
%% August 2004
%%


gui_interface_fig_tag = ['gui_interface_fig_for_', mfilename];


if nargin == 0
    mode = 'create_ui';
end


switch (mode)
    
    case ('create_ui')
        create_xcorr_gui_interface_figure(gui_interface_fig_tag);
        return;
        
    case('key_press_intercept')
        key_press_intercept(gui_interface_fig_tag);
        return;
        
    case('cb_sound_list_popup')
        cb_sound_list_popup(gui_interface_fig_tag);
        return;
        
        % Modified SCK
    case ('cb_sound_list_add_log')
        cb_sound_list_add_log(gui_interface_fig_tag);
        return;
        
    case ('help_callback')
        help_callback(gui_interface_fig_tag);
        return;
        
    case ('cb_sound_list_add')
        cb_sound_list_add(gui_interface_fig_tag);
        return;
        
    case ('cb_sound_list_delete')
        cb_sound_list_delete(gui_interface_fig_tag);
        return;
        
    case ('cb_sound_list_moveup')
        cb_sound_list_moveup(gui_interface_fig_tag);
        return;
        
    case ('cb_sound_list_movedown')
        cb_sound_list_movedown(gui_interface_fig_tag);
        return;
        
    case ('cb_sound_list_order')
        cb_sound_list_order(gui_interface_fig_tag);
        return;
        
    case ('cb_sound_list_commit')
        cb_sound_list_commit(gui_interface_fig_tag);
        return;
        
    case ('cb_sndspc_view_xaxis')
        cb_sndspc_view_xaxis(gui_interface_fig_tag);
        return;
        
    case ('cb_sndspc_view_fullxaxis')
        cb_sndspc_view_fullxaxis(gui_interface_fig_tag);
        return;
        
    case ('cb_sndspc_view_yaxis')
        cb_sndspc_view_yaxis(gui_interface_fig_tag);
        return;
        
    case ('cb_sndspc_view_fullyaxis')
        cb_sndspc_view_fullyaxis(gui_interface_fig_tag);
        return;
        
    case ('cb_sndspc_view_colormap')
        cb_sndspc_view_colormap(gui_interface_fig_tag);
        return;
        
    case ('cb_sndspc_view_colormap_invert')
        cb_sndspc_view_colormap_invert(gui_interface_fig_tag);
        return;
        
    case ('cb_sndspc_view_gain_range')
        cb_sndspc_view_gain_range(gui_interface_fig_tag);
        return;
        
    case ('cb_sndspc_view_peak_range')
        cb_sndspc_view_peak_range(gui_interface_fig_tag);
        return;
        
    case ('cb_sndspc_view_gain_range_display')
        cb_sndspc_view_gain_range_display(gui_interface_fig_tag);
        return;
        
    case ('cb_sndspc_view_peak_range_display')
        cb_sndspc_view_peak_range_display(gui_interface_fig_tag);
        return;
        
    case ('cb_sndspc_view_full_range_display')
        cb_sndspc_view_full_range_display(gui_interface_fig_tag);
        return;
        
    case ('cb_sndspc_view_fullrange_checkbox')
        cb_sndspc_view_fullrange_checkbox(gui_interface_fig_tag);
        return;
        
    case ('cb_sndspc_view_enlarge')
        cb_sndspc_view_enlarge(gui_interface_fig_tag);
        return;
        
        %   case ('cb_sndspc_view_reduce')
        %     cb_sndspc_view_reduce(gui_interface_fig_tag);
        %     return;
        
    case ('cb_sound_list_play')
        cb_sound_list_play(gui_interface_fig_tag);
        return;
        
    case ('cb_output_file_select')
        cb_output_file_select(gui_interface_fig_tag);
        return;
        
    case ('cb_export_corr_data_file')
        cb_export_corr_data_file(gui_interface_fig_tag);
        return;
        
    case ('cb_param_fft_len_edit')
        cb_param_fft_len_edit(gui_interface_fig_tag);
        return;
        
    case ('cb_param_data_len_edit')
        cb_param_data_len_edit(gui_interface_fig_tag);
        return;
        
    case ('cb_param_taper_fun_popup')
        cb_param_taper_fun_popup(gui_interface_fig_tag);
        return;
        
    case ('cb_param_data_overlap_edit')
        cb_param_data_overlap_edit(gui_interface_fig_tag);
        return;
        
    case ('cb_param_data_form_popup')
        cb_param_data_form_popup(gui_interface_fig_tag);
        return;
        
    case ('cb_param_low_freq_edit')
        cb_param_low_freq_edit(gui_interface_fig_tag);
        return;
        
    case ('cb_param_high_freq_edit')
        cb_param_high_freq_edit(gui_interface_fig_tag);
        return;
        
    case ('cb_param_sig_preproc_popup')
        cb_param_sig_preproc_popup(gui_interface_fig_tag);
        return;
        
    case ('cb_param_masking_meth_popup')
        cb_param_masking_meth_popup(gui_interface_fig_tag);
        return;
        
    case ('cb_param_masking_param_edit')
        cb_param_masking_param_edit(gui_interface_fig_tag);
        return;
        
    case ('cb_param_mask_adj_popup')
        cb_param_mask_adj_popup(gui_interface_fig_tag);
        return;
        
    case ('cb_param_corr_type_popup')
        cb_param_corr_type_popup(gui_interface_fig_tag);
        return;
        
    case ('cb_param_corr_std_popup')
        cb_param_corr_std_popup(gui_interface_fig_tag);
        return;
        
    case ('cb_param_max_time_lag_edit')
        cb_param_max_time_lag_edit(gui_interface_fig_tag);
        return;
        
    case ('cb_param_max_freq_lag_edit')
        cb_param_max_freq_lag_edit(gui_interface_fig_tag);
        return;
        
    case ('cb_param_save_settings_button')
        cb_param_save_settings_button(gui_interface_fig_tag);
        return;
        
    case ('cb_param_load_settings_button')
        cb_param_load_settings_button(gui_interface_fig_tag);
        return;
        
    case ('cb_status_window')
        cb_status_window(gui_interface_fig_tag);
        return;
        
    case ('cb_status_window_clear')
        cb_status_window_clear(gui_interface_fig_tag);
        return;
        
    case ('cb_status_window_save')
        cb_status_window_save(gui_interface_fig_tag);
        return;
        
    case ('cb_alternate_view_close')
        cb_alternate_view_close(gui_interface_fig_tag);
        return;
        
    case ('cb_alternate_view_open')
        cb_alternate_view_open(gui_interface_fig_tag);
        return;
        
    case ('cb_correlation_run')
        cb_correlation_run(gui_interface_fig_tag);
        return;
        
    case ('cb_analysis_pco')
        cb_analysis_pco(gui_interface_fig_tag);
        return;
        
    case ('cb_analysis_template_rank')
        cb_analysis_template_rank(gui_interface_fig_tag);
        return;
        
    case ('cb_analysis_nearest_neighbor')
        cb_analysis_nearest_neighbor(gui_interface_fig_tag);
        return;
        
    case ('cb_program_quit')
        cb_program_quit(gui_interface_fig_tag);
        return;
        
    case ('cb_about_prg')
        cb_about_prg(gui_interface_fig_tag);
        return;
        
    otherwise
        fprintf(1, 'Unrecognized calling mode...\n');
        
        return %% end switch
        
        
end;

end





%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: gui_interface_fig_handle
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function create_xcorr_gui_interface_figure(gui_interface_fig_tag)


%% defeat for now
if 0
    %% make sure this is running in MATLAB 6.5x
    matver = version;
    if  ~strcmpi(matver(1:3), '6.5')
        fprintf(1, '\nThis program should be run in MATLAB version 6.5x.  Quitting.\n\n');
        return;
    end
end


%% check for open dialog
set(0, 'showhiddenhandles', 'on');
if ~isempty(findobj(0, 'tag', gui_interface_fig_tag))
    %% dialog already open, reset handles and drop out
    set(0, 'showhiddenhandles', 'off');
    return;
end
set(0, 'showhiddenhandles', 'off');



try
    %% check for a default file, and store in figure userdata
    
    load([pwd, '\settings\default_settings.mat'], '-mat');
    %% this should contain the variable 'figdata'
    
catch
    %% set up defaults manually, and store in figure userdata
    
    
    %% set up some aesthetic defaults first
    %% --> defaults for gui layout
    
    black = [0 0 0];
    
    % %% batch-tool color scheme
    % figbackgroundcolor = [.6 .65 .8];
    % buttoncolor = [.85 .85 .85];
    % editablecontrolcolor = [.85 .85 .85];
    % slidercolor = [.85 .85 .85];
    % displayfieldcolor = [.85 .85 .85];
    % displayfieldtextcolor = [0 0 0];
    % c_map = spring(256);
    
    % %% itunes color scheme
    % figbackgroundcolor = [.825 .825 .825];
    % buttoncolor = [.975 .975 .975];
    % editablecontrolcolor = [.89 .9 .79];
    % slidercolor = [.825 .825 .825];
    % displayfieldcolor = [.84 .85 .74];
    % displayfieldtextcolor = [0 0 0];
    % c_map = bone(256);
    
    % %% oscilloscope I color scheme
    % figbackgroundcolor = [.875 .875 .875];
    % buttoncolor = [.775 .775 .775];
    % editablecontrolcolor = [.8 .825 .8];
    % slidercolor = [.875 .875 .875];
    % displayfieldcolor = [.45 .95 .9];
    % displayfieldtextcolor = [0 0 0];
    % c_map = copper(256);
    
    %% oscilloscope II color scheme
    figure_bgcolor = [.875 .875 .875];
    figure_fgcolor = [.5 .5 .5];
    checkbox_bgcolor = figure_bgcolor;
    checkbox_fgcolor = black;
    editfield_bgcolor = [.8 .825 .8];
    editfield_fgcolor = black;
    listbox_bgcolor = editfield_bgcolor;
    listbox_fgcolor = editfield_fgcolor;
    popup_bgcolor = editfield_bgcolor;
    popup_fgcolor = editfield_fgcolor;
    pushbutton_bgcolor = [.775 .775 .775];
    pushbutton_fgcolor = black;
    radiobutton_bgcolor = figure_bgcolor;
    radiobutton_fgcolor = black;
    slider_bgcolor = [.875 .875 .875];
    slider_fgcolor = black;
    textfield_bgcolor = figure_bgcolor;
    textfield_fgcolor = black;
    togglebutton_bgcolor = editfield_bgcolor;
    togglebutton_fgcolor = editfield_fgcolor;
    displayfield_bgcolor = [0 .15 .1];
    displayfield_fgcolor = [.6 1 .6];
    
    colormap = (flipud(copper(256)));
    
    % %% med image color scheme
    % figbackgroundcolor = [.925 .915 .85];
    % buttoncolor = [.965 .975 .965];
    % editablecontrolcolor = [.965 .975 .965];
    % slidercolor = [.85 .85 .85];
    % displayfieldcolor = [.24 .25 .24];
    % displayfieldtextcolor = [.85 1 1];
    % c_map = bone(256);
    
    
    % %% silver color scheme
    % figbackgroundcolor = [.875 .875 .875];
    % buttoncolor = [.8 .8 .8];
    % editablecontrolcolor = [.975 .975 .95];
    % slidercolor = [.85 .85 .85];
    % displayfieldcolor = [.85 .85 .85];
    % displayfieldtextcolor = [0 0 0];
    
    
    
    figdata.layout.figure_bgcolor = figure_bgcolor;
    figdata.layout.figure_fgcolor = figure_fgcolor;
    
    figdata.layout.checkbox_bgcolor = checkbox_bgcolor;
    figdata.layout.checkbox_fgcolor = checkbox_fgcolor;
    
    figdata.layout.editfield_bgcolor = editfield_bgcolor;
    figdata.layout.editfield_fgcolor = editfield_fgcolor;
    
    figdata.layout.listbox_bgcolor = listbox_bgcolor;
    figdata.layout.listbox_fgcolor = listbox_fgcolor;
    
    figdata.layout.popup_bgcolor = popup_bgcolor;
    figdata.layout.popup_fgcolor = popup_fgcolor;
    
    figdata.layout.pushbutton_bgcolor = pushbutton_bgcolor;
    figdata.layout.pushbutton_fgcolor = pushbutton_fgcolor;
    
    figdata.layout.radiobutton_bgcolor = radiobutton_bgcolor;
    figdata.layout.radiobutton_fgcolor = radiobutton_fgcolor;
    
    figdata.layout.slider_bgcolor = slider_bgcolor;
    figdata.layout.slider_fgcolor = slider_fgcolor;
    
    figdata.layout.textfield_bgcolor = textfield_bgcolor;
    figdata.layout.textfield_fgcolor = textfield_fgcolor;
    
    figdata.layout.togglebutton_bgcolor = togglebutton_bgcolor;
    figdata.layout.togglebutton_fgcolor = togglebutton_fgcolor;
    
    figdata.layout.displayfield_bgcolor = displayfield_bgcolor;
    figdata.layout.displayfield_fgcolor = displayfield_fgcolor;
    
    figdata.layout.colormap = colormap;
    figdata.layout.colormap_invert = 'off';
    
    
    %% text style
    figdata.layout.fontname_default = 'arial'; %%'franklin gothic medium';
    figdata.layout.fontunits_default = 'pixels';
    figdata.layout.fontweight_default = 'normal';
    
    
    %% default spectrogram display options
    figdata.layout.axes_xrange = [0, 2]; %% start and range
    figdata.layout.axes_yrange = [0, 1000]; %% start and range
    
    figdata.layout.axes_crange = [-10, 60]; %% start and range
    figdata.layout.axes_dBrange = 40; %% range
    
    
    %% default computation parameters
    figdata.param.fft_len = 512;
    figdata.param.data_len = 50;  %% % of FFT size
    figdata.param.taper_func = 'Hann';
    figdata.param.data_overlap = 75;  %% % of data window
    figdata.param.data_form = 'dB';
    
    figdata.param.low_freq = 0;
    figdata.param.high_freq = 0;
    figdata.param.sig_preproc = 'None';
    
    figdata.param.masking_meth = 'None';
    figdata.param.masking_param = 50;
    figdata.param.mask_adj = 'Bias';
    figdata.param.corr_type = 'Spec: Pairwise';
    
    figdata.param.corr_std = 'None';
    figdata.param.max_time_lag = 0;
    figdata.param.max_freq_lag = 0;
    
    
    %% last directories used
    figdata.last_sndpath = [pwd, filesep];
    figdata.last_outpath = [pwd, filesep];
    %figdata.last_export_path = [pwd, filesep]; %% not sure I need this
    figdata.last_analysis_path = [pwd, filesep];
    
end


%% set run-specific defaults
%% sound xcorr tool path
figdata.defaultpath = pwd;

%% add gui tool directory to path for use during program
%% (assume it was started from it's home dir)
% Pathing controlled by Sedna
% path(figdata.defaultpath, path);


%% loaded sound data
figdata.sound_list = [];
%% sound list fields:
%%   figdata.sound_list.data = [];
%%   figdata.sound_list.samplerate = [];
%%   figdata.sound_list.num_samples = [];
%%   figdata.sound_list.filename = [];
%%   figdata.sound_list.path = [];
%%   figdata.sound_list.resample_flag = [];


%% output file name
figdata.output = [];


%% get info about current screen
scr_units = figdata.layout.fontunits_default;
set(0, 'units', scr_units);
scr_dim = get(0, 'screensize');
scr_left = scr_dim(1);
scr_bottom = scr_dim(2);
scr_width = scr_dim(3);
scr_height = scr_dim(4);


%% set up a little hack to deal with double headed screens
if scr_width > 3000
    scr_width = scr_width/2;
end


% left = 0.25 * scr_width + scr_left;
% bottom = 0.4 * scr_height + scr_bottom;
% width = 0.4 * scr_width;
% height = 0.5 * scr_height;
left = scr_left + 5;
bottom = scr_height + scr_bottom - 751;
width = 650;
height = 700;


fig_position = [left, bottom, width, height];

figdata.layout.fontsize_default = round(0.0075 * scr_dim(3));


% waitbar_units = fontunits_default;
% waitbar_position = [left + 0.05*width, bottom + 0.6*height, 0.9*width, 0.3*height];


%% create the gui interface figure
dialog_title = ' :SoundXT: Sound Cross Correlation Tool';


keypress_fun = [mfilename, '(''key_press_intercept'');'];

gui_interface_fig_handle = figure('tag', gui_interface_fig_tag,...
    'name', dialog_title,...
    'numbertitle', 'off',...
    'menubar', 'none',...
    'color', figdata.layout.figure_bgcolor,...
    'units', scr_units,...
    'position', fig_position,...
    'resize', 'on',...
    'handlevisibility', 'callback',...
    'closerequestfcn', [mfilename, '(''cb_program_quit'');'],...
    'keypressfcn', keypress_fun);


%% switch to using 'handlevisibility', 'callback',... (see above)
% %% do this little trick to prevent other programs (namely ) using gca
% %% from screwing up my gui figure
% set(gca, 'visible', 'off');

%% set sound read-in variables
figdata.log_flag = 0;
figdata.clip_flag = 0;
figdata.log = cell(0);
figdata.logpath = struct();

%% save figure data
set(gui_interface_fig_handle, 'userdata', figdata);





%%~~~
%% set up interface and controls
%%~~~


%% Menu Bar
setup_menu_items(gui_interface_fig_handle);


%% Sound List
setup_sound_list_gui(gui_interface_fig_handle);


%% Spec Viewer
setup_sound_spec_viewer(gui_interface_fig_handle);


%% Output File
setup_output_gui(gui_interface_fig_handle);


%% Spectrogram and Correlation Settings
setup_spec_xcorr_param_gui(gui_interface_fig_handle);


%% Run and Quit Buttons
setup_run_quit_gui(gui_interface_fig_handle);


%% Status Window
setup_status_window_gui(gui_interface_fig_handle);


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: setup_menu_items
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function setup_menu_items(gui_interface_fig_handle)

figdata = get(gui_interface_fig_handle, 'userdata');


%%~~~
%% Files
%%~~~
files_menu = uimenu(gui_interface_fig_handle,...
    'label', 'Files',...
    'enable', 'on');

% Added SCK
callback_fun = [mfilename, '(''cb_sound_list_add_log'');'];
uimenu(files_menu,...
    'label', 'Add selection table...',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_sound_list_add'');'];
uimenu(files_menu,...
    'label', 'Add Sound Files...',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_sound_list_delete'');'];
uimenu(files_menu,...
    'label', 'Delete Sound Files',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_output_file_select'');'];
uimenu(files_menu,...
    'label', 'Select Output File...',...
    'separator', 'on',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_export_corr_data_file'');'];
uimenu(files_menu,...
    'label', 'Export Correlation Data...',...
    'separator', 'on',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_param_save_settings_button'');'];
uimenu(files_menu,...
    'label', 'Save Settings File...',...
    'separator', 'on',...
    'callback', callback_fun,...
    'enable', 'off');

callback_fun = [mfilename, '(''cb_param_load_settings_button'');'];
uimenu(files_menu,...
    'label', 'Load Settings File...',...
    'callback', callback_fun,...
    'enable', 'off');

callback_fun = [mfilename, '(''cb_program_quit'');'];
uimenu(files_menu,...
    'label', 'Exit SoundXT',...
    'separator', 'on',...
    'callback', callback_fun,...
    'enable', 'on');


%%~~~
%% Preferences
%%~~~
preferences_menu = uimenu(gui_interface_fig_handle,...
    'label', 'Preferences',...
    'enable', 'off');

color_scheme_submenu = uimenu(preferences_menu,...
    'label', 'Color Schemes',...
    'enable', 'off');

uimenu(color_scheme_submenu,...
    'label', 'Classic',...
    'callback', callback_fun,...
    'enable', 'on');
uimenu(color_scheme_submenu,...
    'label', 'iTunes',...
    'callback', callback_fun,...
    'enable', 'on');
uimenu(color_scheme_submenu,...
    'label', 'Oscilloscope',...
    'callback', callback_fun,...
    'enable', 'on');
uimenu(color_scheme_submenu,...
    'label', 'Silver',...
    'callback', callback_fun,...
    'enable', 'on');


%%~~~
%% Controls
%%~~~
controls_menu = uimenu(gui_interface_fig_handle,...
    'label', 'Controls',...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_sound_list_movedown'');'];
uimenu(controls_menu,...
    'label', 'Move Sounds Down',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_sound_list_moveup'');'];
uimenu(controls_menu,...
    'label', 'Move Sounds Up',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_sound_list_order'');'];
uimenu(controls_menu,...
    'label', 'Order Sounds By Name',...
    'callback', callback_fun);

callback_fun = [mfilename, '(''cb_sound_list_revert_order'');'];
uimenu(controls_menu,...
    'label', 'Revert Sound Order',...
    'enable', 'off',...
    'callback', callback_fun);

callback_fun = [mfilename, '(''cb_sound_list_commit'');'];
uimenu(controls_menu,...
    'label', 'Commit Sound Files',...
    'callback', callback_fun,...
    'enable', 'on');


callback_fun = [mfilename, '(''cb_correlation_run'');'];
uimenu(controls_menu,...
    'tag', ['run_menu_item_uicontrol', mfilename],...
    'label', 'Run Correlation',...
    'separator', 'on',...
    'callback', callback_fun,...
    'enable', 'off');



%%~~~
%% Settings
%%~~~
settings_menu = uimenu(gui_interface_fig_handle,...
    'label', 'Settings',...
    'enable', 'off');

sig_preproc_param_menu = uimenu(settings_menu,...
    'label', 'Signal Preprocessing...',...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(sig_preproc_param_menu,...
    'label', 'Preprocessing Method...',...
    'callback', callback_fun,...
    'enable', 'on');

spec_param_menu = uimenu(settings_menu,...
    'label', 'Spectrogram Generation...',...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(spec_param_menu,...
    'label', 'FFT Length...',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(spec_param_menu,...
    'label', 'Data Length...',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(spec_param_menu,...
    'label', 'Taper Function...',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(spec_param_menu,...
    'label', 'Data Overlap...',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(spec_param_menu,...
    'label', 'Data Format...',...
    'callback', callback_fun,...
    'enable', 'on');

masking_param_menu = uimenu(settings_menu,...
    'label', 'Spectrogram Masking...',...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(masking_param_menu,...
    'label', 'Masking Method...',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(masking_param_menu,...
    'label', 'Masking Parameter...',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(masking_param_menu,...
    'label', 'Mask Adjustment...',...
    'callback', callback_fun,...
    'enable', 'on');

filter_param_menu = uimenu(settings_menu,...
    'label', 'Bandpass Filtering...',...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(filter_param_menu,...
    'label', 'Low Frequency Limit...',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(filter_param_menu,...
    'label', 'High Frequency Limit...',...
    'callback', callback_fun,...
    'enable', 'on');

xcorr_param_menu = uimenu(settings_menu,...
    'label', 'Correlation...',...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(xcorr_param_menu,...
    'label', 'Correlation Type...',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(xcorr_param_menu,...
    'label', 'Standardization Method...',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(xcorr_param_menu,...
    'label', 'Max Time Lag...',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_'');'];
uimenu(xcorr_param_menu,...
    'label', 'Max Frequency Lag...',...
    'callback', callback_fun,...
    'enable', 'on');



%%~~~
%% Analysis
%%~~~
analysis_menu = uimenu(gui_interface_fig_handle,...
    'label', 'R-matrix Analysis',...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_analysis_pco'');'];
uimenu(analysis_menu,...
    'label', 'PCO...',...
    'tag', ['analysis_pco_uimenu', mfilename],...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_analysis_template_rank'');'];
uimenu(analysis_menu,...
    'label', 'Template Rank...',...
    'tag', ['analysis_template_rank_uimenu', mfilename],...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_analysis_nearest_neighbor'');'];
uimenu(analysis_menu,...
    'label', 'Nearest Neighbor...',...
    'tag', ['analysis_template_rank_uimenu', mfilename],...
    'callback', callback_fun,...
    'enable', 'on');


%%~~~
%% About
%%~~~
callback_fun = [mfilename, '(''cb_about_prg'');'];
controls_menu = uimenu(gui_interface_fig_handle,...
    'label', 'About',...
    'callback', callback_fun,...
    'enable', 'on');

% callback_fun = [mfilename, '(''cb_about_prg'');'];
% uimenu(controls_menu,...
%   'label', 'About SoundXT',...
%   'callback', callback_fun,...
%   'enable', 'on');

%%~~~
%% Help
%%~~
callback_fun = [mfilename, '(''help_callback'');'];
help_menu = uimenu(gui_interface_fig_handle,...
    'label', 'Help',...
    'callback', callback_fun,...
    'enable', 'on');

% % callback_fun = [mfilename, '(''about_ase_sedna_callback'');'];
% % uimenu(help_menu,...
% %     'label', 'About ASE Sedna',...
% %     'callback', callback_fun,...
% %     'enable', 'on');
% % 
% % callback_fun = [mfilename, '(''user_manual_callback'');'];
% % uimenu(help_menu,...
% %     'label', 'User Manual and Development Guide',...
% %     'callback', callback_fun,...
% %     'enable', 'on');
% % 
% % callback_fun = [mfilename, '(''jira_menu_callback'');'];
% % uimenu(help_menu,...
% %     'label', 'JIRA: ASE Sedna',...
% %     'callback', callback_fun,...
% %     'enable', 'on');
% % 
% % callback_fun = [mfilename, '(''known_bugs_callback'');'];
% % uimenu(help_menu,...
% %     'label', 'Known Bugs',...
% %     'callback', callback_fun,...
% %     'enable', 'on');
% % 
% % callback_fun = [mfilename, '(''change_password_callback'');'];
% % uimenu(help_menu,...
% %     'label', 'ASE Repository:Change Password',...
% %     'callback', callback_fun,...
% %     'enable', 'on');

end



%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: setup_sound_list_gui
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function setup_sound_list_gui(gui_interface_fig_handle)


figdata = get(gui_interface_fig_handle, 'userdata');


lftedge = 0.025;
btmedge = 0.925;

txtwidth = .4125;
txtheight = 0.035;

buttonwidth = 0.045;
buttonheight = 0.04;

inc = 0.045;


%%~~~
%% Text Label
%%~~~
uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge+.04 txtwidth txtheight],...
    'string', 'Selections to Correlate',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);
% 'position', [lftedge btmedge txtwidth txtheight],... % Modified SCK

uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'tag', ['sound_list_text_uicontrol', mfilename],...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge+.02 txtwidth txtheight],...
    'string', '(0 sounds in list)',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', .8*figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.figure_fgcolor,...
    'enable', 'on');

%  'position', [lftedge btmedge-.02 txtwidth txtheight],... Modified SCK
%%~~~
%% Sound List
%%~~~
%% make a context menu for the soundlist window
sound_list_popup_uicontextmenu = uicontextmenu(...
    'parent', gui_interface_fig_handle);

callback_fun = [mfilename, '(''cb_sound_list_play'');'];
uimenu(sound_list_popup_uicontextmenu,...
    'label', 'Play',...
    'callback', callback_fun,...
    'enable', 'on');

% Added SCK
callback_fun = [mfilename, '(''cb_sound_list_add_log'');'];
uimenu(sound_list_popup_uicontextmenu,...
    'label', 'Add selection table',...
    'separator', 'on',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_sound_list_add'');'];
uimenu(sound_list_popup_uicontextmenu,...
    'label', 'Add...',...
    'separator', 'on',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_sound_list_delete'');'];
uimenu(sound_list_popup_uicontextmenu,...
    'label', 'Delete',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_sound_list_moveup'');'];
uimenu(sound_list_popup_uicontextmenu,...
    'label', 'Move Up',...
    'separator', 'on',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_sound_list_movedown'');'];
uimenu(sound_list_popup_uicontextmenu,...
    'label', 'Move Down',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_sound_list_order'');'];
uimenu(sound_list_popup_uicontextmenu,...
    'label', 'Order By Name',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_sound_list_revert_order'');'];
uimenu(sound_list_popup_uicontextmenu,...
    'label', 'Revert Sound Order',...
    'callback', callback_fun,...
    'enable', 'off');


callback_fun = [mfilename, '(''cb_sound_list_commit'');'];
uimenu(sound_list_popup_uicontextmenu,...
    'label', 'Commit',...
    'separator', 'on',...
    'callback', callback_fun,...
    'enable', 'on');



callback_fun = [mfilename, '(''cb_sound_list_popup'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'listbox',...
    'handlevisibility', 'callback',...
    'min', 1,...
    'max', 3,...
    'tag', ['sound_list_popup_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge-.26 txtwidth .25],...
    'string', {' '},...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.displayfield_bgcolor,...
    'foregroundcolor', figdata.layout.displayfield_fgcolor,...
    'tooltipstring', 'select and order sounds to correlate',...
    'uicontextmenu', sound_list_popup_uicontextmenu,...
    'callback', callback_fun,...
    'enable', 'on');


lftedge = lftedge + .2125;



%%~~~
%% Commit List Button
%%~~~
callback_fun = [mfilename, '(''cb_sound_list_commit'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'pushbutton',...
    'handlevisibility', 'callback',...
    'tag', ['sound_list_commit_button_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge+1.1*buttonheight 4.3*buttonwidth .55*buttonheight],...
    'string', 'commit',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.pushbutton_bgcolor,...
    'foregroundcolor', figdata.layout.pushbutton_fgcolor,...
    'tooltipstring', 'commit sound list',...
    'callback', callback_fun,...
    'enable', 'on');




%%~~~
%% Added SCK
% Add XBAT Log Button
%%~~~
callback_fun = [mfilename, '(''cb_sound_list_add_log'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'pushbutton',...
    'handlevisibility', 'callback',...
    'tag', ['sound_list_add_log_button_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge-.21 btmedge buttonwidth*4.2 buttonheight*.7],...
    'string', 'add selection table',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.pushbutton_bgcolor,...
    'foregroundcolor', figdata.layout.pushbutton_fgcolor,...
    'tooltipstring', 'add selection table',...
    'callback', callback_fun,...
    'enable', 'on');


lftedge = lftedge;


%%~~~
%% Add Sound Button
%%~~~
callback_fun = [mfilename, '(''cb_sound_list_add'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'pushbutton',...
    'handlevisibility', 'callback',...
    'tag', ['sound_list_add_button_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge buttonwidth buttonheight],...
    'string', 'add',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.pushbutton_bgcolor,...
    'foregroundcolor', figdata.layout.pushbutton_fgcolor,...
    'tooltipstring', 'add sound files to list',...
    'callback', callback_fun,...
    'enable', 'on');


lftedge = lftedge + 1.1*buttonwidth;


%%~~~
%% Delete Sound Button
%%~~~
callback_fun = [mfilename, '(''cb_sound_list_delete'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'pushbutton',...
    'handlevisibility', 'callback',...
    'tag', ['sound_list_del_button_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge buttonwidth buttonheight],...
    'string', 'del',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.pushbutton_bgcolor,...
    'foregroundcolor', figdata.layout.pushbutton_fgcolor,...
    'tooltipstring', 'delete sounds files from list',...
    'callback', callback_fun,...
    'enable', 'on');


lftedge = lftedge + 1.1*buttonwidth;


%%~~~
%% Move Up in List Button
%%~~~
callback_fun = [mfilename, '(''cb_sound_list_moveup'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'pushbutton',...
    'handlevisibility', 'callback',...
    'tag', ['sound_list_mvup_button_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge buttonwidth buttonheight],...
    'string', 'up',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.pushbutton_bgcolor,...
    'foregroundcolor', figdata.layout.pushbutton_fgcolor,...
    'tooltipstring', 'move sound up in list',...
    'callback', callback_fun,...
    'enable', 'on');


lftedge = lftedge + 1.1*buttonwidth;


%%~~~
%% Move Down in List Button
%%~~~
callback_fun = [mfilename, '(''cb_sound_list_movedown'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'pushbutton',...
    'handlevisibility', 'callback',...
    'tag', ['sound_list_mvdwn_button_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge buttonwidth buttonheight],...
    'string', 'dwn',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.pushbutton_bgcolor,...
    'foregroundcolor', figdata.layout.pushbutton_fgcolor,...
    'tooltipstring', 'move sound down in list',...
    'callback', callback_fun,...
    'enable', 'on');


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: setup_sound_spec_viewer
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function setup_sound_spec_viewer(gui_interface_fig_handle)

addpath(fileparts(mfilename('fullpath')))


figdata = get(gui_interface_fig_handle, 'userdata');


lftedge = 0.025;
btmedge = 0.925;

txtwidth = .4125;
txtheight = 0.035;

buttonwidth = 0.045;
buttonheight = 0.04;

inc = 0.045;



%%~~~
%% Sound Spectrogram Viewer
%%~~~

%% make a context menu for the viewer frame
sound_spec_view_uicontextmenu = uicontextmenu(...
    'parent', gui_interface_fig_handle);

callback_fun = [mfilename, '(''cb_sndspc_view_xaxis'');'];
uimenu(sound_spec_view_uicontextmenu,...
    'label', 'Set X-Axis Range...',...
    'callback', callback_fun);

callback_fun = [mfilename, '(''cb_sndspc_view_yaxis'');'];
uimenu(sound_spec_view_uicontextmenu,...
    'label', 'Set Y-Axis Range...',...
    'callback', callback_fun);

callback_fun = [mfilename, '(''cb_sndspc_view_fullxaxis'');'];
uimenu(sound_spec_view_uicontextmenu,...
    'label', 'Display Full X-Axis Range',...
    'callback', callback_fun);

callback_fun = [mfilename, '(''cb_sndspc_view_fullyaxis'');'];
uimenu(sound_spec_view_uicontextmenu,...
    'label', 'Display Full Y-Axis Range',...
    'callback', callback_fun);

callback_fun = [mfilename, '(''cb_sndspc_view_colormap'');'];
sndspc_view_colormap_uicontextmenu = uimenu(sound_spec_view_uicontextmenu,...
    'separator', 'on',...
    'label', 'Set Colormap');
uimenu(sndspc_view_colormap_uicontextmenu,...
    'label', 'Hot',...
    'callback', callback_fun);
uimenu(sndspc_view_colormap_uicontextmenu,...
    'label', 'Cool',...
    'callback', callback_fun);
uimenu(sndspc_view_colormap_uicontextmenu,...
    'label', 'Autumn',...
    'callback', callback_fun);
uimenu(sndspc_view_colormap_uicontextmenu,...
    'label', 'Winter',...
    'callback', callback_fun);
uimenu(sndspc_view_colormap_uicontextmenu,...
    'label', 'Spring',...
    'callback', callback_fun);
uimenu(sndspc_view_colormap_uicontextmenu,...
    'label', 'Summer',...
    'callback', callback_fun);
uimenu(sndspc_view_colormap_uicontextmenu,...
    'label', 'Jet',...
    'callback', callback_fun);
uimenu(sndspc_view_colormap_uicontextmenu,...
    'label', 'Bone',...
    'callback', callback_fun);
uimenu(sndspc_view_colormap_uicontextmenu,...
    'label', 'Gray',...
    'callback', callback_fun);
uimenu(sndspc_view_colormap_uicontextmenu,...
    'label', 'Copper',...
    'callback', callback_fun);
uimenu(sndspc_view_colormap_uicontextmenu,...
    'label', 'Pink',...
    'callback', callback_fun);

callback_fun = [mfilename, '(''cb_sndspc_view_colormap_invert'');'];
uimenu(sound_spec_view_uicontextmenu,...
    'tag', ['invert_colormap_uimenu', mfilename],...
    'label', 'Invert Colormap',...
    'checked', figdata.layout.colormap_invert,...
    'callback', callback_fun);


callback_fun = [mfilename, '(''cb_sndspc_view_gain_range'');'];
sndspc_view_colormap_uicontextmenu = uimenu(sound_spec_view_uicontextmenu,...
    'label', 'Set Gain/Range...',...
    'separator', 'on',...
    'callback', callback_fun);

callback_fun = [mfilename, '(''cb_sndspc_view_peak_range'');'];
sndspc_view_colormap_uicontextmenu = uimenu(sound_spec_view_uicontextmenu,...
    'label', 'Set Peak Range...',...
    'callback', callback_fun);


callback_fun = [mfilename, '(''cb_sndspc_view_gain_range_display'');'];
sndspc_view_colormap_uicontextmenu = uimenu(sound_spec_view_uicontextmenu,...
    'label', 'Display Gain/Range',...
    'separator', 'on',...
    'callback', callback_fun);

callback_fun = [mfilename, '(''cb_sndspc_view_peak_range_display'');'];
sndspc_view_colormap_uicontextmenu = uimenu(sound_spec_view_uicontextmenu,...
    'label', 'Display Peak Range',...
    'callback', callback_fun);

callback_fun = [mfilename, '(''cb_sndspc_view_full_range_display'');'];
sndspc_view_colormap_uicontextmenu = uimenu(sound_spec_view_uicontextmenu,...
    'label', 'Display Full Range',...
    'callback', callback_fun);


callback_fun = [mfilename, '(''cb_sndspc_view_enlarge'');'];
sndspc_view_colormap_uicontextmenu = uimenu(sound_spec_view_uicontextmenu,...
    'label', 'Expand Spectrogram Viewer',...
    'separator', 'on',...
    'enable', 'off',...
    'callback', callback_fun);


callback_fun = [mfilename, '(''cb_sndspc_view_waveform'');'];
sndspc_view_colormap_uicontextmenu = uimenu(sound_spec_view_uicontextmenu,...
    'label', 'Show Time Waveform',...
    'separator', 'on',...
    'enable', 'off',...
    'callback', callback_fun);


% callback_fun = [mfilename, '(''cb_sndspc_view_reduce'');'];
% sndspc_view_colormap_uicontextmenu = uimenu(sound_spec_view_uicontextmenu,...
%   'label', 'Reduce Display',...
%   'callback', callback_fun);

callback_fun = [mfilename, '(''cb_sound_list_playback_rate'');'];
sndspc_view_colormap_uicontextmenu = uimenu(sound_spec_view_uicontextmenu,...
    'label', 'Set Playback Rate',...
    'separator', 'on',...
    'enable', 'off',...
    'callback', callback_fun);



%% make a frame around it with labels
sound_spec_view_axes_frame = axes('parent', gui_interface_fig_handle,...
    'position', [lftedge btmedge-.5125 .411 .25],...
    'uicontextmenu', sound_spec_view_uicontextmenu,...
    'visible', 'off',...
    'handlevisibility', 'callback');

rectangle('parent', sound_spec_view_axes_frame,...
    'position', [0, 0, 1, 1]);

%% do it this way so position of labels is invariant
text('parent', sound_spec_view_axes_frame,...
    'tag', ['sound_spec_view_axes_xlabel', mfilename],...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [.4, .05, 0],...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default*.75,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'color', figdata.layout.textfield_fgcolor,...
    'string', 'Time (sec)');

text('parent', sound_spec_view_axes_frame,...
    'tag', ['sound_spec_view_axes_ylabel', mfilename],...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [.035, .425, 0],...
    'rotation', 90,...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default*.75,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'color', figdata.layout.textfield_fgcolor,...
    'string', 'Freq (Hz)');


%% create the spec display axes
sound_spec_view_axes = axes('parent', gui_interface_fig_handle,...
    'tag', ['sound_spec_view_axes', mfilename],...
    'units', 'normalized',...
    'position', [lftedge+.065 btmedge-.465 .32 .175],...
    'layer', 'top',...
    'uicontextmenu', sound_spec_view_uicontextmenu,...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default*.75,...
    'fontweight', figdata.layout.fontweight_default,...
    'color', figdata.layout.textfield_bgcolor,...
    'xcolor', figdata.layout.textfield_fgcolor,...
    'ycolor', figdata.layout.textfield_fgcolor,...
    'box', 'on',...
    'xlim', [figdata.layout.axes_xrange(1), sum(figdata.layout.axes_xrange)],...
    'ylim', [figdata.layout.axes_yrange(1), sum(figdata.layout.axes_yrange)],...
    'handlevisibility', 'callback');

% set(get(sound_spec_view_axes, 'xlabel'),...
%   'fontname', figdata.layout.fontname_default,...
%   'fontunits', figdata.layout.fontunits_default,...
%   'fontsize', figdata.layout.fontsize_default*.75,...
%   'fontweight', figdata.layout.fontweight_default,...
%   'backgroundcolor', figdata.layout.textfield_bgcolor,...
%   'color', figdata.layout.textfield_fgcolor,...
%   'string', 'Time (sec)');
%
% set(get(sound_spec_view_axes, 'ylabel'),...
%   'fontname', figdata.layout.fontname_default,...
%   'fontunits', figdata.layout.fontunits_default,...
%   'fontsize', figdata.layout.fontsize_default*.75,...
%   'fontweight', figdata.layout.fontweight_default,...
%   'backgroundcolor', figdata.layout.textfield_bgcolor,...
%   'color', figdata.layout.textfield_fgcolor,...
%   'string', 'Freq (Hz)');


%% and a colorbar
set(0, 'showhiddenhandles', 'on'); %% to make sure it sees the parent fig handle
colorbar_h = colorbar('peer', sound_spec_view_axes);
set(0, 'showhiddenhandles', 'off');
set(colorbar_h,...
    'tag', ['sound_spec_view_colorbar', mfilename],...
    'handlevisibility', 'callback',...
    'fontname', figdata.layout.fontname_default,...
    'fontsize', figdata.layout.fontsize_default*.75,...
    'fontweight', figdata.layout.fontweight_default,...
    'color', figdata.layout.textfield_bgcolor,...
    'xcolor', figdata.layout.textfield_fgcolor,...
    'ycolor', figdata.layout.textfield_fgcolor);
% %     'fontunits', figdata.layout.fontunits_default,...

% %% blank out colormap for now
% set(gui_interface_fig_handle, 'colormap', white(256));
%% set to user selected color map
set(gui_interface_fig_handle, 'colormap', figdata.layout.colormap);


%% make an autoscale checkbox
callback_fun = [mfilename, '(''cb_sndspc_view_fullrange_checkbox'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'checkbox',...
    'handlevisibility', 'callback',...
    'tag', ['display_scale_checkbox_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge+.35 btmedge-.51 .06 .025],...
    'string', 'full',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default*.75,...
    'fontweight', figdata.layout.fontweight_default,...
    'value', 1,...
    'userdata', 'peakrange',...
    'backgroundcolor', figdata.layout.checkbox_bgcolor,...
    'foregroundcolor', figdata.layout.checkbox_fgcolor,...
    'tooltipstring', 'check to display full range of spectrogram values',...
    'callback', callback_fun);


%% and a play button
callback_fun = [mfilename, '(''cb_sound_list_play'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'pushbutton',...
    'handlevisibility', 'callback',...
    'tag', ['cb_sndspc_view_play_snd_button_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge+.01 btmedge-.504 .025 .025],...
    'string', '>',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default*.75,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.pushbutton_bgcolor,...
    'foregroundcolor', figdata.layout.pushbutton_fgcolor,...
    'tooltipstring', 'push to play sound',...
    'callback', callback_fun);

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: setup_output_gui
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function setup_output_gui(gui_interface_fig_handle)


figdata = get(gui_interface_fig_handle, 'userdata');


lftedge = 0.025;
btmedge = 0.35;

txtwidth = .4125;
txtheight = 0.035;

editwidth = .4125;
editheight = 0.04;

buttonwidth = 0.065;
buttonheight = 0.04;


%%~~~
%% Text Label
%%~~~
uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'Output File Name',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

%%~~~
%% Output File Select Button
%%~~~
callback_fun = [mfilename, '(''cb_output_file_select'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'pushbutton',...
    'handlevisibility', 'callback',...
    'tag', ['output_select_button_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge+.34 btmedge+.005 buttonwidth buttonheight],...
    'string', 'select',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.pushbutton_bgcolor,...
    'foregroundcolor', figdata.layout.pushbutton_fgcolor,...
    'tooltipstring', 'select output file name',...
    'callback', callback_fun);

%%~~~
%% Output Display Box
%%~~~
uicontrol(gui_interface_fig_handle,...
    'style', 'edit',...
    'handlevisibility', 'callback',...
    'tag', ['output_edit_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge-.045 editwidth editheight],...
    'string', ' ',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'foregroundcolor', figdata.layout.displayfield_fgcolor,...
    'backgroundcolor', figdata.layout.displayfield_bgcolor,...
    'tooltipstring', 'output file name',...
    'enable', 'inactive');

% %%~~~
% %% Select Output
% %%~~~
% callback_fun = [mfilename, '(''output_edit_cb'');'];
% buttondown_fun = [mfilename, '(''output_edit_bdf'');'];
% output_edit_handle = uicontrol(gui_interface_fig_handle,...
%   'style', 'edit',...
%   'tag', ['output_edit_uicontrol', mfilename],...
%   'units', 'normalized',...
%   'position', [lftedge btmedge-.875 .4125 editheight],...
%   'string', ' ',...
%   'userdata', [pwd, filesep],...
%   'fontname', fontname_default,...
%   'fontunits', fontunits_default,...
%   'fontsize', fontsize_default,...
%   'horizontalalignment', 'left',...
%   'backgroundcolor', editablecontrolcolor,...
%   'tooltipstring', 'enter output file name',...
%   'callback', callback_fun);
% % ,...
% %   'selectionhighlight', 'on',...
% %   'buttondownfcn', buttondown_fun,...
% %   'enable', 'off');



end


%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: setup_spec_xcorr_param_gui
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function setup_spec_xcorr_param_gui(gui_interface_fig_handle)


figdata = get(gui_interface_fig_handle, 'userdata');


lftedge = 0.5;
btmedge = 0.95;

txtwidth = .4125;
txtheight = 0.035;

editwidth = 0.075;
editheight = 0.0345;

popupwidth = 0.225;
popupheight = 0.0345;

sliderwidth = 0.15;
sliderheight = 0.034;

buttonwidth = 0.045;
buttonheight = 0.04;

first_drop = 0.0525;
inc = 0.027;
popinc = 0.027;


%%~~~
%% Text Label
%%~~~
uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'Spectrogram and Correlation Settings',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

btmedge = btmedge - first_drop;


%%~~~
%% FFT Length
%%~~~
uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'FFT Length (pts)',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

btmedge = btmedge - inc;

callback_fun = [mfilename, '(''cb_param_fft_len_edit'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'edit',...
    'handlevisibility', 'callback',...
    'tag', ['fft_len_edit_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge editwidth editheight],...
    'string', figdata.param.fft_len,...
    'userdata', [pwd, filesep],...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.editfield_bgcolor,...
    'foregroundcolor', figdata.layout.editfield_fgcolor,...
    'tooltipstring', 'enter FFT length in points',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_param_fft_len_slider'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'slider',...
    'handlevisibility', 'callback',...
    'tag', ['fft_len_slider_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge+editwidth btmedge sliderwidth sliderheight],...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.slider_bgcolor,...
    'foregroundcolor', figdata.layout.slider_fgcolor,...
    'tooltipstring', 'enter FFT length in points',...
    'callback', callback_fun,...
    'enable', 'off');

btmedge = btmedge - 2*inc;


%%~~~
%% Data Length
%%~~~
uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'Data Length (%)',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

data_len_pts = round(figdata.param.fft_len*figdata.param.data_len/100);
uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'tag', ['data_length_text_uicontrol', mfilename],...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge+.15 btmedge txtwidth txtheight],...
    'string', ['(', num2str(data_len_pts), 'pts)'],...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', .8*figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.figure_fgcolor,...
    'enable', 'on');

btmedge = btmedge - inc;

callback_fun = [mfilename, '(''cb_param_data_len_edit'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'edit',...
    'handlevisibility', 'callback',...
    'tag', ['data_len_edit_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge editwidth editheight],...
    'string', figdata.param.data_len,...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.editfield_bgcolor,...
    'foregroundcolor', figdata.layout.editfield_fgcolor,...
    'tooltipstring', 'enter data length as a percentage of FFT length',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_param_data_len_slider'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'slider',...
    'handlevisibility', 'callback',...
    'tag', ['data_len_slider_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge+editwidth btmedge sliderwidth sliderheight],...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.slider_bgcolor,...
    'foregroundcolor', figdata.layout.slider_fgcolor,...
    'tooltipstring', 'enter data length as a percentage of FFT length',...
    'callback', callback_fun,...
    'enable', 'off');

btmedge = btmedge - 2*inc;


%%~~~
%% Taper Function
%%~~~
available_tapers = get_taper_func;

uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'Taper Function',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

btmedge = btmedge - popinc;

callback_fun = [mfilename, '(''cb_param_taper_fun_popup'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'popupmenu',...
    'handlevisibility', 'callback',...
    'tag', ['taper_func_popup_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge popupwidth popupheight],...
    'string', available_tapers,...
    'value', find(strcmpi(available_tapers, figdata.param.taper_func)),...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.popup_bgcolor,...
    'foregroundcolor', figdata.layout.popup_fgcolor,...
    'tooltipstring', 'select taper function to use',...
    'callback', callback_fun,...
    'enable', 'on');

btmedge = btmedge - 2*inc;


%%~~~
%% Data Overlap
%%~~~
uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'Data Overlap (%)',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

data_overlap_pts = round(data_len_pts*figdata.param.data_overlap/100);
uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'tag', ['data_overlap_text_uicontrol', mfilename],...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge+.157 btmedge txtwidth txtheight],...
    'string', ['(', num2str(data_overlap_pts), 'pts)'],...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', .8*figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.figure_fgcolor,...
    'enable', 'on');

btmedge = btmedge - inc;

callback_fun = [mfilename, '(''cb_param_data_overlap_edit'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'edit',...
    'handlevisibility', 'callback',...
    'tag', ['data_overlap_edit_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge editwidth editheight],...
    'string', figdata.param.data_overlap,...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.editfield_bgcolor,...
    'foregroundcolor', figdata.layout.editfield_fgcolor,...
    'tooltipstring', 'enter data overlap as a percentage of data length',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_param_data_overlap_slider'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'slider',...
    'handlevisibility', 'callback',...
    'tag', ['data_ovrlap_slider_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge+editwidth btmedge sliderwidth sliderheight],...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.slider_bgcolor,...
    'foregroundcolor', figdata.layout.slider_fgcolor,...
    'tooltipstring', 'enter data overlap as a percentage of data length',...
    'callback', callback_fun,...
    'enable', 'off');

btmedge = btmedge - 2*inc;


%%~~~
%% Data Format
%%~~~
data_format_options = {'Amplitude'; 'Power'; 'dB'};

uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'Data Format',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

btmedge = btmedge - popinc;

callback_fun = [mfilename, '(''cb_param_data_form_popup'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'popupmenu',...
    'handlevisibility', 'callback',...
    'tag', ['data_form_popup_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge popupwidth popupheight],...
    'string', data_format_options,...
    'value', find(strcmpi(data_format_options, figdata.param.data_form)),...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.popup_bgcolor,...
    'foregroundcolor', figdata.layout.popup_fgcolor,...
    'tooltipstring', 'select data format to use for spectrogram',...
    'callback', callback_fun,...
    'enable', 'on');

btmedge = btmedge - 2*inc;


%%~~~
%% Low Frequency Bandlimit
%%~~~
uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'Low Bandlimit (Hz)',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

btmedge = btmedge - inc;

callback_fun = [mfilename, '(''cb_param_low_freq_edit'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'edit',...
    'handlevisibility', 'callback',...
    'tag', ['low_freq_edit_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge editwidth editheight],...
    'string', figdata.param.low_freq,...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.editfield_bgcolor,...
    'foregroundcolor', figdata.layout.editfield_fgcolor,...
    'tooltipstring', 'enter low frequency cutoff for bandlimiting',...
    'callback', callback_fun,...
    'enable', 'off');

callback_fun = [mfilename, '(''cb_param_low_freq_slider'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'slider',...
    'handlevisibility', 'callback',...
    'tag', ['low_freq_slider_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge+editwidth btmedge sliderwidth sliderheight],...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.slider_bgcolor,...
    'foregroundcolor', figdata.layout.slider_fgcolor,...
    'tooltipstring', 'enter low frequency cutoff for bandlimiting',...
    'callback', callback_fun,...
    'enable', 'off');

btmedge = btmedge - 2*inc;


%%~~~
%% High Frequency Bandlimit
%%~~~
uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'High Bandlimit (Hz)',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

btmedge = btmedge - inc;

callback_fun = [mfilename, '(''cb_param_high_freq_edit'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'edit',...
    'handlevisibility', 'callback',...
    'tag', ['high_freq_edit_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge editwidth editheight],...
    'string', figdata.param.high_freq,...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.editfield_bgcolor,...
    'foregroundcolor', figdata.layout.editfield_fgcolor,...
    'tooltipstring', 'enter high frequency cutoff for bandlimiting',...
    'callback', callback_fun,...
    'enable', 'off');

callback_fun = [mfilename, '(''cb_param_high_freq_slider'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'slider',...
    'handlevisibility', 'callback',...
    'tag', ['high_freq_slider_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge+editwidth btmedge sliderwidth sliderheight],...
    'string', '0',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.slider_bgcolor,...
    'foregroundcolor', figdata.layout.slider_fgcolor,...
    'tooltipstring', 'enter high frequency cutoff for bandlimiting',...
    'callback', callback_fun,...
    'enable', 'off');

btmedge = btmedge - 2*inc;


%%~~~
%% Signal Preprocessing
%%~~~
sig_preproc_options = {'None'; 'Difference'};

uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'Signal Preprocessing',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

btmedge = btmedge - popinc;

callback_fun = [mfilename, '(''cb_param_sig_preproc_popup'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'popupmenu',...
    'handlevisibility', 'callback',...
    'tag', ['sig_preproc_popup_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge popupwidth popupheight],...
    'string', sig_preproc_options,...
    'value', find(strcmpi(sig_preproc_options, figdata.param.sig_preproc)),...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.popup_bgcolor,...
    'foregroundcolor', figdata.layout.popup_fgcolor,...
    'tooltipstring', 'select waveform preprocessing to perform',...
    'callback', callback_fun,...
    'enable', 'on');

btmedge = btmedge - 2*inc;


lftedge = 0.75;
btmedge = 0.95;
btmedge = btmedge - first_drop;

%%~~~
%% Masking Method
%%~~~
%masking_options = {'none'; 'subM'; 'subF'; 'subFVec'; 'subMsubF'; 'divFsubM'; 'subFsubM'; thresh; 'KMF'};
%masking_options = {'Broadband'; 'Narrowband'; 'Broadband/Narrowband'; 'Narrowband/Broadband'; 'KMF Special Recipe'; 'Threshold'};
masking_options = {'None'; 'Broadband'; 'Narrowband'; 'Broadband/Narrowband'; 'Narrowband/Broadband'; 'KMF I'; 'KMF II'; 'Peak Threshold'};
%% maybe it is time to make this 'extensible'...

uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'Masking Method',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

btmedge = btmedge - popinc;

callback_fun = [mfilename, '(''cb_param_masking_meth_popup'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'popupmenu',...
    'handlevisibility', 'callback',...
    'tag', ['masking_meth_popup_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge popupwidth popupheight],...
    'string', masking_options,...
    'value', find(strcmpi(masking_options, figdata.param.masking_meth)),...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.popup_bgcolor,...
    'foregroundcolor', figdata.layout.popup_fgcolor,...
    'tooltipstring', 'select masking method to use',...
    'callback', callback_fun,...
    'enable', 'on');

btmedge = btmedge - 2*inc;


%%~~~
%% Masking Percentile
%%~~~
if strcmpi(figdata.param.masking_meth, 'Peak Threshold')
    de_string = 'Threshold (dB)';
    de_tip = 'enter dB threshold from peak for masking';
else
    de_string = 'Masking Percent (%)';
    de_tip = 'enter percentile to use for masking threshold';
end
uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'tag', ['masking_param_text_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', de_string,...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

btmedge = btmedge - inc;

callback_fun = [mfilename, '(''cb_param_masking_param_edit'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'edit',...
    'handlevisibility', 'callback',...
    'tag', ['masking_param_edit_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge editwidth editheight],...
    'string', figdata.param.masking_param,...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.editfield_bgcolor,...
    'foregroundcolor', figdata.layout.editfield_fgcolor,...
    'tooltipstring', de_tip,...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_param_masking_param_slider'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'slider',...
    'handlevisibility', 'callback',...
    'tag', ['masking_param_slider_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge+editwidth btmedge sliderwidth sliderheight],...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.slider_bgcolor,...
    'foregroundcolor', figdata.layout.slider_fgcolor,...
    'tooltipstring', de_tip,...
    'callback', callback_fun,...
    'enable', 'off',...
    'visible', 'on');


btmedge = btmedge - 2*inc;


%%~~~
%% Mask Adjustment
%%~~~
mask_adj_options = {...
    'None';...
    'Bias';...
    'Demean';...
    'Binarize';...
    'Flood-fill Binarize';...
    };

uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'tag', ['mask_adj_text_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'Mask Adjustment',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

btmedge = btmedge - inc;

callback_fun = [mfilename, '(''cb_param_mask_adj_popup'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'popupmenu',...
    'handlevisibility', 'callback',...
    'tag', ['mask_adj_popup_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge popupwidth popupheight],...
    'string', mask_adj_options,...
    'value', find(strcmpi(mask_adj_options, figdata.param.mask_adj)),...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.popup_bgcolor,...
    'foregroundcolor', figdata.layout.popup_fgcolor,...
    'tooltipstring', 'select method for adjusting the masked spectrogram values',...
    'callback', callback_fun,...
    'enable', 'on');


btmedge = btmedge - 2*inc;


%%~~~
%% Correlation Method
%%~~~
corr_options = {...
    'Time: Pairwise';...
    'Time: Column';...
    'Complex Env: Pairwise';...
    'Complex Env: Column';...
    'Spec: Pairwise';...
    'Spec: Column';...
    };
% corr_options = {...
%     'Time: Pairwise';...
%     'Time: Column';...
%     'Complex Env: Pairwise';...
%     'Complex Env: Column';...
%     'Spec: Pairwise';...
%     'Spec: Column';...
%     'Spec: Matrix Cross';...
%     'Spec DTW: Pairwise';...
%     'Spec DTW: Column';...
%   };

uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'Correlation Type',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

btmedge = btmedge - popinc;

callback_fun = [mfilename, '(''cb_param_corr_type_popup'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'popupmenu',...
    'handlevisibility', 'callback',...
    'tag', ['corr_type_popup_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge popupwidth popupheight],...
    'string', corr_options,...
    'value', find(strcmpi(corr_options, figdata.param.corr_type)),...
    'fontname', figdata.layout.fontname_default,...
    'fontunits',figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.popup_bgcolor,...
    'foregroundcolor', figdata.layout.popup_fgcolor,...
    'tooltipstring', 'select correlation type to perform',...
    'callback', callback_fun,...
    'enable', 'on');

btmedge = btmedge - 2*inc;


%%~~~
%% Standardization Method
%%~~~
std_options= {...
    'Matrix';...
    'Column';...
    'None';...
    };

uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'Standardization Method',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

btmedge = btmedge - popinc;

callback_fun = [mfilename, '(''cb_param_corr_std_popup'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'popupmenu',...
    'handlevisibility', 'callback',...
    'tag', ['corr_std_popup_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge popupwidth popupheight],...
    'string', std_options,...
    'value', find(strcmpi(std_options, figdata.param.corr_std)),...
    'fontname', figdata.layout.fontname_default,...
    'fontunits',figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.popup_bgcolor,...
    'foregroundcolor', figdata.layout.popup_fgcolor,...
    'tooltipstring', 'select spectrogram standardization to perform for correlation',...
    'callback', callback_fun,...
    'enable', 'on');

btmedge = btmedge - 2*inc;


%%~~~
%% Max Time Lag
uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'Max Time Lag (sec)',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

btmedge = btmedge - inc;

callback_fun = [mfilename, '(''cb_param_max_time_lag_edit'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'edit',...
    'handlevisibility', 'callback',...
    'tag', ['max_time_lag_edit_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge editwidth editheight],...
    'string', figdata.param.max_time_lag,...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.editfield_bgcolor,...
    'foregroundcolor', figdata.layout.editfield_fgcolor,...
    'tooltipstring', 'enter maximum time lag to use for cross-correlation',...
    'callback', callback_fun,...
    'enable', 'off');

callback_fun = [mfilename, '(''cb_param_max_time_lag_slider'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'slider',...
    'handlevisibility', 'callback',...
    'tag', ['max_time_lag_slider_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge+editwidth btmedge sliderwidth sliderheight],...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.slider_bgcolor,...
    'foregroundcolor', figdata.layout.slider_fgcolor,...
    'tooltipstring', 'enter maximum time lag to use for cross-correlation',...
    'callback', callback_fun,...
    'enable', 'off');

btmedge = btmedge - 2*inc;


%%~~~
%% Max Frequency Lag
%%~~~
uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge btmedge txtwidth txtheight],...
    'string', 'Max Freq Lag (Hz)',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

btmedge = btmedge - inc;

callback_fun = [mfilename, '(''cb_param_max_freq_lag_edit'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'edit',...
    'handlevisibility', 'callback',...
    'tag', ['max_freq_lag_edit_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge editwidth editheight],...
    'string', figdata.param.max_freq_lag,...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.editfield_bgcolor,...
    'foregroundcolor', figdata.layout.editfield_fgcolor,...
    'tooltipstring', 'enter maximum frequency lag to use for cross-correlation',...
    'callback', callback_fun,...
    'callback', callback_fun,...
    'enable', 'off');

callback_fun = [mfilename, '(''cb_param_max_freq_lag_slider'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'slider',...
    'handlevisibility', 'callback',...
    'tag', ['max_freq_lag_slider_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge+editwidth btmedge sliderwidth sliderheight],...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'left',...
    'backgroundcolor', figdata.layout.slider_bgcolor,...
    'foregroundcolor', figdata.layout.slider_fgcolor,...
    'tooltipstring','enter maximum frequency lag to use for cross-correlation',...
    'callback', callback_fun,...
    'enable', 'off');



btmedge = 0.95;
lftedge = 0.85;

%%~~~
%% Save Settings
%%~~~
callback_fun = [mfilename, '(''cb_param_save_settings_button'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'pushbutton',...
    'handlevisibility', 'callback',...
    'tag', ['save_settings_button_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge 1.1*buttonwidth 1.1*buttonheight],...
    'string', 'save',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.pushbutton_bgcolor,...
    'foregroundcolor', figdata.layout.pushbutton_fgcolor,...
    'callback', callback_fun,...
    'enable', 'off');

lftedge = lftedge + 1.5*buttonwidth;


%%~~~
%% Load Settings
%%~~~
callback_fun = [mfilename, '(''cb_param_load_settings_button'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'pushbutton',...
    'handlevisibility', 'callback',...
    'tag', ['load_settings_button_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge 1.1*buttonwidth 1.1*buttonheight],...
    'string', 'load',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.pushbutton_bgcolor,...
    'foregroundcolor', figdata.layout.pushbutton_fgcolor,...
    'callback', callback_fun,...
    'enable', 'off');


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: setup_run_quit_gui
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function setup_run_quit_gui(gui_interface_fig_handle)


figdata = get(gui_interface_fig_handle, 'userdata');


btmedge = .3;
lftedge = .75;
lftedge = .7725;


%%~~~
%% Run
%%~~~
callback_fun = [mfilename, '(''cb_correlation_run'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'pushbutton',...
    'handlevisibility', 'callback',...
    'tag', ['run_button_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge .075 .045],...
    'string', 'Run',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.pushbutton_bgcolor,...
    'foregroundcolor', figdata.layout.pushbutton_fgcolor,...
    'callback', callback_fun,...
    'enable', 'off');

%%~~~
%% Quit
%%~~~
callback_fun = [mfilename, '(''cb_program_quit'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'pushbutton',...
    'handlevisibility', 'callback',...
    'tag', ['quit_button_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge+0.1 btmedge .075 .045],...
    'string', 'Quit',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.pushbutton_bgcolor,...
    'foregroundcolor', figdata.layout.pushbutton_fgcolor,...
    'callback', callback_fun);

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: setup_status_window_gui
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function setup_status_window_gui(gui_interface_fig_handle)


figdata = get(gui_interface_fig_handle, 'userdata');


btmedge = .025;
lftedge = 0.025;


%%~~~
%% Create a Status Display Area
%%~~~

% uicontrol(gui_interface_fig_handle,...
%   'style', 'text',...
%   'tag', ['status_window_text_uicontrol', mfilename],...
%   'units', 'normalized',...
%   'position', [lftedge btmedge+.09 .5 .05],...
%   'string', 'Status',...
%   'horizontalalignment', 'left',...
%   'fontname', figdata.layout.fontname_default,...
%   'fontunits', figdata.layout.fontunits_default,...
%   'fontsize', figdata.layout.fontsize_default,...
%     'fontweight', figdata.layout.fontweight_default,...
%   'backgroundcolor', figdata.layout.textfield_bgcolor,...
%   'foregroundcolor', figdata.layout.textfield_fgcolor);

%% just a silly divider!
uicontrol(gui_interface_fig_handle,...
    'style', 'edit',...
    'handlevisibility', 'callback',...
    'tag', ['status_window_text_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge btmedge+.245 .95 .01],...
    'string', '',...
    'horizontalalignment', 'left',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.figure_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);


%%~~~
%% Alternate Status Area
%%~~~

%% make a context menu
alternate_view_uicontextmenu = uicontextmenu(...
    'parent', gui_interface_fig_handle);

callback_fun = [mfilename, '(''cb_analysis_pco'');'];
uimenu(alternate_view_uicontextmenu,...
    'tag', ['alternate_view_pco_uimenu', mfilename],...
    'label', 'PCO Analysis',...
    'callback', callback_fun,...
    'enable', 'off');

callback_fun = [mfilename, '(''cb_analysis_template_rank'');'];
uimenu(alternate_view_uicontextmenu,...
    'tag', ['alternate_view_template_rank_uimenu', mfilename],...
    'label', 'Template Rank Analysis',...
    'callback', callback_fun,...
    'enable', 'off');

callback_fun = [mfilename, '(''cb_alternate_view_close'');'];
uimenu(alternate_view_uicontextmenu,...
    'label', 'Close Corr Data View',...
    'separator', 'on',...
    'callback', callback_fun,...
    'enable', 'on');

callback_fun = [mfilename, '(''cb_alternate_view_open'');'];
uimenu(alternate_view_uicontextmenu,...
    'label', 'Open Corr Data View',...
    'callback', callback_fun,...
    'enable', 'on');


%% create alternate view uicontrol
uicontrol(gui_interface_fig_handle,...
    'style', 'text',...
    'tag', ['alternate_view_text_label', mfilename],...
    'handlevisibility', 'callback',...
    'units', 'normalized',...
    'position', [lftedge+.565 btmedge+.21 .355 .02],...
    'string', 'Correlation Data',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'horizontalalignment', 'center',...
    'backgroundcolor', figdata.layout.textfield_bgcolor,...
    'foregroundcolor', figdata.layout.textfield_fgcolor);

alternate_view_uicontrol = uicontrol('parent', gui_interface_fig_handle,...
    'style', 'listbox',...
    'tag', ['alternate_view_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [lftedge+.565 btmedge .355 .2],...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.editfield_bgcolor,...
    'foregroundcolor', figdata.layout.editfield_fgcolor,...
    'handlevisibility', 'callback',...
    'uicontextmenu', alternate_view_uicontextmenu);

%% set up empty spec data structure
alternate_view_data = [];
set(alternate_view_uicontrol, 'userdata', alternate_view_data);



%%~~~
%% Status Window
%%~~~

%% make a context menu for the status window
status_window_edit_uicontextmenu = uicontextmenu(...
    'parent', gui_interface_fig_handle);

callback_fun = [mfilename, '(''cb_status_window_clear'');'];
uimenu(status_window_edit_uicontextmenu,...
    'label', 'Clear Status Window',...
    'callback', callback_fun);

callback_fun = [mfilename, '(''cb_status_window_save'');'];
uimenu(status_window_edit_uicontextmenu,...
    'label', 'Save Status Report',...
    'callback', callback_fun);

callback_fun = [mfilename, '(''cb_alternate_view_close'');'];
uimenu(status_window_edit_uicontextmenu,...
    'label', 'Close Alternate View',...
    'separator', 'on',...
    'callback', callback_fun);

callback_fun = [mfilename, '(''cb_alternate_view_open'');'];
uimenu(status_window_edit_uicontextmenu,...
    'label', 'Open Alternate View',...
    'callback', callback_fun);



%% make the status window (listbox)
callback_fun = [mfilename, '(''cb_status_window'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'listbox',...
    'handlevisibility', 'callback',...
    'tag', ['status_window_edit_uicontrol', mfilename],...
    'min', 1,...
    'max', 3,...
    'units', 'normalized',...
    'position', [lftedge btmedge .95 .225],...
    'string', {'Status:'; 'Ready...'},...
    'horizontalalignment', 'left',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.displayfield_bgcolor,...
    'foregroundcolor', figdata.layout.displayfield_fgcolor,...
    'uicontextmenu', status_window_edit_uicontextmenu,...
    'enable', 'on',...
    'callback', callback_fun);

%'string', {'Status:'; 'Margaritas a los chanchos...'},...

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: key_press_intercept
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function key_press_intercept(gui_interface_fig_tag)

%% wait on this...
% gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
% figdata = get(gui_interface_fig_handle, 'userdata');
%
%
% key = get(gui_interface_fig_handle, 'currentcharacter')
%
% if strcmpi(key, 'd')
%   cb_sound_list_delete(gui_interface_fig_tag);
% end


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sound_list_popup
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sound_list_popup(gui_interface_fig_tag)

display_currentsnd_spectrogram(gui_interface_fig_tag);

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sndspc_view_xaxis
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sndspc_view_xaxis(gui_interface_fig_tag)

gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

xmin = num2str(figdata.layout.axes_xrange(1));
xmax = num2str(figdata.layout.axes_xrange(2));

title = 'Enter X-Axis Range';
query_str = {'X-Axis Start (sec):';'X-Axis End (sec):'};
default_str = {xmin; xmax};
layout = figdata.layout;

%% query user for new range values
values = edit_input_dialog(title, query_str, default_str, layout);

if isempty(values)
    %% cancel requested
    return;
end

xmin = str2num(values{1});
xmax = str2num(values{2});

if xmin < xmax
    %% update the stored values, and replot
    figdata.layout.axes_xrange = [xmin, xmax];
    set(gui_interface_fig_handle, 'userdata', figdata);
    display_currentsnd_spectrogram(gui_interface_fig_tag);
else
    cb_sndspc_view_xaxis(gui_interface_fig_tag);
end


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sndspc_view_fullxaxis
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sndspc_view_fullxaxis(gui_interface_fig_tag)

gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');


if isempty(figdata.sound_list)
    %% nothing to do
    return;
    
else
    %% set default spec viewer display options based on sound list
    %% get max time range
    sampRate = cell2mat({figdata.sound_list.samplerate});
    max_dur = max( cell2mat({figdata.sound_list.num_samples}) ./ sampRate );
    
    %% update default spec display options based on new sound list
    figdata.layout.axes_xrange = [0, max_dur];
    set(gui_interface_fig_handle, 'userdata', figdata);
    
    %% replot
    display_currentsnd_spectrogram(gui_interface_fig_tag);
    
end


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sndspc_view_yaxis
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sndspc_view_yaxis(gui_interface_fig_tag)

gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

ymin = num2str(figdata.layout.axes_yrange(1));
ymax = num2str(figdata.layout.axes_yrange(2));

title = 'Enter Y-Axis Range';
query_str = {'Y-Axis Start (Hz):';'Y-Axis End (Hz):'};
default_str = {ymin; ymax};
layout = figdata.layout;

%% query user for new range values
values = edit_input_dialog(title, query_str, default_str, layout);

if isempty(values)
    %% cancel requested
    return;
end

ymin = str2num(values{1});
ymax = str2num(values{2});

if ymin < ymax
    %% update the stored values, and replot
    figdata.layout.axes_yrange = [ymin, ymax];
    set(gui_interface_fig_handle, 'userdata', figdata);
    display_currentsnd_spectrogram(gui_interface_fig_tag);
else
    cb_sndspc_view_yaxis(gui_interface_fig_tag);
end


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sndspc_view_fullyaxis
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sndspc_view_fullyaxis(gui_interface_fig_tag)

gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');


if isempty(figdata.sound_list)
    %% nothing to do
    return;
    
else
    %% set default spec viewer display options based on sound list
    %% get max frequency range
    sampRate = cell2mat({figdata.sound_list.samplerate});
    nyquist = max(sampRate) / 2;
    
    %% update default spec display options based on new sound list
    figdata.layout.axes_yrange = [0, nyquist];
    
    %% NO!  Don't do this; leave display and analysis settings independent
    %   %% update bandlimit range as well
    %   figdata.param.low_freq = 0;
    %   figdata.param.high_freq = nyquist;
    %
    %   %% set bandlimit display
    %   low_freq_edit_handle = findobj(gui_interface_fig_handle,'tag', ['low_freq_edit_uicontrol', mfilename]);
    %   set(low_freq_edit_handle, 'string', figdata.param.low_freq);
    %
    %   high_freq_edit_handle = findobj(gui_interface_fig_handle,'tag', ['high_freq_edit_uicontrol', mfilename]);
    %   set(high_freq_edit_handle, 'string', figdata.param.high_freq);
    
    %% save new data
    set(gui_interface_fig_handle, 'userdata', figdata);
    
    %% replot
    display_currentsnd_spectrogram(gui_interface_fig_tag);
    
end


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sndspc_view_gain_range
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sndspc_view_gain_range(gui_interface_fig_tag)

gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

cmax = num2str(figdata.layout.axes_crange(1));
crange = num2str(figdata.layout.axes_crange(2));

title = 'Enter Display Range';
query_str =  {'Display Ceiling (dB):';'Display Range (dB):'};
default_str = {cmax; crange};
layout = figdata.layout;

%% query user for new range values
values = edit_input_dialog(title, query_str, default_str, layout);

if isempty(values)
    %% cancel requested
    return;
end

cmax = str2num(values{1});
crange = str2num(values{2});

if crange > 0
    %% update the stored values, and replot
    figdata.layout.axes_crange = [cmax, crange];
    set(gui_interface_fig_handle, 'userdata', figdata);
    
    %% get auto range check box handle, and uncheck
    display_scale_checkbox_handle = findobj(gui_interface_fig_handle, 'tag', ['display_scale_checkbox_uicontrol', mfilename]);
    set(display_scale_checkbox_handle, 'value', 0);
    set(display_scale_checkbox_handle, 'userdata', 'gain/range');
    
    display_currentsnd_spectrogram(gui_interface_fig_tag);
    
end


end





%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sndspc_view_peak_range
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sndspc_view_peak_range(gui_interface_fig_tag)

gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

dBrange = num2str(figdata.layout.axes_dBrange);


title = 'Enter Display Range';
query_str =  {'Display Range From Peak Value (dB):'};
default_str = {dBrange};
layout = figdata.layout;

%% query user for new range value
values = edit_input_dialog(title, query_str, default_str, layout);


if isempty(values)
    %% cancel requested
    return;
end

dBrange = str2num(values{1});

if dBrange > 0
    %% update the stored values, and replot
    figdata.layout.axes_dBrange = dBrange;
    set(gui_interface_fig_handle, 'userdata', figdata);
    
    %% get auto range check box handle, and uncheck
    display_scale_checkbox_handle = findobj(gui_interface_fig_handle, 'tag', ['display_scale_checkbox_uicontrol', mfilename]);
    set(display_scale_checkbox_handle, 'value', 0);
    set(display_scale_checkbox_handle, 'userdata', 'peakrange');
    
    display_currentsnd_spectrogram(gui_interface_fig_tag);
    
end


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sndspc_view_gain_range_display
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sndspc_view_gain_range_display(gui_interface_fig_tag)

gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);

%% get auto range check box handle
display_scale_checkbox_handle = findobj(gui_interface_fig_handle, 'tag', ['display_scale_checkbox_uicontrol', mfilename]);

%% uncheck auto range box
set(display_scale_checkbox_handle, 'value', 0);
set(display_scale_checkbox_handle, 'userdata', 'gain/range');

display_currentsnd_spectrogram(gui_interface_fig_tag);

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sndspc_view_peak_range_display
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sndspc_view_peak_range_display(gui_interface_fig_tag)

gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);

%% get auto range check box handle
display_scale_checkbox_handle = findobj(gui_interface_fig_handle, 'tag', ['display_scale_checkbox_uicontrol', mfilename]);

%% uncheck auto range box
set(display_scale_checkbox_handle, 'value', 0);
set(display_scale_checkbox_handle, 'userdata', 'peakrange');

display_currentsnd_spectrogram(gui_interface_fig_tag);

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sndspc_view_full_range_display
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sndspc_view_full_range_display(gui_interface_fig_tag)

gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);

%% get auto range check box handle
display_scale_checkbox_handle = findobj(gui_interface_fig_handle, 'tag', ['display_scale_checkbox_uicontrol', mfilename]);

%% check auto range box
set(display_scale_checkbox_handle, 'value', 1);

display_currentsnd_spectrogram(gui_interface_fig_tag);

end







%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sndspc_view_fullrange_checkbox
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sndspc_view_fullrange_checkbox(gui_interface_fig_tag)

display_currentsnd_spectrogram(gui_interface_fig_tag);

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sound_list_play
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sound_list_play(gui_interface_fig_tag)

gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

if isempty(figdata.sound_list)
    %% no sounds to play, drop out
    return;
end

sound_list_popup_handle = findobj(gui_interface_fig_handle, 'tag', ['sound_list_popup_uicontrol', mfilename]);
selected_sound_inx = get(sound_list_popup_handle, 'value');


snd_data = figdata.sound_list(selected_sound_inx(1)).data;
fs = figdata.sound_list(selected_sound_inx(1)).samplerate;

soundsc(snd_data, fs);


%% maybe in next version, set up to change play rate
%% for example, get rate = figdata.layout.playback_rate
%% soundsc(snd_data, rate*fs)


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sndspc_view_colormap
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sndspc_view_colormap(gui_interface_fig_tag)

gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get desired colormap from menu callback object
c_map = get(gcbo, 'label');
c_map = eval(c_map);
c_map = (flipud(c_map));

%% update the stored values, and replot
figdata.layout.colormap = c_map;
set(gui_interface_fig_handle, 'userdata', figdata);
display_currentsnd_spectrogram(gui_interface_fig_tag);


end







%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sndspc_view_colormap_invert
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sndspc_view_colormap_invert(gui_interface_fig_tag)

gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% toggle the menu item check
switch (lower(get(gcbo, 'checked')))
    
    case('on')
        
        %% turn invert off and store
        set(gcbo, 'checked', 'off');
        figdata.layout.colormap_invert = 'off';
        
    case('off')
        
        % turn invert on and store
        set(gcbo, 'checked', 'on')
        figdata.layout.colormap_invert = 'on';
        
end

set(gui_interface_fig_handle, 'userdata', figdata);

display_currentsnd_spectrogram(gui_interface_fig_tag);


end







%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: display_currentsnd_spectrogram
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function display_currentsnd_spectrogram(gui_interface_fig_tag)

%% get gui figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');


%% get display axis handle
sound_spec_view_axes_handle = findobj(gui_interface_fig_handle, 'tag', ['sound_spec_view_axes', mfilename]);

%% clear the display axis by deleting the child image
delete(findobj(sound_spec_view_axes_handle, 'tag', ['current_spc_image', mfilename]));

if isempty(figdata.sound_list)
    %% no sounds to display, drop out
    return;
end


%% get uicontext menu
sound_spec_view_uicontextmenu = get(sound_spec_view_axes_handle,'uicontextmenu');

%% get expanded spec view axes
large_sound_spec_view_axes_handle = findobj(gui_interface_fig_handle, 'tag', ['large_sound_spec_view_axes', mfilename]);

%% get axis position
position = get(sound_spec_view_axes_handle, 'position');

%% get yaxis-label handle
sound_spec_view_axes_ylabel = findobj(gui_interface_fig_handle, 'tag', ['sound_spec_view_axes_ylabel', mfilename]);

%% get sound list popup handle and index to highlighted sound
sound_list_popup_handle = findobj(gui_interface_fig_handle, 'tag', ['sound_list_popup_uicontrol', mfilename]);
selected_sound_inx = get(sound_list_popup_handle, 'value');


%% get processing parameters for spectrogram display
fftsz = figdata.param.fft_len;
datasz = round(fftsz*figdata.param.data_len/100);
taper = get_taper_func(figdata.param.taper_func);
ovrlp = round(datasz*figdata.param.data_overlap/100);

%% bandlimiting
loCorner = figdata.param.low_freq;
hiCorner = figdata.param.high_freq;

fs = figdata.sound_list(selected_sound_inx(1)).samplerate;

%% double check flo and fhi in case of uncommitted sounds and different sample rates
% loCorner = min(loCorner, fs/2);
hiCorner = min(hiCorner, fs/2);

if hiCorner == 0
    hiCorner = fs/2;
    figdata.param.high_freq = fs/2;
%     set(high_freq_edit_uicontrol_handle,'string', figdata.param.high_freq)
end

%% signal preprocessing
sig_preproc = figdata.param.sig_preproc;

%% masking
dataform = figdata.param.data_form;
masking = figdata.param.masking_meth;
param = figdata.param.masking_param;
adjust = figdata.param.mask_adj;

%% and corr type and matrix norm type for xcorr
corr_type = figdata.param.corr_type;
normtype = figdata.param.corr_std;


%% get the sound data
snd_data = figdata.sound_list(selected_sound_inx(1)).data;

% % 8/31/12 get correct channel
% snd_data = snd_data(:,figdata.sound_list(selected_sound_inx(1)).channel);

%% de-mean the sound data
%% this is done in 'run' function
snd_data = demeansnd(snd_data);


%% apply signal preprocessing as requested
%% this is done in 'run' function
snd_data = signal_preprocess(snd_data, sig_preproc);


%% get the range of signal to display
startpt = 1;
endpt = length(snd_data);
%% XXXX NOTE FOR LATER WORK: TRANSFORM ONLY THE DATA DISPLAYED AND
%% PUT INTO THE AXES IN THE APPROPRIATE SPOT
%% FOR NOW, TRANSFORM THE ENTIRE CLIP...
% startpt = max(1, floor(figdata.layout.axes_xrange(1) * fs));
% endpt = min(length(snd_data), ceil(figdata.layout.axes_xrange(2) * fs + fftsz));



if sum(strcmpi(corr_type, {'TIME: PAIRWISE'; 'TIME: COLUMN'}))
    %% display data as a time waveform
    
    %% expand the axes
    position(3) = 0.315;
    set(sound_spec_view_axes_handle, 'position', position);
    
    %% change yaxis-label to amplitude
    set(sound_spec_view_axes_ylabel, 'string', 'Amplitude');
    
    %% hide the color bar
    set(0, 'showhiddenhandles', 'on'); %% to make sure it sees the parent fig handle
    colorbar_h = colorbar('peer', sound_spec_view_axes_handle);
    set(0, 'showhiddenhandles', 'off');
    set(colorbar_h,...
        'visible', 'off');
    
    %% show data format type as amplitude
    set(get(sound_spec_view_axes_handle, 'title'), 'string', 'Amplitude',...
        'fontname', figdata.layout.fontname_default,...
        'fontunits', figdata.layout.fontunits_default,...
        'fontsize', figdata.layout.fontsize_default*.75,...
        'fontweight', figdata.layout.fontweight_default);
    
    
    %% bandlimit the waveform
    %% this is done in 'run' function
    [snd_data] = bandlimsnd(snd_data, fs, loCorner, hiCorner);
    
    %% get the time vector
    tvec = [0:length(snd_data)-1] / fs;
    
    %% plot the time waveform
    line('tag', ['current_spc_image', mfilename],...
        'uicontextmenu', sound_spec_view_uicontextmenu,...
        'xdata', tvec,...
        'ydata', snd_data,...
        'color', [0 0 1],...
        'parent', sound_spec_view_axes_handle);
    
    %% set the y-axis range automatically
    yrange = minmax(snd_data');
    inc = diff(yrange)/20;
    yrange(1) = yrange(1) - inc;
    yrange(2) = yrange(2) + inc;
    
    %% set axis ranges
    set(sound_spec_view_axes_handle,...
        'xlim', figdata.layout.axes_xrange,...
        'ylim', yrange);
    
    
else
    %% display data as a spectrogram
    
    %% contract the axes
    position(3) = 0.275;
    set(sound_spec_view_axes_handle, 'position', position);
    
    %% change y-label to frequency
    set(sound_spec_view_axes_ylabel, 'string', 'Freq (Hz)');
    
    %% show data format type
    set(get(sound_spec_view_axes_handle, 'title'), 'string', dataform,...
        'fontname', figdata.layout.fontname_default,...
        'fontunits', figdata.layout.fontunits_default,...
        'fontsize', figdata.layout.fontsize_default*.75,...
        'fontweight', figdata.layout.fontweight_default);
    
    
    %% generate the spectrogram image
    [snd_spc, fvec, tvec] = specgram(snd_data(startpt:endpt), fftsz, fs, feval(taper, datasz), ovrlp);
    
    %% put spec values in requested data format
    switch (dataform)
        case ('Amplitude')
            snd_spc = abs(snd_spc);
        case ('Power')
            snd_spc = snd_spc .* conj(snd_spc);
        case ('dB')
            snd_spc = 10*log10(snd_spc .* conj(snd_spc));
        otherwise %% set up some default
            snd_spc = 10*log10(snd_spc .* conj(snd_spc));
    end
    
    %% bandlimit the spectrogram as requested
    %% this is done in 'run' function
    [snd_spc, fvec] = bandlimspc(snd_spc, fvec, loCorner, hiCorner);
    
    %% mask the spectrogram as requested
    %% this is done in 'run' function
    snd_spc = denoisespc_v2(snd_spc, masking, param, dataform, adjust);
    
    %% standardize the spectrogram as requested
    %% this is done in 'xcorr' function
    snd_spc = standardize_mat(snd_spc, normtype);
    
    
    %% check how to display the image-- fixed range or auto-scale
    display_scale_checkbox_handle = findobj(gui_interface_fig_handle, 'tag', ['display_scale_checkbox_uicontrol', mfilename]);
    
    if get(display_scale_checkbox_handle, 'value')
        %% display image over full data range
        
         snd_spc( snd_spc == -inf )= NaN;
        cmin = nanmin(snd_spc(:));
        cmax = nanmax(snd_spc(:));
        %% check range just in case
        if cmin == cmax
            cmax = cmin + 1;
        end
        
    else
        %% use user defined range
        
        display_mode = get(display_scale_checkbox_handle, 'userdata');
        
        if strcmpi(display_mode, 'gain/range')
            
            switch (dataform)
                case ('Amplitude')
                    cmax = 10.^(figdata.layout.axes_crange(1)./20);
                    dBrange = 10.^(figdata.layout.axes_crange(2)./20);
                    cmin = cmax / dBrange;
                case ('Power')
                    cmax = 10.^(figdata.layout.axes_crange(1)./10);
                    dBrange = 10.^(figdata.layout.axes_crange(2)./10);
                    cmin = cmax / dBrange;
                case ('dB')
                    cmax = figdata.layout.axes_crange(1);
                    dBrange = figdata.layout.axes_crange(2);
                    cmin = cmax - dBrange;
                otherwise %% set up some default
                    cmax = figdata.layout.axes_crange(1);
                    dBrange = figdata.layout.axes_crange(2);
                    cmin = cmax - dBrange;
            end
            
        else %% strcmpi(display_mode, 'peakrange')
            
            cmax = max(snd_spc(:));
            
            switch (dataform)
                case ('Amplitude')
                    dBrange = 10.^(figdata.layout.axes_dBrange./20);
                    cmin = cmax/dBrange;
                case ('Power')
                    dBrange = 10.^(figdata.layout.axes_dBrange./10);
                    cmin = cmax/dBrange;
                case ('dB')
                    dBrange = figdata.layout.axes_dBrange;
                    cmin = cmax - dBrange;
                otherwise %% set up some default
                    dBrange = figdata.layout.axes_dBrange;
                    cmin = cmax - dBrange;
            end
            
        end
        
    end
    
    
    set(sound_spec_view_axes_handle, 'clim', [cmin, cmax]);
    image('tag', ['current_spc_image', mfilename],...
        'uicontextmenu', sound_spec_view_uicontextmenu,...
        'xdata', tvec,...
        'ydata', fvec,...
        'cdata', snd_spc,...
        'cdatamapping', 'scaled',...
        'parent', sound_spec_view_axes_handle);
    
    %% I don't understand why, but using image or imagesc seems to reset the
    %% axis tag to empty
    %% force it back to correct tag
    set(sound_spec_view_axes_handle, 'tag', ['sound_spec_view_axes', mfilename]);
    
    %% flip y-axis
    axis xy;
    
    %% set axis ranges
    %   set(sound_spec_view_axes_handle,...
    %     'xlim', figdata.layout.axes_xrange,...
    %     'ylim', figdata.layout.axes_yrange);
    
    % Modified by SCK - this kind of formatting could go in
    % cb_sndspc_view_enlarge below..
    
    set(sound_spec_view_axes_handle,...
        'clim', [cmin, cmax],...
        'xlim', figdata.layout.axes_xrange,...
        'ylim', [loCorner hiCorner]);
    
    %% set image colormap
    invert_colormap_handle = findobj(gui_interface_fig_handle, 'tag', ['invert_colormap_uimenu', mfilename]);
    
    switch (lower(get(invert_colormap_handle, 'checked')))
        case('on')
            set(gui_interface_fig_handle, 'colormap', flipud(figdata.layout.colormap));
        case ('off')
            set(gui_interface_fig_handle, 'colormap', figdata.layout.colormap);
    end
    
    % %% set up color bar with current axis
    set(0, 'showhiddenhandles', 'on'); %% to make sure it sees the parent fig handle
% %     colorbar_h = colorbar(sound_spec_view_axes_handle);
    colorbar_h = findobj(gui_interface_fig_handle,'Type','Colorbar');
    set(0, 'showhiddenhandles', 'off');
    %% this doesn't work, the colorbar really sucks
    % colorbar_h = findobj('tag', ['sound_spec_view_colorbar', mfilename]);
    
    position(1) = position(1) + position(3) + 0.005;
    position(3) = 0.0245;
    
    set(colorbar_h,...
        'position', position,...
        'ylim', [cmin, cmax],...
        'ylimmode', 'manual',...
        'yaxislocation', 'right',...
        'tickdir', 'in',...
        'fontname', figdata.layout.fontname_default,...
        'fontsize', figdata.layout.fontsize_default*.75,...
        'fontweight', figdata.layout.fontweight_default);
% %         'fontunits', figdata.layout.fontunits_default,...
% %         'yminortick', 'on',...
    
end


end







%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sndspc_view_enlarge
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sndspc_view_enlarge(gui_interface_fig_tag, spc_fig_tag)



%%%%XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
%%%%XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
%%%% FIGURE THIS OUT LATER!!!!



%% get gui figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');



% %% get display axis handle
% sound_spec_view_axes_handle = findobj(gui_interface_fig_handle, 'tag', ['sound_spec_view_axes', mfilename]);
%
%
% %% get current axis position
% cur_position = get(sound_spec_view_axes_handle, 'position');
%
% enlarg_position = [cur_position(1:2), 2*cur_position(3:4)];
%
% set(sound_spec_view_axes_handle, 'position', enlarg_position, 'layer', 'top');
%
% display_currentsnd_spectrogram(gui_interface_fig_tag);





%% check for open figure
set(0, 'showhiddenhandles', 'on');
if findobj(0, 'tag', spc_fig_tag);
    %% already open, reset handles and drop out
    set(0, 'showhiddenhandles', 'off');
    return;
end
set(0, 'showhiddenhandles', 'off');



%% get gui position
position = get(gui_interface_fig_handle, 'position');
%% use this to set figure position
position = [position(1:3)-[0, 333, 0], 300];

%% create the gui interface figure
dialog_title = 'Expanded Spectrogram View';

spc_fig_handle = figure('tag', spc_fig_tag,...
    'name', dialog_title,...
    'numbertitle', 'off',...
    'menubar', 'none',...
    'color', figdata.layout.figure_bgcolor,...
    'units', figdata.layout.fontunits_default,...
    'position', position,...
    'resize', 'on',...
    'handlevisibility', 'callback');





% %% make a frame around it with labels
% sound_spec_view_axes_frame = axes('parent', gui_interface_fig_handle,...
%   'position', [lftedge btmedge-.5125 .411 .25],...
%   'uicontextmenu', sound_spec_view_uicontextmenu,...
%   'visible', 'off',...
%   'handlevisibility', 'callback');
%
% rectangle('parent', sound_spec_view_axes_frame,...
%   'position', [0, 0, 1, 1]);
%
% text('parent', sound_spec_view_axes_frame,...
%   'handlevisibility', 'callback',...
%   'units', 'normalized',...
%   'position', [.45, .05, 0],...
%   'fontname', figdata.layout.fontname_default,...
%   'fontunits', figdata.layout.fontunits_default,...
%   'fontsize', figdata.layout.fontsize_default*.75,...
%   'fontweight', figdata.layout.fontweight_default,...
%   'backgroundcolor', figdata.layout.textfield_bgcolor,...
%   'color', figdata.layout.textfield_fgcolor,...
%   'string', 'Time (sec)');
%
% text('parent', sound_spec_view_axes_frame,...
%   'handlevisibility', 'callback',...
%   'units', 'normalized',...
%   'position', [.04, .45, 0],...
%   'rotation', 90,...
%   'fontname', figdata.layout.fontname_default,...
%   'fontunits', figdata.layout.fontunits_default,...
%   'fontsize', figdata.layout.fontsize_default*.75,...
%   'fontweight', figdata.layout.fontweight_default,...
%   'backgroundcolor', figdata.layout.textfield_bgcolor,...
%   'color', figdata.layout.textfield_fgcolor,...
%   'string', 'Freq (Hz)');


%% create the spec display axes
large_sound_spec_view_axes = axes('parent', spc_fig_handle,...
    'tag', ['large_sound_spec_view_axes', mfilename],...
    'units', 'normalized',...
    'layer', 'top',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default*.75,...
    'fontweight', figdata.layout.fontweight_default,...
    'color', figdata.layout.textfield_bgcolor,...
    'xcolor', figdata.layout.textfield_fgcolor,...
    'ycolor', figdata.layout.textfield_fgcolor,...
    'box', 'on',...
    'xlim', figdata.layout.axes_xrange,...
    'ylim', figdata.layout.axes_yrange,...
    'handlevisibility', 'callback');

%% and a colorbar
set(0, 'showhiddenhandles', 'on'); %% to make sure it sees the parent fig handle
colorbar_h = colorbar('peer', large_sound_spec_view_axes);
set(0, 'showhiddenhandles', 'off');
set(colorbar_h,...
    'tag', ['large_sound_spec_view_colorbar', mfilename],...
    'handlevisibility', 'callback',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default*.75,...
    'fontweight', figdata.layout.fontweight_default,...
    'color', figdata.layout.textfield_bgcolor,...
    'xcolor', figdata.layout.textfield_fgcolor,...
    'ycolor', figdata.layout.textfield_fgcolor);

% %% blank out colormap for now
% set(gui_interface_fig_handle, 'colormap', white(256));
%% set to user selected color map
set(spc_fig_handle, 'colormap', figdata.layout.colormap);



end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sndspc_view_reduce
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sndspc_view_reduce(gui_interface_fig_tag)

% %% get gui figure handle
% gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
%
%
% %% get display axis handle
% sound_spec_view_axes_handle = findobj(gui_interface_fig_handle, 'tag', ['sound_spec_view_axes', mfilename]);
%
%
% %% get current axis position
% cur_position = get(sound_spec_view_axes_handle, 'position');
%
% enlarg_position = [cur_position(1:2), 0.5*cur_position(3:4)];
%
% set(sound_spec_view_axes_handle, 'position', enlarg_position, 'layer', 'top');
%
% display_currentsnd_spectrogram(gui_interface_fig_tag);


end







%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_alternate_view_close
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_alternate_view_close(gui_interface_fig_tag)

%% get gui figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);

%% get status display handle
status_window_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['status_window_edit_uicontrol', mfilename]);

%% stretch it to hide the avg spc axes
set(status_window_edit_handle, 'position', [0.025 0.025 .95 .225]);

%% delete the close button
delete(findobj(gui_interface_fig_handle, 'tag', ['cb_alternate_view_close_button_uicontrol', mfilename]));

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_alternate_view_open
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_alternate_view_open(gui_interface_fig_tag)

%% get gui figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get status display handle
status_window_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['status_window_edit_uicontrol', mfilename]);

%% shrink it to reveal the aalternate view
set(status_window_edit_handle, 'position', [0.025 0.025 .55 .225]);

%% make a close button
callback_fun = [mfilename, '(''cb_alternate_view_close'');'];
uicontrol(gui_interface_fig_handle,...
    'style', 'pushbutton',...
    'handlevisibility', 'callback',...
    'tag', ['cb_alternate_view_close_button_uicontrol', mfilename],...
    'units', 'normalized',...
    'position', [0.96 0.015 .025 .025],...
    'string', 'X',...
    'fontname', figdata.layout.fontname_default,...
    'fontunits', figdata.layout.fontunits_default,...
    'fontsize', figdata.layout.fontsize_default*.75,...
    'fontweight', figdata.layout.fontweight_default,...
    'backgroundcolor', figdata.layout.pushbutton_bgcolor,...
    'foregroundcolor', figdata.layout.pushbutton_fgcolor,...
    'tooltipstring', 'push to close the alternate view',...
    'callback', callback_fun);


end







%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_status_window
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_status_window(gui_interface_fig_tag)

%% nothing to do here

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_status_window_clear
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function cb_status_window_clear(gui_interface_fig_tag)

%% get gui figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);

%% get status window handle
status_window_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['status_window_edit_uicontrol', mfilename]);

%% clear status window
%set(status_window_edit_handle, 'string', {'Status:'; 'Margaritas a los chanchos...'}, 'value', 1);
set(status_window_edit_handle, 'string', {'Status:'; 'Ready...'}, 'value', 1);

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_status_window_save
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function cb_status_window_save(gui_interface_fig_tag)

%% get gui figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);

%% get status window handle, and full status report
status_window_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['status_window_edit_uicontrol', mfilename]);
current_status_report = get(status_window_edit_handle, 'string');

%% for now, just save to the workspace
assignin('base', 'status_window_report', current_status_report);

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sound_list_commit
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sound_list_commit(gui_interface_fig_tag)

%% get gui interface figure handle and figure data structure
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% check for sounds in list
if isempty(figdata.sound_list)
    return;
end


%% turn off all figure controls while doing commit
all_uihandles = findobj(gui_interface_fig_handle, 'type', 'uicontrol');
all_uihandles = [all_uihandles; findobj(gui_interface_fig_handle, 'type', 'uimenu')];
enable_states = get(all_uihandles, 'enable');
set(all_uihandles, 'enable', 'off');

%% get status window edit handle and make status window inactive
status_window_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['status_window_edit_uicontrol', mfilename]);
set(status_window_edit_handle, 'enable', 'inactive');

display_status_line(status_window_edit_handle, ' ');
display_status_line(status_window_edit_handle, 'Committing sounds...');


%% pull out info from the figure userdata structure
sndData = {figdata.sound_list.data};
sampRate = {figdata.sound_list.samplerate};
sndName = {figdata.sound_list.filename};


display_status_line(status_window_edit_handle, 'Checking sampling rate consistency...');

%% make sure sampling rates match; if not upsample to match
%% XXXX ON MY TO DO LIST: GO INTO CHKRATE CODE AND GIVE SOME EXTRA LOWPASS FILTERING
%% TO DEAL WITH MIRROR IMAGE GENERATED IN UPPER FREQUENCY RANGE WHEN UPSAMPLING
%% 'RESAMPLE' FILTERS THIS ALREADY, BUT NOT COMPLETELY
[sndData, sampRate, resample_flag] = chkrate_v2(sndData, sampRate, sndName, status_window_edit_handle);


if sum(resample_flag)
    %% resamples happened
    
    %% check for errors during upsample
    bad_snd_inx = find(cellfun('isempty', sndData));
    
    if bad_snd_inx
        %% remove bad sounds from list, and update
        
        display_status_line(status_window_edit_handle, 'Errors during resampling process. Bad sounds will be removed from list...');
        
        for i = bad_snd_inx
            %% update status window
            txt_message = sprintf('Removing sound : ''%s''...', sndName{i});
            display_status_line(status_window_edit_handle, txt_message);
        end
        
        figdata.sound_list(bad_snd_inx) = [];
        sndData(bad_snd_inx) = [];
        sampRate(bad_snd_inx) = [];
        resample_flag(bad_snd_inx) = [];
        sndName(bad_snd_inx) = [];
        
        %% get sound list popup handle and update list
        sound_list_popup_handle = findobj(gui_interface_fig_handle, 'tag', ['sound_list_popup_uicontrol', mfilename]);
        set(sound_list_popup_handle, 'string', sndName, 'value', 1);
        
    end %% if bad_snd_inx
    
    
    if isempty(figdata.sound_list)
        %% empty the sound list (some code to deal with a matlab quirk)
        figdata.sound_list = [];
        
    else
        %% update the sounds
        for inx = 1:length(figdata.sound_list)
            figdata.sound_list(inx).data = sndData{inx};
            figdata.sound_list(inx).samplerate = sampRate{inx};
            figdata.sound_list(inx).num_samples = length(sndData{inx});
            figdata.sound_list(inx).resample_flag = resample_flag(inx);
        end
        
    end %% if isempty(figdata.sound_list)
    
end %% if resample_flag


%% set figdata
set(gui_interface_fig_handle, 'userdata', figdata);


%% update status window
display_status_line(status_window_edit_handle, 'Commit Done.')
display_status_line(status_window_edit_handle, 'Ready...')
display_status_line(status_window_edit_handle, ' ')


%% release temporarily disabled figure controls
for inx = 1:length(all_uihandles)
    set(all_uihandles(inx), 'enable', enable_states{inx});
end

%% release parameter setting controls
if ~isempty(figdata.sound_list) && isempty(figdata.log_ixes)
    setting_controls_release_all(gui_interface_fig_handle);
else
    low_freq_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['low_freq_edit_uicontrol', mfilename]);
    set(low_freq_edit_uicontrol_handle, 'enable', 'on');
    
    high_freq_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['high_freq_edit_uicontrol', mfilename]);
    set(high_freq_edit_uicontrol_handle, 'enable', 'on');
    
    max_time_lag_edit_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['max_time_lag_edit_uicontrol', mfilename]);
    set(max_time_lag_edit_uicontrol, 'enable', 'on');
    
    max_freq_lag_edit_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['max_freq_lag_edit_uicontrol', mfilename]);
    set(max_freq_lag_edit_uicontrol, 'enable', 'on');
    
    run_button_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['run_button_uicontrol', mfilename]);
    set(run_button_uicontrol, 'enable', 'on');
    
    run_menu_item_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['run_menu_item_uicontrol', mfilename]);
    set(run_menu_item_uicontrol, 'enable', 'on');
end

%% disable some parameter setting controls based on correlation type
cb_param_corr_type_popup(gui_interface_fig_tag);

%% close the alternate view
cb_alternate_view_close(gui_interface_fig_tag);

%% redisplay current spec based on correlation type
display_currentsnd_spectrogram(gui_interface_fig_tag);


end





%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sound_list_add_log
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Added by SCK
function [xbat_log,log_path] = cb_sound_list_add_log(gui_interface_fig_tag)

% set flag to show that XAT log has been read in
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get sound list popup handle
sound_list_popup_handle = findobj(gui_interface_fig_handle, 'tag', ['sound_list_popup_uicontrol', mfilename]);

%% get sound list handle and most recent sound path
sound_path = figdata.last_sndpath;

%% check that sound path exists
if ~exist(sound_path)
    sound_path = [pwd, filesep];
end

%% get status window handle
status_window_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['status_window_edit_uicontrol', mfilename]);

% %% block controls until next sound commit
% setting_controls_initial_block(gui_interface_fig_handle);

%% turn off control while getting sounds
all_uihandles = findobj(gui_interface_fig_handle, 'type', 'uicontrol');
all_uihandles = [all_uihandles; findobj(gui_interface_fig_handle, 'type', 'uimenu')];
enable_states = get(all_uihandles, 'enable');
set(all_uihandles, 'enable', 'off');

%% but make status window inactive only (so it still looks normal)
set(status_window_edit_handle, 'enable', 'inactive');

%% User selects XBAT log to load. Rename structure so we can work with it
[xbat_log,log_path]=uigetfile2_SCK_log;

% if canceled exit and restore GUI
if not(ischar(xbat_log)) && not(ischar(log_path))
    for inx = 1:length(all_uihandles)
        set(all_uihandles(inx), 'enable', enable_states{inx});
    end
    return    
end

% Read selection table
C = read_selection_table(fullfile(log_path,xbat_log));
logLength = size(C,1)-1;
id = get_field_MSP(C,'Selection');
channel = str2double(get_field_MSP(C,'Channel'));
freq = [str2double(get_field_MSP(C,'Low Freq (Hz)')),...
        str2double(get_field_MSP(C,'High Freq (Hz)'))];
if isempty(id)||isempty(channel)||isempty(freq)
    fail(sprintf('Input file is not a valid Raven selection table:\n  %s',xbat_log))
    return;
end
beginPath = get_field_MSP(C,'Begin Path');
fileOffset = str2double(get_field_MSP(C,'File Offset (s)'));
if isempty(beginPath)||isempty(fileOffset)
    fail(sprintf('Input file does not have both "Begin Path" and "File Offset" measurements:\n  %s',xbat_log))
    return;
end
[~,~,ext] = cellfun(@fileparts,beginPath,'Unif',0);
if ~all(strcmp(ext,ext(1)))
    fail('Files in "Begin File" must all have the same file extension.')
else
    ext = ext{1};
end
uniqueSounds = unique(beginPath);
if ~all(isfile(uniqueSounds))
    fail('At least some files listed in "Begin File" cannot be found.')
    return;
end
dur = str2double(get_field_MSP(C,'End Time (s)')) ...
      - str2double(get_field_MSP(C,'Begin Time (s)'));
if any(dur>10)
    fail(sprintf(['Some selections are longer than 1 second long\n'...
                 'and may cause SoundXT to crash.']))
end
time_bounds = [fileOffset,fileOffset+dur];

%---
% Get sound file(s) so we can pull out sound clips of events
%---

% Find sample rate of sound files, making sure that they are all the same
    info = cellfun(@audioinfo,uniqueSounds);
    Fs = [info.SampleRate];
    assert(all(Fs == Fs(1)), 'Sample rate of sound files must be the same')
    Fs = Fs(1);
    sample_bounds = Fs .* time_bounds;
    nbits = [info.BitsPerSample];
    assert(all(nbits == nbits(1)), 'Bit depth of sound files must be the same')
    nChannels = [info.NumChannels];
    assert(all(nChannels == nChannels(1)), 'Number of channels in sound files must be the same')
% %     sizeinfo = [info.TotalSamples];
% %     cumDur = cumsum(sizeinfo) ./ Fs;

%%% Save log info fo variable
log = {dur,freq,Fs};
if isempty(figdata.log)
    figdata.log = {log};
    figdata.log_path = log_path;
    figdata.log_ixes(1:logLength) = 1;
else
    figdata.log{end+1} = log;
    figdata.log_path = log_path;
    ix_cnt = 0; ix_flag = 0;
    for z = 1:length(figdata.log)-1
       ix_cnt = ix_cnt + size(figdata.log{z},1)-1;
       if isequal(figdata.log{end},figdata.log{z})
            ix_val = figdata.log_ixes(ix_cnt);
            figdata.log_ixes(end+1:end+size(figdata.log{end},1)-1) = ix_val;
            ix_flag = 1;
       end
    end
    if ix_flag == 0
        ix_val = max(figdata.log_ixes) + 1;
        figdata.log_ixes(end+1:end+1+size(figdata.log{end},1)-1) = ix_val;
    end
end
    
    % Kind of messy, but for now pull out events one by one
    log_offset = length(figdata.sound_list);
    
    for i = 1:logLength
       
        % Read audio file
        if ismember(ext,{'.aif','.aiff'})
            data_temp = double(aiffread(beginPath{i},round(sample_bounds(i,:))));
        else
            data_temp = audioread(beginPath{i},round(sample_bounds(i,:)));
            if isa(data_temp,'double')
                data_temp = data_temp .* nbits-1;
            end
        end

        % Stash clip and selection metadata into figdata
        figdata.sound_list(i).data = data_temp(:,channel(i));
        figdata.sound_list(i).samplerate = Fs;
        figdata.sound_list(i).num_samples = sample_bounds(i,2) - sample_bounds(i,1);
        [p,file] = fileparts(beginPath{i});
        figdata.sound_list(i).filename = [file,'_',id{i}];
        figdata.sound_list(i).path = p;
        figdata.sound_list(i).resample_flag = 0;
        figdata.sound_list(i).time = time_bounds(i,:);
        figdata.sound_list(i).sample_bounds = sample_bounds(i,:);
        figdata.sound_list(i).duration = dur(i);
        figdata.sound_list(i).freq = freq(i,:);
        figdata.sound_list(i).bandwidth = freq(i,2) - freq(i,1);
        figdata.sound_list(i).level = 1;
        figdata.sound_list(i).channel = channel(i);
        
        % Added in log reference number
        figdata.log_ref(i) = length(figdata.log);
        size_comp = [1,i] == size(figdata.sound_list);
        
        if size_comp(2) == 0
            %  disp(['ERROR: event ', num2str(i), ' not found'])
        end
    end
    
    %% Update sound list in GUI
    if isempty(figdata.sound_list)
        %% set sound list popup to blank
        set(sound_list_popup_handle, 'string', {' '}, 'value', 1);
        nyquist = 0;
        max_dur = 0;
    else
        %% set sound list popup to current list
        set(sound_list_popup_handle, 'string', {figdata.sound_list.filename}, 'value', length(figdata.sound_list));
        
        %% set default spec viewer display options based on new sound list
        %% get max frequency and time range
        sampRate = cell2mat({figdata.sound_list.samplerate});
        nyquist = max(sampRate) / 2;
        max_dur = max( cell2mat({figdata.sound_list.num_samples}) ./ sampRate );
        all_freqs= cell2mat({figdata.sound_list.freq});
        min_freqs = all_freqs(1:2:end);
        max_freqs = all_freqs(2:2:end);
        min_freq_all = round(min(min_freqs));
        max_freq_all = round(max(max_freqs));
        %% update default spec display options based on new sound list by always zooming out
        figdata.layout.axes_xrange = [0, max_dur];
        figdata.layout.axes_yrange = [min_freq_all, max_freq_all];
        figdata.param.low_freq = min_freq_all;
        figdata.param.high_freq = max_freq_all;
    end
    
    %% update running number of sounds
    sound_list_text_handle = findobj(0, 'tag', ['sound_list_text_uicontrol', mfilename]);
    set(sound_list_text_handle, 'string', ['(', num2str(length(figdata.sound_list)), ' sounds in list)']);
    
    display_status_line(status_window_edit_handle, 'Add Done.');
    display_status_line(status_window_edit_handle, 'Ready...')
    display_status_line(status_window_edit_handle, ' ')
    
    %% reset controls
    for inx = 1:length(all_uihandles)
        set(all_uihandles(inx), 'enable', enable_states{inx});
    end
    
    
    figdata.log_flag = 1;
    
    %% save figure data structure (do this before call to 'setting_controls_initial_block')
    set(gui_interface_fig_handle, 'userdata', figdata);
    
    %% block controls until next sound commit, but set values based on new sound list
    setting_controls_initial_block(gui_interface_fig_handle, max_dur, min_freq_all, max_freq_all);
    
    %% plot currently highlighted spec
    display_currentsnd_spectrogram(gui_interface_fig_tag);
    
% % end
%%%% End reading XBAT logs SCK

end
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sound_list_add
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sound_list_add(gui_interface_fig_tag)


%% get gui figure handle and figure data structure
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get sound list popup handle
sound_list_popup_handle = findobj(gui_interface_fig_handle, 'tag', ['sound_list_popup_uicontrol', mfilename]);

%% get sound list handle and most recent sound path
sound_path = figdata.last_sndpath;

%% check that sound path exists
if ~exist(sound_path)
    sound_path = [pwd, filesep];
end

%% get status window handle
status_window_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['status_window_edit_uicontrol', mfilename]);


% %% block controls until next sound commit
% setting_controls_initial_block(gui_interface_fig_handle);

%% turn off control while getting sounds
all_uihandles = findobj(gui_interface_fig_handle, 'type', 'uicontrol');
all_uihandles = [all_uihandles; findobj(gui_interface_fig_handle, 'type', 'uimenu')];
enable_states = get(all_uihandles, 'enable');
set(all_uihandles, 'enable', 'off');

%% but make status window inactive only (so it still looks normal)
set(status_window_edit_handle, 'enable', 'inactive');


%% get sounds to add
%%% Modified 1/27 SCK
% [sound_list, sound_path] = uigetfiles('*.*', 'Select Sounds to Add', sound_path);

[sound_list, sound_path_all] = uigetfiles_SCK('*.*', 'Select Sounds to Add', sound_path, 'multiselect','on');

if isempty(sound_path_all)
    %% cancel requested
    %% reset controls, and exit
    for inx = 1:length(all_uihandles)
        set(all_uihandles(inx), 'enable', enable_states{inx});
    end
    return;
    
else
    [rows,cols]= size(sound_path_all);
    if rows == 1
        sound_path = cell(0);
        sound_path{1} = char(sound_path_all);
    else
        if iscell(sound_path_all) && not(isempty(sound_path_all))
            sound_path = char(sound_path_all{1});
        else
            sound_path = sound_path_all;
        end
        
    end
end

%% store most recent sound path
figdata.last_sndpath = char(sound_path);


add2sound_list = strcat(sound_path, sound_list);


display_status_line(status_window_edit_handle, ' ');
display_status_line(status_window_edit_handle, 'Adding sounds...');


for inx = 1:length(add2sound_list)
    
    if isempty(figdata.sound_list)
        %% start the list from scratch
        
        [sound_data, samplerate] = sndread_v2(add2sound_list{inx}, status_window_edit_handle);
        
        if ~isempty(sound_data) & ~isempty(samplerate)
            figdata.sound_list.data = sound_data;
            figdata.sound_list.samplerate = samplerate;
            figdata.sound_list.num_samples = length(sound_data);
            figdata.sound_list.filename = relnameof(add2sound_list{inx});
            figdata.sound_list.path = sound_path;
            figdata.sound_list.resample_flag = 0;
            %%% Added 9/4 SCK
            figdata.log_ixes = 1;
            
            %% Warn user about sounds longer than 10 secs
            sound_dur = length(sound_data) / samplerate;
            if  sound_dur >10
                display_status_line(status_window_edit_handle,sprintf('Warning: Sound ''%s'' is longer than 10 secs.  May cause SoundXT to crash.', figdata.sound_list.filename));
            end
        end
        
        
    else
        %% add to the list checking for redundancy first
        
        sound_inx = find(strcmpi({figdata.sound_list.path}, sound_path));
        
        if isempty(sound_inx)
            %% new path, add to list
            
            sound_inx = length(figdata.sound_list)+1;
            
            [sound_data, samplerate] = sndread_v2(add2sound_list{inx}, status_window_edit_handle);
            
            if ~isempty(sound_data) & ~isempty(samplerate)
                figdata.sound_list(sound_inx).data = sound_data;
                figdata.sound_list(sound_inx).samplerate = samplerate;
                figdata.sound_list(sound_inx).num_samples = length(sound_data);
                figdata.sound_list(sound_inx).filename = relnameof(add2sound_list{inx});
                figdata.sound_list(sound_inx).path = sound_path;
                figdata.sound_list(sound_inx).resample_flag = 0;
                %%% Added 9/4 SCK
                figdata.log_ixes(end+1) = max(figdata.log_ixes) + 1;
            
                %% Warn user about sounds longer than 10 secs
                sound_dur = length(sound_data) / samplerate;
                if  sound_dur >10
                    display_status_line(status_window_edit_handle,sprintf('Warning: Sound ''%s'' is longer than 10 secs.  May cause SoundXT to crash.',figdata.sound_list.filename));
                end
            end
            
            
        else %% ~isempty(sound_inx)
            %% path already used, check for redundancy
            
            match = sum(strcmpi({figdata.sound_list(sound_inx).filename}, relnameof(add2sound_list{inx})));
            
            if ~match
                %% load up sound
                sound_inx = length(figdata.sound_list)+1;
                
                [sound_data, samplerate] = sndread_v2(add2sound_list{inx}, status_window_edit_handle);
                
                if ~isempty(sound_data) && ~isempty(samplerate)
                    figdata.sound_list(sound_inx).data = sound_data;
                    figdata.sound_list(sound_inx).samplerate = samplerate;
                    figdata.sound_list(sound_inx).num_samples = length(sound_data);
                    figdata.sound_list(sound_inx).filename = relnameof(add2sound_list{inx});
                    figdata.sound_list(sound_inx).path = sound_path;
                    figdata.sound_list(sound_inx).resample_flag = 0;
                    %%% Added 9/4 SCK
                    figdata.log_ixes(end+1) = max(figdata.log_ixes) + 1;
                    
                    %% Warn user about sounds longer than 10 secs
                    sound_dur = length(sound_data) / samplerate;
                    if  sound_dur >10
                        display_status_line(status_window_edit_handle,sprintf('Warning: Sound ''%s'' is longer than 10 secs.  May cause SoundXT to crash.\n\n',figdata.sound_list.filename));
                    end
                end
                
            end %% if ~match
            
        end %% if isempty(sound_inx)
        
    end %% if isempty(figdata.sound_list)
    
end %% for inx = 1:length(add2sound_list)



if isempty(figdata.sound_list)
    
    %% set sound list popup to blank
    set(sound_list_popup_handle, 'string', {' '}, 'value', 1);
    
    nyquist = 0;
    max_dur = 0;
    
else
    
    %% set sound list popup to current list
    set(sound_list_popup_handle, 'string', {figdata.sound_list.filename}, 'value', length(figdata.sound_list));
    
    %% set default spec viewer display options based on new sound list
    %% get max frequency and time range
    sampRate = cell2mat({figdata.sound_list.samplerate});
    nyquist = max(sampRate) / 2;
    max_dur = max( cell2mat({figdata.sound_list.num_samples}) ./ sampRate );
    
    %% update default spec display options based on new sound list by always zooming out
    figdata.layout.axes_xrange = [0, max_dur];
    figdata.layout.axes_yrange = [0, nyquist];
    
    %% XXXX WORK FOR LATER: MAKE THIS A DISPLAY OPTION IN THE PREFERENCES-- I.E. ZOOM OUT ON ADD YES/NO?
    %% SOME SORT OF MENU ITEM YOU CAN CHECK OR UNCHECK
    
end


%% update running number of sounds
sound_list_text_handle = findobj(0, 'tag', ['sound_list_text_uicontrol', mfilename]);
set(sound_list_text_handle, 'string', ['(', num2str(length(figdata.sound_list)), ' sounds in list)']);

display_status_line(status_window_edit_handle, 'Add Done.');
display_status_line(status_window_edit_handle, 'Ready...')
display_status_line(status_window_edit_handle, ' ')


%% reset controls
for inx = 1:length(all_uihandles)
    set(all_uihandles(inx), 'enable', enable_states{inx});
end


figdata.clip_flag = 1;

%% save figure data structure (do this before call to 'setting_controls_initial_block')
set(gui_interface_fig_handle, 'userdata', figdata);

%% block controls until next sound commit, but set values based on new sound list
setting_controls_initial_block(gui_interface_fig_handle, max_dur, 0, nyquist);

%% plot currently highlighted spec
display_currentsnd_spectrogram(gui_interface_fig_tag);

%% and exit
end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sound_list_delete
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sound_list_delete(gui_interface_fig_tag)

%% get gui interface figure handle and figure data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

if isempty(figdata.sound_list)
    %% no sounds to delete, drop out
    return;
end

%% get sound list popup handle and highlighted sounds
sound_list_popup_handle = findobj(0, 'tag', ['sound_list_popup_uicontrol', mfilename]);
selected_sound_inx = get(sound_list_popup_handle, 'value');

%% remove selected sounds
figdata.sound_list(selected_sound_inx) = [];
figdata.log_ixes(selected_sound_inx) = [];

% see what logs need to be deleted
del_ixes =[];
for k = 1:length(figdata.log)
    check = find(figdata.log_ixes == k);
    if isempty(check)
       del_ixes = [del_ixes k]; 
    end
end
figdata.log{del_ixes} = [];

if isempty(figdata.sound_list)
    
    %% set sound list to blank
    set(sound_list_popup_handle, 'string', {' '}, 'value', 1);
    
    %% some code to deal with a matlab quirk
    figdata.sound_list = [];
    figdata.log = cell(0);
    
    nyquist = 0;
    max_dur = 0;
    
else
    
    %% set sound list to current list, and highlight next in list
    set(sound_list_popup_handle, 'string', {figdata.sound_list.filename},...
        'value', min(selected_sound_inx, length(figdata.sound_list)));
    
    %% set default spec viewer display options based on new sound list
    %% get max frequency and time range
    sampRate = cell2mat({figdata.sound_list.samplerate});
    nyquist = max(sampRate) / 2;
    max_dur = max( cell2mat({figdata.sound_list.num_samples}) ./ sampRate );
    
    %% update default spec display options based on new sound list by always zooming into sounds
    if max_dur < figdata.layout.axes_xrange(2) - figdata.layout.axes_xrange(1)
        figdata.layout.axes_xrange = [0, max_dur];
    end
    if nyquist < figdata.layout.axes_yrange(2) - figdata.layout.axes_yrange(1)
        figdata.layout.axes_yrange = [0, nyquist];
    end
    
    %% XXXX WORK FOR LATER: MAKE THIS A DISPLAY OPTION IN THE PREFERENCES-- I.E. ZOOM IN ON DELETE YES/NO?
    %% SOME SORT OF MENU ITEM YOU CAN CHECK OR UNCHECK
    
end

%% set bandlimits and lags as indicated based on added sounds
low_freq_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['low_freq_edit_uicontrol', mfilename]);
high_freq_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['high_freq_edit_uicontrol', mfilename]);
max_time_lag_edit_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['max_time_lag_edit_uicontrol', mfilename]);
max_freq_lag_edit_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['max_freq_lag_edit_uicontrol', mfilename]);

figdata.param.low_freq = min(nyquist, figdata.param.low_freq);
figdata.param.high_freq = min(nyquist, figdata.param.high_freq);

set(low_freq_edit_uicontrol_handle, 'string', figdata.param.low_freq);%%, 'value', 0);
set(high_freq_edit_uicontrol_handle,'string', figdata.param.high_freq);%%, 'value', nyquist);
set(high_freq_edit_uicontrol_handle, 'value', nyquist);

figdata.param.max_time_lag = min(max_dur, figdata.param.max_time_lag);
figdata.param.max_freq_lag = min(nyquist, figdata.param.max_freq_lag);

set(max_time_lag_edit_uicontrol, 'string', figdata.param.max_time_lag);%%, 'value', max_dur);
set(max_freq_lag_edit_uicontrol, 'string', figdata.param.max_freq_lag);%%, 'value', nyquist);


%% set figure data structure
set(gui_interface_fig_handle, 'userdata', figdata);

%% plot currently highlighted spec
display_currentsnd_spectrogram(gui_interface_fig_tag);


%% update running number of sounds
sound_list_text_handle = findobj(0, 'tag', ['sound_list_text_uicontrol', mfilename]);
set(sound_list_text_handle, 'string', ['(', num2str(length(figdata.sound_list)), ')']);


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sound_list_moveup
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sound_list_moveup(gui_interface_fig_tag)

%% get gui interface figure handle and figure data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

if isempty(figdata.sound_list)
    %% no sounds to move, drop out
    return;
end

%% get sound list popup handle and sound list
sound_list_popup_handle = findobj(0, 'tag', ['sound_list_popup_uicontrol', mfilename]);
sound_list = get(sound_list_popup_handle, 'string');

%% get current highlighted sounds
current_sounds = get(sound_list_popup_handle, 'value');


%% move the current sounds up in the list
if current_sounds(1) > 1
    
    %% get all sounds inx
    inx = 1:length(sound_list);
    
    %% get remaining sounds
    rem_sounds = inx;
    rem_sounds(current_sounds) = [];
    
    %% get free spaces after current sounds move up
    free_spaces = inx;
    free_spaces(current_sounds - 1) = [];
    
    %% reorder the inx for all sounds
    inx(current_sounds - 1) = current_sounds;
    inx(free_spaces) = rem_sounds;
    
    %% reorder the sounds list
    sound_list = sound_list(inx);
    
    %% keep the same sound highlighted
    current_sounds = current_sounds - 1;
    
    %% reset the list in the display window
    set(sound_list_popup_handle, 'string', sound_list, 'value', current_sounds);
    
    %% reset the stored sound list order
    figdata.sound_list = figdata.sound_list(inx);
    
    %% save the new figure data
    set(gui_interface_fig_handle, 'userdata', figdata);
    
end


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sound_list_movedown
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sound_list_movedown(gui_interface_fig_tag)

%% get gui interface figure handle and figure data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

if isempty(figdata.sound_list)
    %% no sounds to move, drop out
    return;
end

%% get sound list popup handle and sound list
sound_list_popup_handle = findobj(0, 'tag', ['sound_list_popup_uicontrol', mfilename]);
sound_list = get(sound_list_popup_handle, 'string');

%% get current highlighted sounds
current_sounds = get(sound_list_popup_handle, 'value');


%% move the current sounds down in the list
if current_sounds(end) < length(sound_list)
    
    %% get all sounds inx
    inx = 1:length(sound_list);
    
    %% get remaining sounds
    rem_sounds = inx;
    rem_sounds(current_sounds) = [];
    
    %% get free spaces after current sounds move up
    free_spaces = inx;
    free_spaces(current_sounds + 1) = [];
    
    %% reorder the inx for all sounds
    inx(current_sounds + 1) = current_sounds;
    inx(free_spaces) = rem_sounds;
    
    %% reorder the sounds list
    sound_list = sound_list(inx);
    
    %% keep the same sound highlighted
    current_sounds = current_sounds + 1;
    
    %% reset the list in the display window
    set(sound_list_popup_handle, 'string', sound_list, 'value', current_sounds);
    
    %% reset the stored sound list order
    figdata.sound_list = figdata.sound_list(inx);
    
    %% save the new figure data
    set(gui_interface_fig_handle, 'userdata', figdata);
    
end

end





%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_sound_list_order
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_sound_list_order(gui_interface_fig_tag)

%% get gui interface figure handle and figure data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

if isempty(figdata.sound_list)
    %% no sounds to order, drop out
    return;
end

%% get sound list popup handle and sound list and current_snd
sound_list_popup_handle = findobj(0, 'tag', ['sound_list_popup_uicontrol', mfilename]);
sound_list = get(sound_list_popup_handle, 'string');
current_snd = get(sound_list_popup_handle, 'value');

%% sort the list alpha-numerically
[sound_list, inx] = sort(sound_list);

%% keep the same sounds highlighted
[~, current_snd] = ismember(current_snd, inx);

%% reset the list in the display window
set(sound_list_popup_handle, 'string', sound_list, 'value', current_snd);

%% reset the list in the stored sound
figdata.sound_list = figdata.sound_list(inx);

%% save the new figure data
set(gui_interface_fig_handle, 'userdata', figdata);

%% WORK FOR LATER: THINK ABOUT ADDING AN UNDO FEATURE TO THIS RE-ORDER

end





%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_output_file_select
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_output_file_select(gui_interface_fig_tag)

%% get gui interface figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get output file display handle and most recent output path
output_edit_handle =  findobj(gui_interface_fig_handle, 'tag', ['output_edit_uicontrol', mfilename]);
outpath = figdata.last_outpath;

%% check that outpath exists
if ~exist(outpath)
    outpath = [pwd, filesep];
end

%% get output file name
[outname, outpath] = uiputfile('*.*', 'Select Output File', outpath);

if ~outpath
    %% cancel requested
    return;
end

%% store most recent output path
figdata.last_outpath = outpath;

%% make sure output file opens
fid = fopen([outpath, outname], 'w');

if fid ~= -1
    %% no file open error
    
    fclose(fid);
    delete([outpath, outname]);
    
    %% set output file display field
    set(output_edit_handle, 'string', outname);
    
    %% save output name and path to figure data
    figdata.output = [outpath, outname];
    
    %% save figure data
    set(gui_interface_fig_handle, 'userdata', figdata);
    
end

%% WORK FOR LATER: THINK ABOUT WAYS TO EDIT THE FILE NAME
%% RIGHT IN THE FIELD ONCE IT HAS BEEN SELECTED THE FIRST TIME

end

%% aborted ideas (for now) on how to get output file name
%     %%~~~
%     %% output_edit_bdf
%     %%~~~
%
%   case ('output_edit_bdf')
%
%     %     output_edit_handle = gcbo;
%     %
%     %     set(output_edit_handle, 'enable', 'on', 'selected', 'on', 'string', '|');
%
%     end;
%

% %%~~~
% %% output_edit_cb
% %%~~~
%
% case ('output_edit_cb')
%
%   gui_interface_fig_handle = findobj(0, 'tag', ['gui_interface_fig_001', mfilename]);
%   figdata = get(gui_interface_fig_handle, 'userdata');
%
%   output_edit_handle = gcbo;
%   output = get(output_edit_handle, 'string');
%
%   output = removeillegalchar(output);
%
%   %% make sure output file opens
%   %     fid = fopen()
%
%
%   set(output_edit_handle, 'string', output);
%   % , 'enable', 'off', 'backgroundcolor', editablecontrolcolor);
%
%   figdata.output = output;
%
%   set(gui_interface_fig_handle, 'userdata', figdata);
%
%
%   end;






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_export_corr_data_file
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_export_corr_data_file(gui_interface_fig_tag)

%% get gui interface figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get most recent analysis/export path
export_path = figdata.last_analysis_path; %% figdata.last_export_path; %% not sure which to use here

%% check that outpath exists
if ~exist(export_path)
    export_path = [pwd, filesep];
end

%% get output file to export
%% Modified by SCK
% [export_file, export_path] = uigetfile('*.*', 'Select Correlation Data File to Export', export_path);
[export_file, export_path_all] = uigetfiles_SCK('*.*', 'Select Correlation Data File to Export', export_path);

if isempty(export_path_all)
    %% cancel requested
    return;
end

export_path = char(export_path_all{1});

%% store most recent analysis/export path
%figdata.last_export_path = export_path;
figdata.last_analysis_path = export_path;

%% save figure data
set(gui_interface_fig_handle, 'userdata', figdata);


%% get status window handle
status_window_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['status_window_edit_uicontrol', mfilename]);
%% send a message
display_status_line(status_window_edit_handle, ' ');
display_status_line(status_window_edit_handle, 'Exporting correlation data file...');


try
    corr_data = load('-mat', [export_path, export_file]);
    %% get to the rootfield
    corr_data_fieldnames = fieldnames(corr_data);
    corr_data = corr_data.(corr_data_fieldnames{1});
    
catch
    % unable to load file
    % send an error message and quit
    display_status_line(status_window_edit_handle, sprintf('File "%s" did not open.', analysis_file));
    display_status_line(status_window_edit_handle, 'Unable to export.');
    return;
    
end


if isfield(corr_data, 'f0Peaks')
    %% is a correlation data file
    
    %% get peak correlation, lag data, and sound names
    corrmat = corr_data.f0Peaks.peakVal;
    lagmat = corr_data.f0Peaks.peakLag;
    snd_names = {corr_data.sound_list.filename}';
    
    outputfilename1 = [export_path, basenameof(export_file), '_f0Peaks.txt'];
    fid1 = fopen(outputfilename1, 'w');
    outputfilename2 = [export_path, basenameof(export_file), '_f0Lags.txt'];
    fid2 = fopen(outputfilename2, 'w');
    
    if fid1 == -1
        display_status_line(status_window_edit_handle, sprintf('Unable to open file ''s'' for writing.', outputfilename1));
        if fid2 == -1
            display_status_line(status_window_edit_handle, sprintf('Unable to open file ''s'' for writing.', outputfilename2));
        end
        display_status_line(status_window_edit_handle, 'Unable to export.');
        return;
    elseif fid2 == -1
        display_status_line(status_window_edit_handle, sprintf('Unable to open file ''s'' for writing.', outputfilename2));
        display_status_line(status_window_edit_handle, 'Unable to export.');
        return;
    end
    
    [nrow, ncol] = size(corrmat);
    
    if ncol == 1
        fprintf(fid1, 'SndNames\t%s\n', snd_names{1});
        fprintf(fid2, 'SndNames\t%s\n', snd_names{1});
        for i=1:nrow
            fprintf(fid1,'%s\t%12.6f\n', snd_names{i+1}, corrmat(i));
            fprintf(fid2,'%s\t%12.6f\n', snd_names{i+1}, lagmat(i));
        end
        
    else
        format_Str = ['SndNames'];
        for i=1:nrow
            format_Str = [format_Str, sprintf('\t%s',  snd_names{i})];
        end
        format_Str =  [format_Str, '\n'];
        fprintf(fid1, format_Str);
        fprintf(fid2, format_Str);
        
        format_Str = ['%s'];
        for i=1:nrow
            format_Str = [format_Str, '\t%12.6f'];
        end
        format_Str =  [format_Str, '\n'];
        
        tempMatrix1 = cat(2, snd_names, num2cell(corrmat));
        tempMatrix2 = cat(2, snd_names, num2cell(lagmat));
        tempMatrix1 = tempMatrix1';
        tempMatrix2 = tempMatrix2';
        
        fprintf(fid1, format_Str, tempMatrix1{:});
        fprintf(fid2, format_Str, tempMatrix2{:});
        
    end
    
    fclose(fid1);
    fclose(fid2);
    
    
    if isfield(corr_data, 'absPeaks')
        
        %% get peak correlation, lag data, and sound names
        corrmat = corr_data.absPeaks.peakVal;
        lagmat = corr_data.absPeaks.peakTimeLag;
        freqlagmat = corr_data.absPeaks.peakFreqLag;
        
        outputfilename1 = [analysis_path, basenameof(analysis_file), '_absPeaks.txt'];
        fid1 = fopen(outputfilename1, 'w');
        outputfilename2 = [analysis_path, basenameof(analysis_file), '_absTimeLags'];
        fid2 = fopen(outputfilename2, 'w');
        outputfilename3 = [analysis_path, basenameof(analysis_file), '_absFreqLags'];
        fid3 = fopen(outputfilename3, 'w');
        
        if fid1 == -1
            display_status_line(status_window_edit_handle, sprintf('Unable to open file ''s'' for writing.', outputfilename1));
            if fid2 == -1
                display_status_line(status_window_edit_handle, sprintf('Unable to open file ''s'' for writing.', outputfilename2));
            end
            if fid3 == -1
                display_status_line(status_window_edit_handle, sprintf('Unable to open file ''s'' for writing.', outputfilename3));
            end
            display_status_line(status_window_edit_handle, 'Unable to export.');
            return;
        elseif fid2 == -1
            display_status_line(status_window_edit_handle, sprintf('Unable to open file ''s'' for writing.', outputfilename2));
            if fid3 == -1
                display_status_line(status_window_edit_handle, sprintf('Unable to open file ''s'' for writing.', outputfilename3));
            end
            display_status_line(status_window_edit_handle, 'Unable to export.');
            return;
        elseif fid3 == -1
            display_status_line(status_window_edit_handle, sprintf('Unable to open file ''s'' for writing.', outputfilename3));
            display_status_line(status_window_edit_handle, 'Unable to export.');
        end
        
        if ncol == 1
            fprintf(fid1, 'SndNames\t%s\n', snd_names{1});
            fprintf(fid2, 'SndNames\t%s\n', snd_names{1});
            fprintf(fid3, 'SndNames\t%s\n', snd_names{1});
            for i=1:nrow
                fprintf(fid1,'%s\t%12.6f\n', snd_names{i+1}, corrmat(i));
                fprintf(fid2,'%s\t%12.6f\n', snd_names{i+1}, lagmat(i));
                fprintf(fid3,'%s\t%12.6f\n', snd_names{i+1}, freqlagmat(i));
            end
            
        else
            format_Str = ['SndNames'];
            for i=1:nrow
                format_Str = [format_Str, sprintf('\t%s', snd_names{i})];
            end
            format_Str =  [format_Str, '\n'];
            fprintf(fid1, format_Str);
            fprintf(fid2, format_Str);
            fprintf(fid3, format_Str);
            
            format_Str = ['%s'];
            for i=1:nrow
                format_Str = [format_Str, '\t%12.6f'];
            end
            format_Str =  [format_Str, '\n'];
            
            tempMatrix1 = cat(2, snd_names, num2cell(corrmat));
            tempMatrix2 = cat(2, snd_names, num2cell(lagmat));
            tempMatrix3 = cat(2, snd_names, num2cell(freqlagmat));
            tempMatrix1 = tempMatrix1';
            tempMatrix2 = tempMatrix2';
            tempMatrix3 = tempMatrix3';
            
            fprintf(fid1, format_Str, tempMatrix1{:});
            fprintf(fid2, format_Str, tempMatrix2{:});
            fprintf(fid3, format_Str, tempMatrix3{:});
            
        end
        
        fclose(fid1);
        fclose(fid2);
        fclose(fid3);
        
    end
    
else
    %% not a correlation data file
    %% send an error message
    display_status_line(status_window_edit_handle, sprintf('File ''%s'' is not a correlation data file.', analysis_file));
    display_status_line(status_window_edit_handle, 'Unable to export.');
    return;
    
end

display_status_line(status_window_edit_handle, 'Done.');
display_status_line(status_window_edit_handle, 'Ready...')
display_status_line(status_window_edit_handle, ' ')


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_param_fft_len_edit
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_fft_len_edit(gui_interface_fig_tag)

%% get gui figure handle and figure data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get fft edit handle and current entry
fft_len_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['fft_len_edit_uicontrol', mfilename]);
fft_len = str2num(get(fft_len_edit_handle, 'string'));

data_len_text_handle = findobj(gui_interface_fig_handle, 'tag', ['data_length_text_uicontrol', mfilename]);
data_overlap_text_handle = findobj(gui_interface_fig_handle, 'tag', ['data_overlap_text_uicontrol', mfilename]);

%% make sure entry is a number, and greater than zero
if isempty(fft_len) | fft_len <= 0
    %% non-numeric or invalid entry, don't change previous value
    set(fft_len_edit_handle, 'string', figdata.param.fft_len)
    
else
    %% valid entry, update value
    figdata.param.fft_len = fft_len;
    set(gui_interface_fig_handle, 'userdata', figdata);
    
    %% update points display
    data_len_pts = round(figdata.param.fft_len*figdata.param.data_len/100);
    set(data_len_text_handle, 'string', ['(', num2str(data_len_pts), 'pts)']);
    data_overlap_pts = round(data_len_pts*figdata.param.data_overlap/100);
    set(data_overlap_text_handle, 'string', ['(', num2str(data_overlap_pts), 'pts)']);
    
end

%% redisplay spec in viewer based on new settings
display_currentsnd_spectrogram(gui_interface_fig_tag);


%% WORK FOR LATER: MAYBE CHANGE THIS TO A PULL DOWN IN POWERS OF 2?
%% OR LEAVE AS IS, BUT ADD THE SLIDER

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_param_data_len_edit
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_data_len_edit(gui_interface_fig_tag)

%% get gui figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get data length edit box handle and current entry
data_len_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['data_len_edit_uicontrol', mfilename]);
data_len = str2num(get(data_len_edit_handle, 'string'));
data_len_text_handle = findobj(gui_interface_fig_handle, 'tag', ['data_length_text_uicontrol', mfilename]);
data_overlap_text_handle = findobj(gui_interface_fig_handle, 'tag', ['data_overlap_text_uicontrol', mfilename]);


%% make sure entry is a number, and in valid range
if isempty(data_len) | data_len > 100 | data_len <= 0
    %% non-numeric entry, don't change previous value
    set(data_len_edit_handle, 'string', figdata.param.data_len)
    
else
    %% valid entry, update value
    figdata.param.data_len = data_len;
    set(gui_interface_fig_handle, 'userdata', figdata);
    
    %% update points display
    data_len_pts = round(figdata.param.fft_len*figdata.param.data_len/100);
    set(data_len_text_handle, 'string', ['(', num2str(data_len_pts), 'pts)']);
    data_overlap_pts = round(data_len_pts*figdata.param.data_overlap/100);
    set(data_overlap_text_handle, 'string', ['(', num2str(data_overlap_pts), 'pts)']);
    
end

%% redisplay spec in viewer based on new settings
display_currentsnd_spectrogram(gui_interface_fig_tag);

%% WORK FOR LATER: CHANGE THIS TO FRACTION OF FFT SIZE, AND ADD THE SLIDER

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_taper_func_popup
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_taper_fun_popup(gui_interface_fig_tag)

%% get gui figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get taper function popup handle
taper_func_popup_handle = findobj(gui_interface_fig_handle,'tag', ['taper_func_popup_uicontrol', mfilename]);
taper_options = get(taper_func_popup_handle, 'string');

%% set taper function into figure data
figdata.param.taper_func = taper_options{get(taper_func_popup_handle, 'value')};
set(gui_interface_fig_handle, 'userdata', figdata);

%% redisplay spec in viewer based on new settings
display_currentsnd_spectrogram(gui_interface_fig_tag);


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_param_data_overlap_edit
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_data_overlap_edit(gui_interface_fig_tag)

%% get gui interface figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get data overlap edit handle and current value
data_overlap_edit_handle = findobj(gui_interface_fig_handle,'tag', ['data_overlap_edit_uicontrol', mfilename]);
data_overlap = str2num(get(data_overlap_edit_handle, 'string'));
data_overlap_text_handle = findobj(gui_interface_fig_handle, 'tag', ['data_overlap_text_uicontrol', mfilename]);


%% make sure entry is a number, and in valid range
if isempty(data_overlap) | data_overlap >= 100 | data_overlap < 0
    %% non-numeric or invalid entry, don't change previous value
    set(data_overlap_edit_handle, 'string', figdata.param.data_overlap)
    
else
    %% valid entry, update value
    figdata.param.data_overlap = data_overlap;
    set(gui_interface_fig_handle, 'userdata', figdata);
    
    %% update points display
    data_overlap_pts = round(figdata.param.fft_len*figdata.param.data_len*figdata.param.data_overlap/100.^2);
    set(data_overlap_text_handle, 'string', ['(', num2str(data_overlap_pts), 'pts)'])
    
end

%% redisplay spec in viewer based on new settings
display_currentsnd_spectrogram(gui_interface_fig_tag);

%% WORK FOR LATER: CHANGE THIS TO FRACTION OF FFT SIZE, AND ADD THE SLIDER

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_param_low_freq_edit
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_low_freq_edit(gui_interface_fig_tag);

%% get gui interface figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get low frequency bound edit handle and current value
low_freq_edit_handle = findobj(gui_interface_fig_handle,'tag', ['low_freq_edit_uicontrol', mfilename]);
low_freq = str2num(get(low_freq_edit_handle, 'string'));


%% make sure entry is a number
if isempty(low_freq)
    %% non-numeric entry, don't change previous value
    low_freq = figdata.param.low_freq;
    set(low_freq_edit_handle, 'string', low_freq);
    
elseif low_freq >= figdata.param.high_freq
    %% exceeds or equals high frequency, arbitrarily make 1 Hz less
    low_freq = figdata.param.high_freq - 1;
    set(low_freq_edit_handle, 'string', low_freq);
    
elseif low_freq < 0
    %% less than zero, arbitrarily set to 0 Hz
    low_freq = 0;
    set(low_freq_edit_handle, 'string', low_freq);
    
end

%% update value and save in user data
figdata.param.low_freq = low_freq;
set(gui_interface_fig_handle, 'userdata', figdata);

%% redisplay spec in viewer based on new settings
display_currentsnd_spectrogram(gui_interface_fig_tag);


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_param_high_freq_edit
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_high_freq_edit(gui_interface_fig_tag);

%% get gui interface figure handle and figure data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get high frequency bound edit handle and current value
high_freq_edit_handle = findobj(gui_interface_fig_handle,'tag', ['high_freq_edit_uicontrol', mfilename]);
high_freq = str2num(get(high_freq_edit_handle, 'string'));


% %% get nyquist frequency, which is stored in the unused
% %% value field of edit uicontrol
%% get nyquist frequency, by calculation
nyquist = figdata.sound_list(1).samplerate/2; %%get(high_freq_edit_handle, 'value');


%% make sure entry is a number
if isempty(high_freq)
    %% non-numeric or invalid entry, don't change previous value
    high_freq = figdata.param.high_freq;
    set(high_freq_edit_handle, 'string', high_freq);
    
elseif high_freq <= figdata.param.low_freq
    %% less than or equal to low frequency, arbitrarily make 1 Hz more
    high_freq = figdata.param.low_freq + 1;
    set(high_freq_edit_handle, 'string', high_freq);
    
elseif high_freq > nyquist
    %% exceeds nyquist, arbitrarily set to nyquist
    high_freq = nyquist;
    set(high_freq_edit_handle, 'string', high_freq);
    
end

%% update value
figdata.param.high_freq = high_freq;

%% set figure data
set(gui_interface_fig_handle, 'userdata', figdata);

%% redisplay spec in viewer based on new settings
display_currentsnd_spectrogram(gui_interface_fig_tag);


end





%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_param_sig_preproc_popup
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_sig_preproc_popup(gui_interface_fig_tag);

%% get gui interface figure handle and figure data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get signal preprocessing type popup handle and current value
sig_preproc_popup_handle = findobj(gui_interface_fig_handle,'tag', ['sig_preproc_popup_uicontrol', mfilename]);
sig_preproc_options = get(sig_preproc_popup_handle, 'string');

%% set signal preprocessing type in figure data structure
figdata.param.sig_preproc = sig_preproc_options{get(sig_preproc_popup_handle, 'value')};

%% set figure data
set(gui_interface_fig_handle, 'userdata', figdata);

%% redisplay spec in viewer based on new settings
display_currentsnd_spectrogram(gui_interface_fig_tag);


end





%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_param_masking_meth_popup
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_masking_meth_popup(gui_interface_fig_tag);

%% get gui interface figure handle and figure data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get masking type popup handle and current value
masking_meth_popup_handle = findobj(gui_interface_fig_handle,'tag', ['masking_meth_popup_uicontrol', mfilename]);
masking_meth_options = get(masking_meth_popup_handle, 'string');

%% set masking type in figure data structure
figdata.param.masking_meth = masking_meth_options{get(masking_meth_popup_handle, 'value')};


%% updata the parameter entry uicontrol
if strcmpi(figdata.param.masking_meth, 'Peak Threshold')
    de_string = 'Threshold (dB)';
    de_tip = 'enter dB threshold for masking';
else
    de_string = 'Masking Percent (%)';
    de_tip = 'enter percentile to use for masking threshold';
end

%% get masking param text handle
masking_param_text_handle = findobj(0, 'tag', ['masking_param_text_uicontrol', mfilename]);

%% get masking param edit handle
masking_param_edit_handle = findobj(0, 'tag', ['masking_param_edit_uicontrol', mfilename]);

set(masking_param_text_handle, 'string', de_string);
set(masking_param_edit_handle, 'tooltip', de_tip);

%% set figure data
set(gui_interface_fig_handle, 'userdata', figdata);

%% redisplay spec in viewer based on new settings
display_currentsnd_spectrogram(gui_interface_fig_tag);


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_param_masking_param_edit
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_masking_param_edit(gui_interface_fig_tag);

%% get gui interface figure handle and data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get masking percentile edit handle and current value
masking_param_edit_handle = findobj(gui_interface_fig_handle,'tag', ['masking_param_edit_uicontrol', mfilename]);
masking_param = str2num(get(masking_param_edit_handle, 'string'));


%% make sure entry is a number, and in a valid range
if isempty(masking_param) | masking_param < 0 | masking_param > 100
    %% non-numeric or invalid entry, don't change previous value
    set(masking_param_edit_handle, 'string', figdata.param.masking_param)
    
else
    %% update value
    figdata.param.masking_param = masking_param;
    %% set figure data
    set(gui_interface_fig_handle, 'userdata', figdata);
    
end


%% redisplay spec in viewer based on new settings
display_currentsnd_spectrogram(gui_interface_fig_tag);


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_param_mask_adj_popup
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_mask_adj_popup(gui_interface_fig_tag);

%% get gui interface figure handle and data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get mask adjustment edit handle and current value
mask_adj_popup_handle = findobj(gui_interface_fig_handle,'tag', ['mask_adj_popup_uicontrol', mfilename]);
mask_adj_options = get(mask_adj_popup_handle, 'string');

%% set mask adjustment type in figure data structure
figdata.param.mask_adj = mask_adj_options{get(mask_adj_popup_handle, 'value')};

%% set figure data
set(gui_interface_fig_handle, 'userdata', figdata);

%% redisplay spec in viewer based on new settings
display_currentsnd_spectrogram(gui_interface_fig_tag);


end







%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_param_data_form_popup
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_data_form_popup(gui_interface_fig_tag);

%% get gui interface figure handle and data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get data format popup handle and current value
data_form_popup_handle = findobj(gui_interface_fig_handle,'tag', ['data_form_popup_uicontrol', mfilename]);
data_form_options = get(data_form_popup_handle, 'string');

%% set data format to figure data structure
figdata.param.data_form = data_form_options{get(data_form_popup_handle, 'value')};

%% set figure data structure
set(gui_interface_fig_handle, 'userdata', figdata);

%% redisplay spec in viewer based on new settings
display_currentsnd_spectrogram(gui_interface_fig_tag);


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_param_corr_type_popup
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_corr_type_popup(gui_interface_fig_tag);

%% get figure handle and data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get handle to corr type popup and current value
corr_type_popup_handle = findobj(gui_interface_fig_handle, 'tag', ['corr_type_popup_uicontrol', mfilename]);
corr_type_options = get(corr_type_popup_handle, 'string');

%% set correlation type in figure data structure
figdata.param.corr_type = corr_type_options{get(corr_type_popup_handle, 'value')};


% if sum(strcmpi(figdata.param.corr_type, {'Time: Pairwise'; 'Time: Column';...
%       'Complex Env: Pairwise'; 'Complex Env: Column'}))
%
%   %% turn off blocking
%   %   %% turn off spec parameter controls if time correlation
%   %   spec_controls_block(gui_interface_fig_handle);
%
%   %% turn off frequency lag control since only time alignment is done
%   max_freq_lag_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['max_freq_lag_edit_uicontrol', mfilename]);
%   set(max_freq_lag_edit_handle, 'enable', 'off');
%
%   %% turn off standardization since only used on spectrograms
%   corr_std_popup_handle = findobj(gui_interface_fig_handle, 'tag', ['corr_std_popup_uicontrol', mfilename]);
%   set(corr_std_popup_handle, 'enable', 'off');
%
%   %% close alternate view window
%   cb_alternate_view_close(gui_interface_fig_tag);
%
% else
%
%   %% turn off blocking
%   %   %% turn on
%   %   spec_controls_release(gui_interface_fig_handle);
%
%   %% turn on frequency lag control since only time alignment is done
%   max_freq_lag_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['max_freq_lag_edit_uicontrol', mfilename]);
%   set(max_freq_lag_edit_handle, 'enable', 'on');
%
%   %% turn on standardization since only used on spectrograms
%   corr_std_popup_handle = findobj(gui_interface_fig_handle, 'tag', ['corr_std_popup_uicontrol', mfilename]);
%   set(corr_std_popup_handle, 'enable', 'on');
%
%   if sum(strcmpi(figdata.param.corr_type, {'Spec: Matrix Cross'}))
%
%     %% turn off frequency lag control since only time alignment is done
%     max_freq_lag_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['max_freq_lag_edit_uicontrol', mfilename]);
%     set(max_freq_lag_edit_handle, 'enable', 'off');
%
%   end
%
% end

%% save figure data
set(gui_interface_fig_handle, 'userdata', figdata);

%% redisplay current spec based on correlation type
display_currentsnd_spectrogram(gui_interface_fig_tag);


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_param_corr_std_popup
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_corr_std_popup(gui_interface_fig_tag);

%% get figure handle and data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get handle to corr stand popup and current value
corr_std_popup_handle = findobj(gui_interface_fig_handle, 'tag', ['corr_std_popup_uicontrol', mfilename]);
corr_std_options = get(corr_std_popup_handle, 'string');

%% set correlation standardization in figure data structure
figdata.param.corr_std = corr_std_options{get(corr_std_popup_handle, 'value')};

%% save figure data
set(gui_interface_fig_handle, 'userdata', figdata);


%% redisplay spec in viewer based on new settings
display_currentsnd_spectrogram(gui_interface_fig_tag);

end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_param_max_time_lag_edit
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_max_time_lag_edit(gui_interface_fig_tag);

%% get gui figure handle and data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get max time lag edit handle and current vlaue
max_time_lag_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['max_time_lag_edit_uicontrol', mfilename]);
max_time_lag = str2num(get(max_time_lag_edit_handle, 'string'));


%% make sure entry is a number
if isempty(max_time_lag) | max_time_lag < 0
    
    %% non-numeric or invalid entry, don't change previous value
    set(max_time_lag_edit_handle, 'string', figdata.param.max_time_lag)
    
else
    
    %% check value range
    %% using first sample rate should be okay, because sounds should already have been committed
    maxdur = max(cell2mat({figdata.sound_list.num_samples}))/(figdata.sound_list(1).samplerate);
    if max_time_lag > maxdur
        max_time_lag = maxdur;
    end
    
    %% update value
    set(max_time_lag_edit_handle, 'string', max_time_lag);
    figdata.param.max_time_lag = max_time_lag;
    
    %% set figure data
    set(gui_interface_fig_handle, 'userdata', figdata);
    
end


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_param_max_freq_lag_edit
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_param_max_freq_lag_edit(gui_interface_fig_tag);

%% get gui figure handle and data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get max freq lag edit handle and current value
max_freq_lag_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['max_freq_lag_edit_uicontrol', mfilename]);
max_freq_lag = str2num(get(max_freq_lag_edit_handle, 'string'));

%% make sure entry is a number
if isempty(max_freq_lag) | max_freq_lag < 0
    
    %% non-numeric or invalid entry, don't change previous value
    set(max_freq_lag_edit_handle, 'string', figdata.param.max_freq_lag);
    
else
    
    %% check value range
    %% using first sample rate should be okay, because sounds should already have been committed
    nyquist = figdata.sound_list(1).samplerate / 2;
    if max_freq_lag > nyquist
        max_freq_lag = nyquist;
    end
    
    %% update value
    set(max_freq_lag_edit_handle, 'string', max_freq_lag);
    figdata.param.max_freq_lag = max_freq_lag;
    
    %% set figure data
    set(gui_interface_fig_handle, 'userdata', figdata);
    
end


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_correlation_run
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_correlation_run(gui_interface_fig_tag);

%% get gui interface figure handle and data
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get status window handle
status_window_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['status_window_edit_uicontrol', mfilename]);

%% check for ui input
if isempty(figdata.sound_list)
    %% no sounds selected
    %% send warning message to status window
    display_status_line(status_window_edit_handle, ' ');
    display_status_line(status_window_edit_handle, 'Unable to run: No sounds selected!');
    display_status_line(status_window_edit_handle, ' ');
    return;
    
elseif isempty(figdata.output)
    %% no output file selected
    %% send warning message to status window
    display_status_line(status_window_edit_handle, ' ');
    display_status_line(status_window_edit_handle, 'Unable to run: No output file selected!');
    display_status_line(status_window_edit_handle, ' ');
    return;
    
end

cb_alternate_view_close(gui_interface_fig_tag);

%% turn off ui controls while running correlations
all_uihandles = findobj(gui_interface_fig_handle, 'type', 'uicontrol');
all_uihandles = [all_uihandles; findobj(gui_interface_fig_handle, 'type', 'uimenu')];
enable_states = get(all_uihandles, 'enable');
set(all_uihandles, 'enable', 'off');

%% set status window inactive only so it looks normal
set(status_window_edit_handle, 'enable', 'inactive');

%% send message to status window that run is starting
display_status_line(status_window_edit_handle, ' ');
display_status_line(status_window_edit_handle, 'Running...');


%% pull out info from the figure userdata and rename for ease of use

outputfile = figdata.output;

%% make sure the output file name ends in '.mat'
if ~strcmpi(outputfile(end-3:end), '.mat')
    outputfile = [outputfile, '.mat'];
end

%% generate sound data, sampling rate and sound name cell arrays
sndData = {figdata.sound_list.data};
sampRate = {figdata.sound_list.samplerate};
sndName = {figdata.sound_list.filename};

%% get sample rate as scalar (all sample rates are the same once sounds are committed)
fs = sampRate{1};

%% set up some variables to be saved in output -mat file
sound_list = rmfield(figdata.sound_list, 'data');
param = figdata.param;

corr_type = figdata.param.corr_type;
corr_std = figdata.param.corr_std;

maxtauLag = figdata.param.max_time_lag;
maxphiLag = figdata.param.max_freq_lag;

loCorner = figdata.param.low_freq;
hiCorner = figdata.param.high_freq;

sig_preproc = figdata.param.sig_preproc;


%% get number of sounds
num_snds = length(figdata.sound_list);

%% de-mean the sound data
sndData = demeansnd(sndData);

%% apply signal preprocessing as requested
sndData = signal_preprocess(sndData, sig_preproc);



if sum(strcmpi(corr_type, {'SPEC: PAIRWISE'; 'SPEC: COLUMN'; 'SPEC: MATRIX CROSS'}))
    %% start spectrogram-based processing: generate and condition the spectrograms
    
    %% send message to status window
    display_status_line(status_window_edit_handle, 'Performing spectrogram correlation:');
    
    fftLen = figdata.param.fft_len;
    dataLen = round(fftLen*figdata.param.data_len/100);
    overlap = round(dataLen*figdata.param.data_overlap/100);
    window = get_taper_func(figdata.param.taper_func);
    window = feval(window, dataLen);
    
    data_form = figdata.param.data_form;
    
    masking_meth = figdata.param.masking_meth;
    masking_param = figdata.param.masking_param;
    mask_adj = figdata.param.mask_adj;
    
    %% don't need this later
    % %% calculate time resolution
    % delta_t = (dataLen - overlap) / figdata.sound_list(1).samplerate;
    
    %% send message to status window
    display_status_line(status_window_edit_handle, 'Generating spectrograms....');
    
    %% generate the spectrograms
    [sndSpec, fvec, tvec] = specgramCA(sndData, fftLen, sampRate, window, overlap, data_form, sndName, status_window_edit_handle);
    
    %% check for errors during spectrogram generation
    [sndSpec, fvec, tvec, figdata] = check4badspecs(sndSpec, fvec, tvec, figdata, gui_interface_fig_handle, all_uihandles, enable_states, status_window_edit_handle);
    
    if isempty(sndSpec)
        return;
    end
    
    %% send message to status window
    display_status_line(status_window_edit_handle, 'Bandlimiting spectrograms...');
    
    % bandlimit the spectrograms
    [sndSpec, fvec] = bandlimspc(sndSpec, fvec, loCorner, hiCorner);
    
    %% send message to status window
    display_status_line(status_window_edit_handle, 'Threshold masking spectrograms...');
    
    % masking the spectrograms as the user likes
    sndSpec = denoisespc_v2(sndSpec, masking_meth, masking_param, data_form, mask_adj);
    
    
    
elseif sum(strcmpi(corr_type, {'TIME: PAIRWISE'; 'TIME: COLUMN'}))
    %% start time waveform processing
    
    %% send message to status window
    display_status_line(status_window_edit_handle, 'Performing time waveform correlation:');
    
    %% not used later
    %   %% calculate time resolution for sound waveforms
    %   delta_t = 1/figdata.sound_list(1).samplerate;
    
    %% send message to status window
    display_status_line(status_window_edit_handle, 'Bandlimiting time waveforms...');
    
    %% bandlimit the waveforms
    [sndData] = bandlimsnd(sndData, sampRate, loCorner, hiCorner);
    
    
    
elseif sum(strcmpi(corr_type, {'COMPLEX ENV: PAIRWISE'; 'COMPLEX ENV: COLUMN'}))
    %% start complex envelope processing
    
    %% send message to status window
    display_status_line(status_window_edit_handle, 'Performing complex envelope correlation:');
    
    
    
else
    %% unrecognized correlation mode
    
    %% send message to status window
    display_status_line(status_window_edit_handle, 'Correlation type not recognized : run terminating.');
    return;
    
end %% if sum(strcmpi(corr_type, {'SPEC: PAIRWISE'; 'SPEC: COLUMN'; 'SPEC: MATRIX CROSS'}))






corr_type = upper(corr_type);

%% perform the correlations
switch (corr_type)
    
    
    case {'SPEC: PAIRWISE'; 'SPEC: COLUMN'}
        
        %% send message to status window
        display_status_line(status_window_edit_handle, 'Correlating spectrograms...');
        
        %% get meat of corr_type string
        corr_type2 = corr_type(7:end);
        
        if ~maxphiLag
            
            %% correlate spectrograms with time lag only, and get peaks
            [f0Peaks] = correlateCA_quick(sndSpec, tvec, corr_std, maxtauLag, corr_type2, status_window_edit_handle);
            
            
            %% display correlation matrix in alternate view
            alternate_view_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_uicontrol', mfilename]);
            alternate_view_text_label = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_text_label', mfilename]);
            
            set(alternate_view_uicontrol, 'string', num2str(f0Peaks{1}));
            set(alternate_view_text_label, 'string', ['f0Peak Values for ''', basenameof(outputfile), '''']);
            
            %       alternate_view_pco_uimenu = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_pco_uimenu', mfilename]);
            %       alternate_view_template_rank_uimenu = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_template_rank_uimenu', mfilename]);
            %
            %       if strcmpi(corr_type, 'SPEC: PAIRWISE')
            %         set(alternate_view_pco_uimenu, 'enable', 'on');
            %         set(alternate_view_template_rank_uimenu, 'enable', 'on');
            %       else
            %         set(alternate_view_pco_uimenu, 'enable', 'off');
            %         set(alternate_view_template_rank_uimenu, 'enable', 'off');
            %       end
            
            
            
            %% send message to status window
            display_status_line(status_window_edit_handle, 'Saving results...');
            
            %% save the results
            corr_data.sound_list = sound_list;
            corr_data.param = param;
            corr_data.f0Peaks.peakVal = f0Peaks{1};
            corr_data.f0Peaks.peakLag = f0Peaks{2};
            corr_data.figdata = figdata;
            
            varName = [removeillegalchar(basenameof(outputfile)), '_CorrData'];
            eval([varName,' = corr_data;']);
            save(outputfile, sprintf(varName), '-mat');
            
            %       %% save correlation output in alternate view userdata
            %       set(alternate_view_uicontrol, 'userdata', varName);
            
        else
            
            %% correlate spectrograms with time and frequency lags, and get peaks
            [absPeaks, f0Peaks] = correlateCA_full(sndSpec, tvec, fvec, corr_std, maxtauLag, maxphiLag, corr_type2, status_window_edit_handle);
            
            
            %% display correlation matrix in alternate view
            alternate_view_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_uicontrol', mfilename]);
            alternate_view_text_label = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_text_label', mfilename]);
            
            set(alternate_view_uicontrol, 'string', num2str(absPeaks{1}));
            set(alternate_view_text_label, 'string', ['absPeak Values for ''', basenameof(outputfile), '''']);
            
            %       alternate_view_pco_uimenu = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_pco_uimenu', mfilename]);
            %       alternate_view_template_rank_uimenu = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_template_rank_uimenu', mfilename]);
            %
            %       if strcmpi(corr_type, 'SPEC: PAIRWISE')
            %         set(alternate_view_pco_uimenu, 'enable', 'on');
            %         set(alternate_view_template_rank_uimenu, 'enable', 'on');
            %       else
            %         set(alternate_view_pco_uimenu, 'enable', 'off');
            %         set(alternate_view_template_rank_uimenu, 'enable', 'off');
            %       end
            
            
            %% XXXX NOTE FOR LATER WORK: SET UP TO DISPLAY CORRELATION MATRIX IN ALTERNATE VIEW
            %% SINCE FREQUENCY LAG IS ALSO RUN, GIVE USER CHOICE OF SHOWING F_0 MATRIX OR F_LAG MATRIX
            
            
            %% send message to status window
            display_status_line(status_window_edit_handle, 'Saving results...');
            
            %% save the results
            corr_data.sound_list = sound_list;
            corr_data.param = param;
            corr_data.f0Peaks.peakVal = f0Peaks{1};
            corr_data.f0Peaks.peakLag = f0Peaks{2};
            corr_data.absPeaks.peakVal = absPeaks{1};
            corr_data.absPeaks.peakTimeLag = absPeaks{2};
            corr_data.absPeaks.peakFreqLag = absPeaks{3};
            
            varName = [removeillegalchar(basenameof(outputfile)), '_CorrData'];
            eval([varName,' = corr_data;']);
            save(outputfile, sprintf(varName), '-mat');
            
        end
        
        
        
        %%%%%%%%%%%%%%%%%%%%%% XXXX START HERE  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    case ('SPEC: MATRIX CROSS')
        
        %% send message to status window
        display_status_line(status_window_edit_handle, 'Correlating spectrograms...');
        
        %% perform a spectrogram matrix cross, get correlation peaks for time lag only, and profiles for peak diagonals
        [diagProf, f0Peaks] = matrixCross_peakProfileCA(sndSpec, tvec, corr_std, maxtauLag, sndName, pathof(outputfile), status_window_edit_handle);
        
        
        %% display correlation matrix in alternate view
        alternate_view_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_uicontrol', mfilename]);
        alternate_view_text_label = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_text_label', mfilename]);
        
        set(alternate_view_uicontrol, 'string', num2str(f0Peaks{1}));
        set(alternate_view_text_label, 'string', ['f0Peak Values for ''', basenameof(outputfile), '''']);
        
        %       alternate_view_pco_uimenu = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_pco_uimenu', mfilename]);
        %       alternate_view_template_rank_uimenu = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_template_rank_uimenu', mfilename]);
        %
        %       if strcmpi(corr_type, 'SPEC: PAIRWISE')
        %         set(alternate_view_pco_uimenu, 'enable', 'on');
        %         set(alternate_view_template_rank_uimenu, 'enable', 'on');
        %       else
        %         set(alternate_view_pco_uimenu, 'enable', 'off');
        %         set(alternate_view_template_rank_uimenu, 'enable', 'off');
        %       end
        
        
        
        %% send message to status window
        display_status_line(status_window_edit_handle, 'Saving results...');
        
        %% force max frequency lag to zero in param list, since this is true for the matrix cross
        param.max_freq_lag = 0;
        
        %% save the results
        corr_data.sound_list = sound_list;
        corr_data.param = param;
        corr_data.f0Peaks.peakVal = f0Peaks{1};
        corr_data.f0Peaks.peakLag = f0Peaks{2};
        corr_data.diagProf = diagProf;
        
        varName = [removeillegalchar(basenameof(outputfile)), '_CorrData'];
        eval([varName,' = corr_data;']);
        save(outputfile, sprintf(varName), '-mat');
        
        
        
        
    case ('TIME: PAIRWISE')
        %% do nothing yet
        
        %% show not working message in status window
        txt_message = sprintf('Correlation Option ''%s'' not implemented yet!', corr_type);
        display_status_line(status_window_edit_handle, txt_message);
        
        
        %% display correlation matrix in alternate view
        alternate_view_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_uicontrol', mfilename]);
        alternate_view_text_label = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_text_label', mfilename]);
        
        set(alternate_view_uicontrol, 'string', ' ');
        set(alternate_view_text_label, 'string', ['Peak Values for ''', basenameof(outputfile), '''']);
        
        %     alternate_view_pco_uimenu = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_pco_uimenu', mfilename]);
        %     alternate_view_template_rank_uimenu = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_template_rank_uimenu', mfilename]);
        %
        %     if strcmpi(corr_type, 'SPEC: PAIRWISE')
        %       set(alternate_view_pco_uimenu, 'enable', 'on');
        %       set(alternate_view_template_rank_uimenu, 'enable', 'on');
        %     else
        %       set(alternate_view_pco_uimenu, 'enable', 'off');
        %       set(alternate_view_template_rank_uimenu, 'enable', 'off');
        %     end
        
        
        %     %% force spectrogram related parameters to NA (non-applicable) since these don't apply to time correlations
        %     param.fft_len = 'NA';
        %     param.data_len = 'NA';
        %     param.taper_func = 'NA';
        %     param.data_overlap = 'NA';
        %     param.masking_meth = 'NA';
        %     param.masking_param = 'NA';
        %     param.max_freq_lag = 'NA';
        %
        %
        %         %% save the results
        %     corr_data.sound_list = sound_list;
        %     corr_data.param = param;
        %
        %     varName = [removeillegalchar(basenameof(outputfile)), '_CorrData'];
        %     eval([varName,' = corr_data;']);
        %     save(outputfile, sprintf(varName), '-mat');
        %
        %     %% save workspace variables in a -mat file
        %     save(outputfile, '', '', '', '', '', 'sound_list', 'param', '-mat');
        
        
    case ('TIME: COLUMN')
        %% do nothing yet
        
        %% show not working message in status window
        txt_message = sprintf('Correlation Option ''%s'' not implemented yet!', corr_type);
        display_status_line(status_window_edit_handle, txt_message);
        
        %% display correlation matrix in alternate view
        alternate_view_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_uicontrol', mfilename]);
        alternate_view_text_label = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_text_label', mfilename]);
        
        set(alternate_view_uicontrol, 'string', ' ');
        set(alternate_view_text_label, 'string', ['Peak Values for ''', basenameof(outputfile), '''']);
        
        %     alternate_view_pco_uimenu = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_pco_uimenu', mfilename]);
        %     alternate_view_template_rank_uimenu = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_template_rank_uimenu', mfilename]);
        %
        %     if strcmpi(corr_type, 'SPEC: PAIRWISE')
        %       set(alternate_view_pco_uimenu, 'enable', 'on');
        %       set(alternate_view_template_rank_uimenu, 'enable', 'on');
        %     else
        %       set(alternate_view_pco_uimenu, 'enable', 'off');
        %       set(alternate_view_template_rank_uimenu, 'enable', 'off');
        %     end
        
        %     %% force spectrogram related parameters to NA (non-applicable) since these don't apply to time correlations
        %     param.fft_len = 'NA';
        %     param.data_len = 'NA';
        %     param.taper_func = 'NA';
        %     param.data_overlap = 'NA';
        %     param.masking_meth = 'NA';
        %     param.masking_param = 'NA';
        %     param.max_freq_lag = 'NA';
        %
        %
        %         %% save the results
        %     corr_data.sound_list = sound_list;
        %     corr_data.param = param;
        %
        %     varName = [removeillegalchar(basenameof(outputfile)), '_CorrData'];
        %     eval([varName,' = corr_data;']);
        %     save(outputfile, sprintf(varName), '-mat');
        %
        %     %% save workspace variables in a -mat file
        %     save(outputfile, '', '', '', '', '', 'sound_list', 'param', '-mat');
        
        
        %% this is no longer an option here
        %   case ('TIME: ALIGN')
        %     %% do nothing yet
        %
        %     %% show not working message in status window
        %     txt_message = sprintf('Correlation Option ''%s'' not implemented yet!', corr_type);
        %     display_status_line(status_window_edit_handle, txt_message);
        %
        %     %     %% force spectrogram related parameters to NA (non-applicable) since these don't apply to time correlations
        %     %     param.fft_len = 'NA';
        %     %     param.data_len = 'NA';
        %     %     param.taper_func = 'NA';
        %     %     param.data_overlap = 'NA';
        %     %     param.masking_meth = 'NA';
        %     %     param.masking_param = 'NA';
        %     %     param.max_freq_lag = 'NA';
        %     %
        %     %     %% save workspace variables in a -mat file
        %     %     save(outputfile, '', '', '', '', '', 'sound_list', 'param', '-mat');
        
    case ('COMPLEX ENV: PAIRWISE')
        %% do not
        
        %% show not working message in status window
        txt_message = sprintf('Correlation Option ''%s'' not implemented yet!', corr_type);
        display_status_line(status_window_edit_handle, txt_message);
        
        %% display correlation matrix in alternate view
        alternate_view_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_uicontrol', mfilename]);
        alternate_view_text_label = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_text_label', mfilename]);
        
        set(alternate_view_uicontrol, 'string', ' ');
        set(alternate_view_text_label, 'string', ['Peak Values for ''', basenameof(outputfile), '''']);
        
        %     alternate_view_pco_uimenu = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_pco_uimenu', mfilename]);
        %     alternate_view_template_rank_uimenu = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_template_rank_uimenu', mfilename]);
        %
        %     if strcmpi(corr_type, 'SPEC: PAIRWISE')
        %       set(alternate_view_pco_uimenu, 'enable', 'on');
        %       set(alternate_view_template_rank_uimenu, 'enable', 'on');
        %     else
        %       set(alternate_view_pco_uimenu, 'enable', 'off');
        %       set(alternate_view_template_rank_uimenu, 'enable', 'off');
        %     end
        
        %     %% force spectrogram related parameters to NA (non-applicable) since these don't apply to time correlations
        %     param.fft_len = 'NA';
        %     param.data_len = 'NA';
        %     param.taper_func = 'NA';
        %     param.data_overlap = 'NA';
        %     param.masking_meth = 'NA';
        %     param.masking_param = 'NA';
        %     param.max_freq_lag = 'NA';
        %
        %
        %         %% save the results
        %     corr_data.sound_list = sound_list;
        %     corr_data.param = param;
        %
        %     varName = [removeillegalchar(basenameof(outputfile)), '_CorrData'];
        %     eval([varName,' = corr_data;']);
        %     save(outputfile, sprintf(varName), '-mat');
        %
        %     %% save workspace variables in a -mat file
        %     save(outputfile, '', '', '', '', '', 'sound_list', 'param', '-mat');
        
    case ('COMPLEX ENV: COLUMN')
        %% do nothing yet
        
        %% show not working message in status window
        txt_message = sprintf('Correlation Option ''%s'' not implemented yet!', corr_type);
        display_status_line(status_window_edit_handle, txt_message);
        
        %% display correlation matrix in alternate view
        alternate_view_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_uicontrol', mfilename]);
        alternate_view_text_label = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_text_label', mfilename]);
        
        set(alternate_view_uicontrol, 'string', ' ');
        set(alternate_view_text_label, 'string', ['Peak Values for ''', basenameof(outputfile), '''']);
        
        %     alternate_view_pco_uimenu = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_pco_uimenu', mfilename]);
        %     alternate_view_template_rank_uimenu = findobj(gui_interface_fig_handle, 'tag', ['alternate_view_template_rank_uimenu', mfilename]);
        %
        %     if strcmpi(corr_type, 'SPEC: PAIRWISE')
        %       set(alternate_view_pco_uimenu, 'enable', 'on');
        %       set(alternate_view_template_rank_uimenu, 'enable', 'on');
        %     else
        %       set(alternate_view_pco_uimenu, 'enable', 'off');
        %       set(alternate_view_template_rank_uimenu, 'enable', 'off');
        %     end
        
        %     %% force spectrogram related parameters to NA (non-applicable) since these don't apply to time correlations
        %     param.fft_len = 'NA';
        %     param.data_len = 'NA';
        %     param.taper_func = 'NA';
        %     param.data_overlap = 'NA';
        %     param.masking_meth = 'NA';
        %     param.masking_param = 'NA';
        %     param.max_freq_lag = 'NA';
        %
        %
        %         %% save the results
        %     corr_data.sound_list = sound_list;
        %     corr_data.param = param;
        %
        %     varName = [removeillegalchar(basenameof(outputfile)), '_CorrData'];
        %     eval([varName,' = corr_data;']);
        %     save(outputfile, sprintf(varName), '-mat');
        %
        %     %% save workspace variables in a -mat file
        %     save(outputfile, '', '', '', '', '', 'sound_list', 'param', '-mat');
        
        %% this is no longer an option here
        %  case ('COMPLEX ENV: ALIGN')
        %     %% do nothing yet
        %
        %     %% show not working message in status window
        %     txt_message = sprintf('Correlation Option ''%s'' not implemented yet!', corr_type);
        %     display_status_line(status_window_edit_handle, txt_message);
        %
        %     %     %% force spectrogram related parameters to NA (non-applicable) since these don't apply to time correlations
        %     %     param.fft_len = 'NA';
        %     %     param.data_len = 'NA';
        %     %     param.taper_func = 'NA';
        %     %     param.data_overlap = 'NA';
        %     %     param.masking_meth = 'NA';
        %     %     param.masking_param = 'NA';
        %     %     param.max_freq_lag = 'NA';
        %     %
        %     %     %% save workspace variables in a -mat file
        %     %     save(outputfile, '', '', '', '', '', 'sound_list', 'param', '-mat');
        
        
end


% correlations done
%% send message to status window
display_status_line(status_window_edit_handle, 'Run Done.');
display_status_line(status_window_edit_handle, 'Ready...')
display_status_line(status_window_edit_handle, ' ')

%% reset controls, and exit
for inx = 1:length(all_uihandles)
    set(all_uihandles(inx), 'enable', enable_states{inx});
end


cb_alternate_view_open(gui_interface_fig_tag);

% end the function
end







%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_analysis_pco
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_analysis_pco(gui_interface_fig_tag);

%% get gui interface figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get most recent analysis path
analysis_path = figdata.last_analysis_path;

%% check that analysis exists
if ~exist(analysis_path)
    analysis_path = [pwd, filesep];
end

%% get output file name
%%% Modified by SCK
% [analysis_file, analysis_path] = uigetfile('*.mat', 'Select Pairwise Correlation Data File', analysis_path);
[analysis_file, analysis_path] = uigetfiles_SCK('*.mat', 'Select Pairwise Correlation Data File', analysis_path);

if isempty(analysis_path)
    %% cancel requested
    return;
end

%% save most recent analysis path
% set(analysis_uihandle, 'userdata', analysis_path);
figdata.last_analysis_path = analysis_path;

%% save figure data
set(gui_interface_fig_handle, 'userdata', figdata);


%% get status window handle
status_window_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['status_window_edit_uicontrol', mfilename]);

%% send a message
display_status_line(status_window_edit_handle, ' ');
display_status_line(status_window_edit_handle, 'Performing PCO analysis...');

try
    corr_data = load('-mat', [analysis_path, analysis_file]);
    %% get to the rootfield
    corr_data_fieldnames = fieldnames(corr_data);
    corr_data = corr_data.(corr_data_fieldnames{1});
    
catch
    % uable to load file, send an error message
    display_status_line(status_window_edit_handle, sprintf('File "%s" did not open.', analysis_file));
    display_status_line(status_window_edit_handle, 'Unable to perform analysis.');
    return;
    
end


if isfield(corr_data, 'f0Peaks')
    %% is a correlation data file
    
    %% check that f0Peaks is square, so has pairwise data
    dim = size(corr_data.f0Peaks.peakVal);
    
    if dim(1) ~= dim(2) | dim(1) == 1
        %% not pairwise correlation data, send an error message
        display_status_line(status_window_edit_handle, sprintf('File ''%s'' is not a pairwise correlation file.', analysis_file));
        display_status_line(status_window_edit_handle, 'Unable to perform analysis.');
        
    else
        %% do PCO analysis for f0Peaks
        data_type = 'similarity';
        outputfilename = [analysis_path, basenameof(analysis_file), '_f0Peaks'];
        object_names = {corr_data.sound_list.filename}';
        pco2save = dim(1);
        
        [PCOcoord_f0Peaks, eigen_f0Peaks, cumfit_f0Peaks] = PCO(corr_data.f0Peaks.peakVal, data_type, pco2save, outputfilename, object_names);
        
        corr_data.f0Peaks_PCO.coord = PCOcoord_f0Peaks;
        corr_data.f0Peaks_PCO.eigen = eigen_f0Peaks;
        corr_data.f0Peaks_PCO.cumfit = cumfit_f0Peaks;
        
        if isfield(corr_data, 'absPeaks')
            %% do PCO analysis for absPeaks
            outputfilename = [analysis_path, basenameof(analysis_file), '_absPeaks'];
            
            [PCOcoord_absPeaks, eigen_absPeaks, cumfit_absPeaks] = PCO(corr_data.absPeaks.peakVal, data_type, pco2save, outputfilename, object_names);
            
            corr_data.absPeaks_PCO.coord = PCOcoord_absPeaks;
            corr_data.absPeaks_PCO.eigen = eigen_absPeaks;
            corr_data.absPeaks_PCO.cumfit = cumfit_absPeaks;
            
        end
        
        outputfilename = [analysis_path, basenameof(analysis_file), '_PCO.mat'];
        
        %% save workspace variables in a -mat file
        varName = [removeillegalchar(basenameof(analysis_file)), '_PCOData'];
        eval([varName,' = corr_data;']);
        save(outputfilename, sprintf(varName), '-mat');
        
        display_status_line(status_window_edit_handle, 'Done.');
        display_status_line(status_window_edit_handle, 'Ready...')
        display_status_line(status_window_edit_handle, ' ')
        
    end
    
    
else
    %% not a correlation data file, send an error message
    display_status_line(status_window_edit_handle, sprintf('File ''%s'' is not a correlation data file.', analysis_file));
    display_status_line(status_window_edit_handle, 'Unable to perform analysis.');
    
end


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_analysis_template_rank
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_analysis_template_rank(gui_interface_fig_tag);

%% get gui interface figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get most recent analysis path
try analysis_path = figdata.last_analysis_path;
catch err;
end
%% check that analysis exists
if ~exist(analysis_path)
    analysis_path = [pwd, filesep];
end


%% Make separate GUI window for Template Rank function
[analysis_file, analysis_path, xbat_log_name, xbat_log_dir, xbat_sound_name, xbat_sound_dir, xbat_preset_name, xbat_preset_size, xbat_preset_dir,use_same_log, diff_log_name, diff_log_dir, go] = template_rank_gui();


%% get output file name
%[analysis_file, analysis_path] = uigetfile('C:\', 'Select Pairwise
%Correlation Data File', analysis_path);

if ~go
    %% cancel requested
    return;
end

%% save most recent analysis path
figdata.last_analysis_path = analysis_path;

%% save figure data
set(gui_interface_fig_handle, 'userdata', figdata);

%% get status window handle
status_window_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['status_window_edit_uicontrol', mfilename]);

%% send a message
display_status_line(status_window_edit_handle, ' ');
display_status_line(status_window_edit_handle, 'Performing Template Rank analysis...');


try
    corr_data = load('-mat', [analysis_path, analysis_file]);
    %% get to the rootfield
    corr_data_fieldnames = fieldnames(corr_data);
    corr_data = corr_data.(corr_data_fieldnames{1});
        
catch
    % uable to load file, send an error message
    display_status_line(status_window_edit_handle, sprintf('File "%s" did not open.', analysis_file));
    display_status_line(status_window_edit_handle, 'Unable to perform analysis.');
    return;
    
end

try figdata = corrdata.figdata;
catch
end

if isfield(corr_data, 'f0Peaks')
    %% is a correlation data file
    
    %% check that f0Peaks is square, so has pairwise data
    dim = size(corr_data.f0Peaks.peakVal);
    
    if dim(1) ~= dim(2) | dim(1) == 1
        %% not pairwise correlation data, send an error message
        display_status_line(status_window_edit_handle, sprintf('File ''%s'' is not a pairwise correlation file.', analysis_file));
        display_status_line(status_window_edit_handle, 'Unable to perform analysis.');
        
    else
        %% do template rank analysis for f0Peaks
        corr_mat = corr_data.f0Peaks.peakVal;
        sndNames = {corr_data.sound_list.filename}';
        
        outputfilename = [analysis_path, basenameof(analysis_file), '_f0Peaks_templaterank.txt'];
        fid = fopen(outputfilename, 'w');
        
        if fid == -1
            display_status_line(status_window_edit_handle, sprintf('Unable to open file ''s'' for writing.', outputfilename));
            display_status_line(status_window_edit_handle, 'Unable to continue analysis.');
            return;
        end
        
        [ordered_sndNames, ordered_inx, ordered_corr_rows, ordered_adj_corr_rows] = pick_template_set_v2(corr_mat, sndNames);
        
        corr_data.f0Peaks_templateRank.sndNames_ordered = ordered_sndNames;
        corr_data.f0Peaks_templateRank.peakVal_ordered = ordered_corr_rows;
        corr_data.f0Peaks_templateRank.adjpeakVal_ordered = ordered_adj_corr_rows;
        corr_data.f0Peaks_templateRank.order_inx = ordered_inx;
        
        
        fprintf(fid, 'sndName\tabsAvgCorrSum\tadjAvgCorrSum\n');
        
        tempMat = [ordered_sndNames; num2cell(sum(ordered_corr_rows, 2)/dim(2)); num2cell(sum(ordered_adj_corr_rows, 2)/dim(2))];
        tempMat = reshape(tempMat, length(ordered_sndNames), 3);
        tempMat = tempMat';
        
        format_Str = ['%s\t%24.12f\t%24.12f\n'];
        fprintf(fid, format_Str, tempMat{:});
        fclose(fid);
        
        
        
        if isfield(corr_data, 'absPeaks')
            %% do template rank analysis for absPeaks
            corr_mat = corr_data.absPeaks.peakVal;
            
            outputfilename = [analysis_path, basenameof(analysis_file), '_absPeaks_templaterank.txt'];
            fid = fopen(outputfilename, 'w');
            
            if fid == -1
                display_status_line(status_window_edit_handle, sprintf('Unable to open file ''s'' for writing.', outputfilename));
                display_status_line(status_window_edit_handle, 'Unable to continue analysis.');
                return;
            end
            
            [ordered_sndNames, ordered_inx, ordered_corr_rows, ordered_adj_corr_rows] = pick_template_set_v2(corr_mat, sndNames);
            
            corr_data.absPeaks_templateRank.sndNames_ordered = ordered_sndNames;
            corr_data.absPeaks_templateRank.peakVal_ordered = ordered_corr_rows;
            corr_data.absPeaks_templateRank.adjpeakVal_ordered = ordered_adj_corr_rows;
            corr_data.absPeaks_templateRank.order_inx = ordered_inx;
            
            fprintf(fid, 'sndName\tabsAvgCorrSum\tadjAvgCorrSum\n');
            
            tempMat = [ordered_sndNames; num2cell(sum(ordered_corr_rows, 2)/dim(2)); num2cell(sum(ordered_adj_corr_rows, 2)/dim(2))];
            tempMat = reshape(tempMat, length(ordered_sndNames), 3);
            tempMat = tempMat';
            
            format_Str = ['%s\t%24.12f\t%24.12f\n'];
            fprintf(fid, format_Str, tempMat{:});
            fclose(fid);
            
        end
        
        outputfilename = [analysis_path, basenameof(analysis_file), '_TemplateRank.mat'];
        
        %% save workspace variables in a -mat file
        varName = [removeillegalchar(basenameof(analysis_file)), '_TemplateRankData',];
        eval([varName,' = corr_data;']);
        save(outputfilename, sprintf(varName), '-mat');
        
        display_status_line(status_window_edit_handle, 'Done.');
        display_status_line(status_window_edit_handle, 'Ready...')
        display_status_line(status_window_edit_handle, ' ')
        
        %% See if user wants to save XBAT log with results -  now getting this info from GUI
        % [xbat_log_dir] = uigetdir('*.mat', 'Select directory where XBAT log will be saved');
        %  [xbat_log_name, xbat_log_dir] = uiputfile('*.*', 'Select XBAT log name and directory where log will be saved',figdata.log_path);
        
        %% See if user wants to save XBAT log with results -  now getting this info from GUI
        % [xbat_preset_name, xbat_preset_dir] = uiputfile('*.*', 'Select XBAT preset name and directory where preset will be saved',figdata.log_path);
        % [xbat_preset_name, xbat_preset_dir] = uiputfile('*.*', 'Select XBAT preset name and directory where preset will be saved','C:\XBAT_R5\Extensions\Detectors\Sound\Data Template\Presets');
        
        %% For now only make log if single log was input
        if not(isempty(xbat_log_dir))
            
            if (length(unique(figdata.log_ixes)) > 1) %|| (figdata.log_flag == 1) && (figdata.clip_flag == 1)
                display_status_line(status_window_edit_handle, sprintf('XBAT log not created because clips come from multiple sounds'));
                
                % Code for creating new sound files for log--leave blank for
                % now
                %             %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %             %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %             display_status_line(status_window_edit_handle, sprintf('Creating new XBAT log with selected templates'));
                %
                %             ranked_log = PRBA_log_create();
                %             ranked_log.length = length(ordered_inx);
                %             ranked_log.curr_id = length(ordered_inx) + 1;
                %             ranked_log.file = xbat_log_name;
                %             ranked_log.sound.type = 'file stream';
                %             ranked_log.sound.format =  'aif'; %figdata.sound_list(1,1).filename(end-2:end);
                %             ranked_log.sound.path = xbat_log_dir;
                %             ranked_log.sound.file = cell(1,1);
                %             ranked_log.sound.info.bytes = [];
                %             ranked_log.sound.input = []; ranked_log.sound.output = [];
                %             ranked_log.sound.samplerate = figdata.sound_list(1,1).samplerate;
                %             ranked_log.sound.realtime = []; ranked_log.sound.timestamp = [];
                %             ranked_log.sound.channels = 1;  ranked_log.sound.calibration = [];  ranked_log.sound.geometry = [];  ranked_log.sound.speed = [];
                %             ranked_log.sound.attributes = [];  ranked_log.sound.samplesize = [];  ranked_log.sound.view = [];  ranked_log.sound.specgram = [];
                %             ranked_log.sound.created = now;  ranked_log.sound.modified = now; ranked_log.sound.notes = []; ranked_log.sound.tags = []; ranked_log.sound.userdata = [];
                %
                %             ranked_log.path = xbat_log_dir;
                %             ranked_log.author = 'SoundXT';
                %
                %             ranked_cnt = 0; cum_time = 0; sum_samples = 0;
                %             ranked_log.event = event_create();
                %
                %             for x = 1:length(figdata.sound_list)
                %
                %                 %if this template was inlcuded in ranked list
                %                 find_ix = find(ordered_inx == x);
                %                 if not(isempty(find_ix))
                %
                %                     ranked_cnt = ranked_cnt + 1;
                %                     % find length in seconds
                %                     len = (figdata.sound_list(1,ranked_cnt).num_samples / figdata.sound_list(1,ranked_cnt).samplerate) / 60;
                %                     ranked_log.event(1,ranked_cnt) = event_create;
                %
                %                     ranked_log.event(1,ranked_cnt).id = ranked_cnt;
                %                     ranked_log.event(1,ranked_cnt).tags = cellstr(num2str(find_ix));
                %                     % zero pad on either side
                %                     cum_time = cm_time + .2;
                %                     ranked_log.event(1,ranked_cnt).time = [cum_time (cum_time + len)];
                %                     cum_time = cum_time + len + .2;
                %                     ranked_log.event(1,ranked_cnt).freq = [0 (figdata.sound_list(1,ranked_cnt).samplerate / 2)];
                %                     ranked_log.event(1,ranked_cnt).bandwidth = (figdata.sound_list(1,ranked_cnt).samplerate / 2);
                %                     ranked_log.event(1,ranked_cnt).duration = len;
                %                     ranked_log.event(1,ranked_cnt).channel = 1; % just leave at 1 for now
                %                     ranked_log.event(1,ranked_cnt).level = 1;
                %                     ranked_log.event(1,ranked_cnt).author = 'SoundXT';
                %                     ranked_log.sound.samples(ranked_cnt,1) = figdata.sound_list(1,find_ix).num_samples;
                %                     cum_samples = sum_samples + figdata.sound_list(1,find_ix).num_samples;
                %                     ranked_log.sound.cumulative = cum_samples;
                %                     ranked_log.sound.file{ranked_cnt} = figdata.sound_list(1,ranked_cnt).filename;
                %
                %                     % save sound file to output folder
                %                     current_dir = pwd;
                %                     cd(xbat_log_dir)
                %                     FS = figdata.sound_list(1,ranked_cnt).samplerate;
                %                     aiffwrite(['event_' num2str(ranked_cnt) '.aif'], [zeros(2*FS/10,1); figdata.sound_list(1,find_ix).data; zeros(2*FS/10,1)], FS)
                %                     cd(pwd)
                %                 end
                %             end
                %             ranked_log.sound.duration = cum_time;
                %
                %             % Save log to user selected directory
                %             log_saved = PRBA_log_save(ranked_log);
                %             if not(log_saved)
                %                 % disp('Error: XBAT log not saved')
                %                 display_status_line(status_window_edit_handle, sprintf('Error: XBAT log not saved'));
                %             else
                %                 % disp('New XBAT log saved')
                %                 display_status_line(status_window_edit_handle, sprintf('New XBAT log saved.\n'));
                %                 display_status_line(status_window_edit_handle, sprintf(''));
                %             end
                %
                %             %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %             %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            else
                if figdata.log_flag == 1 && figdata.clip_flag == 0  %isfield(figdata, 'log_path')
                    
                    display_status_line(status_window_edit_handle, sprintf('Creating new XBAT log with selected templates'));
                    
                    ranked_log = PRBA_log_create();
                    ranked_log.length = length(ordered_inx);
                    ranked_log.curr_id = length(ordered_inx) + 1;
                    ranked_log.file = xbat_log_name;
                    ranked_log.sound = figdata.log.sound;
                    ranked_log.path = xbat_log_dir;
                    ranked_log.author = 'SoundXT';
                    
                    ranked_cnt = 0;
                    ranked_log.event = event_create();
                    
                    for x = 1:length(figdata.sound_list)
                        
                        %if this template was inlcuded in ranked list
                        find_ix = find(ordered_inx == x);
                        if not(isempty(find_ix))
                            
                            ranked_cnt = ranked_cnt + 1;
                            ranked_log.event(1,ranked_cnt) = event_create;
                            
                            ranked_log.event(1,ranked_cnt).id = ranked_cnt;
                            ranked_log.event(1,ranked_cnt).tags = cellstr(num2str(find_ix));
                            ranked_log.event(1,ranked_cnt).time = figdata.sound_list(x).time;
                            %  ranked_log.event(1,ranked_cnt).duration = figdata.sound_list(x).time(2) - figdata.sound_list(x).time(1);
                            ranked_log.event(1,ranked_cnt).freq = figdata.sound_list(x).freq;
                            ranked_log.event(1,ranked_cnt).bandwidth = figdata.sound_list(x).bandwidth;
                            ranked_log.event(1,ranked_cnt).duration = figdata.sound_list(x).duration;
                            ranked_log.event(1,ranked_cnt).channel = figdata.sound_list(x).channel;
                            ranked_log.event(1,ranked_cnt).level = figdata.sound_list(x).level;
                            ranked_log.event(1,ranked_cnt).author = 'SoundXT';
                            
                        end
                    end
                    
                    % Save log to user selected directory
                    log_saved = PRBA_log_save(ranked_log);
                    if not(log_saved)
                        % disp('Error: XBAT log not saved')
                        display_status_line(status_window_edit_handle, sprintf('Error: XBAT log not saved'));
                    else
                        % disp('New XBAT log saved')
                        display_status_line(status_window_edit_handle, sprintf('New XBAT log saved.\n'));
                    end
                    
                    % also make XBAT log if input was sound files only or sound files and log
                else
                    display_status_line(status_window_edit_handle, sprintf('Creating new XBAT log with selected templates'));
                    
                    ranked_log = PRBA_log_create();
                    ranked_log.length = length(ordered_inx);
                    ranked_log.curr_id = length(ordered_inx) + 1;
                    ranked_log.file = xbat_log_name;
                    ranked_log.sound.type = 'file stream';
                    ranked_log.sound.format =  figdata.sound_list(1,1).filename(end-2:end);
                    ranked_log.sound.path = xbat_log_dir;
                    ranked_log.sound.file = cell(1,1);
                    ranked_log.sound.info.bytes = [];
                    ranked_log.sound.input = []; ranked_log.sound.output = [];
                    ranked_log.sound.samplerate = figdata.sound_list(1,1).samplerate;
                    ranked_log.sound.realtime = []; ranked_log.sound.timestamp = [];
                    ranked_log.sound.channels = 1;  ranked_log.sound.calibration = [];  ranked_log.sound.geometry = [];  ranked_log.sound.speed = [];
                    ranked_log.sound.attributes = [];  ranked_log.sound.samplesize = [];  ranked_log.sound.view = [];  ranked_log.sound.specgram = [];
                    ranked_log.sound.created = now;  ranked_log.sound.modified = now; ranked_log.sound.notes = []; ranked_log.sound.tags = []; ranked_log.sound.userdata = [];
                    
                    ranked_log.path = xbat_log_dir;
                    ranked_log.author = 'SoundXT';
                    
                    ranked_cnt = 0; cum_time = 0; sum_samples = 0;
                    ranked_log.event = event_create();
                    
                    for x = 1:length(figdata.sound_list)
                        
                        %if this template was inlcuded in ranked list
                        find_ix = find(ordered_inx == x);
                        if not(isempty(find_ix))
                            
                            ranked_cnt = ranked_cnt + 1;
                            % find length in seconds
                            len = (figdata.sound_list(1,ranked_cnt).num_samples / figdata.sound_list(1,ranked_cnt).samplerate) / 60;
                            ranked_log.event(1,ranked_cnt) = event_create;
                            
                            ranked_log.event(1,ranked_cnt).id = ranked_cnt;
                            ranked_log.event(1,ranked_cnt).tags = cellstr(num2str(find_ix));
                            ranked_log.event(1,ranked_cnt).time = [cum_time (cum_time + len)];
                            cum_time = cum_time + len;
                            ranked_log.event(1,ranked_cnt).freq = [0 (figdata.sound_list(1,ranked_cnt).samplerate / 2)];
                            ranked_log.event(1,ranked_cnt).bandwidth = (figdata.sound_list(1,ranked_cnt).samplerate / 2);
                            ranked_log.event(1,ranked_cnt).duration = len;
                            ranked_log.event(1,ranked_cnt).channel = 1; % just leave at 1 for now
                            ranked_log.event(1,ranked_cnt).level = 1;
                            ranked_log.event(1,ranked_cnt).author = 'SoundXT';
                            ranked_log.sound.samples(ranked_cnt,1) = figdata.sound_list(1,find_ix).num_samples;
                            cum_samples = sum_samples + figdata.sound_list(1,find_ix).num_samples;
                            ranked_log.sound.cumulative = cum_samples;
                            ranked_log.sound.file{ranked_cnt} = figdata.sound_list(1,find_ix).filename;
                        end
                    end
                    ranked_log.sound.duration = cum_time;
                    
                    % Save log to user selected directory
                    log_saved = PRBA_log_save(ranked_log);
                    if not(log_saved)
                        % disp('Error: XBAT log not saved')
                        display_status_line(status_window_edit_handle, sprintf('Error: XBAT log not saved'));
                    else
                        % disp('New XBAT log saved')
                        display_status_line(status_window_edit_handle, sprintf('New XBAT log saved.\n'));
                        display_status_line(status_window_edit_handle, sprintf(''));
                    end
                    
                    
                end
            end
        end
        
        %% make preset if specified by user
        if not(isempty(xbat_preset_name))
            
            % Initialize output directories
            templatePath = fullfile(analysis_path,'templates');
            if ~isfolder(templatePath)
                mkdir(templatePath)
            elseif ~isempty(fastDir(templatePath))
                fail(sprintf('Directory must be empty:\n  %s',templatePath))
                return;
            end
            clipsPath = fullfile(templatePath,'clips');
            if ~isfolder(clipsPath)
                mkdir(clipsPath)
            elseif ~isempty(fastDir(clipsPath))
                fail(sprintf('Directory must be empty:\n  %s',clipsPath))
                return;
            end
            
            % Start Selection Table
            STnameFull = fullfile(templatePath,sprintf('%s.selections.txt',xbat_preset_name));
            fid = fopen(STnameFull, 'wt');
        	header =  {
                'Selection'
                'View'
                'Channel'
                'Begin Time (s)'
                'End Time (s)'
                'Low Freq (Hz)'
                'High Freq (Hz)'
                'Tag'
            };
            header = sprintf('%s\t', header{:});
            header = header(1:end-1);
            fprintf(fid, '%s\n', header);
            fmt = '%.0f\t%s\t%.0f\t%.6f\t%.6f\t%.1f\t%.1f\t%s\f\n';
            view = 'Spectrogram 1';
            
            % modify preset name to include # of templates
            xbat_preset_name = [xbat_preset_name '_' num2str(min(str2double(xbat_preset_size),length(ordered_inx)))];
            
            current_dir = pwd;
% %             ranked_preset = preset_create;
% %             ranked_preset.name = xbat_preset_name;
% %             ext = get_extensions;
% %             
% %             for i = 1:length(ext)
% %                 t(i) = strcmp(ext(1,i).name,'Data Template');
% %             end
% %             ix = t==1;
% %             ranked_preset.ext = ext(1,ix);
% %             ranked_cnt = 0; 
            preset_cnt = 0; 
            cum_time = 0;
            
            %%% Imposed limit of 50 templates in preset--may take out or change later
            for ranked_cnt = 1:min(str2double(xbat_preset_size),length(ordered_inx))   %length(figdata.sound_list)
                %if this template was inlcuded in ranked list
                %             find_ix = find(ordered_inx == x);
                %             if not(isempty(find_ix))
                
                % add log reference number in case multiple logs
                if length(figdata.log_ref) >= ordered_inx(ranked_cnt)
                    log_num = figdata.log_ref(ordered_inx(ranked_cnt));
                end
                preset_cnt = preset_cnt + 1;
                
                
% %                 clip_data = double(figdata.sound_list(1,ordered_inx(ranked_cnt)).data);
% %                 X = double(figdata.sound_list(1,ordered_inx(ranked_cnt)).data);
                X = figdata.sound_list(1,ordered_inx(ranked_cnt)).data;
                X = X ./ max(abs(X)); %normalize to -1.0 ? y ? +1.0
                
                
% %                 parameter.templates.clip(1, ranked_cnt).data = clip_data;
% %                 parameter.templates.clip(1, ranked_cnt).code = [xbat_preset_name(1:4) num2str(ranked_cnt) '_a'];    %figdata.log.event(1,x).tags;
                clipName = [xbat_preset_name(1:4) num2str(ranked_cnt) '_a'];
% %                 parameter.templates.clip(1, ranked_cnt).mode = 1;
% %                 parameter.templates.clip(1, ranked_cnt).spectrogram = 0;
% %                 parameter.templates.clip(1, ranked_cnt).mask = 0;
% %                 parameterd.templates.clip(1, ranked_cnt).freq_ix = 0;
% %                 parameter.templates.clip(1, ranked_cnt).pixels = 0;
                
                if (figdata.log_flag == 1) && (figdata.clip_flag == 0)
                    % log_num
                    % ordered_inx(ranked_cnt) - min(find(figdata.log_ref == log_num)) + 1
% %                     parameter.templates.clip(1, ranked_cnt).event = figdata.log{1,log_num}.event(1,ordered_inx(ranked_cnt) - min(find(figdata.log_ref == log_num)) + 1);
% %                     event = figdata.log{1,log_num}.event(1,ordered_inx(ranked_cnt) - min(find(figdata.log_ref == log_num)) + 1);
                    log = figdata.log{log_num};
% %                     dur = diff(event.time);
                    dur = log{1}(ordered_inx(ranked_cnt));
                    time = [cum_time, cum_time+dur];
                    cum_time = cum_time + dur;
% %                     freq = event.freq;
                    freq = log{2}(ordered_inx(ranked_cnt),:);
                    chan = 1;
% %                     parameter.templates.clip(1, ranked_cnt).samplerate = figdata.log{1,log_num}.sound.samplerate;
% %                     Fs = figdata.log{log_num}.sound.samplerate;
                    Fs = log{3};
% %                     parameter.templates.clip(1, ranked_cnt).id = ranked_cnt;  %figdata.log{1,log_num).event(1,x).id;
                    id = ranked_cnt;  %figdata.log{1,log_num).event(1,x).id;
                else
                    len = (figdata.sound_list(1,ranked_cnt).num_samples / figdata.sound_list(1,ranked_cnt).samplerate); % / 60;
% %                     parameter.templates.clip(1, ranked_cnt).samplerate = figdata.sound_list(1,ordered_inx(ranked_cnt)).samplerate;
                    Fs = figdata.sound_list(1,ordered_inx(ranked_cnt)).samplerate;
% %                     parameter.templates.clip(1, ranked_cnt).id = ranked_cnt;
                    id = ranked_cnt;
% %                     cum_time = cum_time + len;
% %                     parameter.templates.clip(1, ranked_cnt).event.id = ranked_cnt;
% %                     parameter.templates.clip(1, ranked_cnt).event.tags = {num2str(ranked_cnt)};
% %                     parameter.templates.clip(1, ranked_cnt).event.rating = [];
% %                     parameter.templates.clip(1, ranked_cnt).event.notes = cell(0,0);
% %                     parameter.templates.clip(1, ranked_cnt).event.score = [];
% %                     parameter.templates.clip(1, ranked_cnt).event.channel = 1;
                    chan = 1;
% %                     parameter.templates.clip(1, ranked_cnt).event.time = [cum_time cum_time+len];
% %                     parameter.templates.clip(1, ranked_cnt).event.freq = [figdata.param.low_freq figdata.param.high_freq];
                    time = [cum_time cum_time+len];
                    freq = [figdata.param.low_freq figdata.param.high_freq];
% %                     parameter.templates.clip(1, ranked_cnt).event.duration = len;
% %                     parameter.templates.clip(1, ranked_cnt).event.bandwidth = figdata.param.high_freq - figdata.param.low_freq;
% %                     parameter.templates.clip(1, ranked_cnt).event.samples = [];
% %                     parameter.templates.clip(1, ranked_cnt).event.rate = [];
% %                     parameter.templates.clip(1, ranked_cnt).event.level = 1;
% %                     parameter.templates.clip(1, ranked_cnt).event.children = [];
% %                     parameter.templates.clip(1, ranked_cnt).event.parent = [];
% %                     parameter.templates.clip(1, ranked_cnt).event.author = ' ';
% %                     parameter.templates.clip(1, ranked_cnt).event.created = now;
% %                     parameter.templates.clip(1, ranked_cnt).event.modified = [];
                end
                
                % Write clip
                clipNameFull = fullfile(clipsPath,[clipName,'.wav']);
                audiowrite(clipNameFull,X,Fs)
                
                % Add record to selection table
                fprintf(fid,fmt,id,view,chan,time,freq,clipName);
                
            end
            
            % Close selection table
            fclose(fid);         
            
% %             % Fill in other fields with preset info
% %             parameter.templates.ix = 1;
% %             parameter.templates.length = ranked_cnt;
% %             parameter.templates.curr_id = ranked_cnt + 1;
% %             parameter.thresh = 0.02;
% %             parameter.thresh_test = 1;
% %             parameter.deviation = 3;
% %             parameter.deviation_test = 0;
% %             parameter.mask = 0;
% %             parameter.mask_percentil = .6;
% %             parameter.preset_name = xbat_preset_name
% %             if (figdata.log_flag == 1) && (figdata.clip_flag == 0)
% %                 parameter.specgram = figdata.log{log_num}.sound.specgram;
% %             else
% %                 parameter.specgram.fft = figdata.param.fft_len;
% %                 parameter.specgram.hop = (100-figdata.param.data_overlap)/100;
% %                 parameter.specgram.hop_auto = 1;
% %                 parameter.specgram.win_type = figdata.param.taper_func;
% %                 parameter.specgram.win_param = [];
% %                 parameter.specgram.win_length = figdata.param.data_len;
% %                 parameter.specgram.sum_type = 'mean';
% %                 parameter.specgram.sum_quality = 'low';
% %                 parameter.specgram.sum_length = 1;
% %                 parameter.specgram.sum_auto = 1;
% %                 parameter.specgram.on = 1;
% %                 parameter.specgram.diff = 0;
% %                 parameter.specgram.filter = 'None';
% %             end
            % Add parameter field to newly created preset
% %             ranked_preset.ext.parameter = parameter;
            % Display status message
            display_status_line(status_window_edit_handle, sprintf('%i Templates selected by ranking algorithm', ranked_cnt));
            display_status_line(status_window_edit_handle, sprintf(''));
            display_status_line(status_window_edit_handle, sprintf('%i Templates added to preset', preset_cnt));
% %             % Save preset in specified directory
% %             preset_save(ranked_preset)
            
            display_status_line(status_window_edit_handle, sprintf('New XBAT preset saved.\n'));
            display_status_line(status_window_edit_handle, sprintf(''));
            
            %% Test preset in DTD
            % Use input log if user specifies
            if use_same_log
                if figdata.log_flag == 1
                    
                    if length(figdata.log) == 1
                        % Run preset through DTD and save templates to XBAT log
                        output_log = PRBA_log_create();
                        output_log.sound = figdata.log.sound;
                        output_log.sound.output.rate = figdata.log.sound.samplerate;
                        
                        % Go through DTD comparisons
                        display_status_line(status_window_edit_handle, sprintf('Calculating ROC curve...\n'));
                        [output_log] = detector_scan(ranked_preset.ext, figdata.log.sound, [], [], output_log);
                        output_log.file = [xbat_log_name '_output'];
                        output_log.path = xbat_log_dir;
                        
                        % Run detector validation tool
                        context.target{1,1} = xbat_log_name;
                        context.target{2,1} = output_log;
                        detector_validation_sub(figdata.log , output_log);
                    else
                        display_status_line(status_window_edit_handle, sprintf('Cannot test preset against multiple input logs.\n\n'));
                        
                        %                      for nl = 1:length(figdata.log)
                        %                          % Run preset through DTD and save templates to XBAT log
                        %                          output_log = PRBA_log_create();
                        %                          output_log.sound = figdata.log{1,nl}.sound;
                        %                          output_log.sound.output.rate = figdata.log{1,nl).sound.samplerate;
                        %
                        %                          % Go through DTD comparisons
                        %                          display_status_line(status_window_edit_handle, sprintf('Calculating ROC curve...\n'));
                        %                          [output_log] = detector_scan(ranked_preset.ext, figdata.log{1,nl}.sound, [], [], output_log);
                        %                          output_log.file = [xbat_log_name '_output'];
                        %                          output_log.path = xbat_log_dir;
                        %
                        %                          % Run detector validation tool
                        %                          context.target{1,1} = xbat_log_name;
                        %                          context.target{2,1} = output_log;
                        %                          detector_validation_sub(figdata.log , output_log);
                        %                      end
                    end
                else
                    display_status_line(status_window_edit_handle, sprintf('XBAT log not input by user. Cannot compare preset to input truth log.\n\n'));
                    display_status_line(status_window_edit_handle, sprintf(''));
                end
            end
            
            % Use different sound and log if user specifies
            if not(isempty(diff_log_name))
                
                if isempty(diff_log_dir) || isempty(xbat_sound_name) || isempty(xbat_sound_dir)
                    display_status_line(status_window_edit_handle, sprintf('Cannot test preset on XBAT sound. Please specify correct sound and log files.\n'));
                    display_status_line(status_window_edit_handle, sprintf(''));
                else
                    % test on other sound that user specified
                    cd(diff_log_dir)
                    truth_log = load(diff_log_name);
                    field = fieldnames(truth_log);
                    eval(['truth = truth_log.' char(field) ';'])
                    cd(current_dir)
                    
                    % Run preset through DTD and save templates to XBAT log
                    output_log = PRBA_log_create();
                    output_log.sound = truth.sound;
                    % fix sound path
                    output_log.sound.path = xbat_sound_dir;
                    output_log.sound.output.rate = truth.sound.samplerate;
                    
                    % Go through DTD comparisons
                    display_status_line(status_window_edit_handle, sprintf('Calculating ROC curve...\n'));
                    display_status_line(status_window_edit_handle, sprintf(''));
                    [output_log] = detector_scan(ranked_preset.ext, output_log.sound, [], [], output_log);
                    output_log.file = truth.sound.file;
                    output_log.path = truth.sound.path;
                    
                    % Run detector validation tool
                    context.target{1,1} = diff_log_name;
                    context.target{2,1} = output_log;
                    detector_validation_sub(truth , output_log);
                    
                end
            end
            
        end
    end
    
else
    %% not a correlation data file, send an error message
    display_status_line(status_window_edit_handle, sprintf('File ''%s'' is not a correlation data file.', analysis_file));
    display_status_line(status_window_edit_handle, 'Unable to perform analysis.');
end

end


%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_analysis_nearest_neighbor
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_analysis_nearest_neighbor(gui_interface_fig_tag)

%% get gui interface figure handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% get most recent analysis path
analysis_path = figdata.last_analysis_path;

%% check that analysis exists
if ~exist(analysis_path)
    analysis_path = [pwd, filesep];
end

%% get output file name
%%% Modified by SCK
% [analysis_file, analysis_path] = uigetfile('*.mat', 'Select Pairwise Correlation Data File', analysis_path);
[analysis_file, analysis_path] = uigetfiles_SCK('*.mat', 'Select Pairwise Correlation Data File', analysis_path);

if isempty(analysis_path)  %%%~analysis_path
    %% cancel requested
    return;
end

%% save most recent analysis path
figdata.last_analysis_path = analysis_path;

%% save figure data
set(gui_interface_fig_handle, 'userdata', figdata);


%% get status window handle
status_window_edit_handle = findobj(gui_interface_fig_handle, 'tag', ['status_window_edit_uicontrol', mfilename]);

%% send a message
display_status_line(status_window_edit_handle, ' ');
display_status_line(status_window_edit_handle, 'Performing Nearest Neighbor analysis...');

try
    corr_data = load('-mat', [analysis_path, analysis_file]);
    %% get to the rootfield
    corr_data_fieldnames = fieldnames(corr_data);
    corr_data = corr_data.(corr_data_fieldnames{1});
    
catch
    % uable to load file
    % send an error message
    display_status_line(status_window_edit_handle, sprintf('File "%s" did not open.', analysis_file));
    display_status_line(status_window_edit_handle, 'Unable to perform analysis.');
    return;
end


if isfield(corr_data, 'f0Peaks')
    %% is a correlation data file, do ordering for f0Peaks
    corr_mat = corr_data.f0Peaks.peakVal;
    sndNames = {corr_data.sound_list.filename}';
    
    outputfilename = [analysis_path, basenameof(analysis_file), '_f0Peaks_neighbor.txt'];
    fid = fopen(outputfilename, 'w');
    
    if fid == -1
        display_status_line(status_window_edit_handle, sprintf('Unable to open file ''s'' for writing.', outputfilename));
        display_status_line(status_window_edit_handle, 'Unable to continue analysis.');
        return;
    end
    
    %% get dimensions
    [nrow, ncol] = size(corr_mat);
    
    %% do correlation ordering (down column, ascending) for all sounds
    [corr_mat_ord, inx_ord] = sort(corr_mat);
    
    %% build up ordered sound names matrix
    sndNames_ord = sndNames(inx_ord);
    
    %% put in decending order
    corr_mat_ord = flipud(corr_mat_ord);
    sndNames_ord = flipud(sndNames_ord);
    inx_ord = flipud(inx_ord);
    
    corr_data.f0Peaks_Neighbor.sndNames_ordered = sndNames_ord;
    corr_data.f0Peaks_Neighbor.peakVal_ordered = corr_mat_ord;
    corr_data.f0Peaks_Neighbor.order_inx = inx_ord;
    
    
    tempMat = [sndNames_ord; num2cell(corr_mat_ord)];
    tempMat = reshape(tempMat, nrow, ncol*2);
    tempMat = tempMat';
    
    format_Str = ['%s\t%12.6f'];
    for i=2:ncol
        format_Str = [format_Str, '\t%s\t%12.6f'];
    end
    format_Str =  [format_Str, '\n'];
    
    fprintf(fid, format_Str, tempMat{:});
    
    fclose(fid);
    
    
    if isfield(corr_data, 'absPeaks')
        %% do ordering for absPeaks
        corr_mat = corr_data.absPeaks.peakVal;
        
        outputfilename = [analysis_path, basenameof(analysis_file), '_absPeaks_neighbor.txt'];
        fid = fopen(outputfilename, 'w');
        
        %% do correlation ordering (down column, ascending) for all sounds
        [corr_mat_ord, inx_ord] = sort(corr_mat);
        
        %% build up ordered sound names matrix
        sndNames_ord = sndNames(inx_ord);
        
        %% put in decending order
        corr_mat_ord = flipud(corr_mat_ord);
        sndNames_ord = flipud(sndNames_ord);
        inx_ord = flipud(inx_ord);
        
        corr_data.absPeaks_Neighbor.sndNames_ordered = sndNames_ord;
        corr_data.absPeaks_Neighbor.peakVal_ordered = corr_mat_ord;
        corr_data.absPeaks_Neighbor.order_inx = inx_ord;
        
        
        tempMat = [sndNames_ord; num2cell(corr_mat_ord)];
        tempMat = reshape(tempMat, nrows, ncol*2);
        tempMat = tempMat';
        
        format_Str = [];
        for i=1:ncol
            format_Str = [format_Str, '%s\t%12.6f'];
        end
        format_Str =  [format_Str, '\n'];
        
        fprintf(fid, format_Str, tempMat{:});
        
        fclose(fid);
        
    end
    
    
    outputfilename = [analysis_path, basenameof(analysis_file), '_Neighbor.mat'];
    
    %% save workspace variables in a -mat file
    varName = [removeillegalchar(basenameof(analysis_file)), '_NeighborData'];
    eval([varName,' = corr_data;']);
    save(outputfilename, sprintf(varName), '-mat');
    
    display_status_line(status_window_edit_handle, 'Done.');
    display_status_line(status_window_edit_handle, 'Ready...')
    display_status_line(status_window_edit_handle, ' ')
    
else
    %% not a correlation data file
    %% send an error message
    display_status_line(status_window_edit_handle, sprintf('File ''%s'' is not a correlation data file.', analysis_file));
    display_status_line(status_window_edit_handle, 'Unable to perform analysis.');
    
end


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_program_quit
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_program_quit(gui_interface_fig_tag)

%% get gui interface fig handle
gui_interface_fig_handle = findobj(0, 'tag', gui_interface_fig_tag);
figdata = get(gui_interface_fig_handle, 'userdata');

%% remove sound tool from path
% pathing now controlled in Sedna
%rmpath(figdata.defaultpath);

f_path = mfilename('fullpath');

% settings_path = [figdata.defaultpath, '\settings'];
settings_path = [f_path(1:end-20) 'settings'];

%% save current settings to default file
if ~exist(settings_path, 'dir')
    mkdir(figdata.defaultpath, 'settings');
    %%% Added SCK 9-3-13
    mkdir(settings_path, 'settings');
    %%% end SCK
end


%% reset the session specific variables
figdata.defaultpath = [];
figdata.sound_list = [];
figdata.output = [];

%% save the default file
save([settings_path, '\default_settings.mat'], 'figdata', '-mat');

%% close the figure
delete(gui_interface_fig_handle);

%% inform the user
fprintf(1, 'Quit Requested\n');


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: cb_about_prg
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cb_about_prg(gui_interface_fig_tag);

helpdlg({' '; ':SoundXT: Sound Cross Correlation Tool  (formerly ''SpecX'')'; 'Version 1.0,  August 2004'; ' ';'Kathryn A. Cortopassi, Ph.D.'; 'Cornell University, Bioacoustics Research Program'}, ' ')

end


%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: change_password_callback
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function help_callback(~, ~, handles)

open(fullfile(fileparts(mfilename('fullpath')), 'private', 'help.html'))

end


%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: setting_controls_release_all
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function setting_controls_release_all(gui_interface_fig_handle)


fft_len_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['fft_len_edit_uicontrol', mfilename]);
set(fft_len_edit_uicontrol_handle, 'enable', 'on');

data_len_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['data_len_edit_uicontrol', mfilename]);
set(data_len_edit_uicontrol_handle, 'enable', 'on');

taper_func_popup_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['taper_func_popup_uicontrol', mfilename]);
set(taper_func_popup_uicontrol_handle, 'enable', 'on');

data_overlap_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['data_overlap_edit_uicontrol', mfilename]);
set(data_overlap_edit_uicontrol_handle, 'enable', 'on');

low_freq_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['low_freq_edit_uicontrol', mfilename]);
set(low_freq_edit_uicontrol_handle, 'enable', 'on');

high_freq_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['high_freq_edit_uicontrol', mfilename]);
set(high_freq_edit_uicontrol_handle, 'enable', 'on');

data_form_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['data_form_popup_uicontrol', mfilename]);
set(data_form_popup_uicontrol, 'enable', 'on');

masking_meth_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['masking_meth_popup_uicontrol', mfilename]);
set(masking_meth_popup_uicontrol, 'enable', 'on');

masking_param_edit_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['masking_param_edit_uicontrol', mfilename]);
set(masking_param_edit_uicontrol, 'enable', 'on');

mask_adj_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['mask_adj_popup_uicontrol', mfilename]);
set(mask_adj_popup_uicontrol, 'enable', 'on');

corr_type_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['corr_type_popup_uicontrol', mfilename]);
set(corr_type_popup_uicontrol, 'enable', 'on');

corr_std_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['corr_std_popup_uicontrol', mfilename]);
set(corr_std_popup_uicontrol, 'enable', 'on');

max_time_lag_edit_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['max_time_lag_edit_uicontrol', mfilename]);
set(max_time_lag_edit_uicontrol, 'enable', 'on');

max_freq_lag_edit_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['max_freq_lag_edit_uicontrol', mfilename]);
set(max_freq_lag_edit_uicontrol, 'enable', 'on');

run_button_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['run_button_uicontrol', mfilename]);
set(run_button_uicontrol, 'enable', 'on');

run_menu_item_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['run_menu_item_uicontrol', mfilename]);
set(run_menu_item_uicontrol, 'enable', 'on');



%% reset bandlimits and lags to full range
figdata = get(gui_interface_fig_handle, 'userdata');

nyquist = figdata.sound_list(1).samplerate / 2;

figdata.param.low_freq = 0;
figdata.param.high_freq = nyquist;

set(low_freq_edit_uicontrol_handle,'string', 0);%%, 'value', 0);
set(high_freq_edit_uicontrol_handle,'string', nyquist);%%, 'value', nyquist);


max_dur = max(cell2mat({figdata.sound_list.num_samples})) / figdata.sound_list(1).samplerate;

figdata.param.max_time_lag = max_dur;
figdata.param.max_freq_lag = nyquist;

set(max_time_lag_edit_uicontrol, 'string', max_dur);%%, 'value', max_dur);
set(max_freq_lag_edit_uicontrol, 'string', nyquist);%%, 'value', nyquist);

set(gui_interface_fig_handle, 'userdata', figdata);


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: setting_controls_initial_block
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function setting_controls_initial_block(gui_interface_fig_handle, max_dur, min_freq_all, max_freq_all)


fft_len_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['fft_len_edit_uicontrol', mfilename]);
set(fft_len_edit_uicontrol_handle, 'enable', 'on');

data_len_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['data_len_edit_uicontrol', mfilename]);
set(data_len_edit_uicontrol_handle, 'enable', 'on');

taper_func_popup_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['taper_func_popup_uicontrol', mfilename]);
set(taper_func_popup_uicontrol_handle, 'enable', 'on');

data_overlap_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['data_overlap_edit_uicontrol', mfilename]);
set(data_overlap_edit_uicontrol_handle, 'enable', 'on');

low_freq_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['low_freq_edit_uicontrol', mfilename]);
set(low_freq_edit_uicontrol_handle, 'enable', 'off');

high_freq_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['high_freq_edit_uicontrol', mfilename]);
set(high_freq_edit_uicontrol_handle, 'enable', 'off');

data_form_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['data_form_popup_uicontrol', mfilename]);
set(data_form_popup_uicontrol, 'enable', 'on');

masking_meth_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['masking_meth_popup_uicontrol', mfilename]);
set(masking_meth_popup_uicontrol, 'enable', 'on');

masking_param_edit_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['masking_param_edit_uicontrol', mfilename]);
set(masking_param_edit_uicontrol, 'enable', 'on');

mask_adj_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['mask_adj_popup_uicontrol', mfilename]);
set(mask_adj_popup_uicontrol, 'enable', 'on');

corr_type_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['corr_type_popup_uicontrol', mfilename]);
set(corr_type_popup_uicontrol, 'enable', 'on');

corr_std_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['corr_std_popup_uicontrol', mfilename]);
set(corr_std_popup_uicontrol, 'enable', 'on');

max_time_lag_edit_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['max_time_lag_edit_uicontrol', mfilename]);
set(max_time_lag_edit_uicontrol, 'enable', 'off');

max_freq_lag_edit_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['max_freq_lag_edit_uicontrol', mfilename]);
set(max_freq_lag_edit_uicontrol, 'enable', 'off');

run_button_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['run_button_uicontrol', mfilename]);
set(run_button_uicontrol, 'enable', 'off');

run_menu_item_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['run_menu_item_uicontrol', mfilename]);
set(run_menu_item_uicontrol, 'enable', 'off');



%% set bandlimits and lags as indicated
if exist('max_dur')
    
    figdata = get(gui_interface_fig_handle, 'userdata');
   
    figdata.param.max_time_lag = max_dur;
    figdata.param.max_freq_lag = max_freq_all;
    
    set(max_time_lag_edit_uicontrol, 'string', max_dur);%%, 'value', max_dur);
    %%%SCK
    set(max_freq_lag_edit_uicontrol,'string', max_freq_all);
    
    set(gui_interface_fig_handle, 'userdata', figdata);   
end
if  exist('min_freq_all') & exist('max_freq_all')
    
    figdata = get(gui_interface_fig_handle, 'userdata');
    
    figdata.param.low_freq = min_freq_all;
    figdata.param.high_freq = max_freq_all;
    
    set(low_freq_edit_uicontrol_handle, 'string', min_freq_all);%%, 'value', 0);
    set(high_freq_edit_uicontrol_handle,'string', max_freq_all);%%, 'value', nyquist);
    
    set(gui_interface_fig_handle, 'userdata', figdata);   
end


end


%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: spec_controls_block
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function spec_controls_block(gui_interface_fig_handle)


fft_len_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['fft_len_edit_uicontrol', mfilename]);
set(fft_len_edit_uicontrol_handle, 'enable', 'off');

data_len_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['data_len_edit_uicontrol', mfilename]);
set(data_len_edit_uicontrol_handle, 'enable', 'off');

taper_func_popup_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['taper_func_popup_uicontrol', mfilename]);
set(taper_func_popup_uicontrol_handle, 'enable', 'off');

data_overlap_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['data_overlap_edit_uicontrol', mfilename]);
set(data_overlap_edit_uicontrol_handle, 'enable', 'off');

data_form_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['data_form_popup_uicontrol', mfilename]);
set(data_form_popup_uicontrol, 'enable', 'off');

masking_meth_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['masking_meth_popup_uicontrol', mfilename]);
set(masking_meth_popup_uicontrol, 'enable', 'off');

masking_param_edit_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['masking_param_edit_uicontrol', mfilename]);
set(masking_param_edit_uicontrol, 'enable', 'off');

mask_adj_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['mask_adj_popup_uicontrol', mfilename]);
set(mask_adj_popup_uicontrol, 'enable', 'off');

corr_std_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['corr_std_popup_uicontrol', mfilename]);
set(corr_std_popup_uicontrol, 'enable', 'off');

max_freq_lag_edit_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['max_freq_lag_edit_uicontrol', mfilename]);
set(max_freq_lag_edit_uicontrol, 'enable', 'off');


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: spec_controls_release
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function spec_controls_release(gui_interface_fig_handle)


fft_len_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['fft_len_edit_uicontrol', mfilename]);
set(fft_len_edit_uicontrol_handle, 'enable', 'on');

data_len_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['data_len_edit_uicontrol', mfilename]);
set(data_len_edit_uicontrol_handle, 'enable', 'on');

taper_func_popup_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['taper_func_popup_uicontrol', mfilename]);
set(taper_func_popup_uicontrol_handle, 'enable', 'on');

data_overlap_edit_uicontrol_handle = findobj(gui_interface_fig_handle, 'tag', ['data_overlap_edit_uicontrol', mfilename]);
set(data_overlap_edit_uicontrol_handle, 'enable', 'on');

data_form_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['data_form_popup_uicontrol', mfilename]);
set(data_form_popup_uicontrol, 'enable', 'on');

masking_meth_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['masking_meth_popup_uicontrol', mfilename]);
set(masking_meth_popup_uicontrol, 'enable', 'on');

masking_param_edit_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['masking_param_edit_uicontrol', mfilename]);
set(masking_param_edit_uicontrol, 'enable', 'on');

mask_adj_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['mask_adj_popup_uicontrol', mfilename]);
set(mask_adj_popup_uicontrol, 'enable', 'on');

corr_std_popup_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['corr_std_popup_uicontrol', mfilename]);
set(corr_std_popup_uicontrol, 'enable', 'on');

max_freq_lag_edit_uicontrol = findobj(gui_interface_fig_handle, 'tag', ['max_freq_lag_edit_uicontrol', mfilename]);
set(max_freq_lag_edit_uicontrol, 'enable', 'on');


end






%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: check4badspecs
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function [sndSpec, fvec, tvec, figdata] = check4badspecs(sndSpec, fvec, tvec, figdata, gui_interface_fig_handle, all_uihandles, enable_states, status_window_edit_handle)


bad_snd_inx = find(cellfun('isempty', sndSpec));

if bad_snd_inx
    query = questdlg(...
        sprintf('Errors during spectrogram generation process.  Do you want to continue?\n(Sounds without a spectrogram will be removed from list)'),...
        'Spectrogram Error!', 'yes', 'no', 'no');
    
    if strcmpi(query, 'no')
        %% reset controls, and exit
        for inx = 1:length(all_uihandles)
            set(all_uihandles(inx), 'enable', enable_states{inx});
        end
        sndSpec = [];
        return;
        
    else %% 'yes'
        %% remove bad sounds from list
        
        current_display = get(status_window_edit_handle, 'string');
        current_display{end+1} = 'Removing bad sounds from list...';
        set(status_window_edit_handle, 'string', current_display, 'listboxtop', length(current_display));
        drawnow;
        
        for i = bad_snd_inx'
            current_display = get(status_window_edit_handle, 'string');
            current_display{end+1} = sprintf('Removing sound : ''%s''...', figdata.sound_list(i).filename);
            set(status_window_edit_handle, 'string', current_display, 'listboxtop', length(current_display));
            drawnow;
        end
        
        %% remove bad sounds
        figdata.sound_list(bad_snd_inx) = [];
        sndSpec(bad_snd_inx) = [];
        fvec(bad_snd_inx) = [];
        tvec(bad_snd_inx) = [];
        figdata.log_ref(bad_snd_inx) = [];
        
        sound_list_popup_handle = findobj(gui_interface_fig_handle, 'tag', ['sound_list_popup_uicontrol', mfilename]);
        
        if isempty(figdata.sound_list)
            %% update sound list to empty
            set(sound_list_popup_handle, 'string', ' ', 'value', 1);
            
            %% some code to deal with a matlab quirk
            figdata.sound_list = [];
            
            %% set figdata
            set(gui_interface_fig_handle, 'userdata', figdata);
            
            %% reset controls, and exit
            for inx = 1:length(all_uihandles)
                set(all_uihandles(inx), 'enable', enable_states{inx});
            end
            return;
            
        else
            %% update sound list
            new_list = get(sound_list_popup_handle, 'string');
            new_list(bad_snd_inx) = [];
            set(sound_list_popup_handle, 'string', new_list, 'value', 1);
            % set(sound_list_popup_handle, 'string', figdata.sound_list, 'value', 1);
            
            %% set and continue
            set(gui_interface_fig_handle, 'userdata', figdata);
            
        end %% if
        
    end %% if
    
end %% if

end
