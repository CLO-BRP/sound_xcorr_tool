function [corr_file, corr_dir, xbat_log_name, xbat_log_dir, diff_sound_name, diff_sound_dir, xbat_preset_name, xbat_preset_size, xbat_preset_dir, use_same_log, diff_log_name, diff_log_dir, go] = template_rank_gui()

%%%  FUNCTION: GUI for Template ranking algorithm
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%~~

corr_file=[]; corr_dir=[]; xbat_log_name=[]; xbat_log_dir=[]; diff_sound_name=[]; diff_sound_dir=[]; xbat_preset_name=[]; xbat_preset_size=50; xbat_preset_dir=[]; use_same_log=[];diff_log_name=[];diff_log_dir=[]; go=0;
test_flag = 0; preset_flag = 0;

temprank_fig = figure('MenuBar','none','Name','Template Ranking Analysis','NumberTitle','off','Position',[100,100,700,600]);
use_same_log = 0;
setappdata(temprank_fig,'corr_file',[]); setappdata(temprank_fig,'corr_dir',[])
setappdata(temprank_fig,'xbat_log_name',[]); setappdata(temprank_fig,'xbat_log_dir',[])
setappdata(temprank_fig,'xbat_preset_name',[]); setappdata(temprank_fig,'xbat_preset_dir',[]); setappdata(temprank_fig,'xbat_preset_size',50);
setappdata(temprank_fig,'xbat_sound_name',[]); setappdata(temprank_fig,'xbat_sound_dir',[]);setappdata(temprank_fig,'go',0);
setappdata(temprank_fig,'use_same_log',0);

% Heading
uicontrol('Style','text','String','Template Ranking Analysis','FontSize',15,'Position',[125,560,450,30]);

% Get file with specgram correlation values
uicontrol('Style','text','String','Spectrogram correlation file:','FontSize',13,'Position',[25,520,250,30]);
corr_file_handle = uicontrol('Style','edit','String','None selected','FontSize',11,'FontAngle','Italic','Position',[25,485,500,30]);
uicontrol('Style','Pushbutton','String','Select file','Position',[550,485,70,30],'CallBack',@CorrFilebutton);

% SubHeading
uicontrol('Style','text','String','Save results as XBAT files (leave blank to omit)','FontSize',14,'Position',[125,425,450,30]);

% Get file name and path for saving XBAT log
uicontrol('Style','text','String','XBAT log file:','FontSize',13,'Position',[25,385,250,30]);
xbat_file_handle = uicontrol('Style','edit','String','AMRE_spgXXXX_pX_Hann_pX_lowfreq_hifreq_xcorr_log','FontSize',11,'FontAngle','Italic','Position',[25,350,500,30]);
uicontrol('Style','Pushbutton','String','Select file','Position',[550,350,70,30],'CallBack',@XBATlogbutton);

% Get file name and path for saving XBAT preset
uicontrol('Style','text','String','XBAT preset file name:','FontSize',13,'Position',[25,310,250,30]);
xbat_preset_handle = uicontrol('Style','edit','String','AMRE_examples_xcorr_#templates','FontSize',11,'FontAngle','Italic','Position',[25,275,325,30],'CallBack',@XBATpresetname);
% uicontrol('Style','Pushbutton','String','Select file','Position',[550,275,70,30],'CallBack',@XBATpresetbutton);
uicontrol('Style','text','String','Max templates:','FontSize',13,'Position',[400,310,150,30]);
xbat_preset_size_handle = uicontrol('Style','edit','String','50','FontSize',11,'FontAngle','Italic','Position',[425,275,100,30],'CallBack',@XBATpresetsize);

% SubHeading
uicontrol('Style','text','String','Calculate performance characteristics of preset (leave blank to omit)','FontSize',14,'Position',[50,215,600,30]);

% Get file name and path for saving XBAT log
box1 = uicontrol('Style','Checkbox','Value',0,'Position',[25,170,30,30],'CallBack',@Samelogbutton);
box1_handle = uicontrol('Style','text','String','Test with input XBAT log or sound files','FontSize',13,'ForegroundColor',[139 137 137]/255,'Position',[60,170,400,30]);
box2 = uicontrol('Style','Checkbox','Value',0,'Position',[25,135,30,30],'CallBack',@Newlogbutton);
box2_handle = uicontrol('Style','text','String','Test with different sound file and associated truth log','FontSize',13,'ForegroundColor',[139 137 137]/255,'Position',[60,135,420,30]);

%uicontrol('Style','text','String','Sound file:','FontSize',13,'Position',[40,100,100,30]);
%test_sound_handle = uicontrol('Style','edit','String','None selected','FontSize',12,'FontAngle','Italic','Position',[150,100,425,30]);
%test_sound_select = uicontrol('Style','Pushbutton','String','Select file','Position',[600,100,70,30],'ForegroundColor',[139 137 137]/255,'CallBack',@TestSoundbutton);
test_log_label = uicontrol('Style','text','String','Test log:','FontSize',13,'ForegroundColor',[139 137 137]/255,'Position',[40,100,100,30]);
test_truth_handle = uicontrol('Style','edit','String','None selected','FontSize',11,'ForegroundColor',[139 137 137]/255,'FontAngle','Italic','Position',[150,100,425,30]);
test_truth_select = uicontrol('Style','Pushbutton','String','Select file','Position',[600,100,70,30],'ForegroundColor',[139 137 137]/255);

% Button to Run Template Ranking
run_button_handle = uicontrol('Style','Pushbutton','String','Run Template Ranking','Position',[125,15,250,30],'BackgroundColor',[60 179 113]/255,'FontSize',14,'CallBack',@Gobutton);
% Button to Quit GUI
quit_button_handle = uicontrol('Style','Pushbutton','String','Cancel','Position',[395,15,150,30],'BackgroundColor',[178 34 34]/255,'FontSize',14,'CallBack',@Quitbutton);

    function CorrFilebutton(temprank_fig, ~)
        [corr_file, corr_dir] = uigetfile2_SCK_xcorr('*.mat*', 'Select SoundXT spectrogram correlation file');
        setappdata(temprank_fig,'corr_file',corr_file)
        setappdata(temprank_fig,'corr_dir',corr_dir)
        %update text box
        set(corr_file_handle, 'string',[corr_dir corr_file],'FontAngle','Normal');
    end
    function XBATlogbutton(temprank_fig, ~)
        [xbat_log_name, xbat_log_dir] = uiputfile2('*.*', 'Select XBAT log name and directory where log will be saved');
        if isempty(xbat_log_name)
            return;
        end
        setappdata(temprank_fig,'xbat_log_dir',xbat_log_dir)
        setappdata(temprank_fig,'xbat_log_name',xbat_log_name)
        %update text box
        set(xbat_file_handle, 'string',[xbat_log_dir xbat_log_name],'FontAngle','Normal');
    end
    function Samelogbutton(temprank_fig, ~)
        if preset_flag == 1
            use_same_log = 1;
            setappdata(temprank_fig,'use_same_log',1);
            set(box1, 'Value',1);
        else
            set(box1, 'Value',0);
        end
    end
    function Newlogbutton(temprank_fig, ~)
            test_flag = 1;
            %set(test_sound_select, 'ForegroundColor','Black');
            set(test_truth_select, 'ForegroundColor','Black');
            set(test_truth_handle, 'ForegroundColor','Black');
            set(test_log_label, 'ForegroundColor','Black');
            
            set(test_truth_select,'CallBack',@TestLogbutton)
           % set(test_sound_select,'CallBack',@TestSoundbutton)
    end
    function TestSoundbutton(temprank_fig, ~)
        if test_flag == 1
            [diff_sound_name, diff_sound_dir_all] = uigetfiles_SCK('*.*', 'Select sound file(s) on which new preset will be run');
            if isempty(diff_sound_dir_all)
                %% cancel requested
                return;
            else
                [rows,cols]= size(diff_sound_dir_all);
                if rows == 1
                    diff_sound_dir = cell(0);
                    diff_sound_dir{1} = char(diff_sound_dir_all);
                else
                    if iscell(diff_sound_dir_all) && not(isempty(diff_sound_dir_all))
                        diff_sound_dir = char(diff_sound_dir_all{1});
                    else
                        diff_sound_dir = diff_sound_dir_all;
                    end
                    
                end
            end
            
            setappdata(temprank_fig,'diff_sound_dir',diff_sound_dir)
            setappdata(temprank_fig,'diff_sound_name',diff_sound_name)
            %update text box
            if iscell(diff_sound_name)
                diff_sound_disp = char(diff_sound_name{1});
            else
                diff_sound_disp = diff_sound_name;
            end
            set(test_sound_handle, 'string',[char(diff_sound_dir) diff_sound_disp],'FontAngle','Normal');
        end
    end
    function TestLogbutton(temprank_fig, ~)
        if test_flag == 1
            
            [diff_log_name, diff_log_dir] = uigetfile2_SCK('*.*', 'Select test log file');
            setappdata(temprank_fig,'diff_log_dir',diff_log_dir)
            setappdata(temprank_fig,'diff_log_name',diff_log_name)
            
            % make sure sound files can be located
            test_log_file = load([diff_log_dir diff_log_name]);
            test_log_name = fieldnames(test_log_file);
            test_log_sound = struct();
            eval(char(strcat('test_log_sound = test_log_file.',test_log_name,'.sound;')))
            % navigate to sound dir
            diff_sound_name =  test_log_sound.file;
            diff_sound_dir_all = test_log_sound.path;
            real_sound_dir = dir(test_log_sound.path);
            real_sound_files = char();
            for x = 1:length(real_sound_dir)
                real_sound_files = [real_sound_files ',' char(real_sound_dir(x).name)];               
            end
            log_sound_files = test_log_sound.file;
            for x = 1:length(test_log_sound.file)
                focal_file = char(test_log_sound.file{x});
                if sum(strfind(real_sound_files,focal_file)) < 1
                    wh = questdlg('Sound files not in path specified in log.  Locate sound files now?',...
                        'Error','Yes','No','Yes');
                    
                    if strcmp(wh,'Yes')
                        [diff_sound_name, diff_sound_dir_all] = uigetfiles_SCK('*.*', 'Select sound file(s) for test log');
                        % save sound name and dir
                        if iscell(diff_sound_dir_all) && not(isempty(diff_sound_dir_all))
                            diff_sound_dir = char(diff_sound_dir_all{1});
                        else
                            diff_sound_dir = diff_sound_dir_all;
                        end
                        setappdata(temprank_fig,'diff_sound_dir',diff_sound_dir)
                        setappdata(temprank_fig,'diff_sound_name',diff_sound_name)
                        
                        %update text box
                        set(test_truth_handle, 'string',[diff_log_dir diff_log_name],'FontAngle','Normal');
                        return;
                    else
                        return;
                    end
                end
            end
            
            % save sound name and dir
            if iscell(diff_sound_dir_all) && not(isempty(diff_sound_dir_all))
                diff_sound_dir = char(diff_sound_dir_all{1});
            else
                diff_sound_dir = diff_sound_dir_all;
            end
            setappdata(temprank_fig,'diff_sound_dir',diff_sound_dir)
            setappdata(temprank_fig,'diff_sound_name',diff_sound_name)
            
            %update text box
            set(test_truth_handle, 'string',[diff_log_dir diff_log_name],'FontAngle','Normal');
        end
    end
    function Gobutton(temprank_fig, ~)      
        selection = questdlg('Run template ranking analysis now?',...
            'Close Request','Yes','No','Yes');
        
        if strcmp(selection,'Yes')
                go = 1; setappdata(temprank_fig,'go',1);
                go = getappdata(temprank_fig,'go');
                set(run_button_handle,'FontAngle','Italic');
        end
    end

    function Quitbutton(temprank_fig, ~)
        selection = questdlg('Quit template ranking analysis now?',...
            'Close Request','Yes','No','Yes');
        
        if strcmp(selection,'Yes')
            go = 0; setappdata(temprank_fig,'go',0);
            go = getappdata(temprank_fig,'go');
           set(run_button_handle,'FontAngle','Italic');
        end
    end

     function XBATpresetbutton(temprank_fig, ~)
        [xbat_preset_name, xbat_preset_dir] = uiputfile('*.*', 'Select XBAT preset name and directory where preset will be saved','C:\XBAT_R5\Extensions\Detectors\Sound\Data Template\Presets');
        setappdata(temprank_fig,'xbat_preset_dir',xbat_preset_dir)
        setappdata(temprank_fig,'xbat_preset_name',xbat_preset_name)
        %update text box
        set(xbat_preset_handle, 'string',[xbat_preset_dir xbat_preset_name],'FontAngle','Normal');
        preset_flag = 1;
        
        set(box1_handle, 'ForegroundColor','Black');
        set(box2_handle, 'ForegroundColor','Black');  
     end
    function XBATpresetname(temprank_fig, ~)
        xbat_preset_name = get(xbat_preset_handle,'String');
        setappdata(temprank_fig,'xbat_preset_dir',xbat_preset_dir)
        set(xbat_preset_handle,'FontAngle','Normal');
        preset_flag = 1;
        
        set(box1_handle, 'ForegroundColor','Black');
        set(box2_handle, 'ForegroundColor','Black');
    end
    function XBATpresetsize(temprank_fig, ~)
        xbat_preset_size = get(xbat_preset_size_handle,'String');
        setappdata(temprank_fig,'xbat_preset_size',str2double(xbat_preset_size))
        set(xbat_preset_size_handle,'FontAngle','Normal');
    end
%
waitfor(run_button_handle,'FontAngle','Italic')
delete(temprank_fig)
end