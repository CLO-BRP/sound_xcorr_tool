function [f0Peaks] = correlateCA_quick_v2(figdata, status_window_edit_handle);


%%
%% 'correlateCA_quick.m'
%% 
%% Kathryn A. Cortopassi, August 2004
%%
%% calculate all the pair-wise spectrogram crosses for the cell-array, and
%% return the peak correlation values and corresponding time delays
%% do the cross in the fast method that assumes no frequency lag
%% 
%% syntax: 
%% -------
%%   f0Peaks = correlateCA_quick(spec, tvec, normtype, maxlag, crossflag);
%%
%% input:
%% ------
%%   spec == a cell array of spectrograms
%%   tvec == a cell array of time vectors
%%   normtype == spectrogram normalization to use, either
%%              'colnorm'; 'column' for column-wise normalization
%%              'matnorm'; 'matrix' for matrix-based normalization
%%   maxlag == max lag in seconds
%%   crossflag == flag signifying the pattern of crosses to perform, either
%%                'sqr'; 'square'; 'pairwise' for all-by-all correlation
%%                'col'; 'column' for 1-by-all correlation
%%   status_window == handle to status window for message display (optional)
%%
%% output:
%% -------
%%   f0Peaks == a cell array containing the peak correlation values,
%%              and their corresponding time lags
%%



% %% check that enough input arguments were given
% argLo = 5; argHi = inf;
% error(nargchk(argLo, argHi, nargin));
% 
% 
% %% check that spec input is a cell array
% if ~iscell(spec)
%   error('spectrogram input must be a cell array of spectrograms');
%   return;
% end
% 
% %% check that tvec input is a cell array
% if ~iscell(tvec)
%   error('time vector input must be a cell array of time vectors');
%   return;
% end
% 
% 
% % let the check be done in standardize_mat only
% %% check normalization type
% if ~sum(strcmpi(normtype, {'colnorm'; 'column'; 'matnorm'; 'matrix'; 'none'}))
%   error(sprintf('normalization flag ''%s'' not recognized', normtype));
%   return;
% end
% 
% %% check crossflag type
% if ~sum(strcmpi(crossflag, {'sqr'; 'square'; 'pairwise'; 'col'; 'column'}))
%   error(sprintf('cross flag ''%s'' not recognized', crossflag));
%   return;
% end
% 
% 
% num_spcs = length(spec);
% 
% if num_spcs ~= length(tvec)
%   error('numbers of spectrograms and time vectors do not match');
%   return;
% end
% 
% %% check consistency of time vectors, and frequency rows
% 
% for i = 1:num_spcs
%   [nfrows(i), ntimecols(i)] = size(spec{i});
%   dt(i) = tvec{i}(2);
% end
% 
% if (max(nfrows) ~= min(nfrows))
%   error('number of frequency bands in spectrograms do not match');
%   return;
% end
% 
% if (max(dt) ~= min(dt))
%   error('time resolution of spectrograms does not match');
%   return;
% end



sampRate = [figdata.sound_list.samplerate];
sndName = {figdata.sound_list.filename};
sndPath = {figdata.sound_list.path};
sndLen = [figdata.sound_list.num_samples];

    
%% get sample rate as scalar (all sample rates are the same once sounds are committed)
fs = sampRate(1);

corr_type = figdata.param.corr_type;
    %% get meat of corr_type string
    crossflag = corr_type(7:end);
    
normtype = figdata.param.corr_std;
% 
maxlag = figdata.param.max_time_lag;


%% get number of sounds
num_snds = length(figdata.sound_list);


  
  fftLen = figdata.param.fft_len;
  dataLen = round(fftLen*figdata.param.data_len/100);
  overlap = round(dataLen*figdata.param.data_overlap/100);
  





%% get time resolution 
%dt = dt(1);
dt = (dataLen - overlap) / fs;

%% convert max lag in time to bins
maxlag = ceil(maxlag / dt);

%% get length of longest duration spectrogram in bins
maxdur = ((max(sndLen) - dataLen) / (dataLen - overlap)) + 1;

%% make sure maxlag does not exceed the max possible
maxlag = min(maxlag, (maxdur - 1));

% %% clean up some space
% clear nfrows;
% clear ntimecols;
% clear tvec;
% 






% %% standardize the spectrograms as requested
% for i = 1:num_spcs
%   spec{i} = standardize_mat(spec{i}, normtype);
% end
% 

% %% zero-pad the spectrograms to match in duration
% for i=1:num_spcs
%   if size(spec{i}, 2) < maxdur
%     spec{i}(:, maxdur) = 0; 
%   end
% end


if exist('status_window')
  %% get the current status text
  current_display = get(status_window, 'string');
end


if sum(strcmpi(crossflag, {'sqr'; 'square'; 'pairwise'}))
  %% calculate all (unique) pair-wise crosses
  
  %% pre-fill the square peak value and lag arrays
  f0Peaks{1} = ones(num_snds, num_snds);
  f0Peaks{2} = zeros(num_snds, num_snds);
  
  if sum(strcmpi(normtype, {'matnorm'; 'matrix'}))
    for i = 1:(num_snds - 1)
      for j = 2:num_snds
        if (i < j)
          
          txt_message = sprintf('Correlating sounds %d X %d', i, j);
          if exist('status_window')
            % display_status_line(status_window, txt_message);
            %% flash the requested message line, but don't add it to the end of the status window
            tmp_display = current_display;
            tmp_display{end+1} = txt_message;
            set(status_window, 'string', tmp_display, 'listboxtop', length(tmp_display));
            drawnow;
          else
            fprintf(1, [txt_message, '\n']);
          end
          
          
          %% get spec data
          
spec_i = get_spec([sndPath{i}, sndName{i}], fs, figdata.param, maxdur);
spec_j = get_spec([sndPath{j}, sndName{j}], fs, figdata.param, maxdur);
 
          
          %% get correlation function and lag vector in bins
          [corrfunc, lagvec] = specx_time(spec_i, spec_j, maxlag);
          
          %% convert lag vector to times
          lagvec = lagvec * dt;
          
          %% get peak values
          [peakval, peakinx] = max(corrfunc);
          
          %% store in f0 peaks as a num_snds-by-num_snds square matrix
          f0Peaks{1}(i, j) = peakval;
          f0Peaks{1}(j, i) = peakval;
          
          f0Peaks{2}(i, j) = lagvec(peakinx);
          f0Peaks{2}(j, i) = -lagvec(peakinx);
          
          
          clear corrfunc;
          clear lagvec;
          clear peakval;
          clear peakinx;
          pack;
          
        end
      end
    end
    
  else %% need to get diagonal too
    for i = 1:num_snds
      for j = 1:num_snds
        if (i <= j)
          
          txt_message = sprintf('Correlating sounds %d X %d', i, j);
          if exist('status_window')
            % display_status_line(status_window, txt_message);
            %% flash the requested message line, but don't add it to the end of the status window
            tmp_display = current_display;
            tmp_display{end+1} = txt_message;
            set(status_window, 'string', tmp_display, 'listboxtop', length(tmp_display));
            drawnow;
          else
            fprintf(1, [txt_message, '\n']);
          end
          
          
          %% get correlation function and lag vector in bins
          [corrfunc, lagvec] = specx_time(spec{i}, spec{j}, maxlag);
          
          %% convert lag vector to times
          lagvec = lagvec * dt;
          
          %% get peak values
          [peakval, peakinx] = max(corrfunc);
          
          %% store in f0 peaks as a num_snds-by-num_snds square matrix
          f0Peaks{1}(i, j) = peakval;
          f0Peaks{1}(j, i) = peakval;
          
          f0Peaks{2}(i, j) = lagvec(peakinx);
          f0Peaks{2}(j, i) = -lagvec(peakinx);
          
          
          clear corrfunc;
          clear lagvec;
          clear peakval;
          clear peakinx;
          pack;
          
        end
      end
    end
  end
  
  
else
  %% calculate the 1-by-all cross (i.e., all other spectrograms 
  %% crossed with spectrogram #1) 
  
  %% pre-fill the peak value and lag vectors
  f0Peaks{1} = ones(num_snds, 1);
  f0Peaks{2} = zeros(num_snds, 1);
  
  if sum(strcmpi(normtype, {'matnorm'; 'matrix'}))
    for i = 2:num_snds
      
      txt_message = sprintf('Correlating sounds 1 X %d', i);
      if exist('status_window')
        % display_status_line(status_window, txt_message);
        %% flash the requested message line, but don't add it to the end of the status window
        tmp_display = current_display;
        tmp_display{end+1} = txt_message;
        set(status_window, 'string', tmp_display, 'listboxtop', length(tmp_display));
        drawnow;
      else
        fprintf(1, [txt_message, '\n']);
      end
      
      %% get correlation function and lag vector in bins
      [corrfunc, lagvec] = specx_time(spec{1}, spec{i}, maxlag);
      
      %% convert lag vector to times
      lagvec = lagvec * dt;
      
      %% get peak values
      [peakval, peakinx] = max(corrfunc);
      
      %% store in f0 peaks as a (num_snds-1) length column
      f0Peaks{1}(i) = peakval;
      
      f0Peaks{2}(i) = lagvec(peakinx);
      
      
      clear corrfunc;
      clear lagvec;
      clear peakval;
      clear peakinx;
      pack;
      
    end
    
  else %% need to corr 1 x 1 too
    for i = 1:num_snds
      
      txt_message = sprintf('Correlating sounds 1 X %d', i);
      if exist('status_window')
        % display_status_line(status_window, txt_message);
        %% flash the requested message line, but don't add it to the end of the status window
        tmp_display = current_display;
        tmp_display{end+1} = txt_message;
        set(status_window, 'string', tmp_display, 'listboxtop', length(tmp_display));
        drawnow;
      else
        fprintf(1, [txt_message, '\n']);
      end
      
      %% get correlation function and lag vector in bins
      [corrfunc, lagvec] = specx_time(spec{1}, spec{i}, maxlag);
      
      %% convert lag vector to times
      lagvec = lagvec * dt;
      
      %% get peak values
      [peakval, peakinx] = max(corrfunc);
      
      %% store in f0 peaks as a (num_snds-1) length column
      f0Peaks{1}(i) = peakval;
      
      f0Peaks{2}(i) = lagvec(peakinx);
      
      
      clear corrfunc;
      clear lagvec;
      clear peakval;
      clear peakinx;
      pack;
      
    end
  end
  
end  









return;