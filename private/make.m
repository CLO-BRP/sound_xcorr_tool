function make(parms, mode)
%
%  Routine:
%       make(parms, mode)
%
%  Inputs:
%       parms.rootpath - rootpath for the ASE toolbox
%       parms.mode - mode, either deploy of src (may be removed for future.)
%
%  Outputs:
%
%  Description:
%       ASE_make routine that is called to make the ASE tools located in
%       the enclosed directory.  
%
% History
%   PDugan       January 2009       Ver 1.0    January 2009
%   JZollweg     January 2013       Ver 1.1    January 2013
%

% THE SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS AND BRP MAKES NO 
% REPRESENTATIONS OR WARRANTIES (WRITTEN OR ORAL). TO THE MAXIMUM EXTENT  
% PERMITTED BY APPLICABLE LAW, BRP DISCLAIMS ALL WARRANTIES AND CONDITIONS,  
% EXPRESS OR IMPLIED, AS TO ANY MATTER WHATSOEVER AND TO ANY PERSON OR  
% ENTITY, INCLUDING, BUT NOT LIMITED TO, ALL IMPLIED WARRANTIES OF  
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND  
% NON-INFRINGEMENT OF THIRD PARTY RIGHTS AND THOSE ARISING FROM A COURSE OF  
% DEALING OR USAGE IN TRADE. NO WARRANTY IS MADE THAT ANY ERRORS OR DEFECTS  
% IN THE SOFTWARE WILL BE CORRECTED, OR THAT THE SOFTWARE WILL MEET YOUR  
% REQUIREMENTS.  END USERS SHALL NOT COPY OR REDISTRIBUTE THE SOFTWARE  
% WITHOUT WRITTEN PERMISSION FROM BRP. 
 
% LIMITATION OF LIABILITY 
 
% IN NO EVENT SHALL BRP OR ITS DIRECTORS, FACULTY, OR EMPLOYEES, BE LIABLE  
% FOR DAMAGES TO OR THROUGH YOU OR ANY OTHER PERSON OR ENTITY FOR BREACH  
% OF, ARISING UNDER, OR RELATED TO THIS AGREEMENT OR THE USE OF SOFTWARE OR  
% DOCUMENTATION PROVIDED HEREUNDER, UNDER ANY THEORY INCLUDING, BUT NOT  
% LIMITED TO, DIRECT, SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, OR  
% SIMILAR DAMAGES (INCLUDING WITHOUT LIMITATION, DAMAGES FOR LOSS OF  
% BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION OR  
% DATA, OR ANY OTHER LOSS) WHETHER FORESEEABLE OR NOT, REGARDLESS OF THE  
% FORM OF ACTION, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT  
% LIABILITY OR OTHERWISE.


PROJ_ROOT = [parms.rootpath];

mypath = mfilename('fullpath');
subdir = fileparts(mypath);
subdir = strrep(subdir, PROJ_ROOT, '');
TARG_DIR_BASE = [parms.targetpath subdir];

switch lower(mode)
   
    case 'src'
        
        inc_src_flag = 1;
        
    case 'deploy'
        
        inc_src_flag = 0;
        
    case 'clean'
        
        ASE_Make_Clean(TARG_DIR_BASE);
        
        return;
        
    otherwise 
        
        parms.mode = 'deploy';
        
        ASE_make_utils(parms);
        
end

% find m-files
files = dir([PROJ_ROOT subdir]);
names = {files.name};
[~, ~, exts] = cellfun(@fileparts, names, 'UniformOutput', false);
LIB_SRC = names(strcmp('.m', exts));

% remove make.m from list of sources
mkfileidx = strcmp('make.m', LIB_SRC);
LIB_SRC(mkfileidx) = [];
mkfileidx = strcmp('make2.m', LIB_SRC);
if ~isempty(mkfileidx)
    LIB_SRC(mkfileidx) = [];
end
LIB_SRC = cellfun(@(x) [subdir filesep x], LIB_SRC, 'UniformOutput', false);

% find non-source files to copy
NON_SRC = names(strcmp('.mat', exts) | strcmp('.jpg', exts) | ...
                strcmp('.fig', exts) | strcmp('.png', exts) | ...
                strcmp('.pdf', exts) | strcmp(['.' mexext], exts) | ...
                strcmp('.bmp', exts) | strcmp('.ico', exts) | ...
                strcmp('.wav', exts) | strcmp('.license', exts));
if ispc
    NON_SRC = [NON_SRC names(strcmpi('.dll', exts) | strcmpi('.exe', exts))];
end
if isunix
    NON_SRC = [NON_SRC names(strcmpi('.so', exts) | strcmpi('.dylib', exts))];
end
NON_SRC = cellfun(@(x) [subdir filesep x], NON_SRC, 'UniformOutput', false);

ASE_Make_Proj_Code( ....
            '-ml_ver', parms.VerStr.Release, ...
            '-ex_ver', parms.ExtVer, ...
            '-thread', parms.ThreadMode, ...
            '-proj_root', PROJ_ROOT, ...
            '-targ_dir_base', TARG_DIR_BASE, ...
            '-lib_src', LIB_SRC, ...
            '-inc_src', inc_src_flag ...
            );


ASE_Make_Proj_Code( ....
            '-ml_ver', parms.VerStr.Release, ...
            '-ex_ver', parms.ExtVer, ...
            '-thread', parms.ThreadMode, ...
            '-proj_root', PROJ_ROOT, ...
            '-targ_dir_base', TARG_DIR_BASE, ...
            '-lib_src', NON_SRC, ...
            '-inc_src', 1 ...
            );

dirs = [files.isdir];
hdir = pwd;
if any(dirs)   % recursively build subdirectories that have make's
    dirnames = names(dirs);
    for d = dirnames
        subd = d{:};
        if strcmp('.', subd) || strcmp('..', subd)
            continue
        end
        if ~isempty(dir([PROJ_ROOT subdir filesep subd filesep 'make.m']))
            if subd(1) == '+'
                ns = subd(2:end);
                makenm = [ns '.make'];
                mkfun = str2func(makenm);
                mkfun(parms, 'clean');
                mkfun(parms, parms.mode);
            elseif subd(1) == '@'
                funclass = [PROJ_ROOT subdir filesep subd];
                tmp = [PROJ_ROOT filesep 'temp'];
                mkdir(tmp)
                copyfile(funclass, tmp);
                cd(tmp);
                ttmp = [parms.targetpath 'temp'];
                mkdir(ttmp);
                copyfile('make.m', 'make2.m');
                make2(parms, 'clean');
                make2(parms, parms.mode);
                cd(hdir);
                rmdir(tmp, 's');
                targdir = [TARG_DIR_BASE filesep subd];
                movefile(ttmp, targdir);
            else
                cd([PROJ_ROOT filesep subdir filesep subd])
                make(parms, 'clean');
                make(parms, parms.mode);
                cd(hdir);
            end
        end
    end
end
                