function [snd] = demeansnd(snd);

%**
%** "demeansnd.m"
%**
%** Kathryn A. Cortopassi, March 2005
%**
%** Function to demean a sound waveform or cell array of sound waveforms
%** 
%** Syntax: [snd] = demeansnd(snd);
%**
%** 'snd' is a single sound waveform or cell array of sound waveforms
%** 
%** 'snd' returns the demeaned sound waveform(s)  
%**


% check for correct number of inputs
argLo = 1; argHi = inf;
error(nargchk(argLo, argHi, nargin));


if iscell(snd)   
  %% demean the sound waveforms     
  for i = 1:length(snd)
    snd{i} = snd{i} - mean(snd{i});
  end
  
else
  %% demean the sound waveform     
  snd = snd - mean(snd);
  
end


% end function
return;