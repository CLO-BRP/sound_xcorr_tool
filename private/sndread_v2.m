function [datavec, Fs] = sndread_v2(fname, status_window_handle, display_flag)

%
% 'sndread_v2.m'
%
% Kathryn A. Cortopassi, 2002
%
% Read in a sound file of any one of the following
% formats: Signal/RTSD, Wave, Aiff, AU
% The format is automatically determined
% Files can be named in any way, no special extensions
% are required
%
% Syntax:
% [datavec, Fs] = sndread_v2(fname)
% [datavec, Fs] = sndread_v2(fname, wh)
%
% 'fname' is the sound file name to read
% 'status_window_handle' is the handle to list box for progress
%      display (if not included, progress is displayed to command window)
% 'display_flag' indicates whether to display progress to command
%      window if status_window_handle is not included
%
% 'datavec' is the vector of sound data
% 'Fs' is the sampling rate
%

%
% by Kathryn A. Cortopassi
% created 21-May-2002
%
% modified August/Fall 2004
% to include optional updates to listbox
% status window, and some other changes
%
% modified February 2006
% to control output to command line
% with display_flag
%


% first some error checking
argLo = 1; argHi = inf;
error(nargchk(argLo, argHi, nargin));

if exist('status_window_handle') & ~isempty(status_window_handle)
  display2statuswindow = 1;
else
  display2statuswindow = 0;
end

if exist('display_flag') & ~isempty(display_flag)
  display2com = 1;
else
  display2com = 0;
end


name = relnameof(fname);
fid = fopen(fname);
if fid == -1
  %% file not found, report and close
  if display2statuswindow
    display_status_line(status_window_handle, sprintf('File ''%s'' is not found', name));
  elseif display2com
    fprintf(1, 'File ''%s'' is not found\n', name);
  end
  datavec = [];
  Fs = [];
  return;
else
  status = fclose(fid);
  %   if status == 0
  %     fprintf('closing file... successful\n');
  %   else
  %     fprintf('closing file... unsuccessful\n');
  %   end
end


% then open the sound file
if ~sigreaderr(fname)
  [datavec, header] = rsig(fname);
  Fs = header.Fs;
  if display2statuswindow
    display_status_line(status_window_handle, sprintf('Reading SIGNAL/RTSD file ''%s''... SR = %d', name, Fs));
  elseif display2com
    fprintf('Reading SIGNAL/RTSD file ''%s''... Sample rate = %d\n', name, Fs);
  end

elseif ~wavreaderr(fname)
  [datavec, Fs] = wavread(fname);
  if display2statuswindow
    display_status_line(status_window_handle, sprintf('Reading WAVE file ''%s''... SR = %d', name, Fs));
  elseif display2com
    fprintf('Reading WAVE file ''%s''... Sample rate = %d\n', name, Fs);
  end
  
elseif ~aifreaderr(fname)
  Fs = AiffReadHeader(fname);
  datavec = AiffReadSamples(fname, 0, inf, 1);
  if display2statuswindow
    display_status_line(status_window_handle, sprintf('Reading AIFF file ''%s''... SR = %d', name, Fs));
  elseif display2com
    fprintf('Reading AIFF file ''%s''... Sample rate = %d\n', name, Fs);
  end
  
  %%% Modified by SCK
  elseif ~aifreaderr_SCK(fname)
  %Fs = AiffReadHeader(fname);
  %datavec = AiffReadSamples(fname, 0, inf, 1);
  [datavec,Fs] = aiffread(fname);
  if display2statuswindow
    display_status_line(status_window_handle, sprintf('Reading AIFF file ''%s''... SR = %d', name, Fs));
  elseif display2com
    fprintf('Reading AIFF file ''%s''... Sample rate = %d\n', name, Fs);
  end

elseif ~aureaderr(fname)
  [datavec, Fs] = auread(fname);
  if display2statuswindow
    display_status_line(status_window_handle, sprintf('Reading AU file ''%s''... SR = %d', name, Fs));
  elseif display2com
    fprintf('Reading AU file ''%s''... Sample rate = %d\n', name, Fs);
  end

else
  if display2statuswindow
    display_status_line(status_window_handle, sprintf('Unknown format for file ''%s''...', name));
  elseif display2com
    fprintf('Unknown format for file ''%s''... Sample rate = %d\n', name, Fs);
  end
  datavec = [];
  Fs = [];

end

%% make sure that all sound files really get closed after reading
%% it looks like the Aiff file read functions aren't closing the
%% files after use
fclose('all');


return;