function Spec = subFROWsubMATper(Spec, P, data_form, adjustment);

%**
%** "subFROWsubMATper.m" 
%**
%** Kathryn A. Cortopassi, 2001-2002
%**
%** Syntax: Spec = subFROWsubMATper(Spec, P, data_form, adjustment);
%**
%** A function to zero P% of the data on a row-wide then matrix-wide basis;
%** various adjustments can be made to the masked (unthresholded) values 
%**
%** 'Spec' == an MxN spectrogram array (or any array of values)
%** 'P' == is a percent value
%** 'data_form' == a flag telling how the values are stored,
%**                Amplitude, Power, or dB
%** 'adjustment' == a flag telling how to deal with masked values after thresholding,
%**                 None, Bias, Demean, or Binarize
%**
%** NOTE: when you want to accomplish true additive noise correction,
%**       data_form should be power, and adjustment should be bias
%**

%**
%** by Kathryn A. Cortopassi
%** created 21-Sept-2001
%** modified 23-May-2002
%**
%** modifications March 2005
%** 


%% check for the correct number of inputs
argLo = 4; argHi = inf;
error(nargchk(argLo, argHi, nargin));

%% split up P %tile
P = [(-200 + sqrt(200^2 - 4*(-1)*(-100*P))) / (2*(-1)), (-200 - sqrt(200^2 - 4*(-1)*(-100*P))) / (2*(-1))];
P = P(find(P > 0 & P < 100));


%% do narrowband thresholding

%% 'Spec' is a MxN matrix, with M frequency rows and N time columns; 
[N_freqRow, N_timeCol] = size(Spec);

%% convert P to a column N_freqRow long
P = zeros(N_freqRow, 1) + P;

%% sort the rows of 'Spec' in ascending order by first transposing 'Spec' 
%% and sorting the resulting columns
SpecDist = sort(Spec');

%% SpecDist is an NxM matrix; N = N_timeCol, M = N_freqRow
%% find our NB absolute threshold for each frequency row as the Pth percentile value for 
%% each column of 'SpecDist' (which equals the sorted rows of 'Spec') by going to the 
%% index which equals P% of N rounded to the nearest integer
%% that is, go to the fRow value for which P% of the other fRow values are less than it
%% since P is a vector, we need to adjust it to index into the right value
%% in the SpecDist matrix using advanced indexing
perc_inx = round(N_timeCol .* (P./100));
perc_inx = min(N_freqRow, max(1, perc_inx));
perc_inx = ([0:N_freqRow-1] .* N_timeCol)' + perc_inx;

%% the resulting matrix of percentile values 'NBabs_thresh' is a 1xM matrix (i.e., a percentile 
%% value (or absolute threshold) for each frequency row 1:M)
NBabs_thresh = SpecDist(perc_inx);
%% expand this across the rows to match size of Spec
NBabs_thresh = NBabs_thresh * ones(1, N_timeCol);

%% find indices of values equal to or below NB absolute threshold values for each frequency row
NBnoise_inx = (Spec - NBabs_thresh) <= 0;
NBmask_inx = ~NBnoise_inx;

%% make a working copy of Spec
Spec_wk = Spec;

%% zero out the spectrogram values below NB abs_threshold (considered noise)
Spec_wk(NBnoise_inx) = 0;

%% bias by the NB abs threshold now
Spec_wk(NBmask_inx) = Spec_wk(NBmask_inx) - NBabs_thresh(NBmask_inx); 



%% do broadband thresholding on Spec_wk

%% unwrap the spectrogram matrix column-wise and sort the values in ascending order
SpecDist = sort(Spec_wk(:));

%% find our BB absolute threshold as the Pth percentile value of 'SpecDist' by going to the 
%% index which equals P% of N 
%% N is the total length 'SpecDist', P% of N equals (NP/100) rounded to the nearest integer
num_bin = length(SpecDist);
perc_inx = round((P(1)/100) * num_bin);
perc_inx = min(num_bin, max(1, perc_inx));
BBabs_thresh = SpecDist(perc_inx);


%% find indices of values equal to or below BB absolute threshold value
%% these result in our our final mask indices
noise_inx = (Spec_wk - BBabs_thresh) <= 0;
mask_inx = ~noise_inx;

%% zero out the actual spectrogram values below combined abs_threshold's (considered noise)
Spec(noise_inx) = 0;


%% adjust the masked (unthresholded) spectrogram values
switch (adjustment)
  
  case {'None'}
    %% do nothing 
    
  case {'Bias'}
    %% bias by the BB abs threshold now
    Spec(mask_inx) = Spec_wk(mask_inx) - BBabs_thresh; 
    
  case {'Demean'}
    Spec(mask_inx) = Spec(mask_inx) - mean(Spec(mask_inx)); 
    
  case {'Binarize'}
    Spec(mask_inx) = 1; 
    
  otherwise
    error(sprintf('Mask adjustment flag ''%s'' not recognized!', data_form));
    
end



%% end function
return;