function error = aifreaderr(ffname)

%
% 'aifreaderr.m'
% 
% Kathryn A. Cortopassi, 2002
%
% Function to report an error reading an Aiff file
%

%
% by Kathryn A. Cortopassi
% created 21-May-2002
%


error = 0;
evalc('AiffReadHeader(ffname);', 'error = errorcaught;');

return;