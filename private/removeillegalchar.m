function astring = removeillegalchar(astring)

%
% 'removeillegalchar.m'
%
% Kathryn A. Cortopassi, 2002
%
% funtion to remove any non-alpha-numeric characters
% and replace them with the underscore character
%

%
% By Kathryn A. Cortopassi
% created 15-May-2002
%


astring(find(astring < 48 | astring > 122 | (astring >= 58 & astring <= 64) | (astring >= 91 & astring <= 96))) = '_';
return;
