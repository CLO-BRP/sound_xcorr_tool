function [corrfunc, lagvec] = specx_time(spec1, spec2, maxlag);


%%
%% 'specx_time'
%%
%% Kathryn A. Cortopassi, August 2004
%%
%% perform a matrix (typically spectrogram) cross-correlation
%% over varying time lags; return the cross-correlation function
%%
%%
%% syntax:
%% -------
%%   corrfunc = specxcorr_t(spec1, spec2, maxlag);
%%
%% input:
%% ------
%%   spec1 == spectrogram matrix
%%   spec2 == spectrogram matrix
%%   maxlag == max allowed time lag in bins
%%
%% output:
%% -------
%%   corrfunc == cross-correlation function
%%   lagvec == lag vector in bins
%%


%% check that number of rows, i.e., frequency bands are equal
if size(spec1, 1) ~=  size(spec2, 1)
  error('number of rows (frequency bands) for matrices (spectrograms) are not equal');
  return;
end


%% calculate the spectrogram cross
crossmat = (spec2') * (spec1);


[m, n] = size(crossmat);

%% make sure the specified lag range does not exceed the
%% available lag range
krange = max(-maxlag, -(m-1)):min(maxlag, n-1);


%% generate the cross-correlation function for the
%% lag range
index = 0;
for k = krange
  
  index = index + 1;
  
  lagvec(index) = k;
  
  corrfunc(index) = sum(diag(crossmat, k));
  
end


return;