function Spec = KMF_denoise_v2(Spec, P, data_form, adjustment);

%% 
%% KMF_denoise_v2
%% 
%% denoise the spectrogram based on a low order statistic for each frequency band 
%% and a matrix-wide percentile; this is the same style denoising used in 
%% Acoustat by K.M. Fristrup
%%
%% Kathryn A. Cortopassi
%% March 2005
%%
%% Based on KMF_denoise from August 2004 with modification for ver 2 where a low order 
%% statistic is used instead of median
%%
%% ** NOTE: when you want to accomplish true additive noise correction,
%% **       data_form should be power, and adjustment should be bias
%% **       anything else is just a masking...
%%



%% convert data to power for noise calcs
switch (data_form)
  case {'Amp'; 'Amplitude'}
    Spec = Spec .^ 2;
  case {'dB';'dBpow'}
    Spec = 10 .^ (Spec ./ 10);
  case {'Pow'; 'Power'}
    %% do nothing
  otherwise
    error(sprintf('Data format flag ''%s'' not recognized!', data_form));
end


P = P / 100;


%% get an estimate of the band noise based on a low order statistic
%% 7th percentile
low_order_p = 0.07;

%% bandnoise is a low order estimate of the noise spectrum
bandnoise = sort(Spec, 2);
num_col = size(Spec, 2);
low_ord_inx = round(low_order_p * num_col);
low_ord_inx = min(num_col, max(1, low_ord_inx));
bandnoise = bandnoise(:, low_ord_inx);


if sum(bandnoise) == 0 
  %% convert data back to original form
  switch (data_form)
    case {'Amp'; 'Amplitude'}
      Spec = sqrt(Spec);
    case {'dB';'dBpow'}
      Spec = 10*log10(Spec);
  end
  %% drop out (skip processing of event with zero noise estimate)
  fprintf('Zero noise estimate in all bands-- no denoising needed.\n');
  return;
  
else
  
  %% set zero noise estimate to the minimum noise value
  bandnoise(bandnoise == 0) = min(bandnoise(bandnoise ~= 0)); 
  
  %% expand bandnoise column vector to the same size as Spec array 
  bandnoise = bandnoise * ones(1, num_col);
  
  %% find a multipler for the bandnoise array that is the Pth
  %% percentile value of the spectrogram in which each band has been 
  %% divided by its estimated band noise (i.e., we generate something like 
  %% a signal to noise ratio (but not exactly) and find the Pth %tile
  %% value of this distribution-- P% of data have a ratio of data value
  %% to bandnoise less than this %tile value (and 100-P% have a ratio
  %% greater than this value)
  %% this multiplier allows us to eliminate P% of the bins below
  snRatio = sort(Spec(:) ./ bandnoise(:));
  num_bin = length(snRatio);
  perc_inx = round(P * num_bin);
  perc_inx = min(num_bin, max(1, perc_inx));
  snRatio = snRatio(perc_inx);
  
  %% use snRatio %tile value and bandnoise to get the absolute threshold for each 
  %% frequency band
  abs_thresh = snRatio * bandnoise;
  
  %% find indices of values equal to or below absolute threshold values for each frequency band
  noise_inx = (Spec - abs_thresh) <= 0;
  mask_inx = ~noise_inx;
  
  
  %% this would be done if you wanted true noise power correction
  %% all the time, regardless of the data format on input
  % % mask out the spectrogram values considered noise
  % Spec(noise_inx) = 0;
  % % subtract the noise estimate from the remaining values
  % Spec(mask_inx) = Spec(mask_inx) - abs_thresh(mask_inx); 
  % % convert the data back to its original form
  % switch (data_form)
  %   case {'Amp'; 'Amplitude'}
  %     Spec(mask_inx) = sqrt(Spec(mask_inx));
  %   case {'dB';'dBpow'}
  %     Spec(mask_inx) = 10*log10(Spec(mask_inx));
  %     Spec(mask_inx) = Spec(mask_inx) - min(Spec(mask_inx));
  % end
  
  
  %% convert data back to original format; and convert absolute threshold value
  %% to that format
  switch (data_form)
    case {'Amp'; 'Amplitude'}
      Spec = sqrt(Spec);
      abs_thresh = sqrt(abs_thresh);
    case {'dB';'dBpow'}
      Spec = 10*log10(Spec);
      abs_thresh = 10*log10(abs_thresh);
  end
  
  %% zero out the spectrogram values below abs_threshold (considered noise)
  Spec(noise_inx) = 0;
  
  %% adjust the masked (unthresholded) spectrogram values
  switch (adjustment)
    case {'None'}
      %% do nothing 
    case {'Bias'}
      Spec(mask_inx) = Spec(mask_inx) - abs_thresh(mask_inx); 
    case {'Demean'}
      Spec(mask_inx) = Spec(mask_inx) - mean(Spec(mask_inx)); 
    case {'Binarize'}
      Spec(mask_inx) = 1; 
    otherwise
      error(sprintf('Mask adjustment flag ''%s'' not recognized!', data_form));
  end
  
end %% if sum(bandnoise) == 0 




return;
