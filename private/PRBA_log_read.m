function s = PRBA_log_read(inLogFname)
%	
%  Usage:
%       s = PRBA_log_add_tag(s, tagstr)
%
%  Input: 
%        inLogFname - input filename and path
%  Output:
%        s - cleaned log file
% 
% 
%  Description: 
%       File is used to read Log file and ensure that the internal variable
%       and the filename embedded in the log structure are set correctly.
%       This error occurs if one copies a Log file to another name; while
%       the fileneme is changed the inside variable name and structure name
%       are not changed.
% 

%  Description
%
%   PDugan                  May 2008


% set if using stand alone
if nargin < 1
    return;
end

% parse files names and init varaibles
[pth, nme, ext, ver] = fileparts(inLogFname);

% parse out crapy chars, matlab does not like these anyway.

rslt = findstr(nme, '-');

if ~isempty(rslt)
    
    nme(rslt) = '_';    
           
end


inLogVname = ['Log_' nme];

% load file
t = load(inLogFname);
try
    eval(['s = t.' char(fieldnames(t)) ';']);
catch 
    disp('stop');
    s = [];
    return;
end

% make sure correct filename is embedded in the Log

s.file = nme;

eval([inLogVname  ' = s;']);

