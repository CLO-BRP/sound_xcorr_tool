function [spec, fvec, tvec] = specgramCA(waveform, fftLen, sampRate, window, overlap,...
  data_form, sndName, status_window_edit_handle);

%%
%% "specgramCA"
%%
%% Kathryn A. Cortopassi, August 2004
%% 
%% Generate short-time Fourier transform functions, STFTs, (i.e., spectrograms)
%% for a cell array of waveforms
%%
%% syntax:
%% -------
%%   [spec, fvec, tvec] = specgramCA(waveform, fftLen, sampRate, window, overlap,...
%%                  data_form, sndName, status_window_edit_handle);
%%
%% input:
%% ------
%%   waveform == a cell array of time wavforms
%%   fftLen  ==  FFT length in points
%%   sampRate == sampling rate of the waveforms-- either a single value, or 
%%               a vector or cell array of values
%%   window  ==  analysis window function in a numeric array
%%   overlap ==  window overlap in points
%%   data_form == form of the data to be stored in the STFT
%%                either 'amp' -- mag(coeff)
%%                       'pow' -- mag(coeff)^2
%%                       'db' -- 10log10(Pow) 
%%                       'complex' -- coeff
%%                (default == 'complex')
%%
%% output:
%% -------
%%   spec = a cell array of spectrograms
%%   fvec = cell array of frequency data vectors for the spectrograms 
%%   tvec = cell array of time data vectors for the spectrograms
%%



% check for correct number of input arguments
argLo = 5; argHi = inf;
error(nargchk(argLo, argHi, nargin));


if ~exist('data_form', 'var') | isempty(data_form)
  data_form = 'complex';
end

if ~exist('sndName', 'var') | isempty(sndName)
  for inx = 1:length(waveform)
    sndName{inx} = num2str(inx);
  end
end

if ~exist('status_window_edit_handle', 'var') | isempty(status_window_edit_handle)
  display2statuswindow = 0;
else
  display2statuswindow = 1;
end



if ~sum(strcmpi(data_form, {'amp'; 'Amplitude'; 'pow'; 'Power'; 'db'; 'complex'}))
  error(sprintf('Data data_form flag ''%s'' is not recognized!', data_form));
  return;
end

if iscell(sampRate)
  % convert sampRate to a vector  maybe use cell2mat instead?
  %sampRate = [sampRate{:}];
  sampRate = ones(1,length(sampRate))* sampRate{1};
end
numFs = length(sampRate);



if iscell(waveform)
  
  numSnd = length(waveform);
  if numFs ~= numSnd & numFs ~= 1
    error(sprintf('Number of sampling rates provided is not valid!\n'));
    return;
  end    
  
  %% pre-allocate cell arrays
  spec = cell(numSnd, 1);
  fvec = cell(numSnd, 1);
  tvec = cell(numSnd, 1);
  
  
  for i = 1:numSnd
    
    if numFs == numSnd
      
      try
        
        [spec{i}, fvec{i}, tvec{i}]= specgram(waveform{i}, fftLen, sampRate(i), window, overlap);
        
        if sum(strcmpi(data_form, {'amp'; 'Amplitude'}))
          spec{i} = abs(spec{i});
        elseif sum(strcmpi(data_form, {'pow'; 'Power'}))
          spec{i} = spec{i}.*conj(spec{i});
        elseif  sum(strcmpi(data_form, {'db'}))
          spec{i} = 10*log10(spec{i}.*conj(spec{i}));
          %% else data_form ==> 'complex'
          %% do nothing
        end 
        
      catch
        %% empty cell marks failed specgram attempt
        
        %% get error message
        error_info = lasterror;
        curr_err_msg = ['Error making spectrogram for sound: ', sndName{i}, ' >> ''', error_info.message, ''''];
        
        if display2statuswindow 
          current_display = get(status_window_edit_handle, 'string');
          current_display{end+1} = curr_err_msg;
          set(status_window_edit_handle, 'string', current_display, 'listboxtop', length(current_display));
          drawnow;
        else
          fprintf(1, curr_err_msg);
        end
        
      end %% try/catch
      
      
      
    else %% numFs ~= numSnd
      
      try
        
        [spec{i}, fvec{i}, tvec{i}] = specgram(waveform{i}, fftLen, sampRate, window, overlap);
        
        if sum(strcmpi(data_form, {'amp'; 'Amplitude'}))
          spec{i} = abs(spec{i});
        elseif sum(strcmpi(data_form, {'pow'; 'Power'}))
          spec{i} = spec{i}.*conj(spec{i});
        elseif  sum(strcmpi(data_form, {'db'}))
          spec{i} = 10*log10(spec{i}.*conj(spec{i}));
          %% else data_form ==> 'complex'
          %% do nothing
        end 
        
      catch
        %% empty cell marks failed specgram attempt
        
        %% get error message
        error_info = lasterror;
        curr_err_msg = ['Error making spectrogram for sound: ', sndName{i}, ' >> ''', error_info.message, ''''];
        
        if display2statuswindow 
          current_display = get(status_window_edit_handle, 'string');
          current_display{end+1} = curr_err_msg;
          set(status_window_edit_handle, 'string', current_display, 'listboxtop', length(current_display));
          drawnow;
        else
          fprintf(1, curr_err_msg);
        end
        
      end %% try/catch
      
    end %% if
    
  end %% for i = 1:numSnd
  
  
else 
  
  error(sprintf('Input to ''specgramCA'' must be a cell array!'\n));
  
end


% end function
return;