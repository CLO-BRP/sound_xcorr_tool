function path = pathof(ffname);

%**
%** "pathof.m"
%**
%** Kathryn A. Cortopassi, 2002
%**
%** syntax: path = pathof(ffname);
%**
%** Accepts either single strings or cell arrays of strings
%** and returns the path of the filename (including the terminal
%** file separator character)
%**


%**
%** by K.A. Cortopassi
%** created 24-May-2002 
%**


% check for correct number of input arguments
argLo = 1; argHi = inf;
error(nargchk(argLo, argHi, nargin));


if iscell(ffname)
    for i=1:length(ffname)
        path{i} = fileparts(ffname{i});
        if ~isempty(path{i})
            path{i} = [path{i} filesep];
        end
    end
else
    path = fileparts(ffname);
    if ~isempty(path)
        path = [path filesep];
    end
end


% end function 
return;