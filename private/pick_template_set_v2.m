function [ordered_sndNames, ordered_inx, ordered_corr_rows, ordered_adj_corr_rows] = pick_template_set_v2(corr_mat, sndNames);


%%
%% 'pick_template_set_v2'
%%
%% An implementation of Kurt Fristrup's method to pick a good reduced
%% order sound template set (from a larger set) spanning the variation (loosely) 
%% of the whole sound set 
%%
%% Kathryn A. Cortopassi, 15 September 2004
%%
%% syntax:
%% -------
%%   ordered_corr_rows = pick_template_set_v2(corr_mat, sndNames, );
%%
%% input:
%% ------
%%   corr_mat == square matrix of pair-wise peak cross-correlation values
%%   sndNames == cell array of sound file names in matching order
%%
%% output:
%% -------
%%   ordered_sndNames == cell array of sound file names in order of those
%%                        providing the best templates
%%   ordered_inx == ordering index
%%   ordered_corr_rows == ordered rows of the xcorr matrix for the top templates
%%

%%
%% some mods 30 March 2005, KAC
%%

%% get size of correlation matrix
NxN = size(corr_mat);
N = NxN(1);


%% make a working copy
wrk_corr_mat = corr_mat;

%% make the diagonal elements zero
%% so they don't contribute to the column max
wrk_corr_mat(logical(eye(NxN))) = 0;


%% this following Kurt's outlined approach to the solution 
%% (and filling in the blanks where needed)

%% normalize each column by dividing by the column max 
wrk_corr_mat = wrk_corr_mat ./ (ones(N, 1) * max(wrk_corr_mat));

%% pick row with the highest average value
[val, inx] = max(mean(wrk_corr_mat, 2));

%% make this the first template in the list
%% (and keep track of the indices)
ordered_inx(1) = inx;
ordered_sndNames{1} = sndNames{inx};
ordered_corr_rows(1, :) = corr_mat(inx, :);
ordered_adj_corr_rows(1, :) = wrk_corr_mat(inx, :);


for i = 2:N
  %% pull out the rest of the templates in order
  
  %% make a working copy
  wrk_corr_mat = corr_mat;
  
  %% subtract out the values of the target columns that are well matched;
  %% what's left high are those columns that have not been well matched
  %% previously; set any negative values to zero
  wrk_corr_mat = wrk_corr_mat - (ones(N, 1) * max(ordered_corr_rows, [], 1));
  wrk_corr_mat(wrk_corr_mat < 0) = 0;
  
  %% make the diagonal elements zero
  %% (don't really need to, just make it consistent with above average calc)
  wrk_corr_mat(logical(eye(NxN))) = 0;
  
  %% remove the sounds that have already been pulled as templates 
  wrk_corr_mat(:, ordered_inx) = 0;
  wrk_corr_mat(ordered_inx, :) = 0;
  
  %% pick row with the highest average value
  [val, inx] = max(mean(wrk_corr_mat, 2));
  
  if inx ~= ordered_inx(i-1) & val ~= 0
    %% make this the next template in the list
    %% (and keep track of the indices)
    ordered_inx(i) = inx;
    ordered_sndNames{i} = sndNames{inx};
    ordered_corr_rows(i, :) = corr_mat(inx, :);
    ordered_adj_corr_rows(i, :) = wrk_corr_mat(inx, :);
  else
    %% no more gains can be made by adding templates
    %% drop out
    break;
  end
  
end

%% make these columns
ordered_inx = ordered_inx';
ordered_sndNames = ordered_sndNames';


return;