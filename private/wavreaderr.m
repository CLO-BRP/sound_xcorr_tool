function error = wavreaderr(ffname)

%
% 'wavreaderr.m'
% 
% Kathryn A. Cortopassi, 2002
%
% Function to report an error reading a Wave file
%

%
% by Kathryn A. Cortopassi
% created 21-May-2002
%

error = 0;
evalc('wavread(ffname);', 'error = errorcaught;');

return;