function spec = denoisespc_v2(spec, method, param, data_form, adjustment);

%**
%** "denoisespc_v2.m"
%**
%** Kathryn A. Cortopassi, 2001-2002
%** 
%** Syntax: spec = denoisespc_v2(spec, method, param, data_form, adjustment);
%** 
%** Apply denoising to a spectrogram-- 'spec' can be either a single spectrogram or 
%** a cell array of spectrograms
%** 
%** Parameters for de-noising--
%**
%**     method => method to use for reducing spectrogram noise, either
%**             'None' -- do no noise correction
%**             'Broadband' -- subtract the param percentile value from the matrix
%**             'Narrowband' -- subtract the param percentile value from each frequency row
%**             'Broadband/Narrowband' -- first subtract matrix percentile then row percentile
%**             'Narrowband/Broadband' -- first subtract row percentile then matrix percentile
%**             'Narrowband Band-Specific' -- use a vector to give the specific percentile for each frequency row
%**             'KMF' -- Fristrup-specific denoising scheme
%**             'KMF II' -- Fristrup-specific denoising scheme version 2
%**             'Threshold' -- threshold the matrix param dB down from the peak
%**     param => denoising percentile or dB threshold to use 
%**              (or column vector of row specific percentile values-- assume for now that the columns contain 
%**              the %bins needed for x% of the row power sum, so convert all values to 100-values (cause the 
%**              denoising functions expect the %-tile value to throw out, not the %-tile value to keep))
%**     data_form => transform applied to the FFT coefficients, either
%**                   'Amp' -- mag(coeff)
%**                   'Pow' -- mag(coeff)^2
%**                   'dB' -- 10log10(Pow) 
%**                  needed for threshold denoising
%**     adjustment => how to deal with the spec values once they've been masked
%**                  'None' -- do nothing
%**                  'Bias' -- bias lowest of masked spec values to zero (leaving zero values alone)
%**                  'Demean' -- demean the masked spec values (leaving zero values alone)
%**                  'Binarize' -- set masked spec values to 1 (leaving zero values alone)
%**
%**


%**
%** by Kathryn A. Cortopassi
%** created 2-Oct-2001
%** modified 
%** 28-Feb-2002
%** 23-May-2002
%**
%**
%** modified August 2004
%** and March 2005
%**


% check for correct number of input arguments
argLo = 5; argHi = inf;
error(nargchk(argLo, argHi, nargin));



if sum(strcmpi(method, {'Threshold'; 'Peak Threshold'; 'dB Threshold'}))
  argLo = 4;
  error(nargchk(argLo, argHi, nargin));
  
  if iscell(spec)
    for i = 1:length(spec)
      spec{i} = dBThresh(spec{i}, param, data_form, adjustment);
    end
    
  else
    spec = dBThresh(spec, param, data_form, adjustment);
    
  end
  
  return;
  
end



if ~strcmpi(method, 'None') & param
  
  if iscell(spec)
    
    if param == 100
      for i = 1:length(spec)
        spec{i}(:) = 0;
      end
      
      
    elseif strcmpi(method, 'Broadband')
      for i = 1:length(spec)
        spec{i} = subMATper(spec{i}, param, data_form, adjustment);
      end
      
      
    elseif strcmpi(method, 'Narrowband')
      for i = 1:length(spec)
        spec{i} = subFROWper(spec{i}, param, data_form, adjustment);
      end
      
      
    elseif sum(strcmpi(method, {'Broadband/Narrowband'; 'Broad/Narrowband'; 'Broad/Narrow'}))
      for i = 1:length(spec)
        spec{i} = subMATsubFROWper(spec{i}, param, data_form, adjustment);
      end
      
      
    elseif sum(strcmpi(method, {'Narrowband/Broadband'; 'Narrow/Broadband'; 'Narrow/Broad'})) 
      for i = 1:length(spec)
        spec{i} = subFROWsubMATper(spec{i}, param, data_form, adjustment);
      end
      
      
    elseif strcmpi(method, 'Narrowband Band-Specific')
      % assume for now that the column of 'param' contains the %bins needed 
      % to accumulate x% of the row power sum, so convert all values to 
      % 100-values (cause we need the %-tile value to throw out, not the %-tile
      % value to keep)
      param = 100 - param;
      for i = 1:length(spec)
        spec{i} = subFROWper(spec{i}, param, data_form, adjustment);
      end    
      
      
    elseif sum(strcmpi(method, {'KMF'; 'KMF Special Recipe'; 'KMF I'; 'KMF Special I'}))
      for i = 1:length(spec)
        spec{i} = KMF_denoise(spec{i}, param, data_form, adjustment);
      end
      
      
    elseif sum(strcmpi(method, {'KMF II'; 'KMF Special II'}))
      for i = 1:length(spec)
        spec{i} = KMF_denoise_v2(spec{i}, param, data_form, adjustment);
      end
      
      
    else
      error(sprintf('Denoising method ''%s'' not recognized!', method));
      
    end
    
    
    
  else
    
    if param == 100
      spec(:) = 0;
      
      
    elseif strcmpi(method, 'Broadband')
      spec = subMATper(spec, param, data_form, adjustment);
      
      
    elseif strcmpi(method, 'Narrowband')
      spec = subFROWper(spec, param, data_form, adjustment);
      
      
    elseif sum(strcmpi(method, {'Broadband/Narrowband'; 'Broad/Narrowband'; 'Broad/Narrow'}))
      spec = subMATsubFROWper(spec, param, data_form, adjustment);
      
      
    elseif sum(strcmpi(method, {'Narrowband/Broadband'; 'Narrow/Broadband'; 'Narrow/Broad'})) 
      spec = subFROWsubMATper(spec, param, data_form, adjustment);
      
      
    elseif strcmpi(method, 'Narrowband Band-Specific')
      % assume for now that the column of 'fname' contains the %bins needed 
      % to accumulate x% of the row power sum, so convert all values to 
      % 100-values (cause we need the %-tile value to throw out, not the %-tile
      % value to keep)
      param = 100 - param;
      spec = subFROWper(spec, param, data_form, adjustment);
      
      
    elseif sum(strcmpi(method, {'KMF'; 'KMF Special Recipe'; 'KMF I'; 'KMF Special I'}))
      spec = KMF_denoise(spec, param, data_form, adjustment);
      
      
    elseif sum(strcmpi(method, {'KMF II'; 'KMF Special II'}))
      spec = KMF_denoise_v2(spec, param, data_form, adjustment);
      
      
    else
      error(sprintf('Denoising method ''%s'' not recognized!', method));
      
    end
    
  end
  
end


%% end function
return;