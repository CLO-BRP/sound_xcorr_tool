function [absPeaks, f0Peaks] = correlateCA_full(specArray, tvec, fvec, normtype, maxtauLag, maxphiLag, crossflag, status_window);

%%
%% 'correlateCA_full.m'
%%
%% Kathryn A. Cortopassi, August 2004 
%%
%% cross-correlate a cell array of spectrograms in both time and frequency
%% (a re-vamp of 'correlateCA.m' by Kathryn A. Cortopassi, 2001-2002)
%%
%% 
%%
%% syntax:
%% -------
%%   [f0Peaks, absPeaks] = correlateCA(specArray, tvec, fvec, normtype, maxtauLag, maxphiLag, crossflag, status_window);
%%
%% input:
%% ------
%%   specArray == a cell array of spectrogram matrices with dimension NxP, N frequencies by P times
%%   tvec == a cell array of the time vectors for each spectrogram
%%   fvec == a cell array of the frequency vectors for each spectrogram
%%   normtype == spectrogram standardization to use, either
%%              'colnorm'; 'column' for column-wise normalization
%%              'matnorm'; 'matrix' for matrix-based normalization
%%   maxtauLag == the maximum time lag to use in seconds 
%%   maxphiLag == the maximum frequency lag to use in Hz 
%%   crossflag == flag signifying the pattern of crosses to perform, either
%%                'sqr'; 'square'; 'pairwise' for all-by-all correlation
%%                'col'; 'column' for 1-by-all correlation
%%   status_window == handle to status window for message display (optional)
%%
%%
%% output:
%% -------
%%  f0Peaks == a cell array containing the peak correlation values,
%%              and their corresponding time lags
%%  absPeaks == a cell array containing the peak correlation values, their
%%              time lags, and frequency lags for the allowed lag ranges
%%
%%




%% make sure enough input arguments are given
argLo = 7; argHi = inf;
error(nargchk(argLo, argHi, nargin));


%% check that spec input is a cell array
if ~iscell(specArray)
  error('spectrogram input must be a cell array of spectrograms');
  return;
end

%% check that tvec input is a cell array
if ~iscell(tvec)
  error('time vector input must be a cell array of time vectors');
  return;
end

%% check that fvec input is a cell array
if ~iscell(fvec)
  error('frequency vector input must be a cell array of frequency vectors');
  return;
end


%% check normalization type
if ~sum(strcmpi(normtype, {'colnorm'; 'column'; 'matnorm'; 'matrix'; 'none'}))
  error(sprintf('normalization flag ''%s'' not recognized', normtype));
  return;
end

%% check crossflag type
if ~sum(strcmpi(crossflag, {'sqr'; 'square'; 'pairwise'; 'col'; 'column'}))
  error(sprintf('cross flag ''%s'' not recognized', crossflag));
  return;
end


numSpecs = length(specArray);

if numSpecs ~= length(tvec)
  error('numbers of spectrograms and time vectors do not match');
  return;
end

if numSpecs ~= length(fvec)
  error('numbers of spectrograms and frequency vectors do not match');
  return;
end


%% check consistency of time-frequency gridding, and number of frequency bands

if numSpecs == 1
  error('Insufficient number of spectrogram files for performing cross!  Exiting program.\n');
end

for i=1:numSpecs
  [NArray(i), PArray(i)] = size(specArray{i}); 
  dta(i) = tvec{i}(2);
  dfa(i) = fvec{i}(2);
end

%% get the time resolution
dt = max(dta);
if dt ~= min(dta)
  error(sprintf('Spectrogram time bin sizes do not match!  Exiting program.\n'));
end

%% get the frequency resolution
df = max(dfa);
if df ~= min(dfa)
  error(sprintf('Spectrogram frequency bin sizes do not match!  Exiting program.'));
end

%% check that the number of frequency rows match
P = max(PArray);
N = max(NArray);
if N ~= min(NArray)
  error(sprintf('Spectrograms frequency rows numbers do not match!  Exiting program.'));
end


%% clear the memory space
clear NArray;
clear PArray;
clear tvec,
clear fvec;



if ~exist('maxtauLag')
  %% max time lag isn't specified, use default value of P-1 points
  
  maxtauLagPts = P-1;
  maxtauLag = maxtauLagPts * dt;
  
else
  %% convert given lag in seconds to the corresponding number of points
  
  maxtauLagPts = round(maxtauLag / dt);
  
  %% make sure the requested tau lag does not exceed the max possible lag
  maxtauLagPts = min(maxtauLagPts, P-1); 
  
end


if ~exist('maxphiLag')
  %% max frequency lag isn't specified, use default value of N-1 points
  
  maxphiLagPts = N-1;
  maxphiLag = maxphiLagPts * df;
  
else
    %%%SCK: not sure if this calculation makes sense...  
  %% convert given lag in Hz to the corresponding number of points
  maxphiLagPts = round(maxphiLag / df);
  
  %% and make sure requested phi lag does not exceed max possible lag
  maxphiLagPts = min(maxphiLagPts, N-1);      
  %%% end SCK
end


%% convert to actual number of points that must used to accomodate both lags
%% in the (soon-to-be) unwrapped spectrogram
maxlag = (maxphiLagPts * 2 * P) + maxtauLagPts;    
%% note: for a 2NP length array, the max possible lag is 2NP-1 points, however
%% the above calculation gives a max possible lag of 2P(N-1)+P-1 = (2PN-1)-P
%% this is because the last P calculations are comparing the signal to the
%% zero-padded ends and are not necessary




%% standardize the spectrograms as requested
for i = 1:numSpecs
  specArray{i} = standardize_mat(specArray{i}, normtype);
end



%% zero pad the spectrograms to match in duration (we've checked to see that they match
%% in the frequency dimension (N) already
%%
%% zeropad to twice the dimension of the longest matrix-- doubling the 
%% longest matrix by adding zeros is necessary so that the correct parts
%% of the spectrograms line up for the correlation later, since we are
%% cross-correlating the unwrapped spectrogram
%%
%% then transpose (so that time increases as you move down a column and 
%% frequency increases as you move along a row)
%% 
%% finally, unwrap transposed spectrograms column-wise resulting in a 
%% frequency-wise unwrap 
%%
for i=1:numSpecs
  specArray{i}(N, 2*P) = 0; 
  specArray{i} = specArray{i}'; 
  specArray{i} = specArray{i}(:); % strictly '(:)' unwraps column-wise
end
%%
%% now 'specArray' holds matrices with dimensions 2max(P)Nx1, that is, 
%% a bunch of column vectors (which are spectrograms that have been 
%% unwrapped frequency-wise and transposed, the frequencies increase 
%% as you go down the column (in chunks of 2max(P) points) from DC to Nyquist)




%% if a crossflag isn't specified, use the default value of 'sqr'
if ~exist('crossflag')
  crossflag = 'sqr';
end



if sum(strcmpi(crossflag, {'sqr'; 'square'; 'pairwise'}))
  %% will eventually calculate all (unique) pair-wise crosses
  
  %% pre-fill the square peak value and lag arrays
  %% (fill with ones because they'll be used as indices below)
  f0Peaks{1} = ones(numSpecs, numSpecs);
  f0Peaks{2} = ones(numSpecs, numSpecs);
  
  absPeaks{1} = ones(numSpecs, numSpecs);
  absPeaks{2} = ones(numSpecs, numSpecs);
  absPeaks{3} = ones(numSpecs, numSpecs);
  
else
  %% will eventually calculate the 1-by-all cross (i.e., all other spectrograms 
  %% crossed with spectrogram #1) 
  
  %% pre-fill the peak value and lag vectors
  %% (fill with ones because they'll be used as indices below)
  f0Peaks{1} = ones(numSpecs, 1);
  f0Peaks{2} = ones(numSpecs, 1);
  
  absPeaks{1} = ones(numSpecs, 1);
  absPeaks{2} = ones(numSpecs, 1);
  absPeaks{3} = ones(numSpecs, 1);
  
end



if exist('status_window')
  %% get the current status text
  current_display = get(status_window, 'string');
end


if sum(strcmpi(normtype, {'matnorm'; 'matrix'}))
  %% compute the first cross of spec1 and spec2
  
  txt_message = sprintf('Correlating sounds 1 X 2');
  if exist('status_window')
    % display_status_line(status_window, txt_message);
    %% flash the requested message line, but don't add it to the end of the status window
    tmp_display = current_display;
    tmp_display{end+1} = txt_message;
    set(status_window, 'string', tmp_display, 'listboxtop', length(tmp_display));
    drawnow;
  else
    fprintf(1, [txt_message, '\n']);
  end
  
  [corrFunc, lags] = xcorr(specArray{1}, specArray{2}, maxlag);
  
else
  %% compute the first cross of spec1 and spec1
  
  txt_message = sprintf('Correlating sounds 1 X 1');
  if exist('status_window')
    % display_status_line(status_window, txt_message);
    %% flash the requested message line, but don't add it to the end of the status window
    tmp_display = current_display;
    tmp_display{end+1} = txt_message;
    set(status_window, 'string', tmp_display, 'listboxtop', length(tmp_display));
    drawnow;
  else
    fprintf(1, [txt_message, '\n']);
  end
  
  [corrFunc, lags] = xcorr(specArray{1}, specArray{1}, maxlag);
end


%% compute the arrays of time and frequency lags
%% if the number of total lags is greater than 2P, then frequency lags
%% greater than 0 Hz were used, so compute the array of actual frequency 
%% lags in Hz, and compute the array of actual time lags in seconds
%% else, the time lags just equal the returned lags array and all frequency
%% lags are zero
numLags = length(lags);

if (numLags > 2*P)
  
  %% compute array of actual frequency lags in Hz
  phiLags = floor( (lags + P) / (2 * P) ); 
  phiLags = phiLags * df;
  
  %% compute the array of actual time lags in seconds
  %% 'find(lags==0)' gets the index of the zero lag value
  centerLags = find(lags == 0);   
  
  %% generate the array of repeating time lags
  taus = [0, 1:(P-1), 0, (-P+1):(-1)];
  
  %% and compute array of actual time lags in seconds
  numTaus = length(taus);     
  i = [(centerLags-1):(-1):1, 1:(numLags-centerLags+1)];
  j = mod(i, numTaus);
  j(j == 0) = numTaus;
  j(1:(centerLags-1)) = (numTaus+1) - j(1:(centerLags-1));
  tauLags(1:numLags) = taus(j);
  
else
  
  tauLags = lags;
  phiLags = zeros(1, numLags);
  
end

tauLags = tauLags*dt;

%% just make them columns
phiLags = phiLags';
tauLags = tauLags';


%% get the indices for zero frequency shift and all allowed time lags
f0tM = find(abs(lags) <= maxtauLagPts);

%% then get the indices for all allowed time and frequency ranges
%% since values are calculated only within the max allowed frequency lag for
%% all time lags, get the subset of indices for the max allowed time lag 
fMtM = find(abs(tauLags) <= maxtauLag);   % maxtauLag in seconds



%% finish the crosses    

%% corrFunc just holds one correlation function at a time
%% done this way because saving all the correlation functions and
%% finding the peaks later is too memory intensive

if sum(strcmpi(crossflag, {'sqr'; 'square'; 'pairwise'}))
  %% get the rest of the correlation functions for the all-by-all cross
  
  
  if sum(strcmpi(normtype, {'matnorm'; 'matrix'}))
    %% first begin to fill the arrays with the peak correlation values, and indices
    %% for corresponding time lags and frequency lags for what we already did
    %% do this for the correlation peaks at zero frequency lag
    [f0Peaks{1}(1, 2), f0Peaks{2}(1, 2)] = max(corrFunc(f0tM));
    [f0Peaks{1}(2, 1), f0Peaks{2}(2, 1)] = max(corrFunc(f0tM));
    
    % then do this for the absolute correlation peaks for the allowed time and frequency ranges
    [absPeaks{1}(1, 2), absPeaks{2}(1, 2)] = max(corrFunc(fMtM));
    [absPeaks{1}(2, 1), absPeaks{2}(2, 1)] = max(corrFunc(fMtM));
    
    for i = 1:(numSpecs - 1)
      for j = 3:numSpecs
        if (i < j)
          
          txt_message = sprintf('Correlating sounds %d X %d', i, j);
          if exist('status_window')
            % display_status_line(status_window, txt_message);
            %% flash the requested message line, but don't add it to the end of the status window
            tmp_display = current_display;
            tmp_display{end+1} = txt_message;
            set(status_window, 'string', tmp_display, 'listboxtop', length(tmp_display));
            drawnow;
          else
            fprintf(1, [txt_message, '\n']);
          end
          
          corrFunc = xcorr(specArray{i}, specArray{j}, maxlag);
          
          [f0Peaks{1}(i, j), f0Peaks{2}(i, j)] = max(corrFunc(f0tM));
          [f0Peaks{1}(j, i), f0Peaks{2}(j, i)] = max(corrFunc(f0tM));
          
          [absPeaks{1}(i, j), absPeaks{2}(i, j)] = max(corrFunc(fMtM));
          [absPeaks{1}(j, i), absPeaks{2}(j, i)] = max(corrFunc(fMtM));
          
          clear corrfunc;
          
          %%% SCK commented out
%           pack;
          
        end
      end
    end
    
    %% create a mask to convert the positive lags to negative lags appropriately
    lagConverter = ones(numSpecs);
    lagConverter(logical(~tril(lagConverter))) = -1;
    
    %% get the time lag array in seconds for f0Peaks 
    f0Peaks{2} =  lagConverter .* tauLags(f0tM(f0Peaks{2}));
    %% set the diagonal lags to zero as they should be
    f0Peaks{2}(logical(eye(numSpecs))) = 0;
    
    %% get the frequency lag array in Hz for absPeaks
    %% do this first cause indices are held temporarily in 
    %% absPeaks{2}
    absPeaks{3} =  lagConverter .* phiLags(fMtM(absPeaks{2}));
    %% set the diagonal lags to zero as they should be
    absPeaks{3}(logical(eye(numSpecs))) = 0;
    
    %% get the time lag array in seconds for absPeaks
    absPeaks{2} =  lagConverter .* tauLags(fMtM(absPeaks{2}));
    %% set the diagonal lags to zero as they should be
    absPeaks{2}(logical(eye(numSpecs))) = 0;
    
  else %% need to fill in diagonal too
    %% first begin to fill the arrays with the peak correlation values, and indices
    %% for corresponding time lags and frequency lags for what we already did
    %% do this for the correlation peaks at zero frequency lag
    [f0Peaks{1}(1, 1), f0Peaks{2}(1, 1)] = max(corrFunc(f0tM));
    
    % then do this for the absolute correlation peaks for the allowed time and frequency ranges
    [absPeaks{1}(1, 1), absPeaks{2}(1, 1)] = max(corrFunc(fMtM));
    
    for i = 1:numSpecs
      for j = 2:numSpecs
        if (i <= j)
          
          txt_message = sprintf('Correlating sounds %d X %d', i, j);
          if exist('status_window')
            % display_status_line(status_window, txt_message);
            %% flash the requested message line, but don't add it to the end of the status window
            tmp_display = current_display;
            tmp_display{end+1} = txt_message;
            set(status_window, 'string', tmp_display, 'listboxtop', length(tmp_display));
            drawnow;
          else
            fprintf(1, [txt_message, '\n']);
          end
          
          corrFunc = xcorr(specArray{i}, specArray{j}, maxlag);
          
          [f0Peaks{1}(i, j), f0Peaks{2}(i, j)] = max(corrFunc(f0tM));
          [f0Peaks{1}(j, i), f0Peaks{2}(j, i)] = max(corrFunc(f0tM));
          
          [absPeaks{1}(i, j), absPeaks{2}(i, j)] = max(corrFunc(fMtM));
          [absPeaks{1}(j, i), absPeaks{2}(j, i)] = max(corrFunc(fMtM));
          
          clear corrfunc;
          % Commented out 1/20/12 SCK
          % pack;
          
        end
      end
    end
    
    %% create a mask to convert the positive lags to negative lags appropriately
    lagConverter = ones(numSpecs);
    lagConverter(logical(~tril(lagConverter))) = -1;
    
    %% get the time lag array in seconds for f0Peaks 
    f0Peaks{2} =  lagConverter .* tauLags(f0tM(f0Peaks{2}));
    %% set the diagonal lags to zero as they should be
    f0Peaks{2}(logical(eye(numSpecs))) = 0;
    
    %% get the frequency lag array in Hz for absPeaks
    %% do this first cause indices are held temporarily in 
    %% absPeaks{2}
    absPeaks{3} =  lagConverter .* phiLags(fMtM(absPeaks{2}));
    %% set the diagonal lags to zero as they should be
    absPeaks{3}(logical(eye(numSpecs))) = 0;
    
    %% get the time lag array in seconds for absPeaks
    absPeaks{2} =  lagConverter .* tauLags(fMtM(absPeaks{2}));
    %% set the diagonal lags to zero as they should be
    absPeaks{2}(logical(eye(numSpecs))) = 0;
    
    
  end
  
  
else
  %% get the rest of the correlation functions for the 1-by-all cross
  
  if sum(strcmpi(normtype, {'matnorm'; 'matrix'}))
    %% first begin to fill the arrays with the peak correlation values, and indices
    %% for corresponding time lags and frequency lags for what we already did
    %% do this for the correlation peaks at zero frequency lag
    [f0Peaks{1}(2), f0Peaks{2}(2)] = max(corrFunc(f0tM));
    
    % then do this for the absolute correlation peaks for the allowed time and frequency ranges
    [absPeaks{1}(2), absPeaks{2}(2)] = max(corrFunc(fMtM));
    
    
    for j = 3:numSpecs
      
      txt_message = sprintf('Correlating sounds 1 X %d', j);
      if exist('status_window')
        % display_status_line(status_window, txt_message);
        %% flash the requested message line, but don't add it to the end of the status window
        tmp_display = current_display;
        tmp_display{end+1} = txt_message;
        set(status_window, 'string', tmp_display, 'listboxtop', length(tmp_display));
        drawnow;
      else
        fprintf(1, [txt_message, '\n']);
      end
      
      [corrFunc, lags] = xcorr(specArray{1}, specArray{j}, maxlag);
      
      [f0Peaks{1}(j), f0Peaks{2}(j)] = max(corrFunc(f0tM));
      
      [absPeaks{1}(j), absPeaks{2}(j)] = max(corrFunc(fMtM));
      
      clear corrfunc;
      clear lags;
      pack;
      
    end
    
    %% get the time lag array in seconds for f0Peaks 
    f0Peaks{2} = tauLags(f0tM(f0Peaks{2})); 
    
    %% get the frequency lag array in Hz for absPeaks
    %% do this first cause indices are held temporarily in 
    %% absPeaks{2}
    absPeaks{3} = phiLags(fMtM(absPeaks{2}));
    
    %% get the time lag array in seconds for absPeaks
    absPeaks{2} = tauLags(fMtM(absPeaks{2})); 
    
  else %% fill in 1x1
    
    %% first begin to fill the arrays with the peak correlation values, and indices
    %% for corresponding time lags and frequency lags for what we already did
    %% do this for the correlation peaks at zero frequency lag
    [f0Peaks{1}(1), f0Peaks{2}(1)] = max(corrFunc(f0tM));
    
    % then do this for the absolute correlation peaks for the allowed time and frequency ranges
    [absPeaks{1}(1), absPeaks{2}(1)] = max(corrFunc(fMtM));
    
    
    for j = 2:numSpecs
      
      txt_message = sprintf('Correlating sounds 1 X %d', j);
      if exist('status_window')
        % display_status_line(status_window, txt_message);
        %% flash the requested message line, but don't add it to the end of the status window
        tmp_display = current_display;
        tmp_display{end+1} = txt_message;
        set(status_window, 'string', tmp_display, 'listboxtop', length(tmp_display));
        drawnow;
      else
        fprintf(1, [txt_message, '\n']);
      end
      
      [corrFunc, lags] = xcorr(specArray{1}, specArray{j}, maxlag);
      
      [f0Peaks{1}(j), f0Peaks{2}(j)] = max(corrFunc(f0tM));
      
      [absPeaks{1}(j), absPeaks{2}(j)] = max(corrFunc(fMtM));
      
      clear corrfunc;
      clear lags;
      pack;
      
    end
    
    %% get the time lag array in seconds for f0Peaks 
    f0Peaks{2} = tauLags(f0tM(f0Peaks{2})); 
    
    %% get the frequency lag array in Hz for absPeaks
    %% do this first cause indices are held temporarily in 
    %% absPeaks{2}
    absPeaks{3} = phiLags(fMtM(absPeaks{2}));
    
    %% get the time lag array in seconds for absPeaks
    absPeaks{2} = tauLags(fMtM(absPeaks{2})); 
    
  end
  
  
end



%% end function
return;