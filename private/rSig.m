function [fileData, fileHeader] = rSig(fileName, filePath);

%** 
%** "rSig.m"
%**
%** Kathryn A. Cortopassi, 2000-2002
%**
%** syntax: [fileData, fileHeader] = rSig(fileName, filePath);
%** opens the file indicated by [filePath fileName]
%** and returns the data buffer in 'fileData' and the Signal/RTSD header info in 'fileHeader'
%**
%** syntax: [fileData, fileHeader] = rSig(fullFileName);
%** opens the file indicated by fullFileName
%** and returns the data buffer in 'fileData' and the Signal/RTSD header info in 'fileHeader'
%**
%** syntax: [fileData, fileHeader] = rSig;
%** opens a gui to get the filePath and fileName
%** and returns the data buffer in 'fileData' and the Signal/RTSD header info in 'fileHeader'
%**
%** syntax: fileData = rSig;
%** opens a gui to get the filePath and fileName
%** and returns the data buffer in 'fileData'
%**
%**
%** a function to read in a SIGNAL file and return the file contents
%** and selected header info in the user supplied variables
%** "fileData" and "fileHeader"
%**
%** the file format is assumed to be SIGNAL floating-point binary with header
%** 32-bit precision, IEEE little-endian byte order
%** unless and integer data type is detected, then file data is read as 16-bit integer
%**

%**
%** by Kathryn A. Cortopassi
%** created 10/00
%** modified 9/01 to deal with integer data types for RTSD files
%** modified 27-Sept-01 to deal with variable input arguments
%** modified 14-May-2002 to provide more error handling
%** modified 23-May-2002 for more error handling
%**


% here are the variables...

% fileName                  % name of the Signal/RTSD file supplied as an input argument to the function
% filePath                  % path to the Signal/RTSD file supplied as an input argument to the function
% fullName                  % full file name

fileID = [];				% generic file ID holder
h = [];						% generic graphics handle

headBlocks =[];			    % number of blocks in the header: 1 block = 512 bytes (stored in bytes 9-12)
bufType = [];				% Signal buffer type (i.e., T, F, or FT) (stored in bytes 17-20) 
dataType =[];				% Signal data type (i.e., integer or floating point (stored in bytes 21-24) 
intConv = [];				% conversion factor for integer data (stored in bytes 25-28) 
intOffset = [];			    % offset factor for integer data (stored in bytes 29-32) 
numPnts = [];               % number of data points (stored in bytes 81-84)
Fs = [];					% sampling rate of Signal file (stored in bytes 85-88)
xOrg = [];				    % x-axis origin in ms or Hz (stored in bytes 29-32)
xRng = [];                  % x-axis range in ms or Hz (stored in bytes 93-96)
yQty = [];					% y-axis quantity (stored in bytes 97-104)
yUnits = [];				% y-axis units (stored in bytes 105-112)
yScale =[];                 % y-axis scale (lin or log) (stored in bytes 113-116)
title = [];                 % buffer title (stored in bytes 113-116)
winFunc = [];				% if F or FT file, get windowing function used (stored in bytes 141-144)
smWin = [];					% get smoothing width (stored in bytes 145-148)
XFscale = [];               % spectrum scaling (stored in bytes 157-160)

nFFT = [];					% FFT length for FT buffers (stored in bytes 209-212)
caption = []; 				% Signal buffer caption (stored in bytes 261-332)

fileData = [];				% matrix holding Signal/RTSD file data returned by the function
fileHeader = [];			% matrix holding selected Signal/RTSD header info returned by the function



% now the function...

if (nargin < 1)
    filePath = [pwd '\'];
    [fileName, filePath] = uigetfile([filePath '*.*'],'Choose the Signal file:');
    fullName = [filePath fileName];
elseif (nargin < 2)
    fullName = fileName;
else
    fullName = [filePath fileName];
end


% open the Signal file
% first read the header info and store in the appropriate variables
% open as binary ('rb') and use IEEE little-endian byte order ('ieee-le') 
% for numerical header data & file data, read numerical data with 32-bit floating point precision
% unless data type in integer
% read literal header data as 1 byte chars using 'fscanf' when necessary
%fprintf('file %s ',fullName);
fileID = fopen(fullName, 'rb', 'ieee-le');
if fileID == -1
    fprintf(1,'\n\n*** ERROR ***\nFile ''%s'' not found\n', fullName);
    error('**** Error using ''rSig'': Unable to open file ****');
end
% get the number of header blocks (1 block = 512 bytes)
prgType = fscanf(fileID, '%c', 4);
if ~strcmpi(prgType, 'sigp') & ~strcmpi(prgType, 'rts')
    fprintf(1,'\n\n*** ERROR ***\nFile ''%s'' is not a Signal\\RTSD file\n', fullName);
    error('*** File not Signal\RTSD format : Returning to command window ***');
end
prgVer = fscanf(fileID, '%c', 4);
%fseek(fileID, 8, 'bof');
headBlocks = fread(fileID, 2, 'float32');
headBlocks(2) = [];

% get the Signal buffer and data storage type
bufType = fscanf(fileID, '%c', 4);
bufType = deblank(bufType);
dataType = fscanf(fileID, '%c', 4);
dataType = deblank(dataType);

% if an integer file, get the offset and conversion factor
if strcmpi(dataType, 'I')
   intConv = fread(fileID, 1, 'float32');
   intOffset = fread(fileID, 1, 'float32');
end

% get the file sampling rate, x-offset, y-axis variable, and units
fseek(fileID, 80, 'bof');
numPnts = fread(fileID, 1, 'float32');
Fs = fread(fileID, 1, 'float32');
xOrg = fread(fileID, 1, 'float32');
xRng = fread(fileID, 1, 'float32');
yQty = fscanf(fileID, '%c', 8);
yUnits = fscanf(fileID, '%c', 8);
yScale = fscanf(fileID, '%c', 8);
title = fscanf(fileID, '%c', 8);

% if the buffer type is F or FT, find out the windowing function and XFSCALE used
if 1-strcmpi(bufType, 'T')
   fseek(fileID, 140, 'bof');
   winFunc = fscanf(fileID, '%c', 4);
   fseek(fileID, 156, 'bof');
   XFscale = fscanf(fileID, '%c', 4);
end

% see if any smoothing was done to the waveform
fseek(fileID, 144, 'bof');
smWin = fread(fileID, 1, 'float32');

% if the file is an FT buffer, find out the FFT length used
if strcmpi(bufType, 'FT')
   fseek(fileID, 208, 'bof');
   nFFT = fread(fileID, 1, 'float32');
end

fseek(fileID, 172, 'bof');
numPnts2 = fread(fileID, 1, 'int16');

% get the buffer caption
fseek(fileID, 260, 'bof');
caption = fscanf(fileID, '%c', 72);

% now skip the header crap and read the actual data into an array that will be returned
fseek(fileID, 512*headBlocks, 'bof');
if strcmpi(dataType, 'I')
    fileData = fread(fileID, inf, 'int16');
    fileData = (fileData - intOffset) * intConv;
else
    fileData = fread(fileID, inf, 'float32');
end

% done! so close the file
fclose(fileID);


% write the header info that was pulled out of the Signal/RTSD file into it's own array that will be returned
% also write in the filename
fileHeader.progType= prgType;
fileHeader.progVer = prgVer;
fileHeader.fileName = fileName;
fileHeader.bufferType = bufType;
fileHeader.numPoints = numPnts;
fileHeader.numPoints2 = numPnts2;
fileHeader.samplingRate = Fs;
fileHeader.Fs = Fs;
fileHeader.Xorigin = xOrg;
fileHeader.Xrange = xRng;
fileHeader.Yquantity = yQty;
fileHeader.Yunits = yUnits;
fileHeader.Yscale = yScale;
fileHeader.Title = title;
fileHeader.windowFunction = winFunc;
fileHeader.XFScale = XFscale;
fileHeader.fftSize = nFFT;
fileHeader.smoothingWindow = smWin;
fileHeader.caption = caption;
fileHeader.dataType = dataType;

% end the function
return;