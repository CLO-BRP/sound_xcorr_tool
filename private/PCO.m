function [coords, eigen, cumfit] = PCOanalysis(M, data_type, pco2save, outputfilename, object_names, group_names);


%**
%** "PCOanalysis.m"
%**
%** Kathryn A. Cortopassi, 2000-2004
%**
%** Function to do a principal coordinates analysis of the square matrix or tab-delimited
%** ascii file given in the function call 
%** If no input argument is passed, a GUI opens to ask for the tab-delimited ascii file name 
%** Ascii output files are written for the eigenvalues, cumulative fit, and pco coordinates
%**
%**
%** Syntax: [coords, eigen, cumfit] = PCOanalysis(M, data_type, pco2save, outputfilename, object_names, group_names);
%**         [coords, eigen, cumfit] = PCOanalysis;
%**
%** Input:  M == square numeric matrix or filename string for a tab-dlm ascii file
%**         data_type == char string, either 'similarity' or 'distance'
%**         pco2save == number of pco values to save
%**         outputfilename == path + basename for pco and eigen file output
%**         object_names == cell array of object name strings
%**         group_names == cell array of object grouping name strings
%**
%**
%** Output: coords == object pco coordinates
%**         eigen == eigen values for all pco axes
%**         cumfit == cumulative distance/simnilarity fit of all pco axes
%**
%**

%** 
%** by Kathryn A. Cortopassi
%** created 7/00 (original version created to read in KAC Signal macro output)
%** modified 9/01 
%** modified 01/02
%** modified 05/02
%** modified 10/04
%**


% here are the variables...
%   M                   nxn symmetric matrix of similarity or distance values input by the user
% or the name of an ascii file containing a tab-delimited square matrix
% M_size = 0;				% number of objects in matrix M
% D = [];					% symmetric matrix of distance values
% B = [];					% transformed matrix of inner products
% Q = [];					% matrix of eigenvectors-- columns are the eigenvectors
% Lambda = [];			% diagonal matrix of eigenvalues
% X = [];           	    % matrix of n PCO coordinates by n objects
% 
% eigen_matrix = [];	    % 2 column matrix of eigenvalues and cumulative goodness of fit
% pco_size = 0;			% number of pco coordinates to save
% 
% startPath = [pwd filesep];		% string indicating were to start looking for files
% % pwd displays the value of the current directory
% fileType = '*.txt; *.out; *.sqr';		    % string indicating what file type to open
% delimiterType = '\t';	                    % string indicating the type of data delimiter to use
% 
% M_name = [];			% name of matrix file
% M_path = [];			% path location of matrix file
% 
% eigen_name =[];			% name of file for storing eigenvalues
% eigen_path = [];		% path to the file for storing eigenvalues
% 
% pco_name = [];			% name of file for storing user requested number of pco coordinates
% pco_path = [];			% path to file for storing user requested number of pco coordinates
% 
% reply = [];				% a generic variable for holding user response to a query
% i = 0;					% a generic counter variable
% tempMatrix1 = [];		% a generic temp matrix for working
% inputPrompt = []; 	    % a generic variable for holding input prompt strings
% value = [];				% a generic variable for holding response values
% format_Str = [];		% a generic variable for holding '*printf' format strings
% fid = 0;				% a generic variable for holding file ID numbers
% 
% grp_num = 0;			% variable to hold the number of groups in the matrix
% grp_matrix = [];		% variable to hold the starting rows of the groups; starts at 1, holds 
% % all other group starting rows, and ends at M_size+1; so has size grp_num+1
% grp_list = [];			% the column of groups for output



% define marker use in the following order : 
% 1) five-pointed star, 2) circle, 3) diamond 4) right pointing triangle, 
% 5) left pointing triangle, 6) square, 7) asterisk, 8) point,
% 9) six-pointed star, 10) cross, 11) plus sign, 12) upward pointing triangle, 
% 13) downward pointing triangle
% markerTypes is a column vector
% in order to get wrapping, it scrolls through in the order 2,3,4,...,size(markerTypes), 1
markerTypes = ['v'; 'p'; 'o'; 'd'; '>'; '<'; 's'; '*'; '.'; 'h'; 'x'; '+'; '^'];

% define color use in the following order : 
% 1) red, 2) green, 3) blue, 4) cyan, 5) magenta, 6) yellow, 7) black
% colorTypes is a column vector
% in order to get wrapping, it scrolls through in the order 2,3,4,...,size(colorTypes), 1
colorTypes = ['k'; 'r'; 'g'; 'b'; 'c'; 'm'; 'y']; 


startPath = [pwd filesep];	 


% now the script...

% let's assume the input is a square, symmetric tab-delimited array of 
% similarity or distance values 
% if matrix not passed in function call, let the user browse to select the matrix file to open
if ~exist('M')
  fileType = '*.txt; *.out; *.sqr';	
  [M_name, M_path] = uigetfile([startPath fileType],'Choose your matrix:');
  M = dlmread([M_path M_name], delimiterType);
  [junk, M_nameBase] = fileparts(M_name);
elseif ischar(M)
  fid = fopen(M);
  if fid == -1
    error(sprintf('The ascii file ''%s'' is not found', M));
  else
    fclose(fid);
    [M_path, M_nameBase, M_ext] = fileparts(M);
    if ~isempty(M_path)
      M_path = [M_path filesep];
    end
    M = dlmread(M, delimiterType);
    M_name = [M_nameBase, M_ext];
  end    
else
  M_path = startPath;
  M_nameBase = '';
  M_name = '';
end

[M_size, M_size2] = size(M);
if M_size ~= M_size2
  error(sprintf('The matrix provided is not square!'));
end



%% determine if this is a similarity or distance matrix
if ~exist('data_type')
  qstring = 'Are these similarity or distance values?'; title = 'Choose:';
  sim = 'Similarity'; dis = 'Distance'; def = sim;
  reply = questdlg(qstring, title, sim, dis, def);
  data_type = reply;
end

if strcmpi(data_type, 'Similarity')
  % it's a matrix of similarities, so we must convert to distances
  D = 1-M;
else
  D = M;   
end





% now, make the transformation to an inner product matrix
B = -(1/2)*( D.^2 + trace(D*D')./M_size^2 ...
  - (ones(M_size, 1)*sum(D.^2))./M_size ...
  - (ones(M_size, 1)*sum(D.^2))'./M_size ); 


% now find the eigenvalues and eigenvectors of B
% the columns of Q are the eigenvectors, Lambda is a diagonal matrix
% of eigenvalues
[Q, Lambda] = eig(B);

% sort Lambda and Q so eigenvalues are in descending order
% start by sorting Q
%% KAC ADDITION 15 JULY 2005 : make sure that any complex values 
%% resulting from possible round-off errors are dealt with
%% in the sort
% tempMatrix1 = flipud( sortrows( cat( 2, Lambda*ones(M_size, 1), Q' ), [1] ) );
% % get rid of the inserted column of eigenvalues used to sort
% tempMatrix1(:, 1) = [];
% Q = tempMatrix1';
% % then sort Lambda
% Lambda = diag( flipud( sort( Lambda*ones(M_size, 1) ) ) );
%% get sort inx for eigenvalues, the sort Lambda and Q accordingly
[sort_val, sort_inx] = sort(real(diag(Lambda)));
sort_val = flipud(sort_val);
sort_inx = flipud(sort_inx);
Lambda = diag(sort_val);

tempMatrix1 = Q';
tempMatrix1 = tempMatrix1(sort_inx, :);
Q = tempMatrix1';





% now find X, the matrix of "PCO axis rows" by "object columns"
X = (Q * sqrt(Lambda))';

% save the eigenvalues and calculate the cumulative goodness of fit
% using the formula from Gower 1987
%Gower, J.C. (1987) Introduction to ordination techniques. In Developments in Numerical
%Ecology, NATO ASI Series, Vol. G14 (P. Legendre & L. Legendre, eds). Springer-Verlag; Berlin,
%pp. 5-64.

coords = X';




if ~exist('outputfilename')
  % first ask the user for the file name to write to
  [eigen_name, eigen_path] = uiputfile([M_path M_nameBase '.eigen'], ...
    'File for writing the eigenvalues and cumulative goodness of fit');
  [junk, eigen_nameBase] = fileparts(eigen_name);
else
  eigen_name = [relnameof(outputfilename), '_eigen.txt'];
  eigen_path = pathof(outputfilename);
end



% now open the file to write to
fid = fopen([eigen_path eigen_name], 'w');

% and write to the file!
fprintf(fid, 'Eigenvalues for matrix \t Cumulative goodness of fit\n\n');
eigen = Lambda*ones(M_size, 1);
cumfit = cumsum(eigen.^2)./sum(eigen.^2);
eigen_matrix = cat(2, eigen, cumfit);
fprintf(fid, '%f \t %f \n', eigen_matrix');

% we're done, so close the file
fclose(fid);





if 0 %% remove this part
  
  % now get the number of groups in the matrix and how they separate
  inputPrompt = {'How many groups in the matrix? '};
  value = inputdlg(inputPrompt);
  grp_num = str2num(value{1});
  % initialize matrix holding group beginning rows and the number of the last row + 1
  grp_matrix = zeros(grp_num+1, 1);
  % of course the first group begins at row # 1
  grp_matrix(1, 1) = 1;
  for i = 1:grp_num-1
    inputPrompt{i} = ['Enter the first row of group ' num2str(i+1) ' :'];
  end
  if grp_num > 1
    value = inputdlg(inputPrompt);
  end
  for i = 2:grp_num
    grp_matrix(i, 1) = str2num(value{i-1});
  end
  grp_matrix(grp_num+1, 1) = M_size+1;
  
  
  % now plot the objects on the first 3 PCO axes
  figure('Name', ['3-D PCO Plot for Matrix ' M_name]);
  hold on;
  for i = 1:grp_num
    plot3( X(1, grp_matrix(i, 1):( grp_matrix(i+1, 1)-1 )), ...
      X(2, grp_matrix(i, 1):( grp_matrix(i+1, 1)-1 )), ...
      X(3, grp_matrix(i, 1):( grp_matrix(i+1, 1)-1 )), ...
      [markerTypes(1+abs(mod(i, size(markerTypes, 1))), 1) ...		% scrolls through in the order
        colorTypes(1+abs(mod(i, size(colorTypes, 1))), 1)] ...	% 2,3,4,...,size(markerTypes), 1
      );
    text(X(1, grp_matrix(i, 1)), X(2, grp_matrix(i, 1)), X(3, grp_matrix(i, 1)), ...
      num2str(grp_matrix(i, 1)), 'HorizontalAlignment', 'left', ...
      'VerticalAlignment', 'baseline', 'FontWeight', 'bold');
  end
  grid on;
  box on
  rotate3d on;
  xlabel('PCO1');
  ylabel('PCO2');
  zlabel('PCO3');
  %title('Plot of 1st 3 PCO Axes');
  
  % and display a message box with the eigenvalues and goodness of fit (only the first 30 
  % if the list is long)
  for i=1:3
    output_text{i, 1} = ' ';
  end
  output_text{4, 1} = ['Eigenvalues for matrix ' M_name ' & Cumulative goodness of fit'];
  output_text{5, 1} = ' ';
  if M_size > 30
    output_text{6, 1} = sprintf( '%-5d %-20f %-20f \n', cat( 1, 1:30, eigen_matrix(1:30, :)' ) );
    output_text{7, 1} = '...';
  else
    output_text{6, 1} = sprintf( '%-5d %-20f %-20f \n', cat( 1, 1:M_size, eigen_matrix' ) );
  end
  boxTitle = 'Eigenvalues & Accuracy of Fit'; 
  msgbox(output_text, boxTitle);
  
end %% if 0




if ~exist('pco2save')
  % get the number of PCO coordinates to save
  inputPrompt = {'How many PCO coordinates do you want to save? '};
  value = inputdlg(inputPrompt);
  pco_size = str2num(value{1});
else
  pco_size = pco2save;
end

if pco_size > M_size
  pco_size = M_size;
end


coords = coords(:, 1:pco_size);



if ~exist('outputfilename')
  % save the requested number of PCO coordinates for the objects
  % first ask the user for the file name to write to
  [pco_name, pco_path] = uiputfile([eigen_path eigen_nameBase '.pco'], 'File for writing the PCO Coordinates');
else
  pco_name = [relnameof(outputfilename), '_pco.txt'];
  pco_path = pathof(outputfilename);
end


% open the file to write to
fid = fopen([pco_path pco_name], 'w');



% and write to it!

if exist('object_names') & exist('group_names')
  
  fprintf(fid, 'Matrix %s', M_name);
  format_Str = [' : Obj Name \t Group Name \t'];
  for i=1:pco_size
    format_Str = [format_Str 'PCO #' num2str(i) ' \t '];
  end
  format_Str =  [format_Str ' \n'];
  fprintf(fid, format_Str);
  format_Str = ['%s \t %s \t'];
  for i=1:pco_size
    format_Str = [format_Str ' %f \t'];
  end
  format_Str =  [format_Str ' \n'];
  for i=1:grp_num
    grp_list(grp_matrix(i, 1):grp_matrix(i+1, 1)-1, 1) = i;
  end
  tempMatrix1 = cat( 2, object_names, group_names, num2cell(X(1:pco_size, :)') );
  tempMatrix1 = tempMatrix1';
  fprintf(fid, format_Str, tempMatrix1{1:end});
  
  
elseif exist('object_names') & ~exist('group_names') & exist('grp_matrix')
  
  fprintf(fid, 'Matrix %s', M_name);
  format_Str = [' : Obj Name \t Group # \t'];
  for i=1:pco_size
    format_Str = [format_Str 'PCO #' num2str(i) ' \t '];
  end
  format_Str =  [format_Str ' \n'];
  fprintf(fid, format_Str);
  format_Str = ['%s \t %3d \t'];
  for i=1:pco_size
    format_Str = [format_Str ' %f \t'];
  end
  format_Str =  [format_Str ' \n'];
  for i=1:grp_num
    grp_list(grp_matrix(i, 1):grp_matrix(i+1, 1)-1, 1) = i;
  end
  tempMatrix1 = cat( 2, object_names, num2cell(grp_list), num2cell(X(1:pco_size, :)') );
  tempMatrix1 = tempMatrix1';
  fprintf(fid, format_Str, tempMatrix1{1:end});
  
  
elseif exist('object_names') & ~exist('group_names') & ~exist('grp_matrix')
  
  fprintf(fid, 'Matrix %s', M_name);
  format_Str = [' : Obj Name \t'];
  for i=1:pco_size
    format_Str = [format_Str 'PCO #' num2str(i) ' \t '];
  end
  format_Str =  [format_Str ' \n'];
  fprintf(fid, format_Str);
  format_Str = ['%s \t'];
  for i=1:pco_size
    format_Str = [format_Str ' %f \t'];
  end
  format_Str =  [format_Str ' \n'];
  tempMatrix1 = cat( 2, object_names, num2cell(X(1:pco_size, :)') );
  tempMatrix1 = tempMatrix1';
  fprintf(fid, format_Str, tempMatrix1{1:end});
  
  
elseif ~exist('object_names') & ~exist('group_names') & ~exist('grp_matrix')
  
  fprintf(fid, 'Matrix %s', M_name);
  format_Str = [' : Obj # \t'];
  for i=1:pco_size
    format_Str = [format_Str 'PCO #' num2str(i) ' \t '];
  end
  format_Str =  [format_Str ' \n'];
  fprintf(fid, format_Str);
  format_Str = ['%3d \t'];
  for i=1:pco_size
    format_Str = [format_Str ' %f \t'];
  end
  format_Str =  [format_Str ' \n'];
  tempMatrix1 = cat( 2, num2cell((1:M_size)'), num2cell(X(1:pco_size, :)') );
  tempMatrix1 = tempMatrix1';
  fprintf(fid, format_Str, tempMatrix1{1:end});
  
  
elseif ~exist('object_names') & ~exist('group_names') & exist('grp_matrix')
  
  fprintf(fid, 'Matrix %s', M_name);
  format_Str = [' : Obj # \t Group # \t'];
  for i=1:pco_size
    format_Str = [format_Str 'PCO #' num2str(i) ' \t '];
  end
  format_Str =  [format_Str ' \n'];
  fprintf(fid, format_Str);
  format_Str = ['%3d \t %3d \t'];
  for i=1:pco_size
    format_Str = [format_Str ' %f \t'];
  end
  format_Str =  [format_Str ' \n'];
  for i=1:grp_num
    grp_list(grp_matrix(i, 1):grp_matrix(i+1, 1)-1, 1) = i;
  end
  tempMatrix1 = cat( 2, num2cell((1:M_size)'), num2cell(grp_list), num2cell(X(1:pco_size, :)') );
  tempMatrix1 = tempMatrix1';
  fprintf(fid, format_Str, tempMatrix1{1:end});
  
end



% we're done, so close the file
fclose(fid);


% % clear the variables and return
% clear;

return;