function [Spec, mask_rms, noise_rms] = subMATper(Spec, P, data_form, adjustment);

%**
%** "subMATper.m" 
%**
%** Kathryn A. Cortopassi, 2001-2002
%**
%** Syntax: Spec = subMATper(Spec, P, data_form, adjustment);
%**
%** A function to zero the data that has a value below that of the given
%** 'P' percentile value on a matrix-wide basis;
%** various adjustments can be made to the masked (unthresholded) values 
%**
%** 'Spec' == an MxN spectrogram array (or any array of values)
%** 'P' == is a percent value
%** 'data_form' == a flag telling how the values are stored,
%**                Amplitude, Power, or dB
%** 'adjustment' == a flag telling how to deal with masked values after thresholding,
%**                 None, Bias, Demean, or Binarize
%**
%** NOTE: when you want to accomplish true additive noise correction,
%**       data_form should be power, and adjustment should be bias
%**

%**
%** by Kathryn A. Cortopassi
%** created 21-Sept-2001
%** modified 23-May-2002
%**
%** modifications March 2005
%** modifications August 2005
%**


%% check for the correct number of inputs
argLo = 4; argHi = inf;
error(nargchk(argLo, argHi, nargin));

switch (data_form)
  %% convert data to amplitude for RMS calc
  case {'Amp'; 'Amplitude'}
    Spec_amp = Spec;
    %Spec_pow = Spec .^2;
  case {'Pow'; 'Power'}
    Spec_amp = sqrt(Spec);
    %Spec_pow = Spec;
  case {'dB';  'dBpow'}
    Spec_amp = 10 .^(Spec ./ 20);
    %Spec_pow = 10 .^(Spec ./ 10);
  otherwise
    error(sprintf('Data format flag ''%s'' not recognized!', data_form));
end

%% make all values positive starting from zero
Spec = Spec - min(Spec(:));

%% unwrap the spectrogram matrix column-wise and sort the values in ascending order
SpecDist = sort(Spec(:));

%% find our absolute threshold as the Pth percentile value of 'SpecDist' by going to the 
%% index which equals P% of N 
%% N is the total length 'SpecDist', P% of N equals (NP/100) rounded to the nearest integer
num_bin = length(SpecDist);
perc_inx = round((P/100) * num_bin);
perc_inx = min(num_bin, max(1, perc_inx));
abs_thresh = SpecDist(perc_inx);

%% find indices of values equal to or below absolute threshold value
noise_inx = (Spec - abs_thresh) <= 0;
mask_inx = ~noise_inx;

%% calculate the RMS amplitude for noise_inx values and mask_inx values
%% use actual amplitude values
noise_rms = sqrt(sum(Spec_amp(noise_inx).^2)/sum(noise_inx(:)));
mask_rms = sqrt(sum(Spec_amp(mask_inx).^2)/sum(mask_inx(:)));
%% *** exploration code ***
% figure; hist(Spec_amp(noise_inx)); title(sprintf('noise (amp) : RMS = %12.2g : num pts = %d', noise_rms, sum(noise_inx(:))));
% figure; hist(Spec_amp(mask_inx)); title(sprintf('sig (amp) : RMS = %12.2g : num pts = %d', mask_rms, sum(mask_inx(:))));
% %% use actual power values
% noise_rms = sqrt(sum(Spec_pow(noise_inx).^2)/sum(noise_inx(:)));
% mask_rms = sqrt(sum(Spec_pow(mask_inx).^2)/sum(mask_inx(:)));
% figure; hist(Spec_pow(noise_inx)); title(sprintf('noise (power) : RMS = %12.2g : num pts = %d', noise_rms, sum(noise_inx(:))));
% figure; hist(Spec_pow(mask_inx)); title(sprintf('sig (power) : RMS = %12.2g : num pts = %d', mask_rms, sum(mask_inx(:))));
% %% use values in whatever data format 
% noise_rms = sqrt(sum(Spec(noise_inx).^2)/sum(noise_inx(:)));
% mask_rms = sqrt(sum(Spec(mask_inx).^2)/sum(mask_inx(:)));
% figure; hist(Spec(noise_inx)); title(sprintf('noise (%s) : RMS = %12.2g : num pts = %d', data_form, noise_rms, sum(noise_inx(:))));
% figure; hist(Spec(mask_inx)); title(sprintf('sig (%s) : RMS = %12.2g : num pts = %d', data_form, mask_rms, sum(mask_inx(:))));
%% *** exploration code ***

spec_vals = Spec(:);
%% zero out the spectrogram values below abs_threshold (considered noise)
Spec(noise_inx) = 0;

switch (adjustment)
%% adjust the masked (unthresholded) spectrogram values
  case {'None'}
    %% do nothing 
    
  case {'Bias'}
    Spec(mask_inx) = Spec(mask_inx) - abs_thresh; 
  case {'Demean'}
    Spec(mask_inx) = Spec(mask_inx) - mean(Spec(mask_inx)); 
    case {'Binarize'}
        Spec(mask_inx) = 1;
    case {'Flood-fill Binarize'}
        [len_x, len_y] = size(Spec);
        all_pixels = [];
        new_spec = zeros(len_x, len_y);
        ff_thresh = mean(spec_vals) + 1.2*std(spec_vals);
        for ix = 1:len_x
            for iy = 1:len_y
                try  pixels = FloodFill2D(Spec, ix, iy, ff_thresh, []);
                    if length(pixels) > 6
                        for z = 1:length(pixels)
                            new_spec(pixels(z,1),pixels(z,2)) = 1;
                        end
                        all_pixels = [all_pixels; pixels];
                    end
                end
            end
        end
       % mask_inx = new_spec;
       % Spec(mask_inx) = 1;
       Spec = new_spec;
    otherwise
    error(sprintf('Mask adjustment flag ''%s'' not recognized!', adjustment));
end


%% end function
return;