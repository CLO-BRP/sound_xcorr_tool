function [result, context] = detection_validiation_sub(truth_log, test_log)
result = struct;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% make vector of selected log names, checking for errors
% 
% % make vector of selected log names
% [status1, target, NumLogs, order_flag] = list_logs(context, parameter);
% if status1   
%     return;     
% end
% 
% %check if any selected log is open in an XBAT browser
% status2 = log_is_open_pitz(target, NumLogs);
% if status2    
%     return;    
% end
% 
% %check if logs are paired correctly
% status3 = logs_paired(target, NumLogs, parameter, order_flag);
% if status3   
%     return;    
% end

%% match log pairs

TEMP = zeros(20,1);
cum_duration = 0;
%if parameter.truth_flag
%   cum_count = struct('truth', struct('TP', TEMP, 'FN', TEMP, 'FP', TEMP, 'TN', TEMP, 'total', 0), ...
%                      'test', struct('TP', TEMP, 'FP', TEMP, 'total', 0));
% else
  cum_count = struct('truth', struct('TP', TEMP, 'FN', TEMP, 'total', 0), ...
                     'test', struct('TP', TEMP, 'FP', TEMP, 'total', 0));
 % end

i=1;

%truth = truth_log;
%test = test_log;
% [sound_truth, fn_truth] = fileparts(truth);
% [sound_test, fn_test] = fileparts(test); 


  %% create headers for output XLS files
%   if parameter.xls_flag || ROC_plots_active
%       
%     %ask user where to put XLS output, if XLS output has been selected
%     out_dir = uigetdir(pwd, 'Select XLS output folder.');
% 
%     if ischar(out_dir) && exist(out_dir, 'dir')
% 
%         cd(out_dir)
%         
%         if parameter.xls_flag
%             
%             out_name = fullfile(out_dir, [context.library.name, '_DetectorValidation_Summary_', datestr(now, 30)]);
%         end
%         
%         %header for xls
%         M = output_header(parameter, fn_truth, fn_test);
%  
%       if ROC_plots_active
% 
%         roc_name = fullfile(out_dir, [context.library.name, '_DetectorValidation_ROC_', datestr(now, 30)]);
% 
        %header for xls
        M_header = roc_header_sub(truth_log.file, test_log.file);

%       end
% 
%     else
% 
%       fprintf('\nUser terminated action.\n')
% 
%       return;
% 
%     end
%   end
  
  
  
  
  
  
  %load truth log
 %%% truth_log = get_library_logs('logs', [], sound_truth, fn_truth);
  
%   %check if a single, valid log is specified
%   if ~(ischar(sound_truth) && ischar(fn_truth) && isequal(length(truth_log),1))
%   
%     txt = sprintf('Action terminated.\nLog did not load.%s', [fn_truth, '\', sound_truth]);
% 
%     fprintf(2,'ERROR:\n%s\n\n', txt);
% 
%     h = warndlg(txt, 'ERROR');
% 
%     movegui(h, 'center')
% 
%     return;
%     
%   else
%     
    % rename file field in log
%     fn_truth_ext = [fn_truth '.mat'];
%     truth_log.file = fn_truth_ext;
% 
%     % rename path field in log
%     LogPath = context.library.path;
%     LogPath = [LogPath sound_truth '\Logs\'];
%     truth_log.path = LogPath;
%     
%     %save updated file name and path to log
%  %   log_save(truth_log);   
%     
  
  %load test log
%%%  test_log = get_library_logs('logs', [], sound_test, fn_test);
  
%   %check if a single, valid log is specified
%   if ~(ischar(sound_test) && ischar(fn_test) && isequal(length(test_log),1))
%   
%     txt = sprintf('Action terminated.\nLog did not load.%s', [fn_test, '\', sound_test]);
% 
%     fprintf(2,'ERROR:\n%s\n\n', txt);
% 
%     h = warndlg(txt, 'ERROR');
% 
%     movegui(h, 'center')
% 
%     return;
%     
%   else
    
    % rename file field in log
%     fn_test_ext = [fn_test '.mat'];
%     test_log.file = fn_test_ext;
% 
%     % rename path field in log
%     LogPath = context.library.path;
%     LogPath = [LogPath sound_test '\Logs\'];
%     test_log.path = LogPath;
%     
    %save updated file name and path to log
 %   log_save(test_log);
 
  % do match
[count] = match(truth_log, test_log);
 
  %%% End changes 1/25 SCK
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%increment cumulative stats
% cum_count = sum_fields(cum_count, count, parameter);
%increment cumulative sound duration
%cum_duration = cum_duration + truth_log.sound.duration;
%output results for xls  
%M = out_matrix(M, count, truth_log.sound.duration, parameter, fn_truth, fn_test);
%write worksheet to ROC XLS, if user selected it
% ROC_plots_active
      
M2 = roc_matrix(M_header, count, truth_log.sound.duration);
    
%%% Plot ROC curve
%if parameter.fig_flag
  
  figure('menubar', 'figure')
  
  Threshold = [M2{15:end,1}];
  TPR = [M2{15:end,2}];
  FPR = [M2{15:end,6}];
  plot(FPR, TPR)
  text(FPR + 0.01, TPR - 0.02, num2str(Threshold'), 'color', [1 0 0])
  ylabel('TPR'); ylim([0 1]); xlabel('FP/hr')
  
  test_log_name =  test_log.file; truth_log_name = truth_log.file;
  if iscell(test_log_name)
      test_log_name = char(test_log_name(1));
  end
  if iscell(truth_log_name)
      truth_log_name = char(truth_log_name(1));
  end
  h2 =title(sprintf('ROC Curve\nTest Log: %s\nTruth Log: %s', test_log_name, truth_log_name));
  set(h2,'Interpreter','none')
%end
  

% Get truth log tags
truth_tags = get_primary_tag(truth_log.event, truth_log.length);
tag_ix=cellfun('isempty',truth_tags);
truth_tags(tag_ix)={'untagged'};
clear num tagged_event truth_log_tagged event temp_count

% Get test log tags
test_tags = get_primary_tag(test_log.event, test_log.length);
test_tag_names_unsorted = unique(test_tags);
y=asort(test_tag_names_unsorted);
test_tag_names = y.anr;
if length(test_tag_names) == 0
    test_tag_names = y.snr;
end

% For each unique tag, make a log and report ROC curve performance
for t = 1:length(test_tag_names)
    
    %Look for events with current focal tag
    temp_ix = strcmp(test_tags,test_tag_names(t));
    %initialize count
    num = 0;
    % TO DO: use indexing instead of for loop
    for k = 1:length(temp_ix)
        
        % if event k has current focal tag
        if temp_ix(k)
            num = num+1;
            event(1,num)= test_log.event(1,k);
        end
    end
    
    % Replace event fields in original test log to only include events with focal tag
    test_log_tagged = test_log;
    test_log_tagged = rmfield(test_log_tagged,'event');
    test_log_tagged = rmfield(test_log_tagged,'length');
    test_log_tagged.event = event;
    test_log_tagged.length = num;
    test_log_tagged.tag_name = test_tag_names(t);
    
    % Get TP/FP for these events and ROC curve values
    [temp_count, ~, ~] = match(truth_log,test_log_tagged);
    M2_tags{t} = roc_matrix(M_header, temp_count, truth_log.sound.duration);
    
    clear num tagged_event test_log_tagged event temp_count
end

% Get overall ROC curve for all test events regardless of tag
M2 = roc_matrix(M_header, count, truth_log.sound.duration);
  
 % Plot ROC by truth log tags 
 if 1 %parameter.ROC_by_template
      
      figure('menubar', 'figure') 
     cmap = hsv(length(test_tag_names));
      
      Threshold = [M2{15:end,1}]; 
      TPR = [M2{15:end,2}];
      FPR = [M2{15:end,6}];
      plot(FPR, TPR,'k','LineWidth',2);
      text(FPR + 0.01, TPR - 0.02, num2str(Threshold'), 'color', [1 0 0])
      %set(p,'Color','blue','LineWidth',2)
      hold on
      for t = 1:length(test_tag_names)
          M2_tags_temp = M2_tags{t};
          TPR = [M2_tags_temp{15:end,2}];
          FPR = [M2_tags_temp{15:end,6}];
          xmax(t) = max(FPR);
          plot(FPR, TPR,'Color',cmap(t,:),'LineWidth',2,'LineStyle','--')
          %set(p,'Color',colors{t},'LineStyle', linsty{t},'LineWidth',2)
         % text(FPR(2) + 0.25, TPR(1) - 0.015,char(test_tag_names(t)))
      end
      
      leg_entries = {'Overall'};
      for j = 1:length(test_tag_names)
          leg_entries{j+1} = char(test_tag_names(j));
      end
      hl = legend(leg_entries) ;
      set(hl,'Interpreter','none')
      
      xlabel('FP/hr'); ylabel('TPR'); ylim([0 1]);
      %xlim([0  max(xmax)]);
      
      test_log_name =  test_log.file; truth_log_name = truth_log.file;  
      if iscell(test_log_name)
          test_log_name = char(test_log_name(1));
      end
      if iscell(truth_log_name)
          truth_log_name = char(truth_log_name(1));
      end
      h2 = title(sprintf('ROC Curve\nTest Log: %s\nTruth Log: %s', test_log_name, truth_log_name)) ;
      set(h2,'Interpreter','none')
  end
  
%  % More plots
%   % Negative ROC curves showing the contribution of individual templates
%   if parameter.negative_ROC_by_template
%       
%       figure('menubar', 'figure') 
%       cmap = hsv(length(test_tag_names));
%       
%       Threshold = [M2{15:end,1}]; 
%       overall_TPR = [M2{15:end,2}];
%       overall_FPR = [M2{15:end,6}];
%       %plot(FPR, TPR,'k','LineWidth',2);
%       %text(FPR + 0.01, TPR - 0.02, num2str(Threshold'), 'color', [1 0 0])
%       %set(p,'Color','blue','LineWidth',2)
%       %hold on
%       for t = 1:length(test_tag_names)
%           M2_tags_temp = M2_tags{t};
%           TPR = [M2_tags_temp{15:end,2}];
%           FPR = [M2_tags_temp{15:end,6}];
%           neg_TPR = TPR - overall_TPR;
%           neg_FPR = overall_FPR-FPR;
%           xmax(t) = max(FPR);
%           plot(neg_FPR, neg_TPR,'Color',cmap(t,:),'LineWidth',2,'LineStyle','--')
%           hold on
%          % text(FPR(2) + 0.25, TPR(1) - 0.015,char(test_tag_names(t)))
%       end
%       
%       leg_entries = {};
%       for j = 1:length(test_tag_names)
%           leg_entries{j} = char(test_tag_names(j));
%       end
%       legend(leg_entries) 
%       
%       if parameter.truth_flag
%           xlabel('FP/hr')
%       else
%           xlabel('FP/hr')
%       end
%       ylabel('TPR'); ylim([-1 0]); xlim([0  max(xmax)]);
%       title(sprintf('Negative ROC Curves\nTest Log: %s\nTruth Log: %s', fn_test, fn_truth))
%   end
%   
%   %%% Plot TPR vs threshold
%   if parameter.TPR_vs_thresh
%       
%       Threshold = [M2{15:end,1}];
%       TPR = [M2{15:end,2}];
%       
%       figure('menubar', 'figure')
%       plot(Threshold, TPR,'LineWidth',2)
%       
%       xlabel('Detector Threshold')
%       ylabel('TPR'); ylim([0 1])
%       title(sprintf('TPR vs Threshold\nTest Log: %s\nTruth Log: %s', fn_test, fn_truth))
%   end
% 
%  %%% Plot TPR vs Threshold for each template 
%   if parameter.TPR_vs_thresh_by_template
%       
%       figure('menubar', 'figure') 
%       cmap = hsv(length(test_tag_names));
%       
%       Threshold = [M2{15:end,1}]; 
%       overall_TPR = [M2{15:end,2}];
%       plot(Threshold, overall_TPR,'k','LineWidth',2);
%    %   text(Threshold + 0.01, overall_TPR - 0.02, num2str(Threshold'), 'color', [1 0 0])
%       hold on
%       for t = 1:length(test_tag_names)
%           M2_tags_temp = M2_tags{t};
%           TPR = [M2_tags_temp{15:end,2}];
%           plot(Threshold, TPR,'Color',cmap(t,:),'LineWidth',2,'LineStyle','--')
%           hold on
%          % text(FPR(2) + 0.25, TPR(1) - 0.015,char(test_tag_names(t)))
%       end
%       
%       leg_entries = {};
%       for j = 1:length(test_tag_names)
%           leg_entries{j} = char(test_tag_names(j));
%       end
%       legend(leg_entries) 
%       xlabel('Detector Threshold')
%       ylabel('TPR'); ylim([0 1]); xlim([0  1]);
%       title(sprintf('TPR vs. Threshold\nTest Log: %s\nTruth Log: %s', fn_test, fn_truth))
%   end
%   
%     %%% Plot FP/hr vs threshold
%   if parameter.FP_hr_vs_thresh
%       
%       Threshold = [M2{15:end,1}];
%       FPR = [M2{15:end,6}];
%       FPR_max_fract = ceil(max(FPR/1000));
%       
%       figure('menubar', 'figure')
%       plot(Threshold, FPR,'LineWidth',2)
%       
%       xlabel('Detector Threshold')
%       ylabel('FP/hr'); ylim([0 1000*FPR_max_fract])
%       
%       title(sprintf('FP/hr vs Threshold\nTest Log: %s\nTruth Log: %s', fn_test, fn_truth))
%   end
%   
%   %%% Plot FP/hr vs Threshold for each template 
%   if parameter.TPR_vs_thresh_by_template
%       
%       figure('menubar', 'figure') 
%       cmap = hsv(length(test_tag_names));
%       
%       Threshold = [M2{15:end,1}]; 
%       overall_FPR = [M2{15:end,6}];
%       plot(Threshold, overall_FPR,'k','LineWidth',2);
%      % text(Threshold + 0.01, overall_FPR - 0.02, num2str(Threshold'), 'color', [1 0 0])
%       hold on
%       for t = 1:length(test_tag_names)
%           M2_tags_temp = M2_tags{t};
%           FPR = [M2_tags_temp{15:end,6}];
%           plot(Threshold, FPR,'Color',cmap(t,:),'LineWidth',2,'LineStyle','--')
%           ymax(t) = max(FPR);
%           hold on
%           %set(p,'Color',colors{t},'LineStyle', linsty{t},'LineWidth',2)
%          % text(FPR(2) + 0.25, TPR(1) - 0.015,char(test_tag_names(t)))
%       end
%       
%       leg_entries = {};
%       for j = 1:length(test_tag_names)
%           leg_entries{j} = char(test_tag_names(j));
%       end
%       legend(leg_entries) 
%       xlabel('Detector Threshold')
%       ylabel('FPR'); xlim([0  1]); %ylim([0 max(ymax)]); 
%       title(sprintf('FP/hr vs. Threshold\nTest Log: %s\nTruth Log: %s', fn_test, fn_truth))
%   end
%   
%   %%% Plot F1 score, precision, recall
% if 1 %parameter.fig_flag
%   
%   figure('menubar', 'figure')
%   
%   Threshold = [M2{15:end,1}];
%   TPR = [M2{15:end,2}];
%   FPR = [M2{15:end,6}];
%   
%   for z = 1:length(Threshold)
%       precision(z) = TPR(z)/(TPR(z)+FPR(z));
%       recall(z) = TPR(z);
%       F1(z) = precision(z)*recall(z) / (precision(z) + recall(z));
%   end
%   
%   subplot(2,1,1);plot(Threshold,precision,':b','LineWidth',2);
%   hold on
%   plot(Threshold, recall,'b--','LineWidth',2); 
%   plot(Threshold, F1,'k','LineWidth',2);
%   xlabel('Threshold'); ylabel('Score'); ylim([0 1]); legend('Precision','Recall','F1 Score')
%   
%   subplot(2,1,2);plot(Threshold,precision,':b','LineWidth',2);
%   hold on
%   plot(Threshold, recall,'b--','LineWidth',2); 
%   plot(Threshold, F1,'k','LineWidth',2);
%   xlabel('Threshold'); ylabel('Score'); ylim([0 2*max(F1)]); hleg1 = legend('Precision','Recall','F1 Score');
%   set(hleg1, 'Location', 'NorthWest');
%   [ax,h3]=suplabel(sprintf('Precision, Recall, F1 Score\nTest Log: %s   Truth Log: %s', fn_test, fn_truth) ,'t'); 
% %  set(h3,'FontSize',30) 
%    
% end
%   
end
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [count, ann_truth_log, ann_test_log] = match(truth_log, test_log)

pos_tag_filter_flag = 0;

%% make truth log matrix
truth_M = make_log_matrix(truth_log, 'truth');
truth_time = truth_M(:,1:2);
truth_chan = truth_M(:,5);
truth_tags = get_primary_tag(truth_log.event, truth_log.length);
pos_truth = true(truth_log.length,1);  
pos_truth_size = sum(pos_truth);

%% make test log matrix
test_M = make_log_matrix(test_log, 'test');

%adjust begin and end time of test events by event duration * overlap
fudge = .5 * diff(test_M(:,1:2)')';

test_time_fudge = [test_M(:,1) - fudge, test_M(:,2) + fudge];
%test_time_fudge = [test_M(:,1) + fudge, test_M(:,2) - fudge];
test_chan = test_M(:,5);
test_score = test_M(:,6)';

%% classify truth events as TP, FN, FP, and TN
match = false(truth_log.length,1);

% truth_out_tag = cell(truth_log.length,1);
truth_score = NaN(1,truth_log.length);

% match_idx = NaN(1,truth_log.length);
for i = 1:truth_log.length
  
    match_v1 = (truth_time(i,1) <  test_time_fudge(:,2));
    match_v2 = (truth_time(i,2) >= test_time_fudge(:,1));
    match_v3 = (truth_chan(i)   ==       test_chan(:));
%   match_vector = (truth_time(i,1) <  test_time_fudge(:,2)) ...
%                & (truth_time(i,2) >= test_time_fudge(:,1)) ; % ...
%               % & (truth_chan(i)   ==       test_chan(:));
     
match_vector = match_v1 & match_v2 & match_v3;
     
  curr_match = any(match_vector);
  match(i) = curr_match;
   
  t_score = max(test_score(match_vector));
  if length(t_score) ~= 0
    truth_score(i) = max(test_score(match_vector)); 
  end
end

%% annotate truth log

ann_truth_log = truth_log;
ann_truth_log.event = set_tags(ann_truth_log.event, '');
ann_truth_log.event(logical(match & pos_truth(:))) = set_tags(ann_truth_log.event(logical(match(:) & pos_truth)), 'TP');
ann_truth_log.event(logical(~match & pos_truth(:))) = set_tags(ann_truth_log.event(logical(~match(:) & pos_truth)), 'FN');

fn = log_name(truth_log);
if iscell(fn)
    fn = char(fn(1));
end
fn = fn(1:min(namelengthmax-13, length(fn)));
fn = [fn, '_AnnTruth'];
ann_truth_log.file = fn;

%% annotate test log
ann_test_log = test_log;

for i = 1:test_log.length
  
  %if test event is matched by a truth event
  if any(pos_truth(:) ...
       & truth_chan(:)   == test_chan(i) ...
       & truth_time(:,1) <  test_time_fudge(i,2) ...
       & truth_time(:,2) >= test_time_fudge(i,1) ...
       & truth_chan(:)   == test_chan(i))
      
    ann_test_log.event(i) = set_tags(ann_test_log.event(i), 'TP');
    
%   elseif any(neg_truth(:) ...                    %matches negative truth event
%            & truth_chan(:)   == test_chan(i) ...
%            & truth_time(:,1) <  test_time_fudge(i,2) ...
%            & truth_time(:,2) >= test_time_fudge(i,1) ...
%            & truth_chan(:)   == test_chan(i))
%       
%     ann_test_log.event(i) = set_tags(ann_test_log.event(i), 'FP');
%    
  else
      
    ann_test_log.event(i) = set_tags(ann_test_log.event(i), 'FP');
   
  end  
end

fn = log_name(test_log);
if iscell(fn)
    fn = char(fn(1));
end
fn = fn(1:min(namelengthmax-13, length(fn)));
fn = [fn, '_AnnTest'];
ann_test_log.file = fn;

%% calculate counts for truth log

  truth_tags = get_tags(ann_truth_log.event);
  TP_truth = strcmp('TP',[truth_tags{:}]);
  FP_truth = strcmp('FP',[truth_tags{:}]);
  FN_truth = strcmp('FN',[truth_tags{:}]);
  TN_truth = strcmp('TN',[truth_tags{:}]);
  TP_truth_count = zeros(20, 1);
  FP_truth_count = TP_truth_count;
  FN_truth_count = TP_truth_count;
  TN_truth_count = TP_truth_count;
   
  test_tags = get_tags(ann_test_log.event);
  TP_test = strcmp('TP',[test_tags{:}]);
  FP_test = strcmp('FP',[test_tags{:}]);
  TP_test_count = TP_truth_count;
  FP_test_count = TP_truth_count;
  test_score_used = test_score(~cellfun(@isempty, test_tags));
  
  threshold = 0 : 0.05 : 1;
  
  for i = 1:20
    
    curr_T = threshold(i);
    score_filter_truth = truth_score > curr_T;
    TP_truth_count(i) = sum(TP_truth & score_filter_truth);
    FN_truth_count(i) = pos_truth_size - TP_truth_count(i);
%     FN_truth_count(i) = sum(FN_truth & score_filter_truth);

    
    score_filter_test = test_score_used > curr_T;
    TP_test_count(i) = sum(TP_test & score_filter_test);
    FP_test_count(i) = sum(FP_test & score_filter_test);     
  end

 
  count.truth.TP = TP_truth_count;
  count.truth.FP = FP_truth_count;
  count.truth.FN = FN_truth_count;
  count.truth.TN = TN_truth_count;
  count.truth.total = truth_log.length;
  count.test.TP = TP_test_count;
  count.test.FP = FP_test_count;
  count.test.total = test_log.length;
 

end


