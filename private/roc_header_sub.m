function M = roc_header_sub(fn_truth, fn_test)

M = cell(14,13);

%set header
M{1} = 'Detection Validation';
M{2,1} = 'Time Run:';
M{2,5} = datestr(now, 21);

M{4,1} = 'Truth Log Suffix:';
M{4,5} = 'truth';

M{5,1} = 'Test Log Suffix:';
M{5,5} = 'test';

M{6,1} = 'Time Overlap (% test event duration):';
M{6,5} = .5;

M{7,1} = 'True Negatives Explicit?';

M{7,5} = 'No';  

M{9,1} = 'Truth Log Name';

M{9,5} = fn_truth;

M{10,1} = 'Test Log Name';

M{10,5} = fn_test;


%% set column heads

%if no negative truth events (TN not defined explicitly)

  M{13,1} = 'Threshold';  
  
  M{13,2}  = 'TPR';
  M{13,3} = sprintf('TPR (%.0f%% CI lower)', .9 * 100);
  M{13,4} = sprintf('TPR (%.0f%% CI upper)',.9 * 100);
  
  M{13,5}  = 'TPR';
  M{13,6}  = 'FP/hr';
  M{13,7}  = 'PPV';

  M{13,8}  = 'Positive Truth Matches';
  M{13,9}  = 'Positive Truth Misses';
  M{13,10}  = 'Positive Truth Total';

  M{13,11}  = 'Test Matches';
  M{13,12}  = 'Test Misses';
  M{13,13}  = 'Test Total';
  
%   M{13,14}  = 'Truth Log';
%   M{13,15} = 'Test Log';

  M{14,8}  = 'TP';
  M{14,9}  = 'FN';
  M{14,12} = 'FP';
  


