function [sndData, sampRate, resample_flag] = chkrate_v2(sndData, sampRate, sndName, status_window_edit_handle);

%**
%** "chkrate_v2.m"
%**
%** Kathryn A. Cortopassi, 2001-2002
%**
%** Checks that all the sampling rates of the sounds stored in the
%** cell array 'sndData' are equal-- their sampling rate info is stored
%** in the vector (or cell array) 'sampRate'
%** If a mismatch is found, the sampling rates of the sounds are
%** converted to the highest sampling rate found, as necessary
%** and this new sampling rate is returned as a scalar 
%**
%** syntax: [sndData, sampRate] = chkrate_v2(sndData, sampRate);
%** 'sndData' is passed in as a cell array and returned as a cell array
%** 'sampRate' is passed in as a vector (or a cell array) and is returned 
%** as a scalar (it was previously returned as a vector with all values 
%** set to the maximum sampling rate, but this was modified)
%**

%** 
%** by Kathryn A. Cortopassi
%** created Mar-2002
%** modified 
%** Apr-2002
%** May-2002
%**
%% modified 11 August 2004



% first some error checking
argLo = 3; argHi = inf;
error(nargchk(argLo, argHi, nargin));



if iscell(sndData)
  
  
  if length(sampRate) ~= length(sndData)
    if exist('status_window_edit_handle')
      current_display = get(status_window_edit_handle, 'string');
      current_display{end+1} = 'The number of sounds and sampling rates must be equal!';
      set(status_window_edit_handle, 'string', current_display, 'listboxtop', length(current_display));
      drawnow;
    else
      error(sprintf('The number of sounds and sampling rates must be equal!\n'));
    end
  end
  
  
  resample_flag = zeros(length(sndData), 1);
  
  
  cell_flag = 0;
  if iscell(sampRate)
    cell_flag = 1;
    % convert to a vector
    sampRate = [sampRate{:}];
  end 
  
  maxRate = max(sampRate);
  
  
  
  if (maxRate ~= min(sampRate))
    % all the sampling rates are not equal, give a warning and resample
    
    if exist('status_window_edit_handle')
      current_display = get(status_window_edit_handle, 'string');
      current_display{end+1} = sprintf('Sampling rates don''t match. Resampling to maximum rate of %d ...', maxRate);
      set(status_window_edit_handle, 'string', current_display, 'listboxtop', length(current_display));
      drawnow;
    else
      fprintf(1, 'Sampling rates don''t match-- resampling to maximum rate of %d ...\n', maxRate);
    end
    
    
    for i = find(sampRate ~= maxRate)
      
      %% try to consolidate memory
      pack;
      
      %% then resample
      try
        
        current_display = get(status_window_edit_handle, 'string');
        current_display{end+1} = sprintf('Upsampling sound : ''%s''...', sndName{i});
        set(status_window_edit_handle, 'string', current_display, 'listboxtop', length(current_display));
        drawnow;
        
        sndData{i} = resample(sndData{i}, maxRate, sampRate(i));
        
        resample_flag(i) = 1;
        
      catch
        %% make cell empty to mark failed resample attempt
        
        sndData{i} = [];
        
        %% get error message
        error_info = lasterror;
        curr_err_msg = ['Error resampling sound: ', sndName{i}, ' >> ''', error_info.message, ''''];
        
        if exist('status_window_edit_handle')
          current_display = get(status_window_edit_handle, 'string');
          current_display{end+1} = curr_err_msg;
          set(status_window_edit_handle, 'string', current_display, 'listboxtop', length(current_display));
          drawnow;
        else
          fprintf(1, [curr_err_msg, '\n']);
        end
        
      end %% try/catch
      
    end %%     for i = find(sampRate ~= maxRate)
    
    % change all rates in the vector to the max rate
    sampRate(:) = maxRate;
    
  else
    
    resample_flag = 0;
    
  end %%   if (maxRate ~= min(sampRate))
  
  
  %% restore to cell
  if cell_flag
    sampRate = num2cell(sampRate);
  end
  
  
  
else %% ~iscell(sndData)
  
  if exist('status_window_edit_handle')
    current_display = get(status_window_edit_handle, 'string');
    current_display{end+1} =  'Input must be a cell array!';
    set(status_window_edit_handle, 'string', current_display, 'listboxtop', length(current_display));
    drawnow;
  else
    error(sprintf('Input must be a cell array\n'));
  end
  
end %% if iscell(sndData)



return;