
function snd_spc = get_spec(snd_file_name, fs, param, maxdur);

%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%  FUNCTION: get_spec
%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




%% get processing parameters for spectrogram display
fftsz = param.fft_len;
datasz = round(fftsz*param.data_len/100);
taper = get_taper_func(param.taper_func);
ovrlp = round(datasz*param.data_overlap/100);

%% bandlimiting
loCorner = param.low_freq;
hiCorner = param.high_freq;
 
%% signal preprocessing
sig_preproc = param.sig_preproc;

%% masking
dataform = param.data_form;
masking = param.masking_meth;
maskparam = param.masking_param;
adjust = param.mask_adj;

%% and corr type and matrix norm type for xcorr
corr_type = param.corr_type;
normtype = param.corr_std;


%% get the sound data
snd_data = sndread_v2(snd_file_name);


%% de-mean the sound data
%% this is done in 'run' function
snd_data = demeansnd(snd_data);


%% apply signal preprocessing as requested
%% this is done in 'run' function
snd_data = signal_preprocess(snd_data, sig_preproc);




  %% generate the spectrogram 
  [snd_spc, fvec, tvec] = specgram(snd_data, fftsz, fs, feval(taper, datasz), ovrlp);
  
  %% put spec values in requested data format
  switch (dataform)
    case ('Amplitude')
      snd_spc = abs(snd_spc);
    case ('Power')
      snd_spc = snd_spc .* conj(snd_spc);
    case ('dB')
      snd_spc = 10*log10(snd_spc .* conj(snd_spc));
    otherwise %% set up some default
      snd_spc = 10*log10(snd_spc .* conj(snd_spc));
  end
  
  %% bandlimit the spectrogram as requested
  %% this is done in 'run' function
  [snd_spc, fvec] = bandlimspc(snd_spc, fvec, loCorner, hiCorner);
  
  %% mask the spectrogram as requested
  %% this is done in 'run' function
  snd_spc = denoisespc_v2(snd_spc, masking, param, dataform, adjust);
  
  %% standardize the spectrogram as requested
  %% this is done in 'xcorr' function
  snd_spc = standardize_mat(snd_spc, normtype);
  
  
  
  
  %% zero-pad the spectrograms to match in duration
  if size(snd_spc, 2) < maxdur
    snd_spc(:, maxdur) = 0; 
  end

  
  return;
