function [spec, f] = bandlimspc(spec, f, lo, hi);

%**
%** "bandlimspc.m"
%**
%** Kathryn A. Cortopassi, 2001-2002
%**
%** Function to bandlimit a spectrogram or cell array of spectrograms
%** 
%** Syntax: [spec, f] = bandlimspc(spec, f, lo, hi);
%**
%** 'spec' is a single spectrogram or cell array of spectrogram matrices
%** with N frequency bins (rows)
%** 'f' is the frequency vector of the spectrogram, or a cell array of
%** frequency vectors as appropriate (even if 'spec' is a cell array,
%** 'f' can still be a single vector)
%** ('f(2)' or 'f{i}(2)' = 'df' equals samplingRate/FFT size)
%** 'lo' is the low frequency corner in Hz
%** 'hi' is the high frequency corner in Hz
%** 
%** 'spec' returns the bandlimited spectrogram or cell array of spectrograms
%** 'f' returns the bandlimited frequency vector or cell array of frequency vectors
%**

%**
%** by Kathryn A. Cortopassi
%** created 2-Oct-2001
%** modified
%** 28-Feb-2002
%** 23-May-2002
%**


% check for correct number of inputs
argLo = 4; argHi = inf;
error(nargchk(argLo, argHi, nargin));


if iscell(spec)    
    if iscell(f)
        for i=1:length(spec)
            df = f{i}(2);
            maxIndex = length(f{i});
            
            % get start and end indeces based on given frequency values
            % force positive frequency values
            if (lo < df/2)
                startIndex = 2;  % force it to remove DC
            else
                startIndex = round(lo/df) + 1;
            end
            endIndex = round(hi/df) + 1;
            
            frange2keep = [min(startIndex, maxIndex):min(endIndex, maxIndex)];
            
            % pull out the desired frequency range of the spectrogram
            spec{i} = spec{i}(frange2keep, :);
            
            % pull out the desired range of the frequency vector
            f{i} = f{i}(frange2keep);
        end
        
    else
        df = f(2);
        maxIndex = length(f);
        
        % get start and end indeces based on given frequency values
        % force positive frequency values
        if (lo < df/2)
            startIndex = 2;  % force it to remove DC
        else
            startIndex = round(lo/df) + 1;
        end
        endIndex = round(hi/df) + 1;
        
        frange2keep = [min(startIndex, maxIndex):min(endIndex, maxIndex)];
        
        % pull out the desired range of the frequency vector
        f = f(frange2keep);
        
        for i=1:length(spec)
            % pull out the frequency range of the spectrogram
            spec{i} = spec{i}(frange2keep, :);
        end
        
    end
    
    
else
    df = f(2);
    maxIndex = length(f);
    
    % get start and end indeces based on given frequency values
    % force positive frequency values
    if (lo < df/2)
        startIndex = 2;  % force it to remove DC
    else
        startIndex = round(lo/df) + 1;
    end
    endIndex = round(hi/df) + 1;
    
    frange2keep = [min(startIndex, maxIndex):min(endIndex, maxIndex)];
    
    % pull out the desired range of the frequency vector
    f = f(frange2keep);
    % pull out the frequency range of the spectrogram
    spec = spec(frange2keep, :);
    
end


% end function
return;