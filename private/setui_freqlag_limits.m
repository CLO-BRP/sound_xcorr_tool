function setui_freqlag_limits(fig_handle, fig_tag, low_freq, high_freq, max_time_lag, max_freq_lag);

%%
%% 'setui_freqlag_limits'
%%
%% Kathryn A. Cortopassi, February 2006
%%
%% set limits for ui parameters
%%
%% syntax:
%% -------
%%   setui_freqlag_limits(fig_handle, fig_tag, low_freq, high_freq,
%%   max_time_lag, max_freq_lag);
%%
%% input:
%% ------
%%
%% output:
%% -------
%%



%% set bandlimits and lags as indicated
low_freq_edit_uicontrol_handle = findobj(fig_handle, 'tag', ['low_freq_edit_uicontrol', fig_tag]);
high_freq_edit_uicontrol_handle = findobj(fig_handle, 'tag', ['high_freq_edit_uicontrol', fig_tag]);
max_time_lag_edit_uicontrol = findobj(fig_handle, 'tag', ['max_time_lag_edit_uicontrol', fig_tag]);
max_freq_lag_edit_uicontrol = findobj(fig_handle, 'tag', ['max_freq_lag_edit_uicontrol', fig_tag]);

set(low_freq_edit_uicontrol_handle, 'string', sprintf('%d', low_freq));
set(high_freq_edit_uicontrol_handle,'string', sprintf('%d', high_freq));

set(max_time_lag_edit_uicontrol, 'string', sprintf('%12.4f', max_time_lag));
set(max_freq_lag_edit_uicontrol, 'string', sprintf('%d', max_freq_lag));

return;
