function M = roc_header(parameter, fn_truth, fn_test)

%set header
M{1} = 'Detection Validation';
M{2,1} = 'Time Run:';
M{2,5} = datestr(now, 21);

M{4,1} = 'Truth Log Suffix:';
M{4,5} = parameter.truth_suffix;

M{5,1} = 'Test Log Suffix:';
M{5,5} = parameter.test_suffix;

M{6,1} = 'Time Overlap (% test event duration):';
M{6,5} = parameter.overlap;

M{7,1} = 'True Negatives Explicit?';

if parameter.truth_flag
  
  M{7,5} = 'Yes';
  
else
  
  M{7,5} = 'No';
  
end

M{9,1} = 'Truth Log Name';

M{9,5} = fn_truth;

M{10,1} = 'Test Log Name';

M{10,5} = fn_test;


%% set column heads

%if no negative truth events (TN not defined explicitly)
if ~parameter.truth_flag
  
  M{13,1} = 'Threshold';  
  
  M{13,2}  = 'TPR';
  M{13,3} = sprintf('TPR (%.0f%% CI lower)', parameter.plevel * 100);
  M{13,4} = sprintf('TPR (%.0f%% CI upper)', parameter.plevel * 100);
  
  M{13,5}  = 'TPR';
  M{13,6}  = 'FP/hr';
  M{13,7}  = 'PPV';

  M{13,8}  = 'Positive Truth Matches';
  M{13,9}  = 'Positive Truth Misses';
  M{13,10}  = 'Positive Truth Total';

  M{13,11}  = 'Test Matches';
  M{13,12}  = 'Test Misses';
  M{13,13}  = 'Test Total';
  
%   M{13,14}  = 'Truth Log';
%   M{13,15} = 'Test Log';

  M{14,8}  = 'TP';
  M{14,9}  = 'FN';
  M{14,12} = 'FP';
  

%if negative truth events (TN defined explicitly)
else
  
  M{13,1} = 'Threshold';  

  M{13,2} = 'TPR';
  M{13,3} = 'TPR (CI lower)';
  M{13,4} = 'TPR (CI upper)';  
  
  M{13,5} = 'TPR(check)';
  M{13,6} = 'FPR';
  M{13,7} = 'FP/hr';
  M{13,8} = 'PPV';

  M{13,9} = 'Positive Truth Matches';
  M{13,10} = 'Positive Truth Misses';
  M{13,11} = 'Positive Truth Total';

  M{13,12} = 'Negative Truth Matches';
  M{13,13} = 'Negative Truth Misses';
  M{13,14} = 'Negative Truth Total';
  
  M{13,15} = 'Positive + Negative Truth Total';
  M{13,16} = 'Truth Total';

  M{13,17} = 'Test Matches Positive Truth';
  M{13,18} = 'Test Matches Negative Truth';
  M{13,19} = 'Test Matches Positive or Negative Truth';
  M{13,20} = 'Test Total';
  
%   M{13,21} = 'Truth Log';
%   M{13,22} = 'Test Log';

  M{14,9}  = 'TP';
  M{14,10}  = 'FN';
  M{14,12} = 'FP';
  M{14,13} = 'TN';

  M{14,17} = 'TP (test)';
  M{14,18} = 'FP (test)';

end
