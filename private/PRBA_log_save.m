function flag = PRBA_log_save(log)
%
% Writes XBAT log to path indicated in [log.path log.file], after updating
% log.length and log.curr_id.
%
% input
%   XBAT log, with log.path and log.file set to intended destination
%
% output
%   flag = 1 log written, flag = 0 log not written
% 
% History
%   pjd78       Jan 2009           Initial
%   pjd78       Dec 2011           Added to avoid error from over length
%                                  path names.
%   



if isempty(log.event) || isequal(log.event.id, 0)
  log.length = 0;
  log.curr_id = 1;
  
else
  log.length = length(log.event);
  log.curr_id = log.event(end).id + 1;
  
end

fnme = log.file;

if length(fnme) > 63
   
    fnme = [fnme(1:59) '.mat'];
    
    log.file = fnme;
    
end


flag = log_save(log);
