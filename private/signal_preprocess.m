function sndData = signal_preprocess(sndData, method);

%**
%** "signal_preprocess.m"
%**
%** Kathryn A. Cortopassi, March 2005
%** 
%** Syntax: snd_data = signal_preprocess(snd_data, sig_preproc);
%** 
%** Apply preprocessing to sound waveform-- 'sndData' can be either a single waveform or 
%** a cell array of waveforms
%** 
%** Parameters for preprocessing--
%**
%**     method => method to use for signal preprocessing, either
%**             'None' -- do no preprocessing
%**             'Difference' -- apply signal differencing
%**
%**


% check for correct number of input arguments
argLo = 2; argHi = inf;
error(nargchk(argLo, argHi, nargin));


switch (upper(method))
  
  case ('NONE')
    %% do nothing
    
  case ('DIFFERENCE')
    %% do signal differencing
    if iscell(sndData)
      for i = 1:length(sndData)
        sndData{i} = diff(sndData{i});
      end
    else
      sndData = diff(sndData);
    end
    
  otherwise
    fprintf(1, 'Signal preprocessing method %s not recognized.', method)
    
end


%% end function
return;