function bname = basenameof(ffname)

%**
%** "basenameof.m"
%**
%** Kathryn A. Cortopassi, 2002
%**
%** syntax: bname = basenameof(ffname);
%**
%** Accepts either single strings or cell arrays of strings
%** and returns the base name of the filename without path
%** or extension
%**


%**
%** by K.A. Cortopassi
%** created 24-May-2002 
%**


% check for correct number of input arguments
argLo = 1; argHi = inf;
error(nargchk(argLo, argHi, nargin));


if iscell(ffname)
    for i=1:length(ffname)
        [junk, bname{i}] = fileparts(ffname{i});
    end
else
    [junk, bname] = fileparts(ffname);
end


% end function 
return;