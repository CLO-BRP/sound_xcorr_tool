function error = aureaderr(ffname)

%
% 'aureaderr.m'
% 
% Kathryn A. Cortopassi, 2002
%
% Function to report an error reading an AU file
%

%
% by Kathryn A. Cortopassi
% created 21-May-2002
%

error = 0;
evalc('auread(ffname);', 'error = errorcaught;');

return;