function [peakProfiles, f0Peaks] = matrixCross_peakProfileCA(spec, tvec, normtype, maxallowedlag, sndNames, outPath, status_window);


%%
%% 'matrixCross_peakProfileCA.m'
%% 
%% Kathryn A. Cortopassi, August 2004
%%
%% calculate the 1-by-all spectrogram crosses, the peak diagonals,
%% the peak correlation values and lags, and plot them to visualize
%% where in time spectrograms are matching best
%% (a re-vamp of 'calcNplotXsses.m' by Kathryn A. Cortopassi, 2001-2002)
%% 
%% syntax:
%% -------
%%   [peakProfiles, f0Peaks] = matrixCross_peakProfileCA(spec, tvec, normtype, maxallowedlag, sndNames, outPath, status_window);
%%
%% input:
%% ------
%%   spec == cell array of spectrogram matrices
%%   tvec == cell array of time vectors correponding to the spectrograms
%%   normtype == spectrogram standardization type to use
%%   maxallowedlag == maximum allowed time lag to use in seconds
%%   sndNames == cell array of sound file names
%%   outPath == output file path for saving figures
%%   status_window == window for text display
%%
%% output:
%% -------
%%   peakProfiles == profiles for diagonals giving peak correlation values in the 1-by-all matrix crosses 
%%   f0Peaks == a cell array containing the peak correlation values, and their corresponding time lags
%%
%%


%% check that enough input arguments were given
argLo = 6; argHi = inf;
error(nargchk(argLo, argHi, nargin));


%% check that spec input is a cell array
if ~iscell(spec)
  error('spectrogram input must be a cell array of spectrograms');
  return;
end

%% check that tvec input is a cell array
if ~iscell(tvec)
  error('time vector input must be a cell array of time vectors');
  return;
end

%% check normalization type
if ~sum(strcmpi(normtype, {'colnorm'; 'column'; 'matnorm'; 'matrix'; 'none'}))
  error(sprintf('normalization flag ''%s'' not recognized', normtype));
  return;
end


numSpecs = length(spec);

if numSpecs ~= length(tvec)
  error('numbers of spectrograms and time vectors do not match');
  return;
end

%% check consistency of time vectors, and frequency rows

for i = 1:numSpecs
  [nfrows(i), ntimecols(i)] = size(spec{i});
  dt(i) = tvec{i}(2);
end

%% get time resolution
dt = max(dt);

if (dt ~= min(dt))
  error('time resolution of spectrograms does not match');
  return;
end

if (max(nfrows) ~= min(nfrows))
  error('number of frequency bands in spectrograms do not match');
  return;
end


%% get relative and base file names
relNames = relnameof(sndNames);
baseNames = basenameof(sndNames);


%% standardize the spectrograms as requested
for i = 1:numSpecs
  spec{i} = standardize_mat(spec{i}, normtype);
end


%% get max allowed allowed time lag in points
%% (or here, in number of off diagonals)
kmax = round(maxallowedlag / dt);

%% get size of spec{1}
[m1, n1] = size(spec{1});

%% get base name of spec{1}
spec1str = removeillegalchar(baseNames{1});


if exist('status_window')
  %% get the current status text
  current_display = get(status_window, 'string');
end




for i = 1:numSpecs
  %% do cross for all specs to accomodate non-mat norm
  
  txt_message = sprintf('Correlating sounds 1 X %d', i);
  if exist('status_window')
    % display_status_line(status_window, txt_message);
    %% flash the requested message line, but don't add it to the end of the status window
    tmp_display = current_display;
    tmp_display{end+1} = txt_message;
    set(status_window, 'string', tmp_display, 'listboxtop', length(tmp_display));
    drawnow;
  else
    fprintf(1, [txt_message, '\n']);
  end
  
  
  [cross{i}, dispcross{i}] = matcross(spec{1}, spec{i});
  
  
  %% determine max range for searching diagonals based on size of cross mat
  %% and max allowed time lag
  [m, n] = size(cross{i});
  krange = max(-kmax, -(m-1)):min(kmax, n-1);
  
  
  index = 1;
  
  for k = krange
    %% search the diagonals of the cross matrix to obtain the time lags, diagonal profiles, 
    %% and resulting cross-correlation values at those lags
    
    %% store lag in bins
    lag_bins(index) = k;
    %% store diagonal profile at that lag
    diag_prof{index} = diag(cross{i}, k);
    %% store correlation value at that lag
    corr_func(index) = sum(diag_prof{index});
    
    index = index + 1;
    
  end
  
  %% get correlation peak
  [corr_peak, peak_inx] = max(corr_func);
  
  peak_lag_bins = lag_bins(peak_inx);
  peak_lag = peak_lag_bins * dt;
  peak_profile = diag_prof{peak_inx}';
  
  %% save the peak information  
  f0Peaks{1}(i) = corr_peak;
  f0Peaks{2}(i) = peak_lag;
  peakProfiles{i} = peak_profile;
  
  
  %% adjust spectrograms appropriately for display, front pad to align them at peak lag
  padlength = abs(peak_lag_bins);
  [m2, n2] = size(spec{i});
  
  if peak_lag_bins < 0
    A = [zeros(m1, padlength), spec{1}];
    n1 = n1 + padlength;
    B = spec{i};
  elseif peak_lag_bins > 0
    A = spec{1};
    B = [zeros(m1, padlength), spec{i}];
    n2 = n2 + padlength;
    peak_profile = [zeros(1, padlength), peak_profile];
  else
    A = spec{1};
    B = spec{i};
  end
  
  %% back pad so spectrograms are the same length
  maxlength = max(n1, n2) + 1;
  A(m1, maxlength) = 0;
  B(m1, maxlength) = 0;
  
  %% make composite showing optimum alignment
  C = [A; B];
  
  
  %% generate figure for spec{1} x spec{i}
  fh(1) = figure('units', 'normalized', 'position', [.1, .1, .8, .8], 'visible', 'off');
  
  %% show display cross, i.e., spec{1}, spec{i}, and cross 
  ah(1) = subplot(2, 2, 1);
  imagesc(dispcross{i});
  colormap(flipud(gray));
  axis xy;
  xlabel([relNames{1} ' (bins)'], 'FontSize', 8, 'fontweight', 'demi', 'interpreter', 'none');
  ylabel([relNames{i} ' (bins)'], 'FontSize', 8, 'fontweight', 'demi', 'interpreter', 'none');
  set(ah(1), 'FontSize', 8, 'fontweight', 'demi', 'tickdir', 'out', 'xtick', [0:50:5000], 'ytick', [0:50:5000]);
  %set(ah(1),'yticklabel',[],'XTickLabel',[]); 
  %bh(1) = colorbar('vert');
  %set(bh(1), 'FontSize',7,'fontweight','demi');
  
  
  %% show cross matrix only with a line along the peak diagonal and reference line along the zero diagonal
  ah(2) = subplot(2, 2, 2);
  imagesc(tvec{1}, tvec{i}, cross{i});
  colormap(flipud(gray));
  axis xy;
  xlabel([relNames{1} ' : time (s)'], 'FontSize', 8, 'fontweight',' demi', 'interpreter', 'none');
  ylabel([relNames{i} ' : time (s)'], 'FontSize', 8, 'fontweight', 'demi', 'interpreter', 'none');
  bh(2) = colorbar('vert');
  set(ah(2), 'FontSize', 8, 'fontweight', 'demi', 'tickdir', 'out');
  set(bh(2), 'FontSize', 7, 'fontweight', 'demi');
  
  %% plot the peak and zero diagonals
  ah(3) = axes('Position', get(ah(2), 'Position'));
  if peak_lag > 0 
    plot(tvec{1}, tvec{1}, 'b:', tvec{1}+peak_lag, tvec{1}, 'g', 'linewidth', .5);
  else
    plot(tvec{1}, tvec{1}, 'b:', tvec{1}, tvec{1}-peak_lag, 'g', 'linewidth', .5);
  end
  set(ah(3), 'tickdir', 'out', 'yticklabel', []', 'XTickLabel', [], 'Color', 'none',...
    'yLim', get(ah(2),'yLim'), 'XLim', get(ah(2),'XLim'), 'Layer', 'top');
  
  
  %% plot the profile for the peak diagonal
  ah(4) = subplot(2, 2, 4);
  plot(diag_prof{peak_inx});
  ylabel('Amp', 'FontSize', 8, 'fontweight', 'demi');
  xlabel('Profile along max diagonal (bins)', 'FontSize', 8, 'fontweight', 'demi');
  set(ah(4), 'FontSize', 8, 'fontweight', 'demi', 'tickdir', 'out', 'xtick', [0:50:5000]);
  %set(ah(4), 'FontSize', 8, 'fontweight', 'demi', 'tickdir', 'out', 'ylim', [-.002 .009], 'xtick', [0:50:600]);
  
  
  %% plot the composite showing the optimal spectrogram alignment
  ah(5) = subplot(2, 2, 3);
  imagesc(C);
  colormap(flipud(gray));
  axis xy;
  xlabel([relNames{1} ' VS ' relNames{i} ' (bins)'], 'FontSize', 8, 'fontweight', 'demi', 'interpreter', 'none');
  set(ah(5), 'FontSize', 8, 'fontweight', 'demi', 'tickdir', 'out', 'yticklabel', [], 'xtick', [0:50:5000]);
  
  
  %% create a figure spanning axis to put a figure title
  ah(6) = axes('Position', [0,0,1,1], 'visible', 'off', 'tickdir', 'out');
  text(.3, .02, ['max val= ' num2str(corr_peak) '; lag re:' relNames{i} ' = ' num2str(peak_lag) ' sec, '...
      num2str(peak_lag_bins) ' bins'], 'FontSize', 8, 'fontweight', 'demi', 'interpreter', 'none');
  
  
  %% save the figure file
  tmpstri = removeillegalchar(baseNames{i});
  
  set(fh(1), 'visible', 'on');
  saveas(fh(1), [outPath, spec1str, '_x_', tmpstri, '.fig'], 'fig');
  close(fh(1));
  
end


%% return these as a column just because
f0Peaks{1} = f0Peaks{1}';
f0Peaks{2} = f0Peaks{2}';



%% assign peak diagonal profiles to new array
MM1 = peakProfiles;

%% determine max profile length
maxlength = max(cellfun('length', MM1));
maxlength = maxlength + 1;

%% zero-pad all profiles to match maxlength+1
for i = 1:numSpecs
  MM1{i}(maxlength) = 0;
end

%% convert the cell array to an array
MM1 = cell2mat(MM1(:));

%% plot the peak diagonal profiles for all specs crossed against spec{1}
fh1 = figure('units', 'normalized', 'position', [.2, .2, .6, .6], 'visible', 'off');
waterfall(MM1);
title(['cross re: ' relNames{1}], 'FontSize', 8, 'fontweight', 'demi', 'interpreter', 'none');
xlabel('(bins)', 'FontSize', 8, 'fontweight', 'demi');
ylabel(['spec num'], 'FontSize', 8, 'fontweight', 'demi');
zlabel(['cross amp'], 'FontSize', 8, 'fontweight', 'demi');
rotate3d on;
colormap(copper);
w = gca;
set(w, 'FontSize', 8, 'fontweight', 'demi', 'tickdir', 'out');
%set(w, 'FontSize',8,'fontweight','demi','tickdir','out','zlim',[-.001 .008],'xtick',[0:50:1000]);


%% save the figure
set(fh1, 'visible', 'on');
saveas(fh1, [outPath, spec1str, '_vs_all_peakDiagProfs.fig'], 'fig');
close(fh1);


return;