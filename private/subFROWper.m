function Spec = subFROWper(Spec, P, data_form, adjustment);

%**
%** "subFROWper.m" 
%**
%** Kathryn A. Cortopassi, 2001-2002
%**
%** Syntax: cleanedSpec = subFROWper(Spec, P, data_form, adjustment);
%**
%** A function to zero data that has a value below that of the given 'P' percentile 
%** value (or values) on a row-by-row basis;
%** various adjustments can be made to the masked (unthresholded) values 
%**
%** 'Spec' == an MxN spectrogram array (or any array of values)
%**           with M frequency rows and N time columns
%**           Frequency rows go from low to high starting at row 1 
%** 'P' == is a scalar, that is, a single percent value for all rows
%**        or 'P' is a column vector of percent values, so that each frequency
%**        row gets its own specific percent value applied, P also goes from
%**        low to high frequency starting at row 1
%** 'data_form' == a flag telling how the values are stored,
%**                Amplitude, Power, or dB
%** 'adjustment' == a flag telling how to deal with masked values after thresholding,
%**                 Bias, Demean, or None
%**
%** NOTE: when you want to accomplish true additive noise correction,
%**       data_form should be power, and adjustment should be bias
%**

%**
%** by Kathryn A. Cortopassi
%** created 21-Sept-2001
%** modified 
%** 20-Nov-2001
%** 14-Jan-02 (for use of percent vector)
%** 23-May-2002
%**
%** modifications March 2005
%** 


% check for the correct number of inputs
argLo = 4; argHi = inf;
error(nargchk(argLo, argHi, nargin));


%% 'Spec' is a MxN matrix, with M frequency rows and N time columns; 
[N_freqRow, N_timeCol] = size(Spec);

%% if P is a scalar, convert it to a column N_freqRow long
[n, m] = size(P);
if [n, m] == [1, 1]
  P = zeros(N_freqRow, 1) + P;
  [n, m] = size(P);
end


if sum([n, m] ~= [N_freqRow, 1])
  error(sprintf('The given frequency row percent vector has incorrect dimensions!'));
  
else
  %% sort the rows of 'Spec' in ascending order by first transposing 'Spec' 
  %% and sorting the resulting columns
  SpecDist = sort(Spec');
  
  %% SpecDist is an NxM matrix; N = N_timeCol, M = N_freqRow
  %% find our absolute threshold for each frequency row as the Pth percentile value for 
  %% each column of 'SpecDist' (which equals the sorted rows of 'Spec') by going to the 
  %% index which equals P% of N rounded to the nearest integer
  %% that is, go to the fRow value for which P% of the other fRow values are less than it
  %% since P is a vector, we need to adjust it to index into the right value
  %% in the SpecDist matrix using advanced indexing
  perc_inx = round(N_timeCol .* (P./100));
  perc_inx = min(N_freqRow, max(1, perc_inx));
  perc_inx = ([0:N_freqRow-1] .* N_timeCol)' + perc_inx;
  
  %% the resulting matrix of percentile values 'abs_thresh' is a 1xM matrix (i.e., a percentile 
  %% value (or absolute threshold) for each frequency row 1:M)
  abs_thresh = SpecDist(perc_inx);
  %% expand this across the rows to match size of Spec
  abs_thresh = abs_thresh * ones(1, N_timeCol);
  
  
  %% find indices of values equal to or below absolute threshold values for each frequency row
  noise_inx = (Spec - abs_thresh) <= 0;
  mask_inx = ~noise_inx;
  
  %% zero out the spectrogram values below abs_threshold (considered noise)
  Spec(noise_inx) = 0;
  
  %% adjust the masked (unthresholded) spectrogram values
  switch (adjustment)
    
    case {'None'}
      %% do nothing 
      
    case {'Bias'}
      Spec(mask_inx) = Spec(mask_inx) - abs_thresh(mask_inx); 
      
    case {'Demean'}
      Spec(mask_inx) = Spec(mask_inx) - mean(Spec(mask_inx)); 
      
    case {'Binarize'}
      Spec(mask_inx) = 1; 
      
    otherwise
      error(sprintf('Mask adjustment flag ''%s'' not recognized!', data_form));
      
  end
  
end

%% end function
return;