function [mismatch_flag] = checkSR(sound_list, status_window_edit_handle);

%%
%% "checkSR.m"
%%
%% Kathryn A. Cortopassi, 2006
%%
%% Checks that all the sampling rates of the sounds stored in the
%% sound list are equal; the first sound in the list is taken as 
%% having the reference sampling rate
%% If a mismatch is found, the sound is marked as being a 
%% mismatch
%%
%% syntax: 
%% [mismatch_flag] = checkSR(sound_list, status_window_edit_handle);
%%
%% input:
%%  sound_list == structure array containing all information for the sounds
%%                including sampling rate, file name, and path
%%                sound_list.samplerate
%%                sound_list.filename
%%                sound_list.path
%%
%% output:
%%  mismatch_flag == 0/1 array indicating whether sound in that position
%%                   is a mismatch
%%  

%**
%** by Kathryn A. Cortopassi
%** created Mar-2002
%** modified
%** Apr-2002
%** May-2002
%**
%% modified 11 August 2004
%% modified February 2006


% XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
%% initialize the mismatch flag
num_snds = length(sound_list);
 mismatch_flag = zeros(num_snds, 1);

%% pull out info from the structure 
sampRate = [sound_list.samplerate];
sndName = {sound_list.filename};
sndPath = {sound_list.path};





  cell_flag = 0;
  if iscell(sampRate)
    cell_flag = 1;
    % convert to a vector
    sampRate = [sampRate{:}];
  end

  minRate = min(sampRate);


  if (minRate ~= max(sampRate))
    % all the sampling rates are not equal, give a warning and resample

    if exist('status_window_edit_handle')
      display_status_line(status_window_edit_handle, sprintf('Sampling rates don''t match. Resampling to minimum rate = %d ...', minRate));
    else
      fprintf(1, 'Sampling rates don''t match-- resampling to minimum rate = %d ...\n', minRate);
    end

    for i = find(sampRate ~= minRate)

      %% try to consolidate memory
      pack;

      %% then
      try
        %% resample

        display_status_line(status_window_edit_handle, sprintf('Down-sampling sound : ''%s''...', sndName{i}));
        sndData{i} = resample(sndData{i}, minRate, sampRate(i));
        mismatch_flag(i) = 1;

      catch
        %% make cell empty to mark failed resample attempt

        sndData{i} = [];

        %% get error message
        error_info = lasterror;
        curr_err_msg = ['Error resampling sound: ', sndName{i}, ' >> ''', error_info.message, ''''];

        if exist('status_window_edit_handle')
          display_status_line(status_window_edit_handle, curr_err_msg);
        else
          fprintf(1, [curr_err_msg, '\n']);
        end

      end %% try/catch

    end %%     for i = find(sampRate ~= minRate)

    % change all rates in the vector to the max rate
    sampRate(:) = minRate;

  else

    mismatch_flag = 0;

  end %%   if (minRate ~= min(sampRate))


  %% restore to cell
  if cell_flag
    sampRate = num2cell(sampRate);
  end






return;