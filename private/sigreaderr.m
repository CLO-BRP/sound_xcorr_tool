function error = sigreaderr(ffname);

%
% 'sigreaderr.m'
% 
% Kathryn A. Cortopassi, 2002
%
% Function to report an error reading a Signal/RTSD file
%

%
% by Kathryn A. Cortopassi
% created 21-May-2002
%

error = 0;
evalc('rsig(ffname);', 'error = errorcaught;');

return;