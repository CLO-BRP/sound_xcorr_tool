function error = errorcaught;

%
% 'errorcaught.m'
% 
% Kathryn A. Cortopassi, 2002
%
% function to report that an error was caught and 
% return a value of 1
%

% 
% by Kathryn A. Cortopassi
% created 21-May-2002
%

error = 1;
return;