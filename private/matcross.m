function [cross, dispcross] = matcross(mat1, mat2);

%%
%% "matcross.m"
%%
%% Kathryn A. Cortopassi, 2004
%%
%% perform and visualize the multiplication of two matrices
%% (a re-vamp of 'xmat.m' by Kathryn A. Cortopassi, 2001-2002)
%%
%% syntax: [cross, dispcross] = matcross(mat1, mat2);
%% -------
%%
%% input:
%% ------
%%   mat1 == an NxP matrix, N rows by P columns
%%   mat2 == an NxQ matrix, N rows by Q columns
%%           bin numbers increase left to right and up to down
%%
%% output:
%% -------
%%   cross == result of the multiplication (mat2')*(mat1)
%%   dispcross == a composite of the original matrices and the
%%                resulting cross for visualization purposes
%%




%% check if enough inputs were given
argLo = 2; argHi = inf;
error(nargchk(argLo, argHi, nargin));


N = size(mat1, 1);

%% check the matrix sizes match for the cross
if (N ~= size(mat2, 1))
  error(sprintf('Number of rows (column lengths) do not match!'));
  return;
end

%% cross the matrices
B = mat2';
A = mat1;
C = B*A;

cross = C;
dispcross = flipud([fliplr(flipud(B)), flipud(C); zeros(N)+mean(C(:)), flipud(A)]);


% end the function
return;