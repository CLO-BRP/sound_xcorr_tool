function relname = relnameof(ffname);

%**
%** "relnameof.m"
%**
%** Kathryn A. Cortopassi, 2002
%**
%** syntax: relname = relnameof(ffname);
%**
%** Accepts either single strings or cell arrays of strings
%** and returns the relative name of the filename-- that is
%** base name plus extension without path
%**


%**
%** by K.A. Cortopassi
%** created 24-May-2002 
%**


% check for correct number of input arguments
argLo = 1; argHi = inf;
error(nargchk(argLo, argHi, nargin));


if iscell(ffname)
    for i=1:length(ffname)
        [junk, bnamei, exti] = fileparts(ffname{i});
        relname{i} = [bnamei exti];
    end
else
    [junk, bname, ext] = fileparts(ffname);
    relname = [bname ext];
end


% end function 
return;