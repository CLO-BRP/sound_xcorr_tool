function error = aifreaderr_SCK(ffname)

%
% 'aifreaderr.m'
% 
% Kathryn A. Cortopassi, 2002
%
% Function to report an error reading an Aiff file
%

%
% by Kathryn A. Cortopassi
% created 21-May-2002
%
% modified 2/12 by SCK

error = 0;
evalc('aiffread(ffname);', 'error = errorcaught;');

return;