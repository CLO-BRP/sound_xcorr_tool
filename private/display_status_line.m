function display_status_line(status_window_edit_handle, txt_message);

%% 
%% 'display_status_line'
%%
%% Kathryn A. Cortopassi, August 2004
%% 
%% send a text message to the status window
%%


%% get the current content of the status wondow
current_display = get(status_window_edit_handle, 'string');

%% add the requested message line
current_display{end+1} = txt_message;
set(status_window_edit_handle, ...
    'string', current_display, ...
    'value', length(current_display), ...
    'listboxtop', length(current_display));

%% display
drawnow;


return;