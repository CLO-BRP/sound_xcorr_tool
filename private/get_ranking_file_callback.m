function varargout = get_ranking_file_callback(varargin)
% GET_RANKING_FILE_CALLBACK M-file for get_ranking_file_callback.fig
%      GET_RANKING_FILE_CALLBACK, by itself, creates a new GET_RANKING_FILE_CALLBACK or raises the existing
%      singleton*.
%
%      H = GET_RANKING_FILE_CALLBACK returns the handle to a new GET_RANKING_FILE_CALLBACK or the handle to
%      the existing singleton*.
%
%      GET_RANKING_FILE_CALLBACK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GET_RANKING_FILE_CALLBACK.M with the given input arguments.
%
%      GET_RANKING_FILE_CALLBACK('Property','Value',...) creates a new GET_RANKING_FILE_CALLBACK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before get_ranking_file_callback_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to get_ranking_file_callback_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help get_ranking_file_callback

% Last Modified by GUIDE v2.5 21-Mar-2012 09:29:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @get_ranking_file_callback_OpeningFcn, ...
                   'gui_OutputFcn',  @get_ranking_file_callback_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before get_ranking_file_callback is made visible.
function get_ranking_file_callback_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to get_ranking_file_callback (see VARARGIN)

% Choose default command line output for get_ranking_file_callback
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes get_ranking_file_callback wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = get_ranking_file_callback_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function template_rank_file_Callback(hObject, eventdata, handles)
%[rank_file_name rank_file_dir]=uigetfile('*.txt','Select file with template correlation data');
% hObject    handle to template_rank_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of template_rank_file as text
%        str2double(get(hObject,'String')) returns contents of template_rank_file as a double


% --- Executes during object creation, after setting all properties.
function template_rank_file_CreateFcn(hObject, eventdata, handles)
% hObject    handle to template_rank_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
